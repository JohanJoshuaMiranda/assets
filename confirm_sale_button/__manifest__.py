# -*- encoding: utf-8 -*-
{
	'name': 'Boton Confirmar Venta',
	'category': 'account',
	'author': 'ITGRUPO',
	'depends': ['base','sale', 'sales_team'],
	'version': '1.0',
	'description':"""
	Agregar grupo para boton confrimar venta en cotizaciones.
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
			'security/security.xml',
			'security/ir.model.access.csv',
			'confirm_sale.xml'],
	'installable': True
}
