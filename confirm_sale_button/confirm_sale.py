# -*- coding: utf-8 -*-

from odoo import fields, models

class SaleOrder(models.Model):
	_inherit = "sale.order"

	tag_id = fields.Many2one('tag.it', string='Etiqueta', track_visibility='always' )

class TagIT(models.Model):
	_name = "tag.it"
	_description = "Tag IT"

	name = fields.Char(string='Nombre', required=True)
