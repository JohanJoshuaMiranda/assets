# -*- coding: utf-8 -*-
from odoo import api, fields, models

class AccountInvoice(models.Model):
	_inherit = 'account.invoice'

	sales_executive_id = fields.Many2one('hr.employee',string='Ejecutivo de Ventas')