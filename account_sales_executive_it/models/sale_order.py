# -*- coding: utf-8 -*-
from odoo import api, fields, models

class SaleOrder(models.Model):
	_inherit = 'sale.order'

	sales_executive_id = fields.Many2one('hr.employee',string='Ejecutivo de Ventas')

	@api.multi
	def _prepare_invoice(self):
		invoice_vals = super(SaleOrder,self)._prepare_invoice()
		invoice_vals['sales_executive_id'] = self.sales_executive_id.id
		return invoice_vals

class SaleAdvancePaymentInv(models.TransientModel):
	_inherit = "sale.advance.payment.inv"

	@api.multi
	def _create_invoice(self, order, so_line, amount):
		invoice = super(SaleAdvancePaymentInv,self)._create_invoice(order, so_line, amount)
		invoice.sales_executive_id = order.sales_executive_id.id
		return invoice