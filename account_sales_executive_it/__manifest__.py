# -*- encoding: utf-8 -*-
{
	'name': 'Ejecutivos de Ventas',
	'category': 'account',
	'author': 'ITGRUPO',
	'depends': ['account','sale','base','hr'],
	'version': '1.0',
	'description':"""
	Ejecutivo de Ventas
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
			'views/account_invoice.xml',
			'views/sale_order.xml'
			],
	'installable': True
}
