# -*- encoding: utf-8 -*-
{
	'name': 'PAGOS MULTIPLACE ADVANCE -PRESUPUESTO',
	'category': 'account',
	'author': 'ITGRUPO',
	'depends': ['budget_it','account_multipayment_invoices_it_advance'],
	'version': '1.0',
	'description':"""
	PAGOS MULTIPLACE ADVANCE -PRESUPUESTO
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
			'views/multipayment_invoice_view.xml'
			],
	'installable': True
}
