# -*- coding: utf-8 -*-

from openerp import models, fields, api

class AccountApBudgetIt(models.Model):
	_inherit = 'account.ap.budget.it'

	multipayment_id = fields.Many2one('multipayment.advance.invoice',string='PMA')

class MultipaymentAdvanceInvoice(models.Model):
	_inherit = 'multipayment.advance.invoice'

	budget_id = fields.Many2one('account.budget.it',string='Presupuesto',copy=False)
	line_budget_ids = fields.One2many('account.ap.budget.it','multipayment_id',string='Lineas de Presupuesto')

	@api.multi
	def crear_asiento(self):
		super(MultipaymentAdvanceInvoice,self).crear_asiento()
		self.asiento.budget_id = self.budget_id.id
		for x in self.line_budget_ids:
			x.move_id = self.asiento.id