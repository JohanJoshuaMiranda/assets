# -*- coding: utf-8 -*-
from odoo import models, fields, api, osv

class PleFileWizard(models.TransientModel):
	_name = "ple.file.wizard"

	binary_file = fields.Binary('File', readonly=True)
	file_name = fields.Char('File Name', readonly=True)