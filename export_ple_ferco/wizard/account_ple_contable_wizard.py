# -*- coding: utf-8 -*-
import base64
import StringIO
import re
import uuid
from openerp import models, fields, api
from odoo.exceptions import UserError, ValidationError
from datetime import *

class AccountPleContableWizard(models.TransientModel):
	_name = 'account.ple.contable.wizard'

	period = fields.Many2one('account.period',string='Periodo')

	@api.multi
	def do_rebuild(self):
		direccion = self.env['main.parameter'].search([])[0].dir_create_file
		ruc = self.env['res.company'].search([])[0].partner_id.nro_documento
		mond = self.env['res.company'].search([])[0].currency_id.name

		if not ruc:
			raise UserError('No configuro el RUC de su Compañia.')

		if not mond:
			raise UserError('No configuro la moneda de su Compañia.')

		#LE + RUC + AÑO(YYYY) + MES(MM) + DIA(00) 
		name_doc = "LE"+str(ruc)+self.period.code[3:7]+ self.period.code[:2]+"00"+"050300"

		#Get CSV
		sql_query = self._get_sql(self.period)
		sql_query = sql_query.decode('utf-8')
		self.env.cr.execute(sql_query)

		sql_query = "COPY (%s) TO STDOUT WITH %s" % (sql_query, "CSV DELIMITER '|'")
		rollback_name = self._create_savepoint()

		try:
			output = StringIO.StringIO()
			self.env.cr.copy_expert(sql_query, output)
			output.getvalue()
			res = base64.b64encode(output.getvalue())
			output.close()
		finally:
			self._rollback_savepoint(rollback_name)

		res = res.encode('utf-8')

		name_doc += "00"+"1"+("1" if len(res) > 0 else "0") + ("1" if mond == 'PEN' else "2") + "1.txt"

		vals = {
			'file_name': name_doc,
			'binary_file': res,
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.model
	def _create_savepoint(self):
		rollback_name = '%s_%s' % (
			self._name.replace('.', '_'), uuid.uuid1().hex)
		req = "SAVEPOINT %s" % (rollback_name)
		self.env.cr.execute(req)
		return rollback_name

	@api.model
	def _rollback_savepoint(self, rollback_name):
		req = "ROLLBACK TO SAVEPOINT %s" % (rollback_name)
		self.env.cr.execute(req)

	def _get_sql(self,period):
		sql = """
		select 
		to_char('%s'::date,'yyyymmdd') as columna1,
		replace(code,'.','') as columna2, -- aqui va elperiodo elegido
		left(name,100) as columna3,
		'01' as columna4,
		' ' as columna5,
		' ' as columna6,
		' ' as columna7,
		1 as columna8,
		' ' as columna9
		from account_account where deprecated=FALSE and internal_type<>'view'

		""" % (datetime.strptime(period.date_stop, '%Y-%m-%d').strftime("%Y/%m/%d"))

		return sql