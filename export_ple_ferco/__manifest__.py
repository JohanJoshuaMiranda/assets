# -*- encoding: utf-8 -*-
{
	'name': 'PLE FERCO',
	'category': 'account',
	'author': 'ITGRUPO-FERCO',
	'depends': ['account','import_base_it'],
	'version': '1.0',
	'description':"""
	- Este modulo agrega menu para PLE actualizados
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
			'wizard/account_ple_contable_wizard.xml',
			'wizard/ple_file_wizard.xml'
			],
	'installable': True
}
