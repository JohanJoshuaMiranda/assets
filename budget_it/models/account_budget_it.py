# -*- coding: utf-8 -*-

from openerp import models, fields, api
from odoo.exceptions import UserError, ValidationError
import base64

class AccountBudgetIt(models.Model):
	_name='account.budget.it'

	code = fields.Char(string='Codigo',size=6,required=True)
	name = fields.Char(string=u'Descripción',size=150,required=True)
	fiscal_year_id = fields.Many2one('account.fiscalyear',string=u'Año Fiscal',required=True)
	amount_budget = fields.Float(string='Presupuesto',digits=(12,2),compute='compute_amounts')
	amount_expense = fields.Float(string='Gasto',digits=(12,2),compute='compute_amounts')
	amount_residual = fields.Float(string='Por Gastar',digits=(12,2),compute='compute_amounts')
	line_ids = fields.One2many('account.budget.it.line','main_id',string='Lineas')
	state = fields.Selection([('draft','Borrador'),('confirm','Confirmado')],string='Estado',default='draft')

	@api.multi
	def calculate(self):
		for line in self:
			for x in line.line_ids:
				x._compute_expense()
				x._compute_residual()
			line.compute_amounts()

	@api.multi
	@api.depends('line_ids')
	def compute_amounts(self):
		for line in self:
			amount_budget = 0
			amount_expense = 0
			amount_residual = 0
			for elem in line.line_ids:
				amount_budget += elem.amount_budget
				amount_expense += elem.amount_expense
				amount_residual += elem.amount_residual
			
			line.amount_budget = amount_budget
			line.amount_expense = amount_expense
			line.amount_residual = amount_residual

	def get_sql(self):
		sql = """SELECT B.code,B.name,B.amount_budget,B.amount_expense,B.amount_residual,
				M.enero,M.febrero,M.marzo,M.abril,M.mayo,M.junio,M.julio,M.agosto,M.septiembre,M.octubre,M.noviembre,M.diciembre
				FROM account_budget_it_line B
				LEFT JOIN (SELECT T.line_id,
				SUM(CASE WHEN to_char(T.date , 'mm') = '01' THEN T.amount
				ELSE 0 END ) as enero,
				SUM(CASE WHEN to_char(T.date , 'mm') = '02' THEN T.amount
				ELSE 0 END) as febrero,
				SUM(CASE WHEN to_char(T.date , 'mm') = '03' THEN T.amount
				ELSE 0 END) as marzo,
				SUM(CASE WHEN to_char(T.date , 'mm') = '04' THEN T.amount
				ELSE 0 END) as abril,
				SUM(CASE WHEN to_char(T.date , 'mm') = '05' THEN T.amount
				ELSE 0 END) as mayo,
				SUM(CASE WHEN to_char(T.date , 'mm') = '06' THEN T.amount
				ELSE 0 END) as junio,
				SUM(CASE WHEN to_char(T.date , 'mm') = '07' THEN T.amount
				ELSE 0 END) as julio,
				SUM(CASE WHEN to_char(T.date , 'mm') = '08' THEN T.amount
				ELSE 0 END) as agosto,
				SUM(CASE WHEN to_char(T.date , 'mm') = '09' THEN T.amount
				ELSE 0 END) as septiembre,
				SUM(CASE WHEN to_char(T.date , 'mm') = '10' THEN T.amount
				ELSE 0 END) as octubre,
				SUM(CASE WHEN to_char(T.date , 'mm') = '11' THEN T.amount
				ELSE 0 END) as noviembre,
				SUM(CASE WHEN to_char(T.date , 'mm') = '12' THEN T.amount
				ELSE 0 END) as diciembre
				FROM (SELECT ap.line_budget_id as line_id,
				am.fecha_contable AS date,
				ap.debit - ap.credit as amount
				FROM account_ap_budget_it ap
				LEFT JOIN account_invoice ai ON ai.id = ap.invoice_id
				LEFT JOIN account_move am ON am.id = ap.move_id
				WHERE am.state='posted')T
				WHERE to_char(T.date , 'yyyy') = '%s'
				GROUP BY T.line_id) M ON M.line_id = B.id
				WHERE B.main_id = %d
				ORDER BY B.id"""%(self.fiscal_year_id.name,self.id)
		return sql

	@api.multi
	def get_report(self):
		import io
		from xlsxwriter.workbook import Workbook
		from xlsxwriter.utility import xl_rowcol_to_cell

		direccion = self.env['main.parameter'].search([])[0].dir_create_file
		workbook = Workbook( direccion + 'presupuesto.xlsx')
		worksheet = workbook.add_worksheet("Presupuesto")
		bold = workbook.add_format({'bold': True})

		bold_title = workbook.add_format({'bold': True,'align': 'center','valign': 'vcenter'})
		bold_title.set_font_size(14)
		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=2)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(9)
		boldbord.set_bg_color('#DCE6F1')
		numberdos = workbook.add_format({'num_format':'0.00'})
		bord = workbook.add_format()
		bord.set_border(style=1)
		numberdos.set_border(style=1)

		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		worksheet.write(0,0, 'PRESUPUESTO: '+self.name, bold)
		worksheet.write(1,0, 'EJERCICIO: '+self.fiscal_year_id.name, bold)

		worksheet.write(3,0, u"CÓDIGO",boldbord)
		worksheet.write(3,1, u"DESCRIPCIÓN",boldbord)
		worksheet.write(3,2, u"PRESUPUESTO",boldbord)
		worksheet.write(3,3, u"GASTADO",boldbord)
		worksheet.write(3,4, u"POR GASTAR",boldbord)
		worksheet.write(3,5, u"ENE",boldbord)
		worksheet.write(3,6, u"FEB",boldbord)
		worksheet.write(3,7, u"MAR",boldbord)
		worksheet.write(3,8, u"ABR",boldbord)
		worksheet.write(3,9, u"MAY",boldbord)
		worksheet.write(3,10, u"JUN",boldbord)
		worksheet.write(3,11, u"JUL",boldbord)
		worksheet.write(3,12, u"AGO",boldbord)
		worksheet.write(3,13, u"SET",boldbord)
		worksheet.write(3,14, u"OCT",boldbord)
		worksheet.write(3,15, u"NOV",boldbord)
		worksheet.write(3,16, u"DIC",boldbord)

		x = 4

		self.env.cr.execute(self.get_sql())
		res = self.env.cr.dictfetchall()

		for line in res:
			worksheet.write(x,0,line['code'] if line['code']  else '',bord)
			worksheet.write(x,1,line['name'] if line['name']  else '',bord)
			worksheet.write(x,2,line['amount_budget'] if line['amount_budget']  else 0,numberdos)
			worksheet.write(x,3,line['amount_expense'] if line['amount_expense']  else 0,numberdos)
			worksheet.write(x,4,line['amount_residual'] if line['amount_residual']  else 0,numberdos)
			worksheet.write(x,5,line['enero'] if line['enero']  else 0,numberdos)
			worksheet.write(x,6,line['febrero'] if line['febrero']  else 0,numberdos)
			worksheet.write(x,7,line['marzo'] if line['marzo']  else 0,numberdos)
			worksheet.write(x,8,line['abril'] if line['abril']  else 0,numberdos)
			worksheet.write(x,9,line['mayo'] if line['mayo']  else 0,numberdos)
			worksheet.write(x,10,line['junio'] if line['junio']  else 0,numberdos)
			worksheet.write(x,11,line['julio'] if line['julio']  else 0,numberdos)
			worksheet.write(x,12,line['agosto'] if line['agosto']  else 0,numberdos)
			worksheet.write(x,13,line['septiembre'] if line['septiembre']  else 0,numberdos)
			worksheet.write(x,14,line['octubre'] if line['octubre']  else 0,numberdos)
			worksheet.write(x,15,line['noviembre'] if line['noviembre']  else 0,numberdos)
			worksheet.write(x,16,line['diciembre'] if line['diciembre']  else 0,numberdos)
			x += 1

		worksheet.write(x,1, 'TOTAL', bord)
		worksheet.write_formula(x,2, '=sum(' + xl_rowcol_to_cell(4,2) +':' +xl_rowcol_to_cell(x-1,2) + ')', numberdos)
		worksheet.write_formula(x,3, '=sum(' + xl_rowcol_to_cell(4,3) +':' +xl_rowcol_to_cell(x-1,3) + ')', numberdos)
		worksheet.write_formula(x,4, '=sum(' + xl_rowcol_to_cell(4,4) +':' +xl_rowcol_to_cell(x-1,4) + ')', numberdos)

		tam_col = [8,40,14,14,14,12,12,12,12,12,12,12,12,12,12,12,12]

		worksheet.set_column('A:A', tam_col[0])
		worksheet.set_column('B:B', tam_col[1])
		worksheet.set_column('C:C', tam_col[2])
		worksheet.set_column('D:D', tam_col[3])
		worksheet.set_column('E:E', tam_col[4])
		worksheet.set_column('F:F', tam_col[5])
		worksheet.set_column('G:G', tam_col[6])
		worksheet.set_column('H:H', tam_col[7])
		worksheet.set_column('I:I', tam_col[8])
		worksheet.set_column('J:J', tam_col[9])
		worksheet.set_column('K:K', tam_col[10])

		workbook.close()
		
		f = open(direccion + 'presupuesto.xlsx', 'rb')
		
		
		vals = {
			'output_name': 'REPORTE DE PRESUPUESTO - %s.xlsx'%(self.fiscal_year_id.name),
			'output_file': base64.encodestring(''.join(f.readlines())),		
		}

		sfs_id = self.env['export.file.save'].create(vals)
		return {
			"type": "ir.actions.act_window",
			"res_model": "export.file.save",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}

	@api.multi
	def set_draft(self):
		self.write({'state': 'draft'})

	@api.multi
	def set_confirm(self):
		self.write({'state': 'confirm'})

class AccountBudgetIt1line(models.Model):
	_name='account.budget.it.line'

	main_id = fields.Many2one('account.budget.it',string='Main')
	code = fields.Char(string='Codigo',size=6,required=True)
	name = fields.Char(string=u'Descripción',required=True)
	amount_budget = fields.Float(string='Presupuesto',digits=(12,2),default=0,required=True)
	amount_expense = fields.Float(string='Gasto',digits=(12,2),compute='_compute_expense',store=True)
	amount_residual = fields.Float(string='Por Gastar',digits=(12,2),compute='_compute_residual',store=True)

	@api.multi
	def _compute_expense(self):
		for line in self:
			if line.main_id.state == 'draft':
				line.amount_expense = 0
			else:
				self.env.cr.execute("""
							SELECT SUM(amount) as amount FROM (SELECT 
							am.fecha_contable AS date,
							ap.debit - ap.credit as amount
							FROM account_ap_budget_it ap
							LEFT JOIN account_invoice ai ON ai.id = ap.invoice_id
							LEFT JOIN account_move am ON am.id = ap.move_id
							WHERE ap.line_budget_id = %d AND am.state='posted')T
							WHERE to_char(T.date , 'yyyy') = '%s'
				"""%(line.id, line.main_id.fiscal_year_id.name))
				res = self.env.cr.dictfetchall()
				if res[0]:
					line.amount_expense = res[0]['amount']
				else:
					line.amount_expense = 0

	@api.multi
	def _compute_residual(self):
		for line in self:
			if line.amount_budget and line.amount_expense:
				line.amount_residual = line.amount_budget - line.amount_expense
			else:
				line.amount_residual = 0

	@api.multi
	@api.depends('code')
	def name_get(self):
		result = []
		for table in self:
			l_name =  table.code if table.code else '/'
			result.append((table.id, l_name ))
		return result

	@api.multi
	def open_reconcile_view(self):
		[action] = self.env.ref('budget_it.action_account_ap_budget_it').read()
		ids = []
		for aml in self:
			detail = self.env['account.ap.budget.it'].search([('line_budget_id','=',aml.id)])
			for elem in detail:
				ids.append(elem.id)
		action['domain'] = [('id', 'in', ids)]
		return action