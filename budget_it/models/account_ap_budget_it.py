# -*- coding: utf-8 -*-

from openerp import models, fields, api

class AccountApBudgetIt(models.Model):
	_name = 'account.ap.budget.it'

	line_budget_id = fields.Many2one('account.budget.it.line',string='Codigo',required=True)
	name = fields.Char(related='line_budget_id.name',readonly=True)
	debit = fields.Float(string='Debe',digits=(64,2))
	credit = fields.Float(string='Haber',digits=(64,2))
	debit_me = fields.Float(string='Debe ME',digits=(64,2))
	credit_me = fields.Float(string='Haber ME',digits=(64,2))
	move_id = fields.Many2one('account.move',string='Asiento')
	invoice_id = fields.Many2one('account.invoice',string='Asiento')
	partner_id = fields.Many2one('res.partner',string='Proveedor',related='move_id.partner_id',readonly=True)
	it_type_document = fields.Many2one('einvoice.catalog.01',string='Tipo de Documento',related='invoice_id.it_type_document',readonly=True)
	ref = fields.Char(string='Nro Comprobante',related='move_id.ref',readonly=True)
	date = fields.Date(string='Fecha Contable',related='move_id.fecha_contable',readonly=True)
	journal_id = fields.Many2one('account.journal',string='Diario',related='move_id.journal_id',readonly=True)
	name_journal = fields.Char(string='Nro Asiento',related='move_id.name',readonly=True)

	@api.onchange('debit_me','invoice_id.currency_id','invoice_id.currency_rate_auto')
	def onchange_debit_me(self):
		if self.invoice_id.currency_id.name == 'USD':
			self.debit = self.debit_me * self.invoice_id.currency_rate_auto
		else:
			self.debit = self.debit_me

	@api.onchange('credit_me','invoice_id.currency_id','invoice_id.currency_rate_auto')
	def onchange_credit_me(self):
		if self.invoice_id.currency_id.name == 'USD':
			self.credit = self.credit_me * self.invoice_id.currency_rate_auto
		else:
			self.credit = self.credit_me

class AccountInvoice(models.Model):
	_inherit = 'account.invoice'

	budget_id = fields.Many2one('account.budget.it',string='Presupuesto',copy=False)
	line_budget_ids = fields.One2many('account.ap.budget.it','invoice_id',string='Lineas de Presupuesto')

	@api.multi
	def action_move_create(self):
		t = super(AccountInvoice,self).action_move_create()
		self.move_id.budget_id = self.budget_id.id
		for x in self.line_budget_ids:
			x.move_id = self.move_id.id

		return t

	@api.one
	def write(self,vals):
		t = super(AccountInvoice,self).write(vals)
		self.refresh()
		if self.move_id:
			self.move_id.budget_id = self.budget_id.id
			for x in self.line_budget_ids:
				x.move_id = self.move_id.id
		return t

class AccountMove(models.Model):
	_inherit = 'account.move'

	budget_id = fields.Many2one('account.budget.it',string='Presupuesto',copy=False)
	line_budget_ids = fields.One2many('account.ap.budget.it','move_id',string='Lineas de Presupuesto')