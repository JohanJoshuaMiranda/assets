# -*- encoding: utf-8 -*-
{
	'name': 'Presupuestos IT',
	'category': 'account',
	'author': 'ITGRUPO',
	'depends': ['account','base','account_periods_it','account_invoice_it','account_move_it','import_base_it'],
	'version': '1.0',
	'description':"""
	Modulo para Presupuestos
	""",
	'auto_install': False,
	'application': True,
	'demo': [],
	'data':	[
			'security/ir.model.access.csv',
			'views/budget_menuitem.xml',
			'views/account_budget_it.xml',
			'views/account_invoice_views.xml'
			],
	'installable': True
}
