# -*- encoding: utf-8 -*-
{
    'name': 'Reporte Consolidado de Nonima - Ceelimp',
    'category': 'report',
    'author': 'ITGRUPO-CEELIMP',
    'depends': ['planilla'],
    'version': '1.0',
    'description':"""
        Modulo para emitir el reporte Consolidado de Nóminas en Ceelimp
    """,
    'auto_install': False,
    'demo': [],
    'data': [
        'hr_payslip_run.xml'
        ],
    'installable': True
}