# -*- encoding: utf-8 -*-
import base64,codecs,pprint
from odoo import fields,models,api,exceptions
from datetime import datetime,timedelta
from time import time
import string

class HrPayslipRun(models.Model):
	_inherit = 'hr.payslip.run'

	@api.multi
	def build_report_consolidado(self):
		from xlsxwriter.workbook import Workbook
		objs = self._context.get('active_ids',False)
		if not objs or len(objs) > 1:
			raise exceptions.Warning(u'Sólo puede imprimir un reporte a la vez!')
		obj = self.browse(objs[0])
		
		if len(self.ids) > 1:
			raise UserError(
				'Solo se puede mostrar una planilla a la vez, seleccione solo una nómina')
		
		self.env['planilla.planilla.tabular.wizard'].reconstruye_tabla(obj.date_start,obj.date_end)

		path = self.env['main.parameter'].search([])[0].dir_create_file
		now = str(datetime.now()-timedelta(hours=5))[:19].replace(':','_')
		file_name = u'Reporte Consolidado '+now+'.xlsx'
		path+=file_name
		workbook = Workbook(path)
		worksheet = workbook.add_worksheet(
			str(obj.id)+'-'+obj.date_start+'-'+obj.date_end)
		worksheet.set_landscape()  # Horizontal
		worksheet.set_paper(9)  # A-4
		worksheet.set_margins(left=0.75, right=0.75, top=1, bottom=1)
		worksheet.fit_to_pages(1, 0)  # Ajustar por Columna

		fontSize = 8
		bold = workbook.add_format(
			{'bold': True, 'font_name': 'Arial', 'font_size': fontSize})
		normal = workbook.add_format()

		dateformat = workbook.add_format({'num_format':'dd-mm-yyyy'})
		dateformat.set_font_size(fontSize)
		dateformat.set_font_name('Arial')

		boldbord = workbook.add_format({'bold': True, 'font_name': 'Arial'})
		# boldbord.set_border(style=1)
		boldbord.set_align('center')
		boldbord.set_align('bottom')
		boldbord.set_text_wrap()
		boldbord.set_font_size(fontSize)
		boldbord.set_bg_color('#99CCFF')
		numberdos = workbook.add_format(
			{'num_format': '0.00', 'font_name': 'Arial', 'align': 'right'})
		formatLeft = workbook.add_format(
			{'num_format': '0.00', 'font_name': 'Arial', 'align': 'left', 'font_size': fontSize})
		formatLeftColor = workbook.add_format(
			{'bold': True, 'num_format': '0.00', 'font_name': 'Arial', 'align': 'left', 'bg_color': '#99CCFF', 'font_size': fontSize})
		styleFooterSum = workbook.add_format(
			{'bold': True, 'num_format': '0.00', 'font_name': 'Arial', 'align': 'right', 'font_size': fontSize, 'top': 1, 'bottom': 2})
		styleFooterSum.set_bottom(6)
		numberdos.set_font_size(fontSize)
		bord = workbook.add_format()
		bord.set_border(style=1)
		bord.set_text_wrap()
		# numberdos.set_border(style=1)

		title = workbook.add_format({'bold': True, 'font_name': 'Arial'})
		title.set_align('center')
		title.set_align('vcenter')
		# title.set_text_wrap()
		title.set_font_size(18)
		company = self.env['res.company'].search([], limit=1)[0]

		x = 0

		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		worksheet.merge_range(
			'D1:O1', u"REPORTE CONSOLIDADO", title)
		worksheet.set_row(x, 29)
		x = x+2

		worksheet.write(x, 0, u"Empresa:", bold)
		worksheet.write(x, 1, company.name, formatLeft)

		x = x+1
		worksheet.write(x, 0, u"Mes:", bold)
		worksheet.write(
			x, 1, obj.get_mes(int(obj.date_end[5:7]) if obj.date_end else 0).upper()+"-"+obj.date_end[:4], formatLeft)

		x = x+3

		extra_headers = [
			'EMPLEADO',
			'FECHA INGRESO',
			'DNI',
			'AFILIACION',
			'CUSSPP',
			'DISTRIBUCION ANALITICA',
			'TITULO DE TRABAJO',
			'BCO REMUNERACIONES',
			'NRO CUENTA',
			'HORAS DIURNAS',
			'HORAS NOCTURNAS',
			'DIAS VACACIONES',
			'DIAS SUBSIDIO MATERNIDAD',
			'DIAS SUBSIDIO ENFERMEDAD',
			'BASICO CONTRATO'
			]

		default_headers = ['x_contract_id','x_employee_id','x_dni','x_afiliacion_id']
		
		header_planilla_tabular = self.env['ir.model.fields'].search([
																('name', 'like', 'x_%'),
																('model', '=', 'planilla.tabular'),
																('name','not in',default_headers)], order="create_date")
		header_planilla_tabular_sn = self.env['ir.model.fields'].search([
																('name', 'like', 'x_%'),
																('model', '=', 'planilla.tabular'),
																('name','not in',default_headers),
																('name','!=','x_payslip_id')], order="create_date")
		
		headers = extra_headers+header_planilla_tabular_sn.mapped('field_description')
		for i,item in enumerate(headers):
			worksheet.write(x, i, item, boldbord)
		worksheet.write(x, i+1, 'Aportes ESSALUD', boldbord)
		worksheet.set_row(x, 50)
		x+=1
		sql_fields = default_headers+['\"'+i.name+'\"' for i in header_planilla_tabular]
		query = 'select %s from planilla_tabular' % (','.join(sql_fields))
		
		self.env.cr.execute(query)
		datos_planilla = self.env.cr.fetchall()
		print datos_planilla
		range_row = len(datos_planilla[0] if len(datos_planilla) > 0 else 0)
		total_essalud = 0
		for i in range(len(datos_planilla)):
			emp = self.env['hr.employee'].search([('identification_id','=',datos_planilla[i][2])]) # dni
			cont = self.env['hr.contract'].browse(datos_planilla[i][0])
			nomina = obj.slip_ids.filtered(lambda x: x.employee_id == emp and x.contract_id == cont)
			worksheet.write(x,0,datos_planilla[i][1], formatLeft)
			if cont.date_start:
				worksheet.write_datetime(x,1,datetime.strptime(cont.date_start,'%Y-%m-%d'), dateformat)
			else:
				worksheet.write(x,1,'', dateformat)
			worksheet.write(x,2,datos_planilla[i][2], formatLeft)
			worksheet.write(x,3,datos_planilla[i][3], formatLeft)
			worksheet.write(x,4,cont.cuspp or '', formatLeft)
			worksheet.write(x,5,cont.distribucion_analitica_id.descripcion or '', formatLeft)
			worksheet.write(x,6,cont.job_id.name or '', formatLeft)
			worksheet.write(x,7,emp.bank_account_id.bank_id.name or '', formatLeft)
			worksheet.write(x,8,emp.bank_account_id.acc_number or '', formatLeft)
			
			lines = nomina.worked_days_line_ids
			hlabd = lines.filtered(lambda x: x.code == 'HLABD').number_of_hours or 0
			hlabn = lines.filtered(lambda x: x.code == 'HLABN').number_of_hours or 0
			vac = lines.filtered(lambda x: x.code=='DVAC').number_of_days or 0
			dsubm = lines.filtered(lambda x: x.code=='DSUBM').number_of_days or 0
			dsube = lines.filtered(lambda x: x.code=='DSUBE').number_of_days or 0
			
			worksheet.write(x,9,hlabd, formatLeft)
			worksheet.write(x,10,hlabn, formatLeft)
			worksheet.write(x,11,vac, formatLeft)
			worksheet.write(x,12,dsubm, formatLeft)
			worksheet.write(x,13,dsube, formatLeft)
			worksheet.write(x,14,cont.wage or 0, numberdos)

			aux = 15
			for j in range(5,range_row):
				worksheet.write(x,aux,datos_planilla[i][j] or '0.00', numberdos)
				aux+=1
			essalud = self.env['hr.payslip'].browse(datos_planilla[i][4]).essalud
			worksheet.write(x,aux,essalud,formatLeft)
			total_essalud += essalud
			x = x+1
		x+=1
		datos_planilla_transpuesta = zip(*datos_planilla)
		size_dtt = len(datos_planilla_transpuesta)
		aux = 15
		for j in range(5,size_dtt):
			worksheet.write(x,aux,sum([float(d) for d in datos_planilla_transpuesta[j]]),styleFooterSum)
			aux+=1

		worksheet.write(x,aux,total_essalud,styleFooterSum)
		tam_col = [30,11] + [11 for i in range(size_dtt+19)]
		alpha,prev,acum = list(string.ascii_uppercase),'',0
		for i,item in enumerate(tam_col):
			worksheet.set_column(prev+alpha[i%26]+':'+prev+alpha[i%26],item)
			if i==26:
				prev = alpha[acum]
				acum+=1

		worksheet.set_column('V:V',None,None,{'hidden':True})
		workbook.close()
		f = open(path, 'rb')

		vals = {
			'output_name': 'planilla_tabular.xls',
			'output_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['planilla.export.file'].create(vals)

		return {
			"type": "ir.actions.act_window",
			"res_model": "planilla.export.file",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}