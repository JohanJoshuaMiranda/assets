# -*- coding: utf-8 -*-

from odoo import models, fields, api

class SaleOrderLine(models.Model):
	_inherit = 'sale.order.line'

	@api.multi
	def _prepare_order_line_procurement(self, group_id=False):
		vals = super(SaleOrderLine, self)._prepare_order_line_procurement(group_id=group_id)
		vals.update({
			'fx_description': self.name,
		})
		return vals

class ProcurementOrder(models.Model):
	_inherit = 'procurement.order'
	
	fx_description = fields.Char(string='FX Description')

	def _get_stock_move_values(self):
		vals = super(ProcurementOrder, self)._get_stock_move_values()
		vals.update({
			'fx_description': self.fx_description,
		})
		return vals