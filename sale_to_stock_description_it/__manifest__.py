# -*- encoding: utf-8 -*-
{
	'name': 'Descripcion Ventas a Albaran',
	'category': 'sale',
	'author': 'ITGRUPO-FERCO',
	'depends': ['sale','stock','fxo_stock_picking_extend_ferco'],
	'version': '1.0',
	'description':"""
	- Copia descripcion de Lineas de Venta a Lineas de Albaran.
	""",
	'auto_install': False,
	'demo': [],
	'data':	[],
	'installable': True
}
