# -*- encoding: utf-8 -*-

from openerp import models, fields, api
from openerp.osv import osv

class main_parameter(models.Model):
	_inherit = 'main.parameter'

	balance_sheet_account = fields.Many2one('account.account',string='Cuenta Cierre Contable Utilidad')
	lost_sheet_account = fields.Many2one('account.account',string='Cuenta Cierre Contable Perdida')
	lost_result_account = fields.Many2one('account.account',string='Cuenta Resultados A.C. Perdida')
	profit_result_account = fields.Many2one('account.account',string='Cuenta Resultados A.C. Ganancia')