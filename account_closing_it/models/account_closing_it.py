# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
import base64
from io import BytesIO
import uuid

class AccountClosingIt(models.Model):
	_name = 'account.closing.it'

	@api.depends('fiscal_year_id')
	def _get_name(self):
		for i in self:
			i.name = i.fiscal_year_id.name

	name = fields.Char(compute=_get_name,store=True)
	fiscal_year_id = fields.Many2one('account.fiscalyear',string=u'Año Fiscal',required=True)
	period = fields.Many2one('account.period',string='Periodo Cierre',required=True)
	journal_id = fields.Many2one('account.journal',string='Diario Cierre',required=True)
	company_id = fields.Many2one('res.company',string=u'Compañía',default=lambda self: self.env.user.company_id)

	account_move_id = fields.Many2one('account.move',string='Asiento de Cierre')

	def unlink(self):
		if self.account_move_id:
			raise UserError("No se puede eliminar un Cierre Contable si tiene asiento.")
		return super(AccountClosingIt,self).unlink()
	
	def preview_closing(self):
		import io
		from xlsxwriter.workbook import Workbook

		direccion = self.env['main.parameter'].search([])[0].dir_create_file

		workbook = Workbook(direccion + 'Cierre_contable.xlsx')

		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=2)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(10)
		boldbord.set_bg_color('#DCE6F1')
		boldbord.set_font_name('Times New Roman')

		especial1 = workbook.add_format()
		especial1.set_align('center')
		especial1.set_align('vcenter')
		especial1.set_border(style=1)
		especial1.set_text_wrap()
		especial1.set_font_size(10)
		especial1.set_font_name('Times New Roman')

		numberdos = workbook.add_format({'num_format': '0.000'})
		numberdos.set_border(style=1)
		numberdos.set_font_size(10)
		numberdos.set_font_name('Times New Roman')


		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		worksheet = workbook.add_worksheet("ASIENTO PRINCIPAL")
		worksheet.set_tab_color('blue')

		x = 0

		worksheet.write(x, 0, "CUENTA", boldbord)
		worksheet.write(x, 1, "DEBE", boldbord)
		worksheet.write(x, 2, "HABER", boldbord)
		# este campo agrego jesus sanchez
		worksheet.write(x, 3, "MONEDA", boldbord)
		worksheet.write(x, 4, "IMPORTE EN MONEDA", boldbord)

		x = 1

		lines = []
		self.cierre_contable_0()
		lines += self.cierre_contable_1()
		lines += self.cierre_contable_2()
		lines += self.cierre_contable_3()
		lines += self.cierre_contable_4()

		if not len(lines)>0:
			raise UserError('No hay datos.')
		

		for line in lines:
			account_id = self.env['account.account'].browse(line[2]['account_id'])
			worksheet.write(x,0,account_id.code if account_id else '',especial1)
			worksheet.write(x,1,line[2]['debit'] if line[2]['debit'] else 0,numberdos)
			worksheet.write(x,2,line[2]['credit'] if line[2]['credit'] else 0,numberdos)
			worksheet.write(x,3,account_id.currency_id.name if account_id.currency_id else '',especial1)
			worksheet.write(x,4,0,numberdos)
			x+=1

		tam_col = [15,12,12,10,15]

		worksheet.set_column('A:A', tam_col[0])
		worksheet.set_column('B:B', tam_col[1])
		worksheet.set_column('C:C', tam_col[2])
		worksheet.set_column('D:D', tam_col[3])
		worksheet.set_column('E:E', tam_col[4])

		workbook.close()

		f = open(direccion + 'Cierre_contable.xlsx', 'rb')

		vals = {
			'output_name': 'Preview - Cierre Contable.xlsx',
			'output_file': base64.encodestring(''.join(f.readlines())),
			'respetar':1,		
		}
		sfs_id = self.env['export.file.save'].create(vals)
		
		return {
			"type": "ir.actions.act_window",
			"res_model": "export.file.save",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}

	def cierre_contable_oficial(self):
		if self.account_move_id:
			raise UserError('Ya existe un Asiento de Cierre.')
		
		lines = []
		self.cierre_contable_0()
		lines += self.cierre_contable_1()
		lines += self.cierre_contable_2()
		lines += self.cierre_contable_3()
		lines += self.cierre_contable_4()

		if len(lines)>0:
			move_id = self.env['account.move'].create({
				'company_id': self.company_id.id,
				'journal_id': self.journal_id.id,
				'date': self.fiscal_year_id.date_stop,
				'line_ids':lines,
				'ref': 'CIERRE - '+self.fiscal_year_id.name,
				'fecha_special':True})
			move_id.post()
			self.account_move_id = move_id.id
		else:
			raise UserError(u'Probablemente este intentando generar un asiento vacío.')	

	def cierre_contable_0(self):
		self.env.cr.execute(""" 
			CREATE TABLE IF NOT EXISTS cierre_contable_view (
				id serial PRIMARY KEY,
				cierre_id integer,
				m_close varchar(3),
				account_id integer,
				debit numeric (12, 2),
				credit numeric (12, 2),
				saldo_deudor numeric (12, 2),
				saldo_acreedor numeric (12, 2),
				debit_costo numeric (12, 2),
				credit_costo numeric (12, 2),
				deudor numeric (12, 2),
				acreedor numeric (12, 2),
				debit_resultado numeric (12, 2),
				credit_resultado numeric (12, 2),
				debit_clase9 numeric (12, 2),
				credit_clase9 numeric (12, 2),
				debit_ap numeric (12, 2),
				credit_ap numeric (12, 2)
				);""")

		self.env.cr.execute("""DELETE FROM cierre_contable_view WHERE cierre_id = %s"""%(str(self.id)))
		self.env.cr.execute(""" 
			INSERT INTO cierre_contable_view (cierre_id, m_close,account_id,debit,credit,saldo_deudor,saldo_acreedor,debit_costo,credit_costo,deudor,acreedor,debit_resultado,credit_resultado,debit_clase9,credit_clase9,debit_ap,credit_ap)
			select
			%d as cierre_id,
			aa.m_close,
			T.account_id,
			T.debit,
			T.credit,
			T.saldo_deudor,
			T.saldo_acreedor,
			0 as debit_costo,
			0 as credit_costo,
			0 as deudor,
			0 as acreedor,
			0 as debit_resultado,
			0 as credit_resultado,
			0 as debit_clase9,
			0 as credit_clase9,
			0 as debit_ap,
			0 as credit_ap from 
			(select aa_id as account_id,sum(debe) as debit,sum(haber)as credit,
			case when sum(debe-haber)>0 then sum(debe-haber) else 0 end as saldo_deudor,
			case when sum(haber-debe)>0 then sum(haber-debe) else 0 end as saldo_acreedor
			from get_libro_diario(%s, %s)
			where aa_id is not null
			group by aa_id)T
			LEFT JOIN account_account aa ON aa.id = T.account_id;
			""" % (self.id,
			str(self.fiscal_year_id.name)+'00',
			str(self.fiscal_year_id.name)+'12'))

		#SI LAS CUENTAS DE CIERRE NO EXISTEN EN NUESTRA TABLA SE INSERTAN
		self.env.cr.execute("""SELECT
			aa.account_close_id,
			aa2.m_close
			FROM cierre_contable_view cc 
			LEFT JOIN account_account aa ON aa.id = cc.account_id
			LEFT JOIN account_account aa2 ON aa2.id = aa.account_close_id
			WHERE cc.cierre_id = %s AND
			cc.m_close = '1' AND
			aa.account_close_id is not null""" % (str(self.id)))
		acc = self.env.cr.dictfetchall()
		
		if len(acc) > 0:
			for elemnt in acc:
				self.env.cr.execute("""SELECT * FROM cierre_contable_view WHERE account_id = %s""" % (str(elemnt['account_close_id'])))
				exists = self.env.cr.dictfetchall()
				if len(exists) <= 0:
					self.env.cr.execute("""INSERT INTO cierre_contable_view (cierre_id, m_close,account_id,debit,credit,saldo_deudor,saldo_acreedor,debit_costo,credit_costo,deudor,acreedor,debit_resultado,credit_resultado,debit_clase9,credit_clase9,debit_ap,credit_ap)
					VALUES
					(%s,'%s',%s,0,0,0,0,0,0,0,0,0,0,0,0,0,0);""" % (str(self.id),str(elemnt['m_close']),str(elemnt['account_close_id'])))

		########
		return True

	def cierre_contable_1(self):
		self.env.cr.execute("""UPDATE cierre_contable_view SET debit_costo = saldo_acreedor, credit_costo = saldo_deudor WHERE m_close = '1' AND cierre_id = %s""" % (str(self.id)))

		self.env.cr.execute("""SELECT cc.account_id, 
								aa.account_close_id,
								cc.saldo_deudor,
								cc.saldo_acreedor 
								FROM cierre_contable_view cc 
								LEFT JOIN account_account aa ON aa.id = cc.account_id
								WHERE cc.cierre_id = %s AND
								cc.m_close = '1' AND
								aa.account_close_id is not null """ % (str(self.id)))

		res = self.env.cr.dictfetchall()
		if len(res) > 0:
			for line in res:
				self.env.cr.execute("""UPDATE cierre_contable_view SET debit_costo = %s, credit_costo = %s WHERE cierre_id = %s AND account_id = %s""" % (str(line['saldo_deudor']),str(line['saldo_acreedor']),str(self.id),str(line['account_close_id'])))

		self.env.cr.execute("""SELECT id,account_id,saldo_deudor,saldo_acreedor,debit_costo,credit_costo FROM cierre_contable_view WHERE cierre_id = %s""" % (str(self.id)))
		data = self.env.cr.dictfetchall()
		if len(data) > 0:
			for line in data:
				deudor = 0
				acreedor = 0
				amount_logic = (line['saldo_deudor']+line['debit_costo'])-(line['saldo_acreedor']+line['credit_costo'])
				if amount_logic != 0:
					deudor = amount_logic if amount_logic > 0 else 0
					acreedor = 0 if amount_logic > 0 else abs(amount_logic)
				self.env.cr.execute("""UPDATE cierre_contable_view SET deudor = %s, acreedor = %s WHERE cierre_id = %s AND account_id = %s AND id =%s"""%(str(deudor),str(acreedor),str(self.id),str(line['account_id']),str(line['id'])))

		self.env.cr.execute("""SELECT account_id,debit_costo,credit_costo FROM cierre_contable_view WHERE cierre_id = %s
							AND (debit_costo <> 0 OR credit_costo <> 0)""" % (str(self.id)))

		tabla = self.env.cr.dictfetchall()
		lineas = []
		for line in tabla:
			vals = (0,0,{
				'account_id': line['account_id'],
				'name': 'ASIENTO DE CIERRE',
				'debit': line['debit_costo'],
				'credit': line['credit_costo'],
				'company_id': self.company_id.id,
			})
			lineas.append(vals)
		return lineas

	def cierre_contable_2(self):
		self.env.cr.execute("""UPDATE cierre_contable_view SET debit_clase9 = acreedor, credit_clase9 = deudor WHERE m_close = '2' AND cierre_id = %s""" % (str(self.id)))
		self.env.cr.execute("""SELECT account_id,debit_clase9,credit_clase9 FROM cierre_contable_view WHERE cierre_id = %s
							AND (debit_clase9 <> 0 OR credit_clase9 <> 0)""" % (str(self.id)))

		tabla = self.env.cr.dictfetchall()
		lineas = []
		for line in tabla:
			vals = (0,0,{
				'account_id': line['account_id'],
				'name': 'ASIENTO DE CIERRE',
				'debit': line['debit_clase9'],
				'credit': line['credit_clase9'],
				'company_id': self.company_id.id,
			})
			lineas.append(vals)
		return lineas

	def cierre_contable_3(self):
		self.env.cr.execute("""UPDATE cierre_contable_view SET debit_resultado = acreedor, credit_resultado = deudor WHERE m_close = '3' AND cierre_id = %s""" % (str(self.id)))
		self.env.cr.execute("""SELECT account_id,debit_resultado,credit_resultado FROM cierre_contable_view WHERE cierre_id = %s
							AND (debit_resultado <> 0 OR credit_resultado <> 0)""" % (str(self.id)))

		tabla = self.env.cr.dictfetchall()
		lineas = []
		sum_debit = 0
		sum_credit = 0
		for line in tabla:
			vals = (0,0,{
				'account_id': line['account_id'],
				'name': 'ASIENTO DE CIERRE',
				'debit': line['debit_resultado'],
				'credit': line['credit_resultado'],
				'company_id': self.company_id.id,
			})
			sum_debit+=line['debit_resultado']
			sum_credit+=line['credit_resultado']
			lineas.append(vals)

		balance_sheet_account = self.env['main.parameter'].search([],limit=1).balance_sheet_account
		profit_result_account = self.env['main.parameter'].search([],limit=1).profit_result_account
		if not balance_sheet_account:
			raise UserError(u'No existe Cuenta Cierre Contable Utilidad en Parametros Principales de Contabilidad para su Compañía')
		if not profit_result_account:
			raise UserError(u'No existe Cuenta Resultados A.C. Ganancia en Parametros Principales de Contabilidad para su Compañía')

		lost_sheet_account = self.env['main.parameter'].search([],limit=1).lost_sheet_account
		lost_result_account = self.env['main.parameter'].search([],limit=1).lost_result_account
		if not lost_sheet_account:
			raise UserError(u'No existe Cuenta Cierre Contable Perdida en Parametros Principales de Contabilidad para su Compañía')
		if not lost_result_account:
			raise UserError(u'No existe Cuenta Resultados A.C. Perdida en Parametros Principales de Contabilidad para su Compañía')
		if (sum_debit - sum_credit) != 0:
			vals = (0,0,{
				'account_id': balance_sheet_account.id if (sum_debit - sum_credit) > 0 else lost_sheet_account.id,
				'name': 'ASIENTO DE CIERRE',
				'debit': 0 if (sum_debit - sum_credit) > 0 else abs(sum_debit - sum_credit),
				'credit': (sum_debit - sum_credit) if (sum_debit - sum_credit) > 0 else 0,
				'company_id': self.company_id.id,
			})
			lineas.append(vals)

		if (sum_debit - sum_credit) != 0:
			vals_prof = (0,0,{
				'account_id': balance_sheet_account.id if (sum_debit - sum_credit) > 0 else lost_sheet_account.id,
				'name': 'ASIENTO DE CIERRE',
				'debit': (sum_debit - sum_credit) if (sum_debit - sum_credit) > 0 else 0,
				'credit': 0 if (sum_debit - sum_credit) > 0 else abs(sum_debit - sum_credit),
				'company_id': self.company_id.id,
			})
			lineas.append(vals_prof)
			vals_prof = (0,0,{
				'account_id': profit_result_account.id if (sum_debit - sum_credit) > 0 else lost_result_account.id,
				'name': 'ASIENTO DE CIERRE',
				'debit': 0 if (sum_debit - sum_credit) > 0 else abs(sum_debit - sum_credit),
				'credit': (sum_debit - sum_credit) if (sum_debit - sum_credit) > 0 else 0,
				'company_id': self.company_id.id,
			})
			lineas.append(vals_prof)
		return lineas

	def cierre_contable_4(self):
		self.env.cr.execute("""UPDATE cierre_contable_view SET debit_ap = acreedor, credit_ap = deudor WHERE m_close = '4' AND cierre_id = %s""" % (str(self.id)))
		self.env.cr.execute("""SELECT ccv.account_id,rc.name AS currency,aa.currency_id,ccv.debit_ap,ccv.credit_ap FROM cierre_contable_view ccv 
								LEFT JOIN account_account aa ON aa.id = ccv.account_id
								LEFT JOIN res_currency rc On rc.id = aa.currency_id
								WHERE ccv.cierre_id = %s AND (ccv.debit_ap <> 0 OR ccv.credit_ap <> 0)""" % (str(self.id)))

		tabla = self.env.cr.dictfetchall()
		lineas = []
		sum_debit = 0
		sum_credit = 0
		for line in tabla:
			vals = (0,0,{
				'account_id': line['account_id'],
				'name': 'ASIENTO DE CIERRE',
				'currency_id': line['currency_id'] if line['currency'] == 'USD' else None,
				'amount_currency': 0 if line['currency'] == 'USD' else None,
				'debit': line['debit_ap'],
				'credit': line['credit_ap'],
				'company_id': self.company_id.id,
			})
			sum_debit += line['debit_ap']
			sum_credit += line['credit_ap']
			lineas.append(vals)

		if abs(sum_debit - sum_credit) > 0:
			profit_result_account = self.env['main.parameter'].search([],limit=1).profit_result_account
			lost_result_account = self.env['main.parameter'].search([],limit=1).lost_result_account
			vals = (0,0,{
				'account_id': profit_result_account.id if (sum_debit - sum_credit) < 0 else lost_result_account.id,
				'name': 'ASIENTO DE CIERRE',
				'debit': abs(sum_debit - sum_credit) if (sum_debit - sum_credit) < 0 else 0,
				'credit': 0 if (sum_debit - sum_credit) < 0 else abs(sum_debit - sum_credit),
				'company_id': self.company_id.id,
			})
			lineas.append(vals)
		return lineas