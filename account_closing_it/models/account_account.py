# -*- coding: utf-8 -*-

from odoo import models, fields, api

class AccountAccount(models.Model):
	_inherit = 'account.account'

	m_close = fields.Selection([
								('1','Costo de Ventas'),
								('2','Cierre Clase 9'),
								('3','Cierre Cuentas Resultados'),
								('4','Cierre de Activo y Pasivo')
								],string='Metodo de cierre')
	account_close_id = fields.Many2one('account.account',string='Cuenta de Cierre')