# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError

class StockPicking(models.Model):
	_inherit = 'stock.picking'

	@api.multi
	def get_stock_lot(self):
		balances = self.env['balances.by.lot'].search([])
		if balances:
			balances.unlink()
		sql = """select * from vst_saldos_por_lote
				 where product_id in (%s)""" % ','.join([str(l.id) for l in self.move_lines.mapped('product_id')])
		self.env.cr.execute(sql)
		data = self.env.cr.dictfetchall()
		for i in data:
			lot_object = self.env['stock.production.lot'].browse(i['lot_id'])
			self.env['balances.by.lot'].create({'product_id':i['product_id'],
												'lot_id':i['lot_id'],
												'life_date':lot_object.life_date if lot_object else '',
												'location_id':i['location_id'],
												'move_in':i['entrada'],
												'move_out':i['salida'],
												'balance':i['saldo']})
		return {
				'name': 'Saldos por Lote',
				'type':'ir.actions.act_window',
				'view_type':'form',
				'view_mode':'tree',
				'res_model':'balances.by.lot',
				'views':[[self.env.ref('balances_by_lot.balances_by_lot_tree_view').id,'tree']]
			}