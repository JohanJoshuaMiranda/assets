# -*- encoding: utf-8 -*-
{
	'name': u'Fxo Lotes Extend IT',
	'category': 'reports',
	'author': 'ITGRUPO-FERCO',
	'depends': ['fxo_lotes_extend', 'balances_by_lot'],
	'version': '10.0',
	'description':"""
	Cambio en boton ver lotes
	""",
	'auto_install': False,
	'demo': [],
	'data':	[],
	'installable': True
}
