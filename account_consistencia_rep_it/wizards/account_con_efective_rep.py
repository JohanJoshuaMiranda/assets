# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import *

class AccountConEfectiveRep(models.TransientModel):
	_name = 'account.con.efective.rep'

	fiscal_year_id = fields.Many2one('account.fiscalyear',string=u'Ejercicio',required=True)
	period_from = fields.Many2one('account.period',string='Periodo Inicial',required=True)
	period_to = fields.Many2one('account.period',string='Periodo Final',required=True)

	@api.multi
	def get_report(self):
		sql = """
			CREATE OR REPLACE view account_con_efective_book as ("""+self._get_sql()+""")"""

		self.env.cr.execute(sql)

		return {
			'name': 'Consistencia Flujo Efectivo',
			'type': 'ir.actions.act_window',
			'res_model': 'account.con.efective.book',
			'view_mode': 'tree',
			'view_type': 'form',
			'views': [(False, 'tree')],
		}

	def _get_sql(self):
		sql = """
				SELECT row_number() OVER () AS id,
				T.account_code,
				T.account_efective_type_name,
				T.ingreso,
				T.egreso,
				T.balance
				FROM
				(SELECT
				aa.code AS account_code,
				ati.concept AS account_efective_type_name,
				sum(vst_d.credit) as ingreso,
				sum(vst_d.debit) as egreso, 
				sum(vst_d.credit-vst_d.debit) as balance 
				FROM account_move_line vst_d 
				LEFT JOIN account_account aa ON aa.id = vst_d.account_id
				LEFT JOIN account_config_efective ati ON ati.id = aa.fefectivo_id
				WHERE vst_d.move_id IN
				(
				SELECT am_id as move_id FROM get_libro_diario(periodo_num('%s'),periodo_num('%s'))
				WHERE LEFT(cuenta,2)='10')
				AND LEFT(aa.code,2)<>'10'
				GROUP BY aa.code,ati.concept) T
			""" % (self.period_from.code,
			self.period_to.code)

		return sql