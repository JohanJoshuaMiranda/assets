# -*- encoding: utf-8 -*-
{
	'name': 'LIBRO MAYOR FERCO',
	'category': 'account',
	'author': 'ITGRUPO-FERCO',
	'depends': ['account','import_base_it','account_bank_report_it'],
	'version': '1.0',
	'description':"""
	- Este modulo reemplaza accion del menu Libros Contables/Libro Auxiliar de Caja y Bancos
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
			'security/ir.model.access.csv',
			'sql_function.sql',
			'views/account_auxiliar_banco_book.xml',
			'wizard/account_auxiliar_banco_wizard.xml'
			],
	'installable': True
}
