DROP FUNCTION IF EXISTS public.get_auxiliar_bancos(integer, integer, integer, integer, character varying) CASCADE;

CREATE OR REPLACE FUNCTION public.get_auxiliar_bancos(
	per_ini_saldo integer,
	per_fin_saldo integer,
	per_ini_det integer,
	per_fin_det integer,
	cuenta character varying)
    RETURNS TABLE(id bigint, fecha date, partner character varying, documento character varying, glosa character varying, cargo numeric, abono numeric, tc numeric, importedivisa numeric, asiento character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
BEGIN
	RETURN QUERY  

select row_number() over() as id,tt.fecha,tt.partner,tt.documento,tt.glosa,tt.cargo,tt.abono,tt.tc,tt.importedivisa,tt.asiento from (

select null as fecha,null as partner,null as documento,'SALDO INICIAL' as glosa,
sum(T.debe) as cargo,sum(T.haber) as abono,null as tc,sum(T.importedivisa) as importedivisa,null as asiento
from get_libro_diario($1,$2) as T where T.cuenta=$5
union all
select 
T2.fechaemision as fecha,
T2.partner,
numero as documento,
T2.glosa as glosa,
T2.debe as cargo,
T2.haber as abono,
T2.tipodecambio as tc,
T2.importedivisa,
T2.voucher as asiento
from get_libro_diario($3,$4) as T2
where T2.cuenta=$5)tt;
END;
$BODY$;