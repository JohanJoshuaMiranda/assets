# -*- coding: utf-8 -*-
import base64
from openerp import models, fields, api
from odoo.exceptions import UserError
from datetime import *

class AccountAuxiliarBancoWizard(models.TransientModel):
	_name = 'account.auxiliar.banco.wizard'

	account_id = fields.Many2one('account.account',string='Cuenta')
	period_start = fields.Many2one('account.period',string='Periodo Inicial')
	period_end = fields.Many2one('account.period',string='Periodo Final')
	type_show =  fields.Selection([('pantalla','Pantalla'),('excel','Excel')],string=u'Mostrar en',default='excel')

	@api.multi
	def do_rebuild(self):
		self.env.cr.execute("""
				CREATE OR REPLACE view account_auxiliar_banco_book as ("""+self._get_sql(self.period_start.code,self.period_end.code,self.account_id.code,)+""")""")
				
		if self.type_show == 'pantalla':
			return {
				'name': 'Auxiliar de Bancos',
				'type': 'ir.actions.act_window',
				'res_model': 'account.auxiliar.banco.book',
				'view_mode': 'tree',
				'view_type': 'form',
				'views': [(False, 'tree')],
			}

		if self.type_show == 'excel':
			import io
			from xlsxwriter.workbook import Workbook
			from xlsxwriter.utility import xl_rowcol_to_cell

			direccion = self.env['main.parameter'].search([])[0].dir_create_file
			workbook = Workbook( direccion + 'auxbank.xlsx')
			worksheet = workbook.add_worksheet("Libro Auxiliar de Caja y Banco")
			bold = workbook.add_format({'bold': True})

			bold_title = workbook.add_format({'bold': True,'align': 'center','valign': 'vcenter'})
			bold_title.set_font_size(14)
			boldbord = workbook.add_format({'bold': True})
			boldbord.set_border(style=2)
			boldbord.set_align('center')
			boldbord.set_align('vcenter')
			boldbord.set_text_wrap()
			boldbord.set_font_size(9)
			boldbord.set_bg_color('#DCE6F1')
			numberdos = workbook.add_format({'num_format':'0.00'})
			bord = workbook.add_format()
			bord.set_border(style=1)
			numberdos.set_border(style=1)	
			dateformat = workbook.add_format({'num_format':'d-m-yyyy'})
			dateformat.set_border(style=1)
			dateformat.set_font_size(10)
			dateformat.set_font_name('Times New Roman')	
			x= 8

			import sys
			reload(sys)
			sys.setdefaultencoding('iso-8859-1')

			compania_obj = self.env['res.company'].search([])[0]

			worksheet.write(0,0, compania_obj.partner_id.name, bold)
			worksheet.write(1,0, compania_obj.partner_id.nro_documento, bold)
			worksheet.merge_range(2,0,2,12, "LIBRO AUXILIAR DE CAJA Y BANCOS", bold_title)
			worksheet.merge_range(3,0,3,12, "(DEL "+ str(self.period_start.date_start) +" AL " + str(self.period_end.date_stop) +")", bold_title)

			worksheet.write(5,0,"Cuenta Bancaria/Caja:",bold)
			worksheet.write(5,2,self.account_id.name,bold)
			worksheet.write(6,0,"Moneda:",bold)
			worksheet.write(6,2,self.account_id.currency_id.name if self.account_id.currency_id.name else '',bold)


			worksheet.write(7,0, "Fecha",boldbord)
			worksheet.write(7,1, u"Nombre/Razón Social",boldbord)
			worksheet.write(7,2, u"Documento",boldbord)
			
			worksheet.write(7,3, u"Glosa",boldbord)
			worksheet.write(7,4, u"Cargo MN",boldbord)
			worksheet.write(7,5, u"Abono MN",boldbord)
			worksheet.write(7,6, u"Saldo MN",boldbord)

			worksheet.write(7,7, u"Cargo ME",boldbord)
			worksheet.write(7,8, u"Abono ME",boldbord)
			worksheet.write(7,9, u"Saldo ME",boldbord)

			worksheet.write(7,10, u"Nro. De Asiento",boldbord)

			for line in self.env['account.auxiliar.banco.book'].search([]):

				worksheet.write(x,0,line.fecha if line.fecha else '' ,dateformat )
				worksheet.write(x,1,line.partner if line.partner  else '',bord )
				worksheet.write(x,2,line.documento if line.documento  else '',bord)
				worksheet.write(x,3,line.glosa if line.glosa  else '',bord)
				worksheet.write(x,4,line.cargomn ,numberdos)
				worksheet.write(x,5,line.abonomn ,numberdos)
				worksheet.write(x,6,line.saldomn ,numberdos)

				worksheet.write(x,7,line.cargome ,numberdos)
				worksheet.write(x,8,line.abonome ,numberdos)
				worksheet.write(x,9,line.saldome ,numberdos)

				worksheet.write(x,10,line.asiento if line.asiento else '' ,bord)

				x += 1

			worksheet.write(x,2, 'TOTAL', bord)
			worksheet.write_formula(x,4, '=sum(' + xl_rowcol_to_cell(8,4) +':' +xl_rowcol_to_cell(x-1,4) + ')', numberdos)
			worksheet.write_formula(x,5, '=sum(' + xl_rowcol_to_cell(8,5) +':' +xl_rowcol_to_cell(x-1,5) + ')', numberdos)
			worksheet.write_formula(x,7, '=sum(' + xl_rowcol_to_cell(8,7) +':' +xl_rowcol_to_cell(x-1,7) + ')', numberdos)
			worksheet.write_formula(x,8, '=sum(' + xl_rowcol_to_cell(8,8) +':' +xl_rowcol_to_cell(x-1,8) + ')', numberdos)

			tam_col = [14,24,14,36,12,12,12,12,12,12,12]

			worksheet.set_column('A:A', tam_col[0])
			worksheet.set_column('B:B', tam_col[1])
			worksheet.set_column('C:C', tam_col[2])
			worksheet.set_column('D:D', tam_col[3])
			worksheet.set_column('E:E', tam_col[4])
			worksheet.set_column('F:F', tam_col[5])
			worksheet.set_column('G:G', tam_col[6])
			worksheet.set_column('H:H', tam_col[7])
			worksheet.set_column('I:I', tam_col[8])
			worksheet.set_column('J:J', tam_col[9])
			worksheet.set_column('K:K', tam_col[10])

			workbook.close()
			
			f = open(direccion + 'auxbank.xlsx', 'rb')
			
			
			vals = {
				'output_name': 'LibroAuxiliarCajaBanco.xlsx',
				'output_file': base64.encodestring(''.join(f.readlines())),		
			}

			sfs_id = self.env['export.file.save'].create(vals)
			return {
			    "type": "ir.actions.act_window",
			    "res_model": "export.file.save",
			    "views": [[False, "form"]],
			    "res_id": sfs_id.id,
			    "target": "new",
			}

	def _get_sql(self,periodo_start,periodo_end,cuenta):

		if periodo_start[:2] == '00' or periodo_start[:2] == '13' or periodo_end[:2] == '00' or periodo_end[:2] == '13':
			raise UserError('NO PUEDE PONER PERIODO APERTURA - CIERRE')

		if int(periodo_start[:2]) > int(periodo_end[:2]):
			raise UserError('EL PERIODO FINAL SIEMPRE TIENE QUE SER  MAYOR O IGUAL QUE EL PERIODO INICIAL')

		periodo1 = '00/' + periodo_start[3:]
		periodo2 = str('{:02d}'.format(int(periodo_start[:2])-1)) + '/' + periodo_start[3:]

		sql = """
		select 
		id,fecha,partner,documento,glosa,cargo as cargomn,abono as abonomn,
		(
			select sum(cargo-abono)
			from get_auxiliar_bancos(periodo_num('%s'),periodo_num('%s'),periodo_num('%s'),periodo_num('%s'),'%s') as b
			where b.id<=a.id
		) as saldomn,
		case when importedivisa > 0 then importedivisa else 0 end as cargome,
		case when importedivisa < 0 then abs(importedivisa) else 0 end as abonome,
		(
			select sum(importedivisa)
			from get_auxiliar_bancos(periodo_num('%s'),periodo_num('%s'),periodo_num('%s'),periodo_num('%s'),'%s') as b
			where b.id<=a.id
		) as saldome,
		asiento
		from get_auxiliar_bancos(periodo_num('%s'),periodo_num('%s'),periodo_num('%s'),periodo_num('%s'),'%s') a

		""" % (periodo1,periodo2,periodo_start,periodo_end,cuenta,
		periodo1,periodo2,periodo_start,periodo_end,cuenta,
		periodo1,periodo2,periodo_start,periodo_end,cuenta)

		return sql