# -*- encoding: utf-8 -*-
from openerp.osv import osv
from openerp import models,fields ,api
from odoo.exceptions import UserError, ValidationError

class resumen_kardex_contable(models.Model):
	_name = 'resumen.kardex.contable'

	cuenta = fields.Many2one('account.account','Cuenta Contable')
	debe = fields.Float('Debe')
	haber = fields.Float('Haber')


class reporte_kardex_contable(models.Model):
	_name='reporte.kardex.contable'
	
	fecha = fields.Many2one('account.period','Periodo',required=True)
	
	_rec_name = 'fecha'


	@api.multi
	def ver_informe(self):
		param = self.env['main.parameter'].search([])[0]
		new_param = self.env['main.parameter'].search([])[0]


		self.env.cr.execute("""
drop table if exists resumen_kardex_contable;
create table resumen_kardex_contable as
select row_number() OVER () AS id,
aml.account_id as cuenta, sum(aml.debit) as debe, sum(aml.credit) as haber from get_kardex_v(""" +str(param.fiscalyear)+ """0101,""" +str(self.fecha.date_stop).replace('-','')+ """, (select array_agg(id) from product_product), (select array_agg(id) from stock_location ),-1 ) 
inner join stock_move sm on sm.id = get_kardex_v.stock_moveid
inner join stock_picking sp on sp.id = sm.picking_id
inner join account_invoice ai on ai.id = sp.invoice_id
inner join account_move_line aml on aml.move_id = ai.move_id
where get_kardex_v.fecha between '""" +str(self.fecha.date_start)+ """' and '""" +str(self.fecha.date_stop)+ """'
group by aml.account_id
			
			""")

		return {
			'type': 'ir.actions.act_window',
			'res_model': 'resumen.kardex.contable',
			'view_mode': 'tree',
			'view_type': 'form',
			'views': [(False, 'tree')],
		}


class resumen_kardex_vs_compra(models.Model):
	_name = 'resumen.kardex.vs.compra'

	fecha_albaran = fields.Date('Fecha Albaran')
	fecha = fields.Date('Fecha')
	doc_type_ope = fields.Char('Tipo Documento')
	serial = fields.Char('Serie')
	nro = fields.Char('Numero')
	nro_guia = fields.Char('Nro Guia')
	doc_almac = fields.Char('Doc. Almacen')
	doc_partner = fields.Char('RUC')
	name = fields.Char('Empresa')
	operation_type = fields.Char('T. OP.')
	default_code = fields.Char('Codigo')
	name_template = fields.Char('Producto')
	unidad = fields.Char('Unidad')
	ingreso = fields.Float('Ingreso Cantidad')
	debit = fields.Float('Ingreso Costo')

class reporte_kardex_vs_compra(models.Model):
	_name = 'reporte.kardex.vs.compra'

	period_id = fields.Many2one('account.period','Periodo',required=True)

	@api.multi
	def ver_informe(self):
		param = self.env['main.parameter'].search([])[0]
		new_param = self.env['main.parameter'].search([])[0]


		self.env.cr.execute("""
drop table if exists resumen_kardex_vs_compra;
create table resumen_kardex_vs_compra as
select row_number() OVER () AS id,
get_kardex_v.fecha_albaran,
get_kardex_v.fecha,
get_kardex_v.doc_type_ope,
get_kardex_v.serial,
get_kardex_v.nro,
sp.nro_guia,
get_kardex_v.doc_almac,
get_kardex_v.doc_partner,
get_kardex_v.name,
get_kardex_v.operation_type,
get_kardex_v.default_code,
get_kardex_v.name_template,
get_kardex_v.unidad,
get_kardex_v.ingreso,
get_kardex_v.debit

from get_kardex_v(""" +str(param.fiscalyear)+ """0101,""" +str(self.fecha.date_stop).replace('-','')+ """, (select array_agg(id) from product_product), (select array_agg(id) from stock_location ),-1 ) 
left join stock_move sm on sm.id = get_kardex_v.stock_moveid
left join stock_picking sp on sp.id = sm.picking_id
left join (select distinct tipodocumento, serie, numero, razonsocial, ruc from get_compra_1_1_1(""" +self.period_id.code.split('/')[1]+self.period_id.code.split('/')[0]+ """,""" +self.period_id.code.split('/')[1]+self.period_id.code.split('/')[0]+ """)) as O on O.tipodocumento = doc_type_ope
and O.serie = serial and O.numero = nro and O.ruc = doc_partner 
where O.tipodocumento is null and get_kardex_v.serial is not null and origen= 'Ubicaciones de empresas/Clientes'
and get_kardex_v.fecha between '""" +str(self.fecha.date_start)+ """' and '""" +str(self.fecha.date_stop)+ """'

			
			""")

		return {
			'type': 'ir.actions.act_window',
			'res_model': 'resumen.kardex.vs.compra',
			'view_mode': 'tree',
			'view_type': 'form',
			'views': [(False, 'tree')],
		}





class resumen_kardex_sinfactura(models.Model):
	_name = 'resumen.kardex.sinfactura'

	fecha_albaran = fields.Date('Fecha Albaran')
	fecha = fields.Date('Fecha')
	doc_type_ope = fields.Char('Tipo Documento')
	serial = fields.Char('Serie')
	nro = fields.Char('Numero')
	nro_guia = fields.Char('Nro Guia')
	doc_almac = fields.Char('Doc. Almacen')
	doc_partner = fields.Char('RUC')
	name = fields.Char('Empresa')
	operation_type = fields.Char('T. OP.')
	default_code = fields.Char('Codigo')
	name_template = fields.Char('Producto')
	unidad = fields.Char('Unidad')
	ingreso = fields.Float('Ingreso Cantidad')
	debit = fields.Float('Ingreso Costo')

class reporte_kardex_sinfactura(models.Model):
	_name = 'reporte.kardex.sinfactura'

	period_id = fields.Many2one('account.period','Periodo',required=True)

	@api.multi
	def ver_informe(self):
		param = self.env['main.parameter'].search([])[0]
		new_param = self.env['main.parameter'].search([])[0]


		self.env.cr.execute("""
drop table if exists resumen_kardex_sinfactura;
create table resumen_kardex_sinfactura as
select row_number() OVER () AS id,
get_kardex_v.fecha_albaran,
get_kardex_v.fecha,
get_kardex_v.doc_type_ope,
get_kardex_v.serial,
get_kardex_v.nro,
sp.nro_guia,
get_kardex_v.doc_almac,
get_kardex_v.doc_partner,
get_kardex_v.name,
get_kardex_v.operation_type,
get_kardex_v.default_code,
get_kardex_v.name_template,
get_kardex_v.unidad,
get_kardex_v.ingreso,
get_kardex_v.debit

from get_kardex_v(""" +str(param.fiscalyear)+ """0101,""" +str(self.fecha.date_stop).replace('-','')+ """, (select array_agg(id) from product_product), (select array_agg(id) from stock_location ),-1 ) 
left join stock_move sm on sm.id = get_kardex_v.stock_moveid
left join stock_picking sp on sp.id = sm.picking_id
left join (select distinct tipodocumento, serie, numero, razonsocial, ruc from get_compra_1_1_1(""" +self.period_id.code.split('/')[1]+self.period_id.code.split('/')[0]+ """,""" +self.period_id.code.split('/')[1]+self.period_id.code.split('/')[0]+ """)) as O on O.tipodocumento = doc_type_ope
and O.serie = serial and O.numero = nro and O.ruc = doc_partner 
where O.tipodocumento is null and get_kardex_v.serial is null and origen= 'Ubicaciones de empresas/Clientes'
and get_kardex_v.fecha between '""" +str(self.fecha.date_start)+ """' and '""" +str(self.fecha.date_stop)+ """'

			
			""")

		return {
			'type': 'ir.actions.act_window',
			'res_model': 'resumen.kardex.sinfactura',
			'view_mode': 'tree',
			'view_type': 'form',
			'views': [(False, 'tree')],
		}





class resumen_kardex_compras_kardex(models.Model):
	_name = 'resumen.kardex.compras.kardex'

	periodo = fields.Char('Periodo')
	libro = fields.Char('Libro')
	tipodocumento = fields.Char('Tipo Documento')
	serie = fields.Char('Serie')
	numero = fields.Char('Numero')
	code = fields.Char('Cuenta')
	debit = fields.Float('Debito')

class reporte_kardex_compras_kardex(models.Model):
	_name = 'reporte.kardex.compras.kardex'

	period_id = fields.Many2one('account.period','Periodo',required=True)

	@api.multi
	def ver_informe(self):
		param = self.env['main.parameter'].search([])[0]
		new_param = self.env['main.parameter'].search([])[0]


		self.env.cr.execute("""
drop table if exists resumen_kardex_compras_kardex;
create table resumen_kardex_compras_kardex as
select row_number() OVER () AS id,
O.periodo,
O.libro,
O.tipodocumento,
O.serie,
O.numero,
aa.code,
aml.debit
from get_kardex_v(""" +str(param.fiscalyear)+ """0101,""" +str(self.fecha.date_stop).replace('-','')+ """, (select array_agg(id) from product_product), (select array_agg(id) from stock_location ),-1 ) 
right join (select * from get_compra_1_1_1(""" +self.period_id.code.split('/')[1]+self.period_id.code.split('/')[0]+ """,""" +self.period_id.code.split('/')[1]+self.period_id.code.split('/')[0]+ """)) as O on O.tipodocumento = doc_type_ope
and O.serie = serial and O.numero = nro and O.ruc = doc_partner 
inner join account_move_line aml on aml.move_id = O.am_id
inner join account_account aa on aa.id = aml.account_id and aa.code like '60%'
where get_kardex_v.fecha is null and am_id in (
select distinct am.id from account_move am
inner join account_move_line aml on aml.move_id = am.id
inner join account_account aa on aa.id = aml.account_id
where aa.code like '60%')			
			""")

		return {
			'type': 'ir.actions.act_window',
			'res_model': 'resumen.kardex.compras.kardex',
			'view_mode': 'tree',
			'view_type': 'form',
			'views': [(False, 'tree')],
		}
