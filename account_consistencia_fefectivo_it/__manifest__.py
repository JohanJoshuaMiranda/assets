# -*- encoding: utf-8 -*-
{
	'name': 'Reportes CONSISTENCIAS',
	'category': 'account',
	'author': 'ITGRUPO',
	'depends': ['account_state_financial_it'],
	'version': '1.0',
	'description':"""
		Generar Reportes para CONSISTENCIA FLUJO EFECTIVO
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		'security/ir.model.access.csv',
		'wizards/account_con_efective_rep.xml',
		'views/account_con_efective_book.xml'
	],
	'installable': True
}
