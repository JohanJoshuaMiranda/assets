# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import *

class AccountConEfectiveRep(models.TransientModel):
	_name = 'account.con.efective.rep'

	fiscal_year_id = fields.Many2one('account.fiscalyear',string=u'Ejercicio',required=True)
	period_ini = fields.Many2one('account.period',string='Periodo Saldo Inicial',required=True)
	period_from = fields.Many2one('account.period',string='Periodo Inicio',required=True)
	period_to = fields.Many2one('account.period',string='Periodo Fin',required=True)

	@api.multi
	def get_report(self):
		sql = """
			CREATE OR REPLACE view account_con_efective_book as ("""+self._get_sql()+""")"""

		self.env.cr.execute(sql)

		return {
			'name': 'Consistencia Flujo Efectivo',
			'type': 'ir.actions.act_window',
			'res_model': 'account.con.efective.book',
			'view_mode': 'tree',
			'view_type': 'form',
			'views': [(False, 'tree')],
		}

	def _get_sql(self):
		sql = """
				SELECT row_number() OVER () AS id, FE.* FROM
				(select '2' as orden,'SI' as cuenta,'SALDO INICIAL' AS concept, sum(debe-haber) as movimiento,null as fe_id from get_libro_diario(periodo_num('%s'),periodo_num('%s'))  where left(cuenta,2)='10'
				union all
				(
				select 
				'1' as orden,
				a.cuenta,
				case when char_length(trim(c.concept))<>0 then c.concept else
				( case when sum(a.haber-a.debe)> 0 then 'INGRESO ND' else 'EGRESO ND' end) end as concept,
				sum(a.haber-a.debe) as movimiento,
				c.id as fe_id
				from get_libro_diario(periodo_num('%s'),periodo_num('%s')) a
				left join account_account b on b.id=a.aa_id
				left join account_config_efective c on c.id=b.fefectivo_id
				where am_id in 
				(
				select am_id from get_libro_diario(periodo_num('%s'),periodo_num('%s')) where left(cuenta,2)='10'
				)
				and left(cuenta,2)<>'10'
				group by a.cuenta,c.concept,c.id 
				having sum(debe-haber)<>0
				order by a.cuenta
				)
				union all
				select '3' as orden,'SF' as cuenta,'SALDO FINAL' AS concept, sum(debe-haber)  as movimiento, null as fe_id from get_libro_diario(periodo_num('%s'),periodo_num('%s'))  where left(cuenta,2)='10')FE

			""" % (self.period_ini.code,self.period_ini.code,
			self.period_from.code,self.period_to.code,
			self.period_from.code,self.period_to.code,
			self.period_ini.code,self.period_to.code)

		return sql