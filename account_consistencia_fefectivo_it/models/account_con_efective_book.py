# -*- coding: utf-8 -*-

from odoo import models, fields, api

class AccountConEfectiveBook(models.Model):
	_name = 'account.con.efective.book'
	_auto = False
	
	cuenta = fields.Char(string='Cuenta')
	concept = fields.Char(string='Concepto')
	movimiento = fields.Float(string='Movimiento', digits=(64,2))
	fe_id = fields.Many2one('account.config.efective',string='Flujo Efectivo')