# -*- encoding: utf-8 -*-
{
	'name': 'IMPORTAR LINEAS -PRESUPUESTO',
	'category': 'account',
	'author': 'ITGRUPO',
	'depends': ['budget_it'],
	'version': '1.0',
	'description':"""
	- IMPORTAR LINEAS -PRESUPUESTO
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
			'data/attachment_sample.xml',
			'wizard/import_budget_line_wizard.xml'
			],
	'installable': True
}
