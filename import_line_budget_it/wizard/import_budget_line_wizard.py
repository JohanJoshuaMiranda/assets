# -*- coding: utf-8 -*-

from datetime import *
from dateutil.relativedelta import relativedelta

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import requests
import tempfile
import binascii
import xlrd
from odoo.exceptions import Warning, UserError

import requests
import json

class ImportBudgetLineWizard(models.TransientModel):
	_name = "import.budge.line.wizard"
	
	document_file = fields.Binary(string='Excel')
	name_file = fields.Char(string='Nombre de Archivo')

	def importar(self):
		if not self.document_file:
			raise UserError('Tiene que cargar un archivo.')
		
		try:
			fp = tempfile.NamedTemporaryFile(delete= False,suffix=".xlsx")
			fp.write(binascii.a2b_base64(self.document_file))
			fp.seek(0)
			workbook = xlrd.open_workbook(fp.name)
			sheet = workbook.sheet_by_index(0)
		except:
			raise Warning(_("Archivo invalido!"))

		for row_no in range(sheet.nrows):
			if row_no <= 0:
				continue
			else:
				line = list(map(lambda row:isinstance(row.value, bytes) and row.value.encode('utf-8') or str(row.value), sheet.row(row_no)))
				if len(line) == 8:
					date_string = False
					if line[2] != '':
						a1 = int(float(line[2]))
						a1_as_datetime = datetime(*xlrd.xldate_as_tuple(a1, workbook.datemode))
						date_string = a1_as_datetime.date().strftime('%Y-%m-%d')
					values = ({'date':date_string,
								'libro': line[0],
								'voucher': line[1],
								'presupuesto': line[3],
								'debe': line[4],
								'haber': line[5],
								'debeme': line[6],
								'haberme': line[7]
								})
					self.make_line_budget(values)
				elif len(line) > 8:
					raise Warning(_('Tu archivo tiene columnas mas columnas de lo esperado.'))
				else:
					raise Warning(_('Tu archivo tiene columnas menos columnas de lo esperado.'))

		return True

	def make_line_budget(self, values):
		obj = self.env['account.ap.budget.it']
		move_obj = self.env['account.move']
		if not values.get('date'):
			raise Warning(_('El campo "fecha" no puede estar vacio.'))
		if not values.get('libro'):
			raise Warning(_('El campo "libro" no puede estar vacio.'))
		if not values.get('voucher'):
			raise Warning(_('El campo "voucher" no puede estar vacio.'))
		if not values.get('presupuesto'):
			raise Warning(_('El campo " codigo de presupuesto" no puede estar vacio.'))

		s = str(values.get("libro"))
		code = s.rstrip('0').rstrip('.') if '.' in s else s
		journal_id = self.find_journal(code)

		s = str(values.get("presupuesto"))
		code = s.rstrip('0').rstrip('.') if '.' in s else s
		line_budget_id = self.find_line_budget(code)

		move_search = move_obj.search([
					('name', '=', values.get('voucher')),
					('journal_id','=',journal_id.id),
					('date','=',values.get('date'))
				],limit=1)

		obj.create({
			'line_budget_id': line_budget_id.id,
			'debit': values.get('debe'),
			'credit': values.get('haber'),
			'debit_me': values.get('debeme'),
			'credit_me': values.get('haberme'),
			'move_id': move_search.id
		})

	def find_line_budget(self,code):
		search = self.env['account.budget.it.line'].search([('code','=',code)],limit=1)
		if search:
			return search
		else:
			raise Warning(_('No existe el Detalle de presupuesto "%s"') % code)

	def find_journal(self,code):
		journal_search = self.env['account.journal'].search([('code','=',code)],limit=1)
		if journal_search:
			return journal_search
		else:
			raise Warning(_('No existe el diario "%s".') % code)

	def download_template(self):
		return {
			 'type' : 'ir.actions.act_url',
			 'url': '/web/binary/download_template_import_budget_lines',
			 'target': 'new',
			 }
