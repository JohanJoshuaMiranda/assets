
 # -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from ast import literal_eval
from odoo.tools.safe_eval import safe_eval
from odoo.exceptions import UserError, ValidationError

from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT

import pudb

counter_function_run = 0

class SaleOrder(models.Model):
    _inherit = "sale.order"
    require_approval = fields.Boolean('Requiere Aprobación', default=False)

    @api.depends('order_line.price_total')
    def _amount_all(self):
        """
        Compute the total amounts of the SO.
        """

        #pudb.set_trace()
        global counter_function_run
        counter_function_run = 0

        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                amount_tax += line.price_tax
            order.update({
                'amount_untaxed': order.pricelist_id.currency_id.round(amount_untaxed),
                'amount_tax': order.pricelist_id.currency_id.round(amount_tax),
                'amount_total': amount_untaxed + amount_tax,
    })


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    @api.model
    def create(self, vals):
        #vals.list_price = vals.list_price_aux
        #vals.list_discount = vals.list_discount_aux
        print "vals ", vals
        super(SaleOrderLine, self).create(vals)

    @api.one
    @api.depends('product_id')
    def get_formulav2(self):
        #if self.order_id.pricelist_id:
        #    for items in self.order_id.pricelist_id.item_ids:
        #        print items
        #        if items.compute_price == "price_formula_2":
        #            self.is_formulav2 = True
        #else:
        #    print "NADA"
        self.is_formulav2 = False

    @api.one
    @api.depends('product_id')
    def compute_clean_fields(self):

        #self.discount = 0.0
        #pudb.set_trace()
        self.discount1 = 0.0
        self.discount2 = 0.0
        self.discount3 = 0.0
        self.verificar_descuento1 = '1'
        self.status_discount1 = 'null'
        self.verificar_descuento2 = '1'
        self.status_discount2 = 'null'
        self.buscaRegla()

    @api.onchange('discount1')
    def onchange_discount_one(self):
        if self.discount1 > 0:
            self.price_unit_manual=True
            if not self.product_id:
                res = {}
                warning = {'title': "Advertencia!.", 'message': 'Seleccionar Producto!.'}
                return {'value': res, 'warning': warning}

            require_approval1 = literal_eval(self.env['ir.config_parameter'].get_param(
            'fxo_sale_three_discounts.validate_discounts1', 'False'))

            if require_approval1:

                if self.product_id.brand_id:
                    product_brand = self.product_id.brand_id.id
                else:
                    product_brand = 9999999999

                if self.product_id.product_type:
                    product_type = self.product_id.product_type
                else:
                    product_type = 9999999999

                customer_type = str(self.order_id.pricelist_id.name.encode('utf-8'))

                for item in self.order_id.pricelist_id.item_ids:
                    if item.applied_on == "product_brand":
                        if item.brand_id.id == product_brand:
                            max_discount = item.max_discount_one
                            if max_discount == 0 or self.discount1 > max_discount:
                                res = {'status_discount1': 'FAIL', 'verificar_descuento1': ''}
                                warning = {'title': "Advertencia!",
                                           'message': """El máximo DESCUENTO 1 para el tipo de cliente: """ + customer_type + """ y la marca de
                                    producto: """ + str(self.product_id.brand_id.name.encode('utf-8')) + """ es : """ + str(max_discount) + " %"
                                           }
                                return {'value': res, 'warning': warning}
                            else:
                                res = {'require_approval1': True, 'status_discount1': 'OK', 'verificar_descuento1': '1'}
                                return {'value': res}
                    elif item.applied_on == "product_type":
                        if item.product_type == product_type:
                            max_discount = item.max_discount_one
                            if max_discount == 0 or self.discount1 > max_discount:
                                b_table = self.env['base.table'].search([('code', '=', 'PE.PRODUCTTYPE')])
                                b_element = self.env['base.element'].search(
                                    [('table_id', '=', b_table.id), ('name', '=', product_type)])
                                name_s = self.env['base.element'].browse(b_element.id).element_char

                                res = {'status_discount1': 'FAIL', 'verificar_descuento1': ''}
                                warning = {'title': "Advertencia!",
                                           'message': """El máximo DESCUENTO 1 para el tipo de cliente: """ + customer_type + """ y el tipo de
                                    producto: """ + str(name_s).upper() + """ es : """ + str(max_discount) + " %"
                                           }
                                return {'value': res, 'warning': warning}
                            else:
                                res = {'require_approval1': True, 'status_discount1': 'OK', 'verificar_descuento1': '1'}
                                return {'value': res}

                for item in self.order_id.pricelist_id.item_ids:
                    if item.applied_on == "0_product_variant":
                        if item.product_id.id == self.product_id.id:
                            max_discount = item.max_discount_one
                            if max_discount == 0 or self.discount1 > max_discount:
                                res = {'status_discount1': 'FAIL', 'verificar_descuento1': ''}
                                warning = {'title': "Advertencia!",
                                           'message': """El máximo DESCUENTO 1 para el tipo de cliente: """ + customer_type + """ y el
                                    producto: """ + str(self.product_id.name).upper() + """ es : """ + str(
                                               max_discount) + " %"
                                           }
                                return {'value': res, 'warning': warning}
                            else:
                                res = {'require_approval1': True, 'status_discount1': 'OK', 'verificar_descuento1': '1'}
                                return {'value': res}
                    elif item.applied_on == "1_product":
                        if item.product_tmpl_id.id == self.product_id.product_tmpl_id.id:
                            max_discount = item.max_discount_one
                            if max_discount == 0 or self.discount1 > max_discount:
                                res = {'status_discount1': 'FAIL', 'verificar_descuento1': ''}
                                warning = {'title': "Advertencia!",
                                           'message': """El máximo DESCUENTO 1 para el tipo de cliente: """ + customer_type + """ y el
                                    producto: """ + str(
                                               self.product_id.product_tmpl_id.name).upper() + """ es : """ + str(
                                               max_discount) + " %"
                                           }
                                return {'value': res, 'warning': warning}
                            else:
                                res = {'require_approval1': True, 'status_discount1': 'OK', 'verificar_descuento1': '1'}
                                return {'value': res}
                    elif item.applied_on == "2_product_category":
                        if item.categ_id.id == self.product_id.categ_id.id:
                            max_discount = item.max_discount_one
                            if max_discount == 0 or self.discount1 > max_discount:
                                res = {'status_discount1': 'FAIL', 'verificar_descuento1': ''}
                                warning = {'title': "Advertencia!",
                                           'message': """El máximo DESCUENTO 1 para el tipo de cliente: """ + customer_type + """ y la
                                    categoria: """ + str(self.product_id.categ_id.name).upper() + """ es : """ + str(
                                               max_discount) + " %"
                                           }
                                return {'value': res, 'warning': warning}
                            else:
                                res = {'require_approval1': True, 'status_discount1': 'OK', 'verificar_descuento1': '1'}
                                return {'value': res}
                    elif item.applied_on == "3_global":
                        max_discount = item.max_discount_one
                        if max_discount == 0 or self.discount1 > max_discount:
                            res = {'status_discount1': 'FAIL', 'verificar_descuento1': ''}
                            warning = {'title': "Advertencia!",
                                       'message': """El máximo DESCUENTO 1 para el tipo de cliente: """ + customer_type + """ y el
                                    producto global es : """ + str(max_discount) + " %"
                                       }
                            return {'value': res, 'warning': warning}
                        else:
                            res = {'require_approval1': True, 'status_discount1': 'OK', 'verificar_descuento1': '1'}
                            return {'value': res}
        else:
            res = {'require_approval1': False, 'status_discount1': 'OK', 'verificar_descuento1': '1'}
            print('saliendo del discount 1 onchange ')
            return {'value': res}

    @api.onchange('discount2', 'product_uom_qty')
    def onchange_discount_two(self):
        #print "onchange_discount_two"

        if self.discount2 > 0:
            #pudb.set_trace()
            #self.price_unit_manual=True
            valor = 0
            minimum_volume = self.searchRuleAmount()

            if self.product_id.special_product:
                valor = valor + 1
            if minimum_volume:
                #minimum_volume = self.searchRuleAmount()
                if self.product_uom_qty >= minimum_volume:
                    valor = valor + 1

            if valor == 0:
                cantidad = ""
                if minimum_volume:
                    cantidad = str(minimum_volume) + " - " + str(self.product_id.uom_id.name)
                res = {}
                warning = {'title': "Advertencia!.", 'message': 'El Descuento Especial solo aplica para : Productos Especiales o por Volumen de ' + str(cantidad) + ' '}
                res = {'verificar_cantidad': ''}
                return {'value': res, 'warning': warning}

            if not self.product_id:
                res = {}
                warning = {'title': "Advertencia!.", 'message': 'Seleccionar Producto!.'}
                return {'value': res, 'warning': warning}
            require_approval = literal_eval(self.env['ir.config_parameter'].get_param('fxo_sale_three_discounts.validate_discounts', 'False'))
            if require_approval:
                customer_type = ""

                if self.product_id.brand_id:
                    product_brand = self.product_id.brand_id.id
                else:
                    product_brand = 9999999999

                if self.product_id.product_type:
                    product_type = self.product_id.product_type
                else:
                    product_type = 9999999999

                for item in self.order_id.pricelist_id.item_ids:
                    if item.applied_on == "product_brand":
                        print("item brand id = "+str(item.brand_id.id) +", product_brand = " + str(product_brand))
                        if item.brand_id.id == product_brand: #and (self.order_id.payment_term_id.payment_type == item.payment_type):
                            max_discount = item.max_discount_two

                            if max_discount == 0 or self.discount2 > max_discount:
                                res = {'status_discount2': 'FAIL', 'verificar_descuento2': ''}
                                warning = {'title': "Advertencia!",
                                           'message': u"""El máximo DESCUENTO 2 para el tipo de cliente: """ + str(self.order_id.pricelist_id.name.encode('utf-8')) + """ y la marca de
                                    producto: """ + str(self.product_id.brand_id.name).upper() + """ es : """ + str(
                                               max_discount) + " %"
                                           }
                                return {'value': res, 'warning': warning}
                            else:
                                res = {'require_approval': True, 'verificar_cantidad': '1', 'status_discount2': 'OK', 'verificar_descuento2': '1'}
                                return {'value': res}
                    elif item.applied_on == "product_type":
                        if item.product_type == product_type: # and (self.order_id.payment_term_id.payment_type == item.payment_type):
                            max_discount = item.max_discount_two
                            if max_discount == 0 or self.discount2 > max_discount:
                                b_table = self.env['base.table'].search([('code', '=', 'PE.PRODUCTTYPE')])
                                b_element = self.env['base.element'].search(
                                    [('table_id', '=', b_table.id), ('name', '=', product_type)])
                                name_s = self.env['base.element'].browse(b_element.id).element_char

                                res = {'status_discount2': 'FAIL', 'verificar_descuento2': ''}
                                warning = {'title': "Advertencia!",
                                           'message': u"""El máximo DESCUENTO 2 para el tipo de cliente: """ + customer_type + """ y el tipo de
                                    producto: """ + str(name_s).upper() + """ es : """ + str(max_discount) + " %"
                                           }
                                return {'value': res, 'warning': warning}
                            else:
                                res = {'require_approval': True, 'verificar_cantidad': '1', 'status_discount2': 'OK', 'verificar_descuento2': '1'}
                                return {'value': res}

                for item in self.order_id.pricelist_id.item_ids:
                    if item.applied_on == "0_product_variant":
                        if item.product_id.id == self.product_id.id : #and (self.order_id.payment_term_id.payment_type == item.payment_type):
                            max_discount = item.max_discount_two
                            if max_discount == 0 or self.discount2 > max_discount:
                                res = {'status_discount2': 'FAIL', 'verificar_descuento2': ''}
                                warning = {'title': "Advertencia!",
                                           'message': u"""El máximo DESCUENTO 2 para el tipo de cliente: """ + customer_type + """ y el
                                    producto: """ + str(self.product_id.name).upper() + """ es : """ + str(
                                               max_discount) + " %"
                                           }
                                return {'value': res, 'warning': warning}
                            else:
                                res = {'require_approval': True, 'verificar_cantidad': '1', 'status_discount2': 'OK', 'verificar_descuento2': '1'}
                                return {'value': res}
                    elif item.applied_on == "1_product":
                        if item.product_tmpl_id.id == self.product_id.product_tmpl_id.id: #and (self.order_id.payment_term_id.payment_type == item.payment_type):
                            max_discount = item.max_discount_two
                            if max_discount == 0 or self.discount2 > max_discount:
                                res = {'status_discount2': 'FAIL', 'verificar_descuento2': ''}
                                warning = {'title': "Advertencia!",
                                           'message': u"""El máximo DESCUENTO 2 para el tipo de cliente: """ + customer_type + """ y el
                                    producto: """ + str(
                                               self.product_id.product_tmpl_id.name).upper() + """ es : """ + str(
                                               max_discount) + " %"
                                           }
                                return {'value': res, 'warning': warning}
                            else:
                                res = {'require_approval': True, 'verificar_cantidad': '1', 'status_discount2': 'OK', 'verificar_descuento2': '1'}
                                return {'value': res}
                    elif item.applied_on == "2_product_category":
                        if item.categ_id.id == self.product_id.categ_id.id : #and (self.order_id.payment_term_id.payment_type == item.payment_type):
                            max_discount = item.max_discount_two
                            if max_discount == 0 or self.discount2 > max_discount:
                                res = {'status_discount2': 'FAIL', 'verificar_descuento2': ''}
                                warning = {'title': "Advertencia!",
                                           'message': u"""El máximo DESCUENTO 2 para el tipo de cliente: """ + customer_type + """ y la
                                    categoria: """ + str(self.product_id.categ_id.name).upper() + """ es : """ + str(
                                               max_discount) + " %"
                                           }
                                return {'value': res, 'warning': warning}
                            else:
                                res = {'require_approval': True, 'verificar_cantidad': '1', 'status_discount2': 'OK', 'verificar_descuento2': '1'}
                                return {'value': res}
                    elif item.applied_on == "3_global" : #and (self.order_id.payment_term_id.payment_type == item.payment_type):
                        max_discount = item.max_discount_two
                        if max_discount == 0 or self.discount2 > max_discount:
                            res = {'status_discount2': 'FAIL', 'verificar_descuento2': ''}
                            warning = {'title': "Advertencia!",
                                       'message': u"""El máximo DESCUENTO 2 para el tipo de cliente: """ + customer_type + """ y el
                                    producto global es : """ + str(max_discount) + " %"
                                       }
                            return {'value': res, 'warning': warning}
                        else:
                            res = {'require_approval': True, 'verificar_cantidad': '1', 'status_discount2': 'OK', 'verificar_descuento2': '1'}
                            return {'value': res}
        else:
            res = {'require_approval': False, 'verificar_cantidad': '1', 'status_discount2': 'OK', 'verificar_descuento2': '1'}
            return {'value': res}

    #clean_fields = fields.Char(string="Clean", compute='compute_clean_fields')
    aux_discount2 = fields.Char(string="Aux discount2")
    discount1 = fields.Float(string='Discount 1 (%)')
    discount2 = fields.Float(string='Discount 2 (%)')
    discount3 = fields.Float(string='Discount 3 (%)')
    discount = fields.Float(compute='get_discount', digits=(16, 2), store=True, states={}, default=0.00)
    #discount = fields.Float(compute='glue_function_discount', digits=(16, 2), store=True, states={}, default=0.00)
    is_formulav2 = fields.Boolean(compute='get_formulav2')

    require_approval = fields.Boolean('Requiere Aprobación', default=False)
    require_approval1 = fields.Boolean('Requiere Aprobación1', default=False)
    verificar_cantidad = fields.Char('Verificar Cantidad', default="1")
    verificar_descuento1 = fields.Char('Verificar Descuento 1', default="1")
    status_discount1 = fields.Selection([
        ('null', ' '),
        ('OK', 'OK'),
        ('FAIL', 'Excedió Máximo Permitido !.'),
    ], string='status_discount1', readonly=True, copy=False, index=True, )
    verificar_descuento2 = fields.Char('Verificar Descuento 2', default="1")
    status_discount2 = fields.Selection([
        ('null', ' '),
        ('OK', 'OK'),
        ('FAIL', 'Excedió Máximo Permitido !.'),
    ], string='status_discount2', readonly=True, copy=False, index=True, )
    verificar_descuento = fields.Char('Verificar Descuento', default="1")

    #Movido de pricelist_discount
    total_discount = fields.Float(string="Total Discount", compute='onchange_list_price')
    # total_discount = fields.Float(
    #     compute='get_discount',
    #     inverse='_set_discount',
    #     string='Total Discount')

    discount_parcial = fields.Float(
        compute='get_discount',
        inverse='_set_discount',
        string='Parcial Discount')

    price_unit_manual = fields.Boolean('Price unit manual', default=False, store=True)
    price_unit_temp = fields.Float(string='Price unit Temp')
    discount_manual = fields.Float(string='Descuento manual')

    @api.model
    def write(self, vals):

        """ Agregar funcionalidades extra a este metodo en el caso que haya
        problemas al editar algunos campos en las lineas de la orden"""
        #pudb.set_trace()
        res = super(SaleOrderLine, self).write(vals)
        return res



    @api.multi
    @api.onchange('list_discount')
    def change_list_discount(self):

        """ Para probar que esta pasando con list_discount"""

        self.onchange_list_price()
        return

    @api.multi
    @api.onchange('list_price','discount1','discount2')
    def onchange_list_price(self):

        for record in self:

            """
                Function to probe if it is correct to change list_price
            """

            d1=record.discount1
            d2=record.discount2
            listDiscount = record.list_discount
            listPrice = record.list_price


            price_unit = listPrice*(1-listDiscount/100)*(1-d1/100)*(1-d2/100)
            total_discount = (1-(1-listDiscount/100)*(1-d1/100)*(1-d2/100))*100


            # if d1 == 0 and d2 ==0:
            #
            #     price_unit = listPrice*(1-listDiscount/100)
            #     total_discount = listDiscount
            #
            # elif d1 != 0 and d2==0:
            #
            #     price_unit = listPrice*(1-listDiscount/100)*(1-d1/100)
            #     total_discount = (1-(1-listDiscount/100)*(1-d1/100))*100
            #
            # elif d1 == 0 and d2!=0:
            #
            #     price_unit = listPrice*(1-listDiscount/100)*(1-d2/100)
            #     total_discount = (1-(1-listDiscount/100)*(1-d2/100))*100
            #
            # elif d1!= 0 and d2!=0:
            #
            #     price_unit = 100*(1-listDiscount/100)*(1-d1/100)*(1-d2/100)
            #     total_discount = (1-(1-listDiscount/100)*(1-d1/100)*(1-d2/100))*100

            #total_discount = (1 -(1-self.list_discount/100)*(1 - d1 / 100) * (1 - d2 / 100) * (1 - d3 / 100)) * 100
            record.price_unit=price_unit
            record.total_discount = total_discount


            print(str(record.discount1))
            record.update({'total_discount':total_discount})
            record.update({'price_unit':record.price_unit})
            record.update({'discount1':record.discount1})
            record.update({'discount2':record.discount2})

        #self.update({'price_unit':price_unit})

        return
    ### updating tCuando es creado o actualizado


    @api.multi
    def get_discount(self):

        pass

    @api.one
    def _set_discount(self):
        print "_set_discount"

    #Cuando es creado o actualizado
    @api.one
    def _set_discount(self):
        print "_set_discount"
    #---------------------------- Fin movido

    def CalculaDescuentoV2(self, items, price):

        #pudb.set_trace()

        if not self.order_id.payment_term_id:
            raise UserError(_("No ha seleccionado Terminos de Pago !."))
        if not self.order_id.payment_term_id.payment_type:
            raise UserError(_("El Termino de Pago no tiene un tipo de pago definido !."))

        min = items.compute_price_min_margin or 0.0
        max = items.compute_price_max_margin or 0.0

        price_min = price*(1+min/100)
        price_max = price/(1-max/100)

        if self.order_id.payment_term_id.payment_type == "counted":

            #price = price*(1+self.discount_parcial/100)

            costo = self.product_id.standard_price
            nivel = items.compute_security_level or 0

            costo_min = costo/(1-nivel/100)
            if price < costo_min:
                raise UserError(_("El precio del producto no puede ser menor a: " + format(costo_min, '.2f') + " Calculado: " + format(price, '.2f') ))


            if price < price_min:
                raise UserError(_("Descuento no Permitido, el precio debe mayor a : " + format(price_min, '.2f') + " Calculado: " + format(price, '.2f') + " para el tipo de pago: CONTADO "))
            else:
                self.price_unit = price

        elif self.order_id.payment_term_id.payment_type == "credit":

            price = price / (1-self.total_discount/100)

            costo = self.product_id.standard_price
            nivel = items.compute_security_level or 0

            costo_min = costo/(1-nivel/100)
            if price < costo_min:
                raise UserError(_("El precio del producto no puede ser menor a: " + format(costo_min, '.2f') + " Calculado: " + format(price, '.2f') ))


            if price < price_min:
                raise UserError(_("Descuento no Permitido, el precio debe mayor a : " + format(price_min, '.2f') + " Calculado: " + format(price, '.2f') + " para el tipo de pago: CONTADO "))
            else:
                self.price_unit = price

    def CalculaDescuentoV3(self, items, price):



        min = items.compute_price_min_margin or 0.0
        max = items.compute_price_max_margin or 0.0

        price_min = price/(1-min/100)
        price_max = price/(1-max/100)

        price = price/(1-self.total_discount/100)

        costo = self.product_id.standard_price
        nivel = items.compute_security_level or 0

        costo_min = costo/(1-nivel/100)
        if price < costo_min:
            raise UserError(_("El precio del producto no puede ser menor a: " + format(costo_min, '.2f') + " Calculado: " + format(price, '.2f') ))

        if price < price_min:
            raise UserError(_("Descuento no Permitido, el precio debe mayor a : " + format(price_min, '.2f') + " Calculado: " + format(price, '.2f') + " para el tipo de pago: CONTADO "))
        else:
            self.price_unit = price

    def CalculaDescuentoV4(self, items, price):



        if not self.order_id.payment_term_id:
            raise UserError(_("No ha seleccionado Terminos de Pago !."))
        if not self.order_id.payment_term_id.payment_type:
            raise UserError(_("El Termino de Pago no tiene un tipo de pago definido !."))

        min = items.compute_price_min_margin or 0.0
        max = items.compute_price_max_margin or 0.0

        price_min = price*(1+min/100)
        price_max = price/(1+max/100)

        if self.order_id.payment_term_id.payment_type == "counted":

            price = price*(1+self.total_discount/100)

            costo = self.product_id.standard_price
            nivel = items.compute_security_level or 0

            costo_min = costo/(1-nivel/100)
            if price < costo_min:
                raise UserError(_("El precio del producto no puede ser menor a: " + format(costo_min, '.2f') + " Calculado: " + format(price, '.2f') ))



            if price < price_min:
                raise UserError(_("Descuento no Permitido, el precio debe mayor a : " + format(price_min, '.2f') + " Calculado: " + format(price, '.2f') + " para el tipo de pago: CONTADO "))
            else:
                self.price_unit = price

        elif self.order_id.payment_term_id.payment_type == "credit":

            price = price / (1+self.total_discount/100)

            costo = self.product_id.standard_price
            nivel = items.compute_security_level or 0

            costo_min = costo/(1-nivel/100)
            if price < costo_min:
                raise UserError(_("El precio del producto no puede ser menor a: " + format(costo_min, '.2f') + " Calculado: " + format(price, '.2f') ))

            if price < price_min:
                raise UserError(_("Descuento no Permitido, el precio debe mayor a : " + format(price_min, '.2f') + " Calculado: " + format(price, '.2f') + " para el tipo de pago: CONTADO "))
            else:
                self.price_unit = price

    def CalculaDescuentoV10(self, items, price_obj):

        #pudb.set_trace()

        #print "INICIANDO CALCULO"
        #print items
        if not self.order_id.payment_term_id:
            #raise UserError(_("No ha seleccionado Terminos de Pago !."))
            return {'value': {'verificar_descuento':False}, 'warning': {'title': 'Error', 'message': 'No ha seleccionado Terminos de Pago !.'}}
        else:
            self.verificar_descuento = True


        # TODO: BLOQUE TEST
        price_min = 0.0
        price = 0.0

        product = self.product_id.with_context(
            lang=self.order_id.partner_id.lang,
            partner=self.order_id.partner_id.id,
            quantity=self.product_uom_qty,
            date_order=self.order_id.date_order,
            pricelist=self.order_id.pricelist_id.id,
            uom=self.product_uom.id,
            fiscal_position=self.env.context.get('fiscal_position')
        )
        price = self.env['account.tax']._fix_tax_included_price(self._get_display_price_price_formula_best(items, product, self.order_id.pricelist_id.id), product.taxes_id, self.tax_id)

        #price = self.execRule(items, price_obj, self.total_discount, items.python_compute)

        price_dscto = 0.0
        rs = (self.total_discount*price/100)
        price_tmp = price - rs

        if items.price_round:
            from odoo import tools
            price_tmp = tools.float_round(price_tmp, precision_rounding=items.price_round)

        price = price_tmp
        #price = price_tmp

        #price_min = self.execRule(items, price, items.compute_price_min_margin, items.python_compute)
        costo_min = self.execRule(items, price_obj, items.compute_security_level, items.security_margin_python_compute)

        #print "================="
        #print price
        #print costo_min
        #print "================="


        """
        if self.order_id.payment_term_id.payment_type == "counted":
            price_min = self.execRule(items, price, items.compute_price_min_margin ,items.min_margin_counted_python_compute)
            price = self.execRule(items, price_obj, self.total_discount ,items.min_margin_counted_python_compute)
        elif self.order_id.payment_term_id.payment_type == "credit":
            price_min = self.execRule(items, price,items.compute_price_min_margin ,items.min_margin_credit_python_compute)
            price = self.execRule(items, price_obj, self.total_discount, items.min_margin_credit_python_compute)
        else:
            price_min = self.execRule(items, price, items.compute_price_min_margin ,items.min_margin_counted_python_compute)
            price = self.execRule(items, price_obj, self.total_discount ,items.min_margin_counted_python_compute)
        costo_min = self.execRule(items, price_obj,items.compute_security_level ,items.security_margin_python_compute)
        """

        if items.compute_security_level:
            if price < costo_min or costo_min == 0:
                if costo_min == 0:
                    self.verificar_descuento = False
                    return {'value': {'verificar_descuento':False},
                            'warning': {'title': 'Nivel de Seguridad',
                                        'message': "El producto no tiene Precio de Compra definido !."}}
                else:
                    self.write({'verificar_descuento': False})
                    self.verificar_descuento = False
                    return {'value': {'verificar_descuento':False},
                        'warning': {'title': 'Nivel de Seguridad',
                                'message': "El precio del producto no puede ser menor a: " + format(costo_min, '.2f') + " Calculado: " + format(price, '.2f')}}
            else:
                self.verificar_descuento = True
        else:
            self.verificar_descuento = True


        if items.price_discount < self.total_discount:
            self.verificar_descuento = False
            #return {'value': {'verificar_descuento':True, 'price_unit': price},
            return {'value': {'verificar_descuento':True, },
                    'warning':
                        {'title': 'Advertencia', 'message': "El Descuento permitido es : " + str(items.price_discount) + " % " }}
        else:
            #self.price_unit = price
            self.verificar_descuento = True

    def execRule(self, items, price,total_discount, python_compute):

        #rs = (total_discount*price/100)
        #price_tmp = price - rs

        #if items.price_round:
        #    from odoo import tools
        #    price_tmp = tools.float_round(price_tmp, precision_rounding=items.price_round)
        #    return price_tmp
        #else:
        #    return price_tmp

        baselocaldict = {'price': price, 'total_discount': total_discount, 'payslip': 1, 'worked_days': 1, 'inputs': 1}

        #employee = contract.employee_id
        order_line = self.env['sale.order.line'].browse([self.id])
        ##print order_line
        localdict = dict(baselocaldict, item=items, order_line=order_line)

        try:
            #print "EJECUTANDO CODIGO"
            #print price
            #print total_discount
            #print python_compute
            safe_eval(python_compute, localdict , mode='exec', nocopy=True)
            #print localdict['result']
            return localdict['result']
        except:
            raise UserError(_('Ocurrio un error al procesar la regla.'))
            # TODO: FIN BLOQUE TEST

    def buscaRegla(self):
        return
        #print "BUSCA REGLA"
        is_formula_v2 = False
        seguir_buscando = True
        for items in self.order_id.pricelist_id.item_ids:
            if self.product_id.brand_id:
                if items.brand_id:
                    if self.product_id.brand_id.id == items.brand_id.id :
                        if items.compute_price != "price_formula_2":
                            is_formula_v2 = False
                            seguir_buscando = False
                            #print "1"
                            break

        if seguir_buscando:
            for items in self.order_id.pricelist_id.item_ids:
                if self.product_id.product_type:
                    if items.product_type:
                        if self.product_id.product_type == items.product_type:
                            if items.compute_price != "price_formula_2":
                                is_formula_v2 = False
                                seguir_buscando = False
                                #print "2"
                                break


        if seguir_buscando:
            for items in self.order_id.pricelist_id.item_ids:
                if items.applied_on == "3_global":

                    if items.compute_price == "price_formula_2" :
                        is_formula_v2 = True
                        #print "3"
                        break


                if items.applied_on == "2_product_category":
                    if self.product_id.categ_id:
                        if self.product_id.categ_id.id == items.categ_id.id:

                            if items.compute_price == "price_formula_2" :
                                is_formula_v2 = True
                                #print "4"
                                break


                if items.applied_on == "1_product":
                    if self.product_id.product_tmpl_id.id == items.product_tmpl_id.id:

                        if items.compute_price == "price_formula_2":
                            #print "5"
                            is_formula_v2 = True
                            break


                if items.applied_on == "0_product_variant":
                    if self.product_id.id == items.product_id.id:
                        if items.compute_price == "price_formula_2" :
                            #print "6"
                            is_formula_v2 = True
                            break

        #print "APLICA==============="
        #print is_formula_v2
        self.is_formulav2 = is_formula_v2

    def searchRuleAmount(self):


        for items in self.order_id.pricelist_id.item_ids:
            if self.product_id.brand_id:
                if items.brand_id:
                    if self.product_id.brand_id.id == items.brand_id.id and (self.order_id.payment_term_id.payment_type == items.payment_type):
                        return items.min_quantity

        for items in self.order_id.pricelist_id.item_ids:
            if self.product_id.product_type:
                if items.product_type:
                    if self.product_id.product_type == items.product_type and (self.order_id.payment_term_id.payment_type == items.payment_type):
                        return items.min_quantity


        for items in self.order_id.pricelist_id.item_ids:
            if items.applied_on == "3_global" and (self.order_id.payment_term_id.payment_type == items.payment_type):
                return items.min_quantity

            if items.applied_on == "2_product_category":
                if self.product_id.categ_id:
                    if self.product_id.categ_id.id == items.categ_id.id and (self.order_id.payment_term_id.payment_type == items.payment_type):
                        return items.min_quantity

            if items.applied_on == "1_product":
                if self.product_id.product_tmpl_id.id == items.product_tmpl_id.id and (self.order_id.payment_term_id.payment_type == items.payment_type):
                    return items.min_quantity

            if items.applied_on == "0_product_variant":
                if self.product_id.id == items.product_id.id and (self.order_id.payment_term_id.payment_type == items.payment_type):
                    return items.min_quantity
        #print "NO HAY NADA"

    #Redefinimos la funcion de creacion de cotizacion agregando los campos del descuento
    @api.multi
    def invoice_line_create(self, invoice_id, qty):
        """ Create an invoice line. The quantity to invoice can be positive (invoice) or negative (refund).
            :param invoice_id: integer
            :param qty: float quantity to invoice
            :returns recordset of account.invoice.line created
        """
        invoice_lines = self.env['account.invoice.line']
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        for line in self:
            if not float_is_zero(qty, precision_digits=precision):
                vals = line._prepare_invoice_line(qty=qty)
                vals.update({'invoice_id': invoice_id, 'sale_line_ids': [(6, 0, [line.id])], 'discount3': line.list_discount, 'discount1': line.discount1, 'discount2': line.discount2, 'total_discount': line.total_discount})
                print "Descuento1: " + str(line.discount1) + " Descuento2: " + str(line.discount2)
                invoice_lines |= self.env['account.invoice.line'].create(vals)
        return invoice_lines
