#!/usr/bin/env python
# -*- coding: utf-8 -*-
from odoo import fields, models, api
from odoo.tools.safe_eval import safe_eval


class sale_config_setting(models.TransientModel):
    _inherit = 'sale.config.settings'

    validate_discounts = fields.Boolean(
        string='Requerir Aprobación para dar descuentos extras (Discount1)', 
        help='Si se marca esta opción los usuarios deberan requerir permisos para validar descuentos extras',
        )

    def get_default_validate_discounts(self,fields):
        icp = self.env['ir.config_parameter']
        return {
            'validate_discounts': safe_eval(icp.get_param('fxo_sale_three_discounts.validate_discounts', 'False')),
        }

    @api.multi
    def set_currency_validate_discounts(self):
        #config = self.browse()
        icp = self.env['ir.config_parameter']
        icp.set_param('fxo_sale_three_discounts.validate_discounts', repr(self.validate_discounts))


    #Validacion para el discount2
    validate_discounts1 = fields.Boolean(
        string='Requerir Aprobación para dar descuentos extras (Discount1)', 
        help='Si se marca esta opción los usuarios deberan requerir permisos para validar descuentos extras',
        )

    def get_default_validate_discounts1(self,fields):
        icp = self.env['ir.config_parameter']
        return {
            'validate_discounts1': safe_eval(icp.get_param('fxo_sale_three_discounts.validate_discounts1', 'False')),
        }

    @api.multi
    def set_currency_validate_discounts1(self):
        #config = self.browse()
        icp = self.env['ir.config_parameter']
        icp.set_param('fxo_sale_three_discounts.validate_discounts1', repr(self.validate_discounts1))

