# -*- coding: utf-8 -*-
from odoo import fields, models


class product_template(models.Model):

    _inherit = 'product.template'

    minimum_volume = fields.Float('Volumén Mínimo')
    special_product = fields.Boolean('Producto Especial', default=False)


class product_product(models.Model):

    _inherit = 'product.product'

    special_product = fields.Boolean('Producto Especial', default=False, related='product_tmpl_id.special_product')
    minimum_volume = fields.Float('Volumén Mínimo', related='product_tmpl_id.minimum_volume') 