# -*- coding: utf-8 -*-
{
    'name': 'Sale Three Discounts',
    'version': '10.0.1.1.0',
    'category': 'Sales Management',
    'sequence': 14,
    'author': 'Flexxoone SAC',
    'website': 'www.flexxoone.com',
    'license': 'AGPL-3',
    'summary': '',
    'depends': [
        'sale',
        'account',
        'nanotec_pricelist_extend'
    ],
    'external_dependencies': {
    },
    'data': [
        'security/approv_security.xml',
        'views/sale_order_view.xml',
        'views/account_invoice_view.xml',
        'views/sale_config_view.xml',
        'views/product_view.xml',
    ],
    'demo': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
