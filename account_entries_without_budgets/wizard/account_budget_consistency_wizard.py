# -*- coding: utf-8 -*-
import base64
from openerp import models, fields, api
from odoo.exceptions import UserError
from datetime import *

class AccountBudgetConsistencyWizard(models.TransientModel):
	_name = 'account.budget.consistency.wizard'

	period_start = fields.Many2one('account.period',string='Periodo Inicial')
	period_end = fields.Many2one('account.period',string='Periodo Final')

	@api.multi
	def do_rebuild(self):
		

		import io
		from xlsxwriter.workbook import Workbook
		from xlsxwriter.utility import xl_rowcol_to_cell

		direccion = self.env['main.parameter'].search([])[0].dir_create_file
		workbook = Workbook( direccion + 'budget_cons.xlsx')
		worksheet = workbook.add_worksheet("Consistencias Presupuestos")
		#bold = workbook.add_format({'bold': True})

		bold_title = workbook.add_format({'bold': True,'align': 'center','valign': 'vcenter'})
		bold_title.set_font_size(14)
		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=2)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(9)
		boldbord.set_bg_color('#DCE6F1')
		numberdos = workbook.add_format({'num_format':'0.00'})
		bord = workbook.add_format()
		bord.set_border(style=1)
		numberdos.set_border(style=1)	
		numbertres = workbook.add_format({'num_format':'0.000'})
		numbertres.set_border(style=1)	
		dateformat = workbook.add_format({'num_format':'d-m-yyyy'})
		dateformat.set_border(style=1)
		dateformat.set_font_size(10)
		dateformat.set_font_name('Times New Roman')	
		x= 1

		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		worksheet.write(0,0, u"Código",boldbord)
		worksheet.write(0,1, "Presupuesto",boldbord)
		worksheet.write(0,2, u"Periodo",boldbord)
		worksheet.write(0,3, u"Libro",boldbord)
		
		worksheet.write(0,4, u"Voucher",boldbord)
		worksheet.write(0,5, u"Cuenta",boldbord)
		worksheet.write(0,6, u"Debe",boldbord)
		worksheet.write(0,7, u"Haber",boldbord)

		worksheet.write(0,8, u"Divisa",boldbord)
		worksheet.write(0,9, u"TC",boldbord)
		worksheet.write(0,10, u"Importe Divisa",boldbord)

		worksheet.write(0,11, u"RUC",boldbord)
		worksheet.write(0,12, u"Partner",boldbord)
		worksheet.write(0,13, u"TD",boldbord)
		worksheet.write(0,14, u"Numero",boldbord)
		worksheet.write(0,15, u"Fecha Emision",boldbord)
		worksheet.write(0,16, u"Fecha Vencimiento",boldbord)
		worksheet.write(0,17, u"Glosa",boldbord)
		worksheet.write(0,18, u"Cta Analitica",boldbord)
		worksheet.write(0,19, u"Estado",boldbord)

		self.env.cr.execute(self._get_sql(self.period_start.code,self.period_end.code))

		for line in self.env.cr.fetchall():
			worksheet.write(x,0,line[0] if line[0]  else '',bord)
			worksheet.write(x,1,line[1] if line[1]  else '',bord)
			worksheet.write(x,2,line[2] if line[2]  else '',bord)
			worksheet.write(x,3,line[3] if line[3]  else '',bord)
			worksheet.write(x,4,line[4] if line[4]  else '',bord)
			worksheet.write(x,5,line[5] if line[5]  else '',bord)
			worksheet.write(x,6,line[6] if line[6]  else 0,numberdos)
			worksheet.write(x,7,line[7] if line[7]  else 0,numberdos)
			worksheet.write(x,8,line[8] if line[8]  else '',bord)
			worksheet.write(x,9,line[9] if line[9]  else 0,numbertres)
			worksheet.write(x,10,line[10] if line[10]  else 0,numberdos)
			worksheet.write(x,11,line[11] if line[11]  else '',bord)
			worksheet.write(x,12,line[12] if line[12]  else '',bord)
			worksheet.write(x,13,line[13] if line[13]  else '',bord)
			worksheet.write(x,14,line[14] if line[14]  else '',bord)
			worksheet.write(x,15,line[15] if line[15]  else '',dateformat)
			worksheet.write(x,16,line[16] if line[16]  else '',dateformat)
			worksheet.write(x,17,line[17] if line[17]  else '',bord)
			worksheet.write(x,18,line[18] if line[18]  else '',bord)
			worksheet.write(x,19,line[19] if line[19]  else '',bord)
			x += 1

		tam_col = [10,16,10,8,11,10,11,11,8,7,19,15,50,8,20,15,15,30,25,10]

		worksheet.set_column('A:A', tam_col[0])
		worksheet.set_column('B:B', tam_col[1])
		worksheet.set_column('C:C', tam_col[2])
		worksheet.set_column('D:D', tam_col[3])
		worksheet.set_column('E:E', tam_col[4])
		worksheet.set_column('F:F', tam_col[5])
		worksheet.set_column('G:G', tam_col[6])
		worksheet.set_column('H:H', tam_col[7])
		worksheet.set_column('I:I', tam_col[8])
		worksheet.set_column('J:J', tam_col[9])
		worksheet.set_column('K:K', tam_col[10])
		worksheet.set_column('L:L', tam_col[11])
		worksheet.set_column('M:M', tam_col[12])
		worksheet.set_column('N:N', tam_col[13])
		worksheet.set_column('O:O', tam_col[14])
		worksheet.set_column('P:P', tam_col[15])
		worksheet.set_column('Q:Q', tam_col[16])
		worksheet.set_column('R:R', tam_col[17])
		worksheet.set_column('S:S', tam_col[18])
		worksheet.set_column('T:T', tam_col[19])

		workbook.close()
		
		f = open(direccion + 'budget_cons.xlsx', 'rb')
		
		
		vals = {
			'output_name': 'Consistencias Presupuestos.xlsx',
			'output_file': base64.encodestring(''.join(f.readlines())),		
		}

		sfs_id = self.env['export.file.save'].create(vals)
		return {
			"type": "ir.actions.act_window",
			"res_model": "export.file.save",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}

	def _get_sql(self,periodo_start,periodo_end):

		sql = """
		select 
		abl.code as codigo_p,
		c.name as presupuesto,
		a.periodo,
		a.libro,
		a.voucher,
		a.cuenta,
		a.debe,
		a.haber,
		a.divisa,
		a.tipodecambio,
		a.importedivisa,
		a.codigo,
		a.partner,
		a.tipodocumento,
		a.numero,
		a.fechaemision,
		a.fechavencimiento,
		a.glosa,
		a.ctaanalitica,
		a.statefiltro as estado

		from get_libro_diario(periodo_num('%s'),periodo_num('%s')) a
		left join account_move b on b.id=a.am_id
		left join account_ap_budget_it apbi on apbi.move_id = b.id
		left join account_budget_it_line abl on abl.id = apbi.line_budget_id
		left join account_budget_it c on c.id=b.budget_id
		where left(a.cuenta,2) in ('62','63','64','65','66','67','68')

		""" % (periodo_start,periodo_end)

		return sql