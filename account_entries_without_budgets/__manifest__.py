# -*- encoding: utf-8 -*-
{
	'name': 'Consistencia Presupuestos FERCO',
	'category': 'account',
	'author': 'ITGRUPO-FERCO',
	'depends': ['budget_it'],
	'version': '1.0',
	'description':"""
	- Consistencia Presupuestos
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
			'wizard/account_budget_consistency_wizard.xml'
			],
	'installable': True
}
