# -*- encoding: utf-8 -*-
{
	'name': 'Cambios Reportes Contables',
	'category': 'account',
	'author': 'ITGRUPO',
	'depends': ['account','activo_fijo','account_asset','account_registro_venta_it','analisis_saldos_comprobantes_periodo_it_extended'],
	'version': '1.0',
	'description':"""
	Modificacion en activos fijos:
		_Boton Mostrar solo en formato 7.4
	Modificacion en saldo comprobante periodo wizard
		_Agregando campo contacto en lineas de reportes
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
			'account_asset.xml',
			'saldo_comprobante_periodo_wizard.xml'],
	'installable': True
}
