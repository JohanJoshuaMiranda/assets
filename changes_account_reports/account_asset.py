# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import calendar
from datetime import date, datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.tools import float_compare, float_is_zero
import base64

from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from reportlab.lib.colors import magenta, red , black , blue, gray, Color, HexColor
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import letter, A4
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Paragraph, Table
from reportlab.lib.units import  cm,mm
from reportlab.lib.utils import simpleSplit
import decimal

class AccountAsset(models.Model):
    _inherit = 'account.asset.asset'

    format_74 = fields.Boolean(string='Mostrar solo en formato 7.4',default=False)

class account_asset_formato_71(models.Model):
    _inherit='account.asset.formato.71'

    tipo = fields.Selection([('excel', 'Excel')],string='Tipo', required=True,default='excel')

    @api.multi
    def do_rebuild(self):
        fechaini_obj = self.date
        periodo = self.env['account.period'].search([('code','=',self.date.split('-')[1] +'/'+self.date.split('-')[0])]) 
        if self.date == periodo.date_stop:
            pass
        else:
            periodo_anterior = {
                '12':'11',
                '11':'10',
                '10':'09',
                '09':'08',
                '08':'07',
                '07':'06',
                '06':'05',
                '05':'04',
                '04':'03',
                '03':'02',
                '02':'01',
                '01':'00',
            }
            periodo = self.env['account.period'].search([('code','=', periodo_anterior[self.date.split('-')[1]] +'/'+self.date.split('-')[0])])
            fechaini_obj = periodo.date_stop


           
        move_obj=self.env['account.asset.asset']
        filtro = []
        filtro.append( ('date','<=',str(fechaini_obj) ) )
        filtro.append( ('parent_id','=',False) )
        filtro.append( ('format_74','=',False) )

        lstidsmove = move_obj.search( filtro )
        
        mod_obj = self.env['ir.model.data']
        act_obj = self.env['ir.actions.act_window']

        if self.tipo == 'excel':
            import io
            from xlsxwriter.workbook import Workbook
            output = io.BytesIO()
            ########### PRIMERA HOJA DE LA DATA EN TABLA
            #workbook = Workbook(output, {'in_memory': True})

            direccion = self.env['main.parameter'].search([])[0].dir_create_file

            workbook = Workbook(direccion +'tempo_activo_71.xlsx')
            worksheet = workbook.add_worksheet("Activo - Formato 74")
            bold = workbook.add_format({'bold': True})
            normal = workbook.add_format()
            boldbord = workbook.add_format({'bold': True})
            boldbord.set_border(style=2)
            boldbord.set_align('center')
            boldbord.set_align('vcenter')
            boldbord.set_text_wrap()
            boldbord.set_font_size(9)
            boldbord.set_bg_color('#DCE6F1')
            numbertres = workbook.add_format({'num_format':'0.000'})
            numberdos = workbook.add_format({'num_format':'0.00'})
            bord = workbook.add_format()
            bord.set_border(style=1)
            numberdos.set_border(style=1)
            numbertres.set_border(style=1)          
            x= 8                
            tam_col = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
            tam_letra = 1.2
            import sys
            reload(sys)
            sys.setdefaultencoding('iso-8859-1')

            worksheet.write(0,0, 'FORMATO 7.1: "REGISTRO DE ACTIVOS FIJOS - DETALLE DE LOS ACTIVOS FIJOS"', bold)



            worksheet.write(2,0, "Periodo:",bold)
            tam_col[0] = tam_letra* len("Periodo:") if tam_letra* len("Periodo:")> tam_col[0] else tam_col[0]

            worksheet.write(2,3, str(self.period_id.name), normal)
            tam_col[1] = tam_letra* len(str(self.period_id.name)) if tam_letra* len(str(self.period_id.name))> tam_col[1] else tam_col[1]
            

            company = self.env['res.company'].search([])[0]

            worksheet.write(3,0, "RUC:",bold)
            tam_col[0] = tam_letra* len("RUC:") if tam_letra* len("RUC:")> tam_col[0] else tam_col[0]

            worksheet.write(3,3, str(company.partner_id.nro_documento), normal)
            tam_col[1] = tam_letra* len(str(company.partner_id.nro_documento)) if tam_letra* len(str(company.partner_id.nro_documento))> tam_col[1] else tam_col[1]
            



            worksheet.write(4,0, u"Apellidos y nombres, denominación o Razón Social:",bold)
            tam_col[0] = tam_letra* len(u"Apellidos y nombres, denominación o Razón Social:") if tam_letra* len(u"Apellidos y nombres, denominación o Razón Social:")> tam_col[0] else tam_col[0]

            worksheet.write(4,3, str(company.partner_id.name), normal)
            tam_col[1] = tam_letra* len(str(company.partner_id.name)) if tam_letra* len(str(company.partner_id.name))> tam_col[1] else tam_col[1]
            

            worksheet.merge_range(6,0,7,0, u"Código Relacionado con el Activo Fijo",boldbord)
            tam_col[0] = tam_letra* len(u"Código Relacionado co") if tam_letra* len(u"Código Relacionado co")> tam_col[0] else tam_col[0]
            worksheet.merge_range(6,1,7,1, "Cuenta Contable del Activo Fijo",boldbord)
            tam_col[1] = tam_letra* len("Cuenta Contable de") if tam_letra* len("Cuenta Contable de")> tam_col[1] else tam_col[1]
            worksheet.write(7,2, u"Descripción",boldbord)
            tam_col[2] = tam_letra* len(u"Descripción") if tam_letra* len(u"Descripción")> tam_col[2] else tam_col[2]
            worksheet.write(7,3, "Marca del Activo Fijo",boldbord)
            tam_col[3] = tam_letra* len("Marca del Activo Fijo") if tam_letra* len("Marca del Activo Fijo")> tam_col[3] else tam_col[3]
            worksheet.write(7,4, u"Modelo del Activo Fijo",boldbord)
            tam_col[4] = tam_letra* len(u"Modelo del Activo Fijo") if tam_letra* len(u"Modelo del Activo Fijo")> tam_col[4] else tam_col[4]
            worksheet.write(7,5, u"Número de Serie y/o Placa del Activo Fijo",boldbord)
            tam_col[5] = tam_letra* len(u"Número de Serie y/o Placa del Activo Fijo") if tam_letra* len(u"Número de Serie y/o Placa del Activo Fijo")> tam_col[5] else tam_col[5]
            
            worksheet.merge_range(6,2,6,5, u"Detalle del Activo Fijo",boldbord)
            
            worksheet.merge_range(6,6,7,6, "Saldo Inicial",boldbord)
            tam_col[6] = tam_letra* len(u"Saldo Inicial") if tam_letra* len(u"Saldo Inicial")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,7,7,7, "Adquisiones Adiciones",boldbord)
            tam_col[7] = tam_letra* len(u"Adquisiones Adiciones") if tam_letra* len(u"Adquisiones Adiciones")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,8,7,8, "Mejoras",boldbord)
            tam_col[8] = tam_letra* len(u"Mejoras") if tam_letra* len(u"Mejoras")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,9,7,9, "Retiros y/o Bajas",boldbord)
            tam_col[9] = tam_letra* len(u"Retiros y/o Bajas") if tam_letra* len(u"Retiros y/o Bajas")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,10,7,10, "Otros Ajustes",boldbord)
            tam_col[10] = tam_letra* len(u"Otros Ajustes") if tam_letra* len(u"Otros Ajustes")> tam_col[0] else tam_col[0]

            
            worksheet.merge_range(6,11,7,11, u"Valor Histórico del Activo Fijo al 31.12",boldbord)
            tam_col[11] = tam_letra* len(u"Valor Histórico del Act") if tam_letra* len(u"Valor Histórico del Act")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,12,7,12, u"Ajuste por Inflación",boldbord)
            tam_col[12] = tam_letra* len(u"Ajuste por Inflación") if tam_letra* len(u"Ajuste por Inflación")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,13,7,13, "Valor Ajustado del Activo Fijo al 31.12",boldbord)
            tam_col[13] = tam_letra* len(u"Valor Ajustado del Act") if tam_letra* len(u"Valor Ajustado del Act")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,14,7,14, u"Fecha de Adquisición",boldbord)
            tam_col[14] = tam_letra* len(u"Fecha de Adquisición") if tam_letra* len(u"Fecha de Adquisición")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,15,7,15, "Fecha Inicio del Uso del Activo Fijo",boldbord)
            tam_col[15] = tam_letra* len(u"Fecha Inicio del Uso") if tam_letra* len(u"Fecha Inicio del Uso")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,16,6,17, u"Depreciación",boldbord)

            worksheet.write(7,16, u"Método Aplicado",boldbord)
            tam_col[16] = tam_letra* len(u"Método Aplicado") if tam_letra* len(u"Método Aplicado")> tam_col[0] else tam_col[0]

            worksheet.write(7,17, u"Nro de Documento de Autorización",boldbord)
            tam_col[17] = tam_letra* len(u"Nro de Documento de Autorización") if tam_letra* len(u"Nro de Documento de Autorización")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,18,7,18, u"Porcentaje de Depreciación",boldbord)
            tam_col[18] = tam_letra* len(u"Porcentaje de Depreciación") if tam_letra* len(u"Porcentaje de Depreciación")> tam_col[0] else tam_col[0]




            worksheet.merge_range(6,19,7,19, u"Depreciación acumulada al Cierre del Ejercicio Anterior",boldbord)
            tam_col[19] = tam_letra* len(u"Depreciación acumulada al Cierre") if tam_letra* len(u"Depreciación acumulada al Cierre")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,20,7,20, u"Depreciación del Ejercicio",boldbord)
            tam_col[20] = tam_letra* len(u"Depreciación del Ejercicio") if tam_letra* len(u"Depreciación del Ejercicio")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,21,7,21, u"Depreciación del Ejercicio Relacionada con los retiros y/o bajas",boldbord)
            tam_col[21] = tam_letra* len(u"lacionada con los retiros y/o bajas") if tam_letra* len(u"lacionada con los retiros y/o bajas")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,22,7,22, u"Depreciación relacionada con otros ajusted",boldbord)
            tam_col[22] = tam_letra* len(u"relacionada con otros ajusted") if tam_letra* len(u"relacionada con otros ajusted")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,23,7,23, u"Depreciación acumulada Histórico",boldbord)
            tam_col[23] = tam_letra* len(u" acumulada Histórico") if tam_letra* len(u" acumulada Histórico")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,24,7,24, u"Ajuste por inflación de la Depreciación",boldbord)
            tam_col[24] = tam_letra* len(u"inflación de la Depreciación") if tam_letra* len(u"inflación de la Depreciación")> tam_col[0] else tam_col[0]

            worksheet.merge_range(6,25,7,25, u"Depreciación acumulada Ajustada por Inflación",boldbord)
            tam_col[25] = tam_letra* len(u" Ajustada por Inflación") if tam_letra* len(u" Ajustada por Inflación")> tam_col[0] else tam_col[0]




            for line in lstidsmove:

                hijos = self.env['account.asset.asset'].search([('parent_id','=',line.id),('date','<=',str(fechaini_obj))])

                worksheet.write(x,0,line.codigo if line.codigo else '' ,bord )
                worksheet.write(x,1,line.category_id.account_asset_id.code if line.category_id.account_asset_id.code else '' ,bord )
                worksheet.write(x,2,line.name if line.name  else '',bord )
                worksheet.write(x,3,line.marca if line.marca  else '',bord )
                worksheet.write(x,4,line.modelo if line.modelo  else '',bord )
                worksheet.write(x,5,line.serie if line.serie  else '',bord )

                primer_acum_neg = (line.value if line.date < self.period_id.date_start else 0 )
                primer_acum_neg += (line.value if line.date >= self.period_id.date_start and line.date <= self.period_id.date_stop else 0) 
                primer_acum_neg += 0#Mejora (line.value if line.date >= self.period_id.date_start  and line.date <= self.period_id.date_stop and line.tipo == 'mejoras' else 0+total[2])
                primer_acum_neg += 0#Otros(line.value if line.date >= self.period_id.date_start  and line.date <= self.period_id.date_stop and line.tipo == 'otros' else 0+total[4])

                worksheet.write(x,6,line.value if line.date < self.period_id.date_start else 0 ,numberdos )
                worksheet.write(x,7,line.value if line.date >= self.period_id.date_start and line.date <= self.period_id.date_stop  else 0,numberdos )
                worksheet.write(x,8,0, numberdos) #line.value if line.date >= self.period_id.date_start  and line.date <= self.period_id.date_stop and line.tipo == 'mejoras' else 0,numberdos )
                worksheet.write(x,9, (-primer_acum_neg) if line.f_baja and line.f_baja >= fechaini_obj else 0,numberdos )
                worksheet.write(x,10,0,numberdos) #line.value if line.date >= self.period_id.date_start  and line.date <= self.period_id.date_stop and line.tipo == 'otros' else 0,numberdos )
                
                primer_acum = (line.value if line.date < self.period_id.date_start else 0 )
                primer_acum += (line.value if line.date >= self.period_id.date_start and line.date <= self.period_id.date_stop  else 0) 
                primer_acum += 0 #(line.value if line.date >= self.period_id.date_start  and line.date <= self.period_id.date_stop and line.tipo == 'mejoras' else 0)
                primer_acum += (-primer_acum_neg) if line.f_baja and line.f_baja >= fechaini_obj else 0
                primer_acum += 0 #(line.value if line.date >= self.period_id.date_start  and line.date <= self.period_id.date_stop and line.tipo == 'otros' else 0)


                #worksheet.write(x,11,line.value+total[5] + primer_acum, numberdos )
                worksheet.write(x,11, primer_acum, numberdos )
                worksheet.write(x,12,0,numberdos )
                worksheet.write(x,13,primer_acum,numberdos )
                worksheet.write(x,14,line.date if line.date else '',bord )
                worksheet.write(x,15,line.date_start if line.date_start else '',bord )
                worksheet.write(x,16,'Metodo Lineal',bord )
                worksheet.write(x,17,line.autorizacion_depreciacion if line.autorizacion_depreciacion else '',bord )
                worksheet.write(x,18,line.category_id.percent_depreciacion,numberdos )
                #worksheet.write(x,18,-line.depreciacion_retiro+total[6],numberdos )
                
                acum_anterior = 0
                for ii in line.depreciation_line_ids:
                    if ii.depreciation_date < self.period_id.date_start:
                        acum_anterior += ii.amount

                worksheet.write(x,19, acum_anterior, numberdos )

                acum_actual = 0
                for ii in line.depreciation_line_ids:
                    if ii.depreciation_date >= self.period_id.date_start and ii.depreciation_date <= fechaini_obj:
                        acum_actual += ii.amount

                worksheet.write(x,20,acum_actual ,numberdos)

                total_ultimo = acum_anterior 
                total_ultimo += acum_actual
                total_ultimo += 0 #acum_actual+total[10] if line.tipo=='otros' else 0+total[10]

                worksheet.write(x,21,( -(total_ultimo ) if line.f_baja and line.f_baja >= fechaini_obj else 0) ,numberdos)

                worksheet.write(x,22,0,numberdos) #acum_actual+total[10] if line.tipo=='otros' else 0+total[10] ,numberdos)
                worksheet.write(x,23,acum_anterior+acum_actual+( ( -total_ultimo ) if line.f_baja and line.f_baja >= fechaini_obj else 0  ) ,numberdos)
                worksheet.write(x,24,0,numberdos )
                worksheet.write(x,25,acum_anterior+acum_actual+( ( -total_ultimo ) if line.f_baja and line.f_baja >= fechaini_obj else 0  ) ,numberdos)

                x = x +1

            tam_col = [10.29,7.14,32.57,9.14,13.57,8.57,8.14,8.14,8.14,8.14,8.14,8.14,8.14,8.14,9.71,9.71,12.7,12.7,4,8.14,8.14,8.14,8.14,8.14,8.14,8.14,8.14,8.14,8.14]

            worksheet.set_row(7, 78.75)
            worksheet.set_column(0,0, tam_col[0])
            worksheet.set_column(1,1, tam_col[1])
            worksheet.set_column(2,2, tam_col[2])
            worksheet.set_column(3,3, tam_col[3])
            worksheet.set_column(4,4, tam_col[4])
            worksheet.set_column(5,5, tam_col[5])
            worksheet.set_column(6,6, tam_col[6])
            worksheet.set_column(7,7, tam_col[7])
            worksheet.set_column(8,8, tam_col[8])
            worksheet.set_column(9,9, tam_col[9])
            worksheet.set_column(10,10, tam_col[10])
            worksheet.set_column(11,11, tam_col[11])
            worksheet.set_column(12,12, tam_col[12])
            worksheet.set_column(13,13, tam_col[13])
            worksheet.set_column(14,14, tam_col[14])
            worksheet.set_column(15,15, tam_col[15])
            worksheet.set_column(16,16, tam_col[16])
            worksheet.set_column(17,17, tam_col[17])
            worksheet.set_column(18,18, tam_col[18])
            worksheet.set_column(19,19, tam_col[19])
            worksheet.set_column(20,20, tam_col[20])
            worksheet.set_column(21,21, tam_col[21])
            worksheet.set_column(22,22, tam_col[22])
            worksheet.set_column(23,23, tam_col[23])
            worksheet.set_column(24,24, tam_col[24])
            worksheet.set_column(25,25, tam_col[25])
            workbook.close()
            
            f = open(direccion + 'tempo_activo_71.xlsx', 'rb')
            
            
            sfs_obj = self.pool.get('account_contable_book_it.sunat_file_save')
            vals = {
                'output_name': 'ActivoFormato7_1.xlsx',
                'output_file': base64.encodestring(''.join(f.readlines())),     
            }

            mod_obj = self.env['ir.model.data']
            act_obj = self.env['ir.actions.act_window']
            sfs_id = self.env['export.file.save'].create(vals)

            return {
                "type": "ir.actions.act_window",
                "res_model": "export.file.save",
                "views": [[False, "form"]],
                "res_id": sfs_id.id,
                "target": "new",
            }



        if self.tipo == 'pdf':
            self.reporteador()
            
            import sys
            reload(sys)
            sys.setdefaultencoding('iso-8859-1')
            mod_obj = self.env['ir.model.data']
            act_obj = self.env['ir.actions.act_window']
            import os

            direccion = self.env['main.parameter'].search([])[0].dir_create_file
            vals = {
                'output_name': 'ActivoFormato7_1.pdf',
                'output_file': open(direccion + "a.pdf", "rb").read().encode("base64"), 
            }
            sfs_id = self.env['export.file.save'].create(vals)

            return {
                "type": "ir.actions.act_window",
                "res_model": "export.file.save",
                "views": [[False, "form"]],
                "res_id": sfs_id.id,
                "target": "new",
            }

    @api.multi
    def reporteador(self):

        import sys
        nivel_left_page = 1
        nivel_left_fila = 0
        
        nivel_right_page = 1
        nivel_right_fila = 0

        reload(sys)
        sys.setdefaultencoding('iso-8859-1')
        width , height = A4  # 595 , 842
        hReal = width- 30
        wReal = height - 40

        direccion = self.env['main.parameter'].search([])[0].dir_create_file
        c = canvas.Canvas( direccion + "a.pdf", pagesize=(height,width) )
        inicio = 0
        pos_inicial = hReal-125

        pagina = 1
        textPos = 0
        
        tamanios = {}
        voucherTamanio = None
        contTamanio = 0

        self.cabezera(c,wReal,hReal)


        c.setFont("Times-Bold", 10)
        #c.drawCentredString(421,25,'Pág. ' + str(pagina))

        #datos a consultar
        
        fechaini_obj = self.date
        periodo = self.env['account.period'].search([('code','=',self.date.split('-')[1] +'/'+self.date.split('-')[0])]) 
        if self.date == periodo.date_stop:
            pass
        else:
            periodo_anterior = {
                '12':'11',
                '11':'10',
                '10':'09',
                '09':'08',
                '08':'07',
                '07':'06',
                '06':'05',
                '05':'04',
                '04':'03',
                '03':'02',
                '02':'01',
                '01':'00',
            }
            periodo = self.env['account.period'].search([('code','=', periodo_anterior[self.date.split('-')[1]] +'/'+self.date.split('-')[0])])
            fechaini_obj = periodo.date_stop


    
        
        move_obj=self.env['account.asset.asset']
        filtro = []
        filtro.append( ('date','<=',str(fechaini_obj) ) )
        filtro.append( ('parent_id','=',False ) )
        filtro.append( ('format_74','=',False) )
        
        lstidsmove = move_obj.search( filtro )


        aa = 0
        bb = 0
        cc = 0
        dd = 0
        ee = 0
        ff = 0
        for line in lstidsmove:

            c.setFont("Times-Roman", 5)

            c.drawString(15+3,pos_inicial, self.particionar_text(line.codigo) if line.codigo else '')
            c.drawString(65+3,pos_inicial, line.category_id.account_asset_id.code if line.category_id.account_asset_id.code else '')
            c.drawString(115+3,pos_inicial, self.particionar_text(line.name) if line.name  else '')
            c.drawString(165+3,pos_inicial, line.marca if line.marca  else '')
            c.drawString(215+3,pos_inicial, line.modelo if line.modelo  else '')
            c.drawString(265+3,pos_inicial, line.serie if line.serie  else '')


            primer_acum_neg = (line.value if line.date < self.period_id.date_start else 0 )
            primer_acum_neg += (line.value if line.date >= self.period_id.date_start and line.date <= self.period_id.date_stop else 0) 
            primer_acum_neg += 0# (line.value+total[2] if line.date >= self.period_id.date_start  and line.date <= self.period_id.date_stop and line.tipo == 'mejoras' else 0+total[2])
            primer_acum_neg += 0# (line.value+total[4] if line.date >= self.period_id.date_start  and line.date <= self.period_id.date_stop and line.tipo == 'otros' else 0+total[4])

            c.drawRightString(365-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(line.value if line.date < self.period_id.date_start else 0))) )
            c.drawRightString(415-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(line.value if line.date >= self.period_id.date_start and line.date <= self.period_id.date_stop  else 0))) )
            c.drawRightString(465-3,pos_inicial, '0.00')
            c.drawRightString(515-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%( (-primer_acum_neg) if line.f_baja and line.f_baja >= fechaini_obj else 0 ))))

            c.drawRightString(565-3,pos_inicial, '0.00')
            
            primer_acum = (line.value if line.date < self.period_id.date_start else 0 )
            primer_acum += (line.value if line.date >= self.period_id.date_start and line.date <= self.period_id.date_stop  else 0) 
            primer_acum += 0# (line.value+total[2] if line.date >= self.period_id.date_start  and line.date <= self.period_id.date_stop and line.tipo == 'mejoras' else 0+total[2])
            primer_acum +=  (-primer_acum_neg+total[3]) if line.f_baja and line.f_baja >= fechaini_obj else 0
            primer_acum += 0#(line.value+total[4] if line.date >= self.period_id.date_start  and line.date <= self.period_id.date_stop and line.tipo == 'otros' else 0+total[4])

            c.drawRightString(615-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(primer_acum) ))) # dos blancos
            c.drawRightString(655-3,pos_inicial, '0') # dos blancos
            c.drawRightString(715-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(primer_acum) ))) # dos blancos
            c.drawString(715+3,pos_inicial, line.date if line.date else '')
            c.drawString(765+3,pos_inicial, line.date_start if line.date_start else '')

            pagina, pos_inicial = self.verify_linea(c,wReal,hReal,pos_inicial,12,pagina)
            aa+=line.value if line.date < self.period_id.date_start else 0
            bb+=line.value if line.date >= self.period_id.date_start and line.date <= self.period_id.date_stop  else 0
            cc+=0 #line.value+total[2] if line.date >= self.period_id.date_start  and line.date <= self.period_id.date_stop and line.tipo == 'mejoras' else 0+total[2]
            dd+= (-primer_acum_neg) if line.f_baja and line.f_baja >= fechaini_obj else 0 
            ee+= 0 #line.value+total[4] if line.date >= self.period_id.date_start  and line.date <= self.period_id.date_stop and line.tipo == 'otros' else 0+total[4]
            ff+=primer_acum

        c.setFont("Times-Bold", 5)
        c.drawString(265+3,pos_inicial, 'TOTAL:')
        c.drawRightString(365-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(aa) )))
        c.drawRightString(415-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(bb) )))
        c.drawRightString(465-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(cc) )))
        c.drawRightString(515-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(dd) )))
        c.drawRightString(565-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(ee) )))
        c.drawRightString(615-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(ff) )))
        c.drawRightString(655-3,pos_inicial, '0')
        c.drawRightString(715-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(ff) ))) # dos blancos



        c.showPage()

        inicio = 0
        pos_inicial = hReal-125

        pagina = 1
        textPos = 0
        
        tamanios = {}
        voucherTamanio = None
        contTamanio = 0

        self.global_controler = False
        self.cabezera(c,wReal,hReal)


        c.setFont("Times-Bold", 10)
        #c.drawCentredString(421,25,'Pág. ' + str(pagina))

        #datos a consultar
        fechaini_obj = self.period_id.date_stop
    
        
        move_obj=self.env['account.asset.asset']
        filtro = []
        filtro.append( ('date','<=',str(fechaini_obj) ) )
        filtro.append( ('parent_id','=',False ) )
        lstidsmove = move_obj.search( filtro )


        aa = 0
        bb = 0
        cc = 0
        dd = 0
        ee = 0
        ff = 0
        for line in lstidsmove:

            c.setFont("Times-Roman", 5)

            c.drawString(15+3,pos_inicial, self.particionar_text(line.codigo) if line.codigo else '')
            c.drawString(65+3,pos_inicial, line.category_id.account_asset_id.code if line.category_id.account_asset_id.code else '')
            c.drawString(115+3,pos_inicial, self.particionar_text(line.name) if line.name  else '')
            c.drawString(165+3,pos_inicial, line.marca if line.marca  else '')
            c.drawString(215+3,pos_inicial, line.modelo if line.modelo  else '')
            c.drawString(265+3,pos_inicial, line.serie if line.serie  else '')
            c.drawString(315+3,pos_inicial, 'Metodo Lineal' )
            c.drawString(365+3,pos_inicial, line.autorizacion_depreciacion if line.autorizacion_depreciacion else '' )
            c.drawRightString(465-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%( line.category_id.percent_depreciacion) )))

            acum_anterior = 0
            for ii in line.depreciation_line_ids:
                if ii.depreciation_date < self.period_id.date_start:
                    acum_anterior += ii.amount

            c.drawRightString(515-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(acum_anterior) )))

            acum_actual = 0
            for ii in line.depreciation_line_ids:
                if ii.depreciation_date >= self.period_id.date_start and ii.depreciation_date <= fechaini_obj:
                    acum_actual += ii.amount

            c.drawRightString(565-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(acum_actual) )))


            total_ultimo = acum_anterior 
            total_ultimo += acum_actual
            total_ultimo += 0 #acum_actual+total[10] if line.tipo=='otros' else 0+total[10]


            c.drawRightString(615-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%( ((-total_ultimo) if total_ultimo!= 0 else 0 ) if line.f_baja and line.f_baja >= fechaini_obj else 0 ) )))
            c.drawRightString(665-3,pos_inicial, '0.00' )
            c.drawRightString(715-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(acum_anterior+acum_actual + ( ((-total_ultimo) if total_ultimo!= 0 else 0 ) if line.f_baja and line.f_baja >= fechaini_obj else 0 ) ) )))
            c.drawRightString(765-3,pos_inicial, "%0.2f"%(0) )
            c.drawRightString(815-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(acum_anterior+acum_actual + ( ((-total_ultimo) if total_ultimo!= 0 else 0 ) if line.f_baja and line.f_baja >= fechaini_obj else 0 ) ) )))

            pagina, pos_inicial = self.verify_linea(c,wReal,hReal,pos_inicial,12,pagina)

            aa+=-line.depreciacion_retiro
            bb+=acum_anterior
            cc+=acum_actual
            dd+=((-total_ultimo) if total_ultimo!= 0 else 0 ) if line.f_baja and line.f_baja >= fechaini_obj else 0
            ee+=0 #acum_actual+total[10] if line.tipo=='otros' else 0+total[10]
            ff+=acum_anterior+acum_actual+ ( ((-total_ultimo) if total_ultimo!= 0 else 0 ) if line.f_baja and line.f_baja >= fechaini_obj else 0 )

                


        c.setFont("Times-Bold", 5)
        c.drawString(265+3,pos_inicial, 'TOTAL:')
        c.drawRightString(465-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(aa) )))
        c.drawRightString(515-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(bb) )))
        c.drawRightString(565-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(cc) )))
        c.drawRightString(615-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(dd) )))
        c.drawRightString(665-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(ee) )))
        c.drawRightString(715-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(ff) ))) # dos blancos
        c.drawRightString(765-3,pos_inicial, "%0.2f"%(0) )
        c.drawRightString(815-3,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%(ff) ))) # dos blancos

        self.finalizar(c)

class account_asset_formato_74(models.Model):
    _inherit='account.asset.formato.74'

    @api.multi
    def do_rebuild(self):
        fechaini_obj = self.date
        periodo = self.env['account.period'].search([('code','=',self.date.split('-')[1] +'/'+self.date.split('-')[0])]) 
        if self.date == periodo.date_stop:
            pass
        else:
            periodo_anterior = {
                '12':'11',
                '11':'10',
                '10':'09',
                '09':'08',
                '08':'07',
                '07':'06',
                '06':'05',
                '05':'04',
                '04':'03',
                '03':'02',
                '02':'01',
                '01':'00',
            }
            periodo = self.env['account.period'].search([('code','=', periodo_anterior[self.date.split('-')[1]] +'/'+self.date.split('-')[0])])
            fechaini_obj = periodo.date_stop


           
        move_obj=self.env['account.asset.asset']
        filtro = []
        filtro.append( ('date','<=',str(fechaini_obj) ) )
        filtro.append( ('f_contra','!=',False ) )
        filtro.append( ('format_74','=',True) )
        
        lstidsmove = move_obj.search( filtro )
            
        mod_obj = self.env['ir.model.data']
        act_obj = self.env['ir.actions.act_window']

        if self.tipo == 'excel':
            import io
            from xlsxwriter.workbook import Workbook
            output = io.BytesIO()
            ########### PRIMERA HOJA DE LA DATA EN TABLA
            #workbook = Workbook(output, {'in_memory': True})

            direccion = self.env['main.parameter'].search([])[0].dir_create_file

            workbook = Workbook(direccion +'tempo_activo_74.xlsx')
            worksheet = workbook.add_worksheet("Activo - Formato 74")
            bold = workbook.add_format({'bold': True})
            normal = workbook.add_format()
            boldbord = workbook.add_format({'bold': True})
            boldbord.set_border(style=2)
            boldbord.set_align('center')
            boldbord.set_align('vcenter')
            boldbord.set_text_wrap()
            boldbord.set_font_size(9)
            boldbord.set_bg_color('#DCE6F1')
            numbertres = workbook.add_format({'num_format':'0.000'})
            numberdos = workbook.add_format({'num_format':'0.00'})
            bord = workbook.add_format()
            bord.set_border(style=1)
            numberdos.set_border(style=1)
            numbertres.set_border(style=1)          
            x= 7                
            tam_col = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
            tam_letra = 1.2
            import sys
            reload(sys)
            sys.setdefaultencoding('iso-8859-1')

            worksheet.write(0,0, 'FORMATO 7.4: "REGISTRO DE ACTIVOS FIJOS - DETALLE DE LOS ACTIVOS FIJOS BAJO LA MODALIDAD DE ARRENDAMIENTO FINANCIERO AL 31.12"', bold)



            worksheet.write(2,0, "Periodo:",bold)
            tam_col[0] = tam_letra* len("Periodo:") if tam_letra* len("Periodo:")> tam_col[0] else tam_col[0]

            worksheet.write(2,1, str(self.period_id.name), normal)
            tam_col[1] = tam_letra* len(str(self.period_id.name)) if tam_letra* len(str(self.period_id.name))> tam_col[1] else tam_col[1]
            

            company = self.env['res.company'].search([])[0]

            worksheet.write(3,0, "RUC:",bold)
            tam_col[0] = tam_letra* len("RUC:") if tam_letra* len("RUC:")> tam_col[0] else tam_col[0]

            worksheet.write(3,1, str(company.partner_id.nro_documento), normal)
            tam_col[1] = tam_letra* len(str(company.partner_id.nro_documento)) if tam_letra* len(str(company.partner_id.nro_documento))> tam_col[1] else tam_col[1]
            



            worksheet.write(4,0, u"Apellidos y nombres, denominación o Razón Social:",bold)
            tam_col[0] = tam_letra* len(u"Apellidos y nombres, denominación o Razón Social:") if tam_letra* len(u"Apellidos y nombres, denominación o Razón Social:")> tam_col[0] else tam_col[0]

            worksheet.write(4,1, str(company.partner_id.name), normal)
            tam_col[1] = tam_letra* len(str(company.partner_id.name)) if tam_letra* len(str(company.partner_id.name))> tam_col[1] else tam_col[1]
            

            worksheet.write(6,0, "ACTIVO FIJO",boldbord)
            tam_col[0] = tam_letra* len("ACTIVO FIJO") if tam_letra* len("ACTIVO FIJO")> tam_col[0] else tam_col[0]
            worksheet.write(6,1, "FECHA DEL CONTRATO",boldbord)
            tam_col[1] = tam_letra* len("FECHA DEL CONTRATO") if tam_letra* len("FECHA DEL CONTRATO")> tam_col[1] else tam_col[1]
            worksheet.write(6,2, "NUMERO DEL CONTRATO DE ARRENDAMIENTO",boldbord)
            tam_col[2] = tam_letra* len("NUMERO DEL CONTRATO DE ARRENDAMIENTO") if tam_letra* len("NUMERO DEL CONTRATO DE ARRENDAMIENTO")> tam_col[2] else tam_col[2]
            worksheet.write(6,3, "FECHA DE INICIO DEL CONTRATO",boldbord)
            tam_col[3] = tam_letra* len("FECHA DE INICIO DEL CONTRATO") if tam_letra* len("FECHA DE INICIO DEL CONTRATO")> tam_col[3] else tam_col[3]
            worksheet.write(6,4, u"NUMERO DE CUOTAS PACTADAS",boldbord)
            tam_col[4] = tam_letra* len(u"NUMERO DE CUOTAS PACTADAS") if tam_letra* len(u"NUMERO DE CUOTAS PACTADAS")> tam_col[4] else tam_col[4]
            worksheet.write(6,5, "MONTO DEL CONTRATO",boldbord)
            tam_col[5] = tam_letra* len("MONTO DEL CONTRATO") if tam_letra* len("MONTO DEL CONTRATO")> tam_col[5] else tam_col[5]
            


            for line in lstidsmove:
                worksheet.write(x,0,line.name if line.name else '' ,bord )
                worksheet.write(x,1,line.f_contra if line.f_contra  else '',bord )
                worksheet.write(x,2,line.num_contra if line.num_contra  else '',bord)
                worksheet.write(x,3,line.f_ini_arren if line.f_ini_arren  else '',bord)
                worksheet.write(x,4,line.num_cuotas,bord)
                worksheet.write(x,5,line.monto_contra ,numberdos)
                

                tam_col[0] = tam_letra* len(line.name if line.name else '' ) if tam_letra* len(line.name if line.name else '' )> tam_col[0] else tam_col[0]
                tam_col[1] = tam_letra* len(line.f_contra if line.f_contra  else '') if tam_letra* len(line.f_contra if line.f_contra  else '')> tam_col[1] else tam_col[1]
                tam_col[2] = tam_letra* len(line.num_contra if line.num_contra  else '') if tam_letra* len(line.num_contra if line.num_contra  else '')> tam_col[2] else tam_col[2]
                tam_col[3] = tam_letra* len(line.f_ini_arren if line.f_ini_arren  else '') if tam_letra* len(line.f_ini_arren if line.f_ini_arren  else '')> tam_col[3] else tam_col[3]
                tam_col[4] = tam_letra* len(str(line.num_cuotas) if line.num_cuotas  else '') if tam_letra* len(str(line.num_cuotas) if line.num_cuotas  else '')> tam_col[4] else tam_col[4]
                tam_col[5] = tam_letra* len("%0.2f"%line.monto_contra ) if tam_letra* len("%0.2f"%line.monto_contra )> tam_col[5] else tam_col[5]
                
                x = x +1


            tam_col = [45,19,39,26,27,22,0,0,0,0,0,0,0,0,0]
            worksheet.set_column('A:A', tam_col[0])
            worksheet.set_column('B:B', tam_col[1])
            worksheet.set_column('C:C', tam_col[2])
            worksheet.set_column('D:D', tam_col[3])
            worksheet.set_column('E:E', tam_col[4])
            worksheet.set_column('F:F', tam_col[5])
            workbook.close()
            
            f = open(direccion + 'tempo_activo_74.xlsx', 'rb')
            
            
            sfs_obj = self.pool.get('account_contable_book_it.sunat_file_save')
            vals = {
                'output_name': 'ActivoFormato7_4.xlsx',
                'output_file': base64.encodestring(''.join(f.readlines())),     
            }

            mod_obj = self.env['ir.model.data']
            act_obj = self.env['ir.actions.act_window']
            sfs_id = self.env['export.file.save'].create(vals)


            return {
                "type": "ir.actions.act_window",
                "res_model": "export.file.save",
                "views": [[False, "form"]],
                "res_id": sfs_id.id,
                "target": "new",
            }



        if self.tipo == 'pdf':
            self.reporteador()
            
            import sys
            reload(sys)
            sys.setdefaultencoding('iso-8859-1')
            mod_obj = self.env['ir.model.data']
            act_obj = self.env['ir.actions.act_window']
            import os

            direccion = self.env['main.parameter'].search([])[0].dir_create_file
            vals = {
                'output_name': 'ActivoFormato7_4.pdf',
                'output_file': open(direccion + "a.pdf", "rb").read().encode("base64"), 
            }
            sfs_id = self.env['export.file.save'].create(vals)

            return {
                "type": "ir.actions.act_window",
                "res_model": "export.file.save",
                "views": [[False, "form"]],
                "res_id": sfs_id.id,
                "target": "new",
            }

    @api.multi
    def reporteador(self):

        import sys
        nivel_left_page = 1
        nivel_left_fila = 0
        
        nivel_right_page = 1
        nivel_right_fila = 0

        reload(sys)
        sys.setdefaultencoding('iso-8859-1')
        width , height = A4  # 595 , 842
        hReal = width- 30
        wReal = height - 40

        direccion = self.env['main.parameter'].search([])[0].dir_create_file
        c = canvas.Canvas( direccion + "a.pdf", pagesize=(height,width) )
        inicio = 0
        pos_inicial = hReal-136

        pagina = 1
        textPos = 0
        
        tamanios = {}
        voucherTamanio = None
        contTamanio = 0

        self.cabezera(c,wReal,hReal)


        c.setFont("Times-Bold", 9)
        #c.drawCentredString(421,25,'Pág. ' + str(pagina))


        #pagina, pos_inicial = self.verify_linea(c,wReal,hReal,pos_inicial,16,pagina)

        fechaini_obj = self.period_id.date_stop
    
        
        move_obj=self.env['account.asset.asset']
        filtro = []
        filtro.append( ('date','<=',str(fechaini_obj) ) )
        filtro.append( ('f_contra','!=',False ) )
        filtro.append( ('format_74','=',True) )
        
        
        lstidsmove = move_obj.search( filtro )
        total = 0
        for line in lstidsmove:
            c.setFont("Times-Roman", 9)
            c.drawString(35,pos_inicial, line.name if line.name else '')
            c.drawString(285,pos_inicial, line.f_contra if line.f_contra  else '')
            c.drawString(375,pos_inicial, line.num_contra if line.num_contra  else '')
            c.drawString(465,pos_inicial, line.f_ini_arren if line.f_ini_arren  else '')
            c.drawString(555,pos_inicial, str(line.num_cuotas) )
            c.drawRightString(645+110,pos_inicial, '{:,.2f}'.format(decimal.Decimal ("%0.2f"%line.monto_contra)))           
            total += line.monto_contra
            pagina, pos_inicial = self.verify_linea(c,wReal,hReal,pos_inicial,15,pagina)


        c.setFont("Times-Bold", 9)
        c.drawString(555,pos_inicial, "TOTAL: ")
        c.drawRightString(645+110,pos_inicial, '{:,.2f}'.format(decimal.Decimal("%0.2f"%total)))

        c.save()