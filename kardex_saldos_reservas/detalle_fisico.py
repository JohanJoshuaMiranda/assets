# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError

class KfisicoDetailWizard(models.TransientModel):
	_name = 'kfisico.detail.wizard'

	location_id = fields.Many2one('stock.location', string='Almacen')

	@api.multi
	def get_disponibilidad_kfisico_d(self):
		detalle = self.env['detalle.simple.kfisicot'].create({})
		where_clause = ''
		if self.location_id:
			where_clause = ' where ubicacion = %s' % self.location_id.id

		self.env.cr.execute("""
					select * from (
					select  product_id , ubicacion,
					sum(stock_disponible) as saldo,
					sum(saldo_fisico) as saldo_fisico,
					sum(por_ingresar) as por_ingresar,
					sum(transito) as transito,
					sum(salida_espera) as salida_espera,
					sum(reservas) as reservas,
					sum(previsto) as saldo_virtual
					,array_agg(id_stock_disponible),array_agg(id_saldo_fisico),array_agg(id_por_ingresar),array_agg(id_transito)
					,array_agg(id_salida_espera),array_agg(id_reservas),array_agg(id_previsto)
					from vst_kardex_onlyfisico_total 
					%s
					group by ubicacion, product_id
					order by product_id,ubicacion
					) Todo
		 """ % where_clause)
		for i in self.env.cr.fetchall():
			self.env['detalle.simple.kfisicot.d'].create({'producto':i[0],'almacen':i[1],'saldo':i[2],
														  'saldo_fisico':i[3],'por_ingresar':i[4],'transito':i[5],
														  'salida_espera':i[6],'reservas':i[7],'saldo_virtual':i[8],
														  'id_stock_disponible':str(i[9]),'id_saldo_fisico':str(i[10]),
														  'id_por_ingresar':str(i[11]),'id_transito':str(i[12]),
														  'id_salida_espera':str(i[13]),'id_reservas':str(i[14]),
														  'id_previsto':str(i[15]),'padre':detalle.id})
		return {
				'type': 'ir.actions.act_window',
				'res_model': 'detalle.simple.kfisicot.d',
				'view_mode': 'tree',
				'view_type': 'form',
				'name':'Saldos',
				'views': [(self.env.ref('kardex_saldos_reservas.view_detalle_simple_kfisicot_d_tree').id, 'tree')],
				'domain': [('padre', '=', detalle.id)]
			}