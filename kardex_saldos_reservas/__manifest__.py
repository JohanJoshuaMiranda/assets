# -*- encoding: utf-8 -*-
{
	'name': 'Saldos y Reservas',
	'category': 'account',
	'author': 'ITGRUPO',
	'depends': ['kardex_product_saldofisico_it'],
	'version': '1.0',
	'description':"""
	Modulo para agregar menu 'Saldos disponibles y Reservas'
	""",
	'auto_install': False,
	'demo': [],
	'data':	['detalle_fisico.xml'],
	'installable': True
}
