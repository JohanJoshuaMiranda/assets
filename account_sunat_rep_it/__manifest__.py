# -*- encoding: utf-8 -*-
{
	'name': 'Reporte SUNAT',
	'category': 'account',
	'author': 'ITGRUPO',
	'depends': ['account','import_base_it','account_parametros_it','export_ple_ferco','activo_fijo'],
	'version': '1.0',
	'description':"""
	- Nuevo menu SUNAT
	""",
	'auto_install': False,
	'application': True,
	'demo': [],
	'data':	[
			'security/security.xml',
			'views/account_sunat_menu.xml',
			'views/catalog_34_values_sunat.xml',
			'views/main_parameter.xml',
			'wizards/account_sunat_rep.xml'
			],
	'installable': True
}
