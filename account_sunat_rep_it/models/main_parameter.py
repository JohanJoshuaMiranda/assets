# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class MainParameter(models.Model):
	_inherit = 'main.parameter'

	codes_report_sunat_caja = fields.Char(string='Prefijos Reporteador Sunat Caja')
	codes_report_sunat_bank = fields.Char(string='Prefijos Reporteador Sunat Banco')