# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class Catalog34ValuesSunat(models.Model):
	_name = 'catalog.34.values.sunat'

	date = fields.Date(string=u'Fecha de Inicio de la Operación',required=True)
	name = fields.Char(string=u'Descripción del Intangible',required=True)
	type = fields.Char(string=u'Tipo de Intangible (Tabla 7)',required=True)
	amount_total = fields.Float(string=u'Valor Contable del Intangible',required=True,digits=(32,2))
	amortizacion = fields.Float(string=u'Amortización Contable Acumulada',required=True,digits=(32,2))
	amount = fields.Float(string=u'Valor Neto Contable del Intangible',required=True,digits=(32,2))