CREATE OR REPLACE VIEW public.vst_diariog AS
SELECT
CASE
    WHEN a1.fecha_special = true AND to_char(a1.date::timestamp with time zone, 'mmdd'::text) = '0101'::text THEN to_char(a1.date::timestamp with time zone, 'yyyy'::text) || '00'::text
    WHEN a1.fecha_special = true AND to_char(a1.date::timestamp with time zone, 'mmdd'::text) = '1231'::text THEN to_char(a1.date::timestamp with time zone, 'yyyy'::text) || '13'::text
    ELSE to_char(a1.date::timestamp with time zone, 'yyyymm'::text)
END AS periodo,
to_char(a1.date::timestamp with time zone, 'yyyy/mm/dd'::text) AS fecha,
a3.code AS libro,
a1.name AS voucher,
a4.code AS cuenta,
a2.debit AS debe,
a2.credit AS haber,
a2.balance,
CASE
    WHEN a2.currency_id IS NULL THEN 'PEN'::character varying
    ELSE a5.name
END AS moneda,
coalesce(case when a2.tc = 0 then  1 else a2.tc end,1) as tc,
a2.amount_currency AS importe_me,
a2.analytic_account_id AS cta_analitica,
a2.name as glosa,
a7.code AS td_partner,
a6.vat AS doc_partner,
a6.name AS partner,
a8.code AS td_sunat,
REPLACE(a2.nro_comprobante,'/','-')::character varying AS nro_comprobante,
ai.date_invoice AS fecha_doc,
a2.date_maturity AS fecha_ven,
a2.tax_amount AS monto_reg,
a12.code AS medio_pago,
a1.state AS estado,
a1.ple_diariomayor AS ple_diario,
ai.estado_ple_compra AS ple_compras,
ai.estado_ple_venta AS ple_ventas,
a1.journal_id,
a2.account_id,
a2.partner_id,
a3.register_sunat AS registro,
a1.id AS move_id,
a2.id AS move_line_id,
a1.company_id,
a11.name AS code_cta_analitica
FROM account_move a1
LEFT JOIN account_move_line a2 ON a2.move_id = a1.id
LEFT JOIN account_invoice ai on ai.move_id = a1.id
LEFT JOIN account_journal a3 ON a3.id = a1.journal_id
LEFT JOIN account_account a4 ON a4.id = a2.account_id
LEFT JOIN res_currency a5 ON a5.id = a2.currency_id
LEFT JOIN res_partner a6 ON a6.id = a2.partner_id
LEFT JOIN einvoice_catalog_06 a7 ON a7.id = a6.type_document_partner_it
LEFT JOIN einvoice_catalog_01 a8 ON a8.id = a2.type_document_it
LEFT JOIN account_analytic_account a11 ON a11.id = a2.analytic_account_id
LEFT JOIN einvoice_means_payment a12 ON a12.id = a1.means_payment_it
WHERE a1.state::text = 'posted'::text AND a2.account_id IS NOT NULL
ORDER BY (date_part('month'::text, a1.date)), a3.code, a1.name, a2.debit DESC, a4.code;

--------------------------------------------------------------------------------------------------------------------
DROP VIEW IF EXISTS public.vst_mayor13 CASCADE;

CREATE OR REPLACE VIEW public.vst_mayor13 AS 
 SELECT tt.periodo,
    tt.fecha,
    tt.libro,
    tt.voucher,
    tt.cuenta,
    tt.debe,
    tt.haber,
    tt.balance,
    tt.moneda,
    tt.tc,
    tt.importe_me,
    tt.code_cta_analitica,
    tt.glosa,
    tt.td_partner,
    tt.doc_partner,
    tt.partner,
    tt.td_sunat,
    tt.nro_comprobante,
    tt.fecha_doc,
    tt.fecha_ven,
    tt.company_id,
    tt.account_id
   FROM ( SELECT vst_diariog.periodo,
            to_date(vst_diariog.fecha, 'yyyy/mm/dd'::text) AS fecha,
            vst_diariog.libro,
            vst_diariog.voucher,
            vst_diariog.cuenta,
            vst_diariog.debe,
            vst_diariog.haber,
            vst_diariog.balance,
            vst_diariog.moneda,
            vst_diariog.tc,
            vst_diariog.importe_me,
            vst_diariog.code_cta_analitica,
            vst_diariog.glosa,
            vst_diariog.td_partner,
            vst_diariog.doc_partner,
            vst_diariog.partner,
            vst_diariog.td_sunat,
            vst_diariog.nro_comprobante,
            vst_diariog.fecha_doc,
            vst_diariog.fecha_ven,
            vst_diariog.company_id,
            vst_diariog.account_id
           FROM vst_diariog
          ORDER BY vst_diariog.cuenta, vst_diariog.periodo, vst_diariog.fecha) tt;
--------------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS public.get_mayor_detalle13(date,date) CASCADE;

CREATE OR REPLACE FUNCTION public.get_mayor_detalle13(
    IN fec_ini date,
    IN fec_fin date)
  RETURNS TABLE(periodo character varying, fecha date, libro character varying, voucher character varying, cuenta character varying, debe numeric, haber numeric, balance numeric, moneda character varying, tc numeric, importe_me numeric, code_cta_analitica character varying, glosa character varying, td_partner character varying, doc_partner character varying, partner character varying, td_sunat character varying, nro_comprobante character varying, fecha_doc date, fecha_ven date, company_id integer, account_id integer, saldo numeric) AS
$BODY$
DECLARE 
    var_r record;
    quiebre TEXT;
    contador INT;
BEGIN
   contador = 0 ;
   saldo = 0;
   FOR var_r IN(
        select * from (
           SELECT * FROM VST_MAYOR13 WHERE VST_MAYOR13.fecha>= fec_ini 
                    AND  VST_MAYOR13.fecha <= fec_fin
            union all
            SELECT "left"(to_char((fec_ini-1)::timestamp with time zone, 'yyyymmdd'::text), 6) AS periodo, fec_ini - 1, '' as libro, '' as voucher, VST_MAYOR13.cuenta,
            CASE
				WHEN SUM (VST_MAYOR13.balance) > 0 THEN SUM(VST_MAYOR13.balance) ELSE 0
			END as debe,
			CASE
				WHEN SUM (VST_MAYOR13.balance) < 0 THEN ABS(SUM (VST_MAYOR13.balance)) ELSE 0
			END as haber,
            SUM (VST_MAYOR13.balance) AS balance,
            '' AS moneda, 0 AS tc, SUM (VST_MAYOR13.importe_me) AS importe_me, '' as code_cta_analitica, 'SALDO INICIAL' as glosa,
            '' AS td_partner, '' AS doc_partner, '' AS partner, '' AS td_sunat, '' AS nro_comprobante, null::date AS fecha_doc, null::date AS fecha_ven, VST_MAYOR13.company_id as company_id, 
            VST_MAYOR13.account_id AS account_id   
            FROM VST_MAYOR13 WHERE VST_MAYOR13.fecha < fec_ini
                            AND  EXTRACT (YEAR FROM VST_MAYOR13.fecha) = EXTRACT (YEAR FROM fec_ini)
            group by VST_MAYOR13.cuenta,VST_MAYOR13.account_id,VST_MAYOR13.company_id
         )T
                ORDER BY cuenta, periodo, fecha
               
               )
   LOOP
        -- Obtiene por unica vez el valor del primer registro
        IF contador = 0 THEN 
            quiebre := var_r.cuenta ;
            contador = contador + 1;
        END IF;
        
        -- Si los registros son de la misma cuenta
        IF quiebre =  var_r.cuenta THEN
            saldo = saldo + var_r.balance;
        -- Si la cuenta cambia, reinicio el saldo y actualizo el quiebre
        ELSE
            saldo = 0;
            quiebre := var_r.cuenta ;
            saldo = saldo + var_r.balance;
        END IF;
        
        periodo = var_r.periodo ;
        fecha = var_r.fecha ;
        libro = var_r.libro ;
        voucher = var_r.voucher ;
        cuenta  = var_r.cuenta ;
        debe = var_r.debe ;
        haber = var_r.haber;
        balance = var_r.balance ;
        moneda = var_r.moneda ;
        tc  = var_r.tc ;
        importe_me  = var_r.importe_me ;
        code_cta_analitica = var_r.code_cta_analitica ;
        glosa = var_r.glosa ;
        td_partner = var_r.td_partner ;
        doc_partner = var_r.doc_partner;
        partner = var_r.partner;
        td_sunat = var_r.td_sunat ;
        nro_comprobante  = var_r.nro_comprobante ;
        fecha_doc = var_r.fecha_doc ;
        fecha_ven  = var_r.fecha_ven ;
        company_id = var_r.company_id ;
        account_id  = var_r.account_id ;
        
   RETURN NEXT;
   END LOOP;
END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
--------------------------------------------------------------------------------------------------------------------

DROP VIEW IF EXISTS public.vst_bancos13 CASCADE;

CREATE OR REPLACE VIEW public.vst_bancos13 AS 
 SELECT tt.periodo,
    tt.fecha,
    tt.libro,
    tt.voucher,
    tt.cuenta,
    tt.debe,
    tt.haber,
    tt.balance,
    tt.moneda,
    tt.tc,
    tt.importe_me,
    tt.code_cta_analitica,
    tt.glosa,
    tt.td_partner,
    tt.doc_partner,
    tt.partner,
    tt.td_sunat,
    tt.nro_comprobante,
    tt.fecha_doc,
    tt.fecha_ven,
    tt.company_id,
    tt.account_id,
    tt.move_id,
    tt.move_line_id
   FROM ( SELECT vst_diariog.periodo,
            to_date(vst_diariog.fecha, 'yyyy-mm-dd'::text) AS fecha,
            vst_diariog.libro,
            vst_diariog.voucher,
            vst_diariog.cuenta,
            vst_diariog.debe,
            vst_diariog.haber,
            vst_diariog.balance,
            vst_diariog.moneda,
            vst_diariog.tc,
            vst_diariog.importe_me,
            vst_diariog.code_cta_analitica,
            vst_diariog.glosa,
            vst_diariog.td_partner,
            vst_diariog.doc_partner,
            vst_diariog.partner,
            vst_diariog.td_sunat,
            vst_diariog.nro_comprobante,
            vst_diariog.fecha_doc,
            vst_diariog.fecha_ven,
            vst_diariog.company_id,
            vst_diariog.account_id,
            vst_diariog.move_id,
            vst_diariog.move_line_id
           FROM vst_diariog
          ORDER BY vst_diariog.cuenta, vst_diariog.periodo, vst_diariog.fecha) tt;

---------------------------------------------------------------------------------------------------

DROP FUNCTION IF EXISTS public.get_bancos13(date,date) CASCADE;

CREATE OR REPLACE FUNCTION public.get_bancos13(
    IN fec_ini date,
    IN fec_fin date)
  RETURNS TABLE(periodo character varying, fecha date, libro character varying, voucher character varying, cuenta character varying, debe numeric, haber numeric, balance numeric, moneda character varying, tc numeric, importe_me numeric, code_cta_analitica character varying, glosa character varying, td_partner character varying, doc_partner character varying, partner character varying, td_sunat character varying, nro_comprobante character varying, fecha_doc date, fecha_ven date, company_id integer, account_id integer, saldo numeric, saldo_me numeric, move_id integer, move_line_id integer) AS
$BODY$
DECLARE 
    var_r record;
    quiebre TEXT;
    contador INT;
BEGIN
   contador = 0 ;
   saldo = 0;
   saldo_me = 0;
   FOR var_r IN(
        select * from (
           SELECT * FROM VST_BANCOS13 WHERE VST_BANCOS13.fecha>= fec_ini 
                    AND  VST_BANCOS13.fecha <= fec_fin
            union all
            SELECT "left"(to_char((fec_ini-1)::timestamp with time zone, 'yyyymmdd'::text), 6) AS periodo, fec_ini - 1, '' as libro, '' as voucher, VST_BANCOS13.cuenta,
            CASE
				WHEN SUM (VST_BANCOS13.balance) > 0 THEN SUM(VST_BANCOS13.balance) ELSE 0
			END as debe,
			CASE
				WHEN SUM (VST_BANCOS13.balance) < 0 THEN ABS(SUM (VST_BANCOS13.balance)) ELSE 0
			END as haber,
            SUM (VST_BANCOS13.balance) AS balance, '' AS moneda, 0 AS tc, SUM (VST_BANCOS13.importe_me) AS importe_me, '' as code_cta_analitica, 'SALDO INICIAL' as glosa,
            '' AS td_partner, '' AS doc_partner, '' AS partner, '' AS td_sunat, '' AS nro_comprobante, null::date AS fecha_doc, null::date AS fecha_ven, VST_BANCOS13.company_id as company_id, 
            VST_BANCOS13.account_id AS account_id, null as move_id, null as move_line_id
            FROM VST_BANCOS13 WHERE VST_BANCOS13.fecha < fec_ini
                            AND  EXTRACT (YEAR FROM VST_BANCOS13.fecha) = EXTRACT (YEAR FROM fec_ini)
            group by VST_BANCOS13.cuenta,VST_BANCOS13.account_id,VST_BANCOS13.company_id
         )T
                ORDER BY cuenta, periodo, fecha
               
               )
   LOOP
        -- Obtiene por unica vez el valor del primer registro
        IF contador = 0 THEN 
            quiebre := var_r.cuenta ;
            contador = contador + 1;
        END IF;
        
        -- Si los registros son de la misma cuenta
        IF quiebre = var_r.cuenta THEN
            saldo = saldo + var_r.balance;
            saldo_me = saldo_me +  var_r.importe_me;
        -- Si la cuenta cambia, reinicio el saldo y actualizo el quiebre
        ELSE
            saldo = 0;
            saldo_me = 0;
            quiebre := var_r.cuenta ;
            saldo = saldo + var_r.balance;
            saldo_me = saldo_me +  var_r.importe_me;
        END IF;
        
        periodo = var_r.periodo ;
        fecha = var_r.fecha ;
        libro = var_r.libro ;
        voucher = var_r.voucher ;
        cuenta  = var_r.cuenta ;
        debe = var_r.debe ;
        haber = var_r.haber;
        balance = var_r.balance ;
        moneda = var_r.moneda ;
        tc  = var_r.tc ;
        importe_me  = var_r.importe_me ;
        code_cta_analitica = var_r.code_cta_analitica ;
        glosa = var_r.glosa ;
        td_partner = var_r.td_partner ;
        doc_partner = var_r.doc_partner;
        partner = var_r.partner;
        td_sunat = var_r.td_sunat ;
        nro_comprobante  = var_r.nro_comprobante ;
        fecha_doc = var_r.fecha_doc ;
        fecha_ven  = var_r.fecha_ven ;
        company_id = var_r.company_id ;
        account_id  = var_r.account_id ;
        move_id  = var_r.move_id ;
        move_line_id  = var_r.move_line_id ;
        
   RETURN NEXT;
   END LOOP;
END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

------------------------------------------------------------------------------------------------------------------------------------

DROP FUNCTION IF EXISTS public.get_detalle_pdf_10(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION public.get_detalle_pdf_10(
	periodo_apertura integer,
	periodo integer)
    RETURNS TABLE(cuenta character varying, nomenclatura character varying, code_bank character varying, account_number character varying, moneda integer, debe numeric, haber numeric, account_id integer) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
BEGIN
	RETURN QUERY 
SELECT 
aa.code as cuenta,
aa.name as nomenclatura,
aa.cashbank_code as code_bank,
aa.cashbank_number as account_number,
CASE 
	WHEN rc.name = 'USD' THEN 2 ELSE 1
END AS moneda,
T.debe,
T.haber,
T.account_id
FROM
(SELECT vst.account_id,SUM(vst.debe) AS debe,SUM(vst.haber) AS haber FROM vst_diariog vst
LEFT JOIN account_account aa ON aa.id = vst.account_id
WHERE LEFT(vst.cuenta,2) = '10' AND (CAST(vst.periodo AS int) BETWEEN $1 AND $2)
GROUP BY vst.account_id)T
LEFT JOIN account_account aa ON aa.id = T.account_id
LEFT JOIN res_currency rc ON rc.id = aa.currency_id;
END;
$BODY$;

--------------------------------------------------------------------------------------------------------------------------------------

DROP FUNCTION IF EXISTS public.get_saldos(date, date, integer) CASCADE;

CREATE OR REPLACE FUNCTION public.get_saldos(
    IN date_ini date,
    IN date_fin date,
    IN query_type integer)
  RETURNS TABLE(id bigint, periodo text, fecha_con date, libro character varying, voucher character varying, td_partner character varying, doc_partner character varying, partner character varying, td_sunat character varying, nro_comprobante character varying, fecha_doc date, fecha_ven date, cuenta character varying, moneda character varying, debe numeric, haber numeric, saldo_mn numeric, saldo_me numeric,aml_ids integer[], journal_id integer, account_id integer, partner_id integer, move_id integer, move_line_id integer, company_id integer) AS
$BODY$
BEGIN
	IF query_type = 0 THEN
		RETURN QUERY 
		SELECT row_number() OVER () AS id,t.*
		   FROM ( select 
		b2.periodo, 
		b2.fecha::date as fecha_con, 
		b2.libro, 
		b2.voucher, 
		b2.td_partner, 
		b2.doc_partner, 
		b2.partner, 
		b2.td_sunat,
		b2.nro_comprobante, 
		b2.fecha_doc,
		b2.fecha_ven,
		b2.cuenta,
		b2.moneda,
		b1.sum_debe as debe,
		b1.sum_haber as haber,
		b1.sum_balance as saldo_mn,
		b1.sum_importe_me as saldo_me,
		b1.aml_ids,
		b2.journal_id,
		b2.account_id,
		b2.partner_id,
		b2.move_id,
		b2.move_line_id,
		b2.company_id
		from(
		select a1.partner_id, a1.account_id, a1.td_sunat, a1.nro_comprobante,
		sum(a1.debe) as sum_debe, sum(a1.haber) as sum_haber, sum(a1.balance) as sum_balance, 
		sum(a1.importe_me) as sum_importe_me, min(a1.move_line_id) as min_line_id,
		array_agg(aml.id) as aml_ids
		from vst_diariog a1
		inner join account_move_line aml on aml.id = a1.move_line_id
		inner join account_move am on am.id = aml.move_id
		left join account_account a2 on a2.id = a1.account_id
		where (a2.analisis_documento = True) and (a1.fecha::date between $1::date and $2::date)
		group by a1.partner_id, a1.account_id, a1.td_sunat, a1.nro_comprobante
		)b1
		left join vst_diariog  b2 on b2.move_line_id = b1.min_line_id
		order by b2.partner, b2.cuenta, b2.td_sunat, b2.nro_comprobante, b2.fecha_doc) t;
	ELSIF query_type = 1 THEN
		RETURN QUERY 
		SELECT row_number() OVER () AS id,t.*
		   FROM ( select 
		b2.periodo, 
		b2.fecha::date as fecha_con, 
		b2.libro, 
		b2.voucher, 
		b2.td_partner, 
		b2.doc_partner, 
		b2.partner, 
		b2.td_sunat,
		b2.nro_comprobante, 
		b2.fecha_doc,
		b2.fecha_ven,
		b2.cuenta,
		b2.moneda,
		b1.sum_debe as debe,
		b1.sum_haber as haber,
		b1.sum_balance as saldo_mn,
		b1.sum_importe_me as saldo_me,
		b1.aml_ids,
		b2.journal_id,
		b2.account_id,
		b2.partner_id,
		b2.move_id,
		b2.move_line_id,
		b2.company_id
		from(
		select a1.partner_id, a1.account_id, a1.td_sunat, a1.nro_comprobante,
		sum(a1.debe) as sum_debe, sum(a1.haber) as sum_haber, sum(a1.balance) as sum_balance, 
		sum(a1.importe_me) as sum_importe_me, min(a1.move_line_id) as min_line_id,
		array_agg(aml.id) as aml_ids
		from vst_diariog a1
		inner join account_move_line aml on aml.id = a1.move_line_id
		inner join account_move am on am.id = aml.move_id
		left join account_account a2 on a2.id = a1.account_id
		where (a2.analisis_documento = True) and (a1.fecha::date between $1::date and $2::date)
		group by a1.partner_id, a1.account_id, a1.td_sunat, a1.nro_comprobante
		having sum(a1.balance) <> 0
		)b1
		left join vst_diariog  b2 on b2.move_line_id = b1.min_line_id
		order by b2.partner, b2.cuenta, b2.td_sunat, b2.nro_comprobante, b2.fecha_doc) t;
	ELSIF query_type = 2 THEN 
		RETURN QUERY 
		SELECT row_number() OVER () AS id,t.*
		   FROM ( select 
		b2.periodo, 
		b2.fecha::date as fecha_con, 
		b2.libro, 
		b2.voucher, 
		b2.td_partner, 
		b2.doc_partner, 
		b2.partner, 
		b2.td_sunat,
		b2.nro_comprobante, 
		b2.fecha_doc,
		b2.fecha_ven,
		b2.cuenta,
		b2.moneda,
		b1.sum_debe as debe,
		b1.sum_haber as haber,
		b1.sum_balance as saldo_mn,
		b1.sum_importe_me as saldo_me,
		b1.aml_ids,
		b2.journal_id,
		b2.account_id,
		b2.partner_id,
		b2.move_id,
		b2.move_line_id,
		b2.company_id
		from(
		select a1.partner_id, a1.account_id, a1.td_sunat, a1.nro_comprobante,
		sum(a1.debe) as sum_debe, sum(a1.haber) as sum_haber, sum(a1.balance) as sum_balance, 
		sum(a1.importe_me) as sum_importe_me, min(a1.move_line_id) as min_line_id,
		array_agg(aml.id) as aml_ids
		from vst_diariog a1
		inner join account_move_line aml on aml.id = a1.move_line_id
		inner join account_move am on am.id = aml.move_id
		left join account_account a2 on a2.id = a1.account_id
		where (a2.analisis_documento = True) and (a1.fecha_doc::date between $1::date and $2::date)
		group by a1.partner_id, a1.account_id, a1.td_sunat, a1.nro_comprobante
		)b1
		left join vst_diariog  b2 on b2.move_line_id = b1.min_line_id
		order by b2.partner, b2.cuenta, b2.td_sunat, b2.nro_comprobante, b2.fecha_doc) t;
	ELSIF query_type = 3 THEN 
		RETURN QUERY 
		SELECT row_number() OVER () AS id,t.*
		   FROM ( select 
		b2.periodo, 
		b2.fecha::date as fecha_con, 
		b2.libro, 
		b2.voucher, 
		b2.td_partner, 
		b2.doc_partner, 
		b2.partner, 
		b2.td_sunat,
		b2.nro_comprobante, 
		b2.fecha_doc,
		b2.fecha_ven,
		b2.cuenta,
		b2.moneda,
		b1.sum_debe as debe,
		b1.sum_haber as haber,
		b1.sum_balance as saldo_mn,
		b1.sum_importe_me as saldo_me,
		b1.aml_ids,
		b2.journal_id,
		b2.account_id,
		b2.partner_id,
		b2.move_id,
		b2.move_line_id,
		b2.company_id
		from(
		select a1.partner_id, a1.account_id, a1.td_sunat, a1.nro_comprobante,
		sum(a1.debe) as sum_debe, sum(a1.haber) as sum_haber, sum(a1.balance) as sum_balance, 
		sum(a1.importe_me) as sum_importe_me, min(a1.move_line_id) as min_line_id,
		array_agg(aml.id) as aml_ids
		from vst_diariog a1
		inner join account_move_line aml on aml.id = a1.move_line_id
		inner join account_move am on am.id = aml.move_id
		left join account_account a2 on a2.id = a1.account_id
		where (a2.analisis_documento = True) and (a1.fecha_doc::date between $1::date and $2::date)
		group by a1.partner_id, a1.account_id, a1.td_sunat, a1.nro_comprobante
		having sum(a1.balance) <> 0
		)b1
		left join vst_diariog  b2 on b2.move_line_id = b1.min_line_id
		order by b2.partner, b2.cuenta, b2.td_sunat, b2.nro_comprobante, b2.fecha_doc) t;
	END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

---------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS public.get_f1_register(character varying,character varying) CASCADE;

CREATE OR REPLACE FUNCTION public.get_f1_register(
    IN period_from character varying,
    IN period_to character varying)
  RETURNS TABLE(account_id integer, mayor text, cuenta character varying, nomenclatura character varying, debe numeric, haber numeric, saldo_deudor numeric, saldo_acreedor numeric, activo numeric, pasivo numeric, perdinat numeric, ganannat numeric, perdifun numeric, gananfun numeric, rubro character varying) AS
$BODY$
BEGIN

RETURN QUERY 
    
	
	select T.account_id, left(aa.code,2) as mayor,aa.code as cuenta,aa.name as nomenclatura,T.debe,T.haber,
		   T.saldo_deudor,T.saldo_acreedor,
	case 
		when T.saldo_deudor > 0 and aa.clasification_sheet = '0'
		then T.saldo_deudor
		else 0
	end as activo,
	case 
		when T.saldo_acreedor > 0 and aa.clasification_sheet = '0'
		then T.saldo_acreedor
		else 0
	end as pasivo,
	case 
		when (T.saldo_deudor > 0 and aa.clasification_sheet = '1') or 
			 (T.saldo_deudor > 0 and aa.clasification_sheet = '3')
		then T.saldo_deudor
		else 0
	end as perdinat,
	case 
		when (T.saldo_acreedor > 0 and aa.clasification_sheet = '1') or
			 (T.saldo_acreedor > 0 and aa.clasification_sheet = '3')
		then T.saldo_acreedor
		else 0
	end as ganannat,
	case 
		when (T.saldo_deudor > 0 and aa.clasification_sheet = '2') or
			 (T.saldo_deudor > 0 and aa.clasification_sheet = '3')
		then T.saldo_deudor
		else 0
	end as perdifun,
	case 
		when (T.saldo_acreedor > 0 and aa.clasification_sheet = '2') or
			 (T.saldo_acreedor > 0 and aa.clasification_sheet = '3')
		then T.saldo_acreedor
		else 0
	end as gananfun,
	ati.name as rubro
	from(select
		aml.account_id,
		sum(aml.debit) as debe,
		sum(aml.credit) as haber,
		case 
			when sum(aml.debit) > sum(aml.credit)
			then sum(aml.debit) - sum(aml.credit)
			else 0
		end as saldo_deudor,
		case
			when sum(aml.credit) > sum(aml.debit)
			then sum(aml.credit) - sum(aml.debit)
			else 0
		end as saldo_acreedor
		from account_move_line aml
		LEFT JOIN account_move am ON am.id = aml.move_id
		where 
		(CASE
				WHEN am.fecha_special = true AND to_char(am.date::timestamp with time zone, 'mmdd'::text) = '0101'::text THEN to_char(am.date::timestamp with time zone, 'yyyy'::text) || '00'::text
				WHEN am.fecha_special = true AND to_char(am.date::timestamp with time zone, 'mmdd'::text) = '1231'::text THEN to_char(am.date::timestamp with time zone, 'yyyy'::text) || '13'::text
				ELSE to_char(am.date::timestamp with time zone, 'yyyymm'::text)
			END::integer between $1::integer and $2::integer) 
			and am.state = 'posted'
		group by aml.account_id)T
	left join account_account aa on aa.id = T.account_id
	left join account_account_type_it ati on ati.id = aa.type_it
	order by left(aa.code,2),aa.code;
                  
END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;