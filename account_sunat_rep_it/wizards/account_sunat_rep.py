# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import *
from odoo.exceptions import UserError
import base64
from io import BytesIO
import re
import uuid

from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import A4,letter
from reportlab.platypus import Table, TableStyle, Paragraph
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.utils import simpleSplit
import decimal

class AccountSunatRep(models.TransientModel):
	_name = 'account.sunat.rep'

	name = fields.Char()

	company_id = fields.Many2one('res.company',string=u'Compañia',required=True, default=lambda self: self.env.user.company_id,readonly=True)
	exercise = fields.Many2one('account.fiscalyear',string=u'Ejercicio')
	period = fields.Many2one('account.period',string='Periodo')
	number = fields.Char(string='Numero')
	type_date =  fields.Selection([('payment_date','Fecha de Pago'),('invoice_date_due','Fecha de Vencimiento')],string=u'Mostrar en base a', default='payment_date')
	opening_close = fields.Boolean(string='Considerar asientos de Cierre',default=False)
	
	@api.multi
	def get_pdf_libro_diario(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_diariog(self):
			sql = """
				SELECT 
				vsd.libro,
				vsd.move_id,
				vsd.voucher,
				vsd.fecha,
				vsd.glosa,
				vsd.cuenta,
				aa.name as des,
				vsd.debe,
				vsd.haber
				FROM vst_diariog vsd
				LEFT JOIN account_account aa ON aa.id = vsd.account_id
				WHERE (vsd.fecha between '%s' and '%s')
				order by vsd.libro,vsd.voucher
			
			"""% (datetime.strptime(self.period.date_start, '%Y-%m-%d').strftime("%Y/%m/%d"),datetime.strptime(self.period.date_stop, '%Y-%m-%d').strftime("%Y/%m/%d"))

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 12)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal-12, u"***LIBRO DIARIO DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-10,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-20, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-30, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=6><b>N° CORREL. ASNTO COD. UNI. DE OPER.</b></font>",style), 
				Paragraph("<font size=6><b>FECHA DE LA OPERACION</b></font>",style), 
				Paragraph("<font size=6><b>GLOSA O DESCRIPCION DE LA OPERACION</b></font>",style), 
				Paragraph("<font size=6><b>CUENTA CONTABLE ASOCIADA A LA OPERACION</b></font>",style), 
				'', 
				Paragraph("<font size=6><b>MOVIMIENTO</b></font>",style), 
				''],
				['','','',Paragraph("<font size=6><b>CODIGO</b></font>",style),
				Paragraph("<font size=6><b>DENOMINACION</b></font>",style),
				Paragraph("<font size=6><b>DEBE</b></font>",style),
				Paragraph("<font size=6><b>HABER</b></font>",style)]]
			t=Table(data,colWidths=size_widths, rowHeights=(20))
			t.setStyle(TableStyle([
				('SPAN',(0,0),(0,1)),
				('SPAN',(1,0),(1,1)),
				('SPAN',(2,0),(2,1)),
				('SPAN',(3,0),(4,0)),
				('SPAN',(5,0),(6,0)),
				('GRID',(0,0),(-1,-1), 1, colors.black),
				('ALIGN',(0,0),(-1,-1),'LEFT'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-85)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-95
			else:
				return pagina,posactual-valor

		width ,height  = A4  # 595 , 842
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "libro_diario_rep.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= A4 )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [60,45,130,40,140,60,60]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-43

		c.setFont("Helvetica", 8)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_diariog(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		libro = ''
		voucher = ''
		sum_debe = 0
		sum_haber = 0

		for i in res:
			first_pos = 30
			
			c.setFont("Helvetica-Bold", 8)
			if cont == 0:
				libro = i['libro']
				voucher = i['voucher']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,libro)
				pos_inicial -= 15

			
			if libro != i['libro']:
				c.line(440,pos_inicial+3,565,pos_inicial+3)
				c.drawString( 350 ,pos_inicial-10,'TOTAL:')
				c.drawRightString( 505,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_debe)) )
				c.drawRightString( 565 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_haber)))
				sum_debe = 0
				sum_haber = 0
				pos_inicial -= 25

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				
				libro = i['libro']
				voucher = i['voucher']
				c.drawString( first_pos+2 ,pos_inicial,libro)
				pos_inicial -= 15

			if voucher != i['voucher']:
				c.line(440,pos_inicial+3,565,pos_inicial+3)
				c.drawString( 350 ,pos_inicial-10,'TOTAL:')
				c.drawRightString( 505,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_debe)) )
				c.drawRightString( 565 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_haber)))
				sum_debe = 0
				sum_haber = 0
				pos_inicial -= 15

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

				voucher = i['voucher']
				pos_inicial -= 10


			c.setFont("Helvetica", 6)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['voucher'] if i['voucher'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha'] if i['fecha'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['glosa'] if i['glosa'] else '',130) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['cuenta'] if i['cuenta'] else '',50) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['des'] if i['des'] else '',150) )
			first_pos += size_widths[4]

			c.drawRightString( first_pos+60 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['debe'])) )
			sum_debe += i['debe']
			first_pos += size_widths[5]

			c.drawRightString( first_pos+60 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['haber'])))
			sum_haber += i['haber']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold", 8)
		c.line(440,pos_inicial+3,565,pos_inicial+3)
		c.drawString( 350 ,pos_inicial-10,'TOTAL:')
		c.drawRightString( 505,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_debe)) )
		c.drawRightString( 565 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_haber)))

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DIARIO.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}


	@api.multi
	def get_pdf_libro_mayor(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_mayor(self):
			sql = """
				SELECT gmd.cuenta, aa.name AS name_cuenta,
				gmd.libro,gmd.td_sunat, gmd.nro_comprobante,
				gmd.voucher, 
				to_char(gmd.fecha::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha,
				gmd.glosa,
				gmd.debe, gmd.haber
				from get_mayor_detalle13('%s','%s') gmd
				LEFT JOIN account_account aa ON aa.code = gmd.cuenta
			
			""" % (datetime.strptime(self.period.date_start, '%Y-%m-%d').strftime("%Y/%m/%d"),datetime.strptime(self.period.date_stop, '%Y-%m-%d').strftime("%Y/%m/%d"))

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 12)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal-12, u"***LIBRO MAYOR DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-10,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-20, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-30, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=6><b>LIBRO</b></font>",style), 
				Paragraph("<font size=6><b>TD</b></font>",style), 
				Paragraph("<font size=6><b>NUMERO</b></font>",style), 
				Paragraph("<font size=6><b>NRO CORRELATIVO</b></font>",style), 
				Paragraph("<font size=6><b>FECHA</b></font>",style), 
				Paragraph("<font size=6><b>DESCRIPCION O GLOSA</b></font>",style),
				Paragraph("<font size=6><b>SALDOS Y MOVIMIENTOS</b></font>",style), 
				''],
				['','','','','','',
				Paragraph("<font size=6><b>DEUDOR</b></font>",style),
				Paragraph("<font size=6><b>ACREEDOR</b></font>",style)]]
			t=Table(data,colWidths=size_widths, rowHeights=(20))
			t.setStyle(TableStyle([
				('SPAN',(0,0),(0,1)),
				('SPAN',(1,0),(1,1)),
				('SPAN',(2,0),(2,1)),
				('SPAN',(3,0),(3,1)),
				('SPAN',(4,0),(4,1)),
				('SPAN',(5,0),(5,1)),
				('SPAN',(6,0),(7,0)),
				('GRID',(0,0),(-1,-1), 1, colors.black),
				('ALIGN',(0,0),(-1,-1),'LEFT'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-85)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-95
			else:
				return pagina,posactual-valor

		width ,height  = A4  # 595 , 842
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "libro_mayor_rep.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= A4 )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [60,35,60,70,50,140,60,60]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-43

		c.setFont("Helvetica", 8)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_mayor(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		cuenta = ''
		sum_debe = 0
		sum_haber = 0
		saldo_debe = 0
		saldo_haber = 0

		for i in res:
			first_pos = 30
			
			c.setFont("Helvetica-Bold", 8)
			if cont == 0:
				cuenta = i['cuenta']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Cod. Cuenta: ' + cuenta + '' + i['name_cuenta'])
				pos_inicial -= 15

			if cuenta != i['cuenta']:
				c.line(440,pos_inicial+3,565,pos_inicial+3)
				c.drawString( 350 ,pos_inicial-10,'TOTAL CUENTA:')
				c.drawRightString( 505,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_debe)) )
				c.drawRightString( 565 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_haber)))
				pos_inicial -= 10

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 8)

				c.line(440,pos_inicial+3,565,pos_inicial+3)
				c.drawString( 350 ,pos_inicial-10,'SALDO FINAL:')
				saldo_debe = (sum_debe - sum_haber) if sum_debe > sum_haber else 0
				saldo_haber = 0 if sum_debe > sum_haber else (sum_haber - sum_debe)

				c.drawRightString( 505,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_debe)) )
				c.drawRightString( 565 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_haber)))
				pos_inicial -= 20

				c.line(440,pos_inicial+3,565,pos_inicial+3)

				sum_debe = 0
				sum_haber = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 8)

				cuenta = i['cuenta']
				c.drawString( first_pos+2 ,pos_inicial,'Cod. Cuenta: ' + cuenta + '' + i['name_cuenta'])
				pos_inicial -= 15


			c.setFont("Helvetica", 6)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['libro'] if i['libro'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',50) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['voucher'] if i['voucher'] else '',50) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha'] if i['fecha'] else '',50) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['glosa'] if i['glosa'] else '',150) )
			first_pos += size_widths[5]

			c.drawRightString( first_pos+60 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['debe'])) )
			sum_debe += i['debe']
			first_pos += size_widths[6]

			c.drawRightString( first_pos+60 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['haber'])))
			sum_haber += i['haber']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold", 8)
		c.line(440,pos_inicial+3,565,pos_inicial+3)
		c.drawString( 350 ,pos_inicial-10,'TOTAL CUENTA:')
		c.drawRightString( 505,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_debe)) )
		c.drawRightString( 565 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_haber)))
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 8)

		c.line(440,pos_inicial+3,565,pos_inicial+3)
		c.drawString( 350 ,pos_inicial-10,'SALDO FINAL:')
		saldo_debe = (sum_debe - sum_haber) if sum_debe > sum_haber else 0
		saldo_haber = 0 if sum_debe > sum_haber else (sum_haber - sum_debe)

		c.drawRightString( 505,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_debe)) )
		c.drawRightString( 565 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_haber)))
		pos_inicial -= 20

		c.line(440,pos_inicial+3,565,pos_inicial+3)

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO MAYOR.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_compras(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_compras(self):
			sql = """
				SELECT 
				voucher,
				to_char(gc.fechaemision::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_e,
				to_char(gc.fechavencimiento::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_v,
				gc.tipodocumento as td,gc.serie,gc.anio,gc.numero,
				gc.tdp,gc.ruc as docp, gc.razonsocial as namep,
				coalesce(gc.bioge,0) as base1,coalesce(gc.igva,0) as igv1,
				coalesce(gc.biogeng,0) as base2,coalesce(gc.igvb,0) as igv2,
				coalesce(gc.biong,0) as base3,coalesce(gc.igvc,0) as igv3,
				coalesce(gc.cng,0) as cng,coalesce(gc.isc,0) as isc,coalesce(gc.otros,0) as otros,0 as icbper,coalesce(gc.total,0) as total,
				CASE
					WHEN rp.is_resident = True THEN gc.numero ELSE ''
				END AS nro_no_dom,
				ai.voucher_number as comp_det,
				to_char(ai.date_detraccion::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_det,
				coalesce(gc.tc,1) as currency_rate,
				to_char(gc.fechadm::timestamp with time zone, 'yyyy/mm/dd'::text) as f_doc_m,
				gc.td as td_doc_m,gc.seried as serie_m,gc.numerodd as numero_m
				FROM get_compra_1_1_1(periodo_num('%s'),periodo_num('%s')) gc
				LEFT JOIN account_invoice ai on ai.move_id = gc.am_id
				LEFT JOIN res_partner rp on rp.id = ai.partner_id
				order by tipodocumento, voucher
			""" % (self.period.code,self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 12)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal-12, u"***REGISTRO DE COMPRAS DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-10,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-20, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-30, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 4
			style.alignment= 1

			data= [[Paragraph("<font size=4.5><b>N° Vou.</b></font>",style), 
				Paragraph("<font size=4.5><b>F. Emision</b></font>",style), 
				Paragraph("<font size=4.5><b>F. Venc</b></font>",style), 
				Paragraph("<font size=4.5><b>Comprobante de pago</b></font>",style), 
				'', 
				'',
				Paragraph("<font size=4.5><b>Nº Comprobante Pago</b></font>",style), 
				Paragraph("<font size=4.5><b>Informacion del Proveedor</b></font>",style),
				'','',
				Paragraph("<font size=4.5><b>Adq. Grav. dest. a Oper. Grav. y/o Exp.</b></font>",style),
				'',
				Paragraph("<font size=4.5><b>Adq. Grav. dest. a Oper. Grav. y/o Exp. y a Oper.</b></font>",style),
				'',
				Paragraph("<font size=4.5><b>Adq. Grav. dest. a Oper. No Gravadas</b></font>",style),
				'',
				Paragraph("<font size=4.5><b>Valor de Adq no Gravadas</b></font>",style),
				Paragraph("<font size=4.5><b>I.S.C.</b></font>",style),
				Paragraph("<font size=4.5><b>ICBPER</b></font>",style),
				Paragraph("<font size=4.5><b>Otros Tributos</b></font>",style),
				Paragraph("<font size=4.5><b>Importe Total</b></font>",style),
				Paragraph("<font size=4.5><b>Nº Comp. de pago emitido por sujeto no domiciliado </b></font>",style),
				Paragraph("<font size=4.5><b>Constancia de Deposito de Detracción</b></font>",style),
				'',
				Paragraph("<font size=4.5><b>T.C.</b></font>",style),
				Paragraph("<font size=4.5><b>Referencia del Documento</b></font>",style),
				'','',''],
				['','','',
				Paragraph("<font size=4.5><b>T/D</b></font>",style),
				Paragraph("<font size=4.5><b>Serie</b></font>",style),
				Paragraph("<font size=4.5><b>Año de Emision DUA o DSI</b></font>",style),
				'',
				Paragraph("<font size=4.5><b>Doc. de Identidad</b></font>",style),
				'',
				Paragraph("<font size=4.5><b>Apellidos y nombres o Razon Social</b></font>",style),
				'','','','','','','','','','','','','','','',
				Paragraph("<font size=4.5><b>Fecha</b></font>",style),
				Paragraph("<font size=4.5><b>T/D</b></font>",style),
				Paragraph("<font size=4.5><b>Serie</b></font>",style),
				Paragraph("<font size=4.5><b>Numero Comprobante Doc Numero de pago</b></font>",style)],
				['','','','','','','',
				Paragraph("<font size=4.5><b>Doc</b></font>",style),
				Paragraph("<font size=4.5><b>Numero</b></font>",style),
				'',
				Paragraph("<font size=4.5><b>Base Imp.</b></font>",style),
				Paragraph("<font size=4.5><b>I.G.V.</b></font>",style),
				Paragraph("<font size=4.5><b>Base Imp.</b></font>",style),
				Paragraph("<font size=4.5><b>I.G.V.</b></font>",style),
				Paragraph("<font size=4.5><b>Base Imp.</b></font>",style),
				Paragraph("<font size=4.5><b>I.G.V.</b></font>",style),
				'','','','','','',
				Paragraph("<font size=4.5><b>Numero</b></font>",style),
				Paragraph("<font size=4.5><b>Fecha Emi.</b></font>",style),
				'','','','',''
				]]
			t=Table(data,colWidths=size_widths, rowHeights=(14))
			t.setStyle(TableStyle([
				('SPAN',(0,0),(0,2)),
				('SPAN',(1,0),(1,2)),
				('SPAN',(2,0),(2,2)),
				('SPAN',(3,0),(5,0)),
				('SPAN',(3,1),(3,2)),
				('SPAN',(4,1),(4,2)),
				('SPAN',(5,1),(5,2)),
				('SPAN',(6,0),(6,2)),
				('SPAN',(7,0),(9,0)),
				('SPAN',(7,1),(8,1)),
				('SPAN',(9,1),(9,2)),
				('SPAN',(10,0),(11,1)),
				('SPAN',(12,0),(13,1)),
				('SPAN',(14,0),(15,1)),
				('SPAN',(16,0),(16,2)),
				('SPAN',(17,0),(17,2)),
				('SPAN',(18,0),(18,2)),
				('SPAN',(19,0),(19,2)),
				('SPAN',(20,0),(20,2)),
				('SPAN',(21,0),(21,2)),
				('SPAN',(22,0),(23,1)),
				('SPAN',(24,0),(24,2)),
				('SPAN',(25,0),(28,0)),
				('SPAN',(25,1),(25,2)),
				('SPAN',(26,1),(26,2)),
				('SPAN',(27,1),(27,2)),
				('SPAN',(28,1),(28,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-110
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "compra_rep.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [30,28,28,14,16,25,32,15,30,67,29,29,27,25,25,26,24,23,24,24,34,38,29,30,20,25,14,18,45]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-60

		c.setFont("Helvetica", 4)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_compras(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		td = ''
		base1, base2, base3, igv1, igv2, igv3, cng, isc, otros, icbper, total = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
		total_base1, total_base2, total_base3, total_igv1, total_igv2, total_igv3, total_cng, total_isc, total_icbper, total_otros, total_total = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

		for i in res:
			first_pos = 30
			
			c.setFont("Helvetica-Bold", 4)
			if cont == 0:
				td = i['td']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Tipo Doc.: ' + td )
				pos_inicial -= 10

			if td != i['td']:
				c.line(314,pos_inicial+3,598,pos_inicial+3)
				c.drawString( 270 ,pos_inicial-5,'TOTALES:')
				c.drawRightString( 342,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % base1)) )
				c.drawRightString( 371 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % igv1)))
				c.drawRightString( 400 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % base2)))
				c.drawRightString( 427 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % igv2)))
				c.drawRightString( 452 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % base3)))
				c.drawRightString( 477 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % igv3)))
				c.drawRightString( 503 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % cng)))
				c.drawRightString( 527 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % isc)))
				c.drawRightString( 550 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % icbper)))
				c.drawRightString( 574 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % otros)))
				c.drawRightString( 598 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total)))

				base1, base2, base3, igv1, igv2, igv3, cng, isc, otros, icbper, total = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 4)

				td = i['td']
				c.drawString( first_pos+2 ,pos_inicial,'Tipo Doc.: ' + td )
				pos_inicial -= 10


			c.setFont("Helvetica", 4)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['voucher'] if i['voucher'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_e'] if i['fecha_e'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_v'] if i['fecha_v'] else '',50) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td'] if i['td'] else '',50) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['serie'] if i['serie'] else '',50) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['anio'] if i['anio'] else '',50) )
			first_pos += size_widths[5]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['numero'] if i['numero'] else '',50) )
			first_pos += size_widths[6]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['tdp'] if i['tdp'] else '',50) )
			first_pos += size_widths[7]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['docp'] if i['docp'] else '',50) )
			first_pos += size_widths[8]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['namep'] if i['namep'] else '',130) )
			first_pos += size_widths[9]

			c.drawRightString( 342 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['base1'])) )
			base1 += i['base1']
			total_base1 += i['base1']
			first_pos += size_widths[10]

			c.drawRightString( 371 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['igv1'])) )
			igv1 += i['igv1']
			total_igv1 += i['igv1']
			first_pos += size_widths[11]

			c.drawRightString( 400 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['base2'])) )
			base2 += i['base2']
			total_base2 += i['base2']
			first_pos += size_widths[12]

			c.drawRightString( 427 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['igv2'])) )
			igv2 += i['igv2']
			total_igv2 += i['igv2']
			first_pos += size_widths[13]

			c.drawRightString( 452 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['base3'])) )
			base3 += i['base3']
			total_base3 += i['base3']
			first_pos += size_widths[14]

			c.drawRightString( 477 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['igv3'])) )
			igv3 += i['igv3']
			total_igv3 += i['igv3']
			first_pos += size_widths[15]

			c.drawRightString( 503 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['cng'])) )
			cng += i['cng']
			total_cng += i['cng']
			first_pos += size_widths[16]

			c.drawRightString( 527 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['isc'])) )
			isc += i['isc']
			total_isc += i['isc']
			first_pos += size_widths[17]

			c.drawRightString( 550 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['icbper'])) )
			icbper += i['icbper']
			total_icbper += i['icbper']
			first_pos += size_widths[18]

			c.drawRightString( 574 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['otros'])) )
			otros += i['otros']
			total_otros += i['otros']
			first_pos += size_widths[19]

			c.drawRightString( 598 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['total'])) )
			total += i['total']
			total_total += i['total']
			first_pos += size_widths[20]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_no_dom'] if i['nro_no_dom'] else '',50) )
			first_pos += size_widths[21]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['comp_det'] if i['comp_det'] else '',50) )
			first_pos += size_widths[22]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_det'] if i['fecha_det'] else '',50) )
			first_pos += size_widths[23]

			c.drawRightString( first_pos+18 ,pos_inicial,'{:,.4f}'.format(decimal.Decimal ("%0.4f" % i['currency_rate'])) )
			first_pos += size_widths[24]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['f_doc_m'] if i['f_doc_m'] else '',50) )
			first_pos += size_widths[25]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_doc_m'] if i['td_doc_m'] else '',50) )
			first_pos += size_widths[26]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['serie_m'] if i['serie_m'] else '',50) )
			first_pos += size_widths[27]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['numero_m'] if i['numero_m'] else '',50) )

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold", 4)
		c.line(314,pos_inicial+3,598,pos_inicial+3)
		c.drawString( 270 ,pos_inicial-5,'TOTALES:')
		c.drawRightString( 342 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % base1)) )
		c.drawRightString( 371 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % igv1)))
		c.drawRightString( 400 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % base2)))
		c.drawRightString( 427 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % igv2)))
		c.drawRightString( 452 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % base3)))
		c.drawRightString( 477 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % igv3)))
		c.drawRightString( 503 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % cng)))
		c.drawRightString( 527 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % isc)))
		c.drawRightString( 550 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % icbper)))
		c.drawRightString( 574 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % otros)))
		c.drawRightString( 598 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total)))
		
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 4)

		c.line(314,pos_inicial+3,598,pos_inicial+3)

		c.drawString( 270 ,pos_inicial-5,'TOTAL GENERAL:')
		c.drawRightString( 342 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_base1)) )
		c.drawRightString( 371 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_igv1)))
		c.drawRightString( 400 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_base2)))
		c.drawRightString( 427 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_igv2)))
		c.drawRightString( 452 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_base3)))
		c.drawRightString( 477 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_igv3)))
		c.drawRightString( 503 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_cng)))
		c.drawRightString( 527 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_isc)))
		c.drawRightString( 550 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_icbper)))
		c.drawRightString( 574 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_otros)))
		c.drawRightString( 598 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_total)))

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.line(314,pos_inicial+3,598,pos_inicial+3)	

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'REGISTRO DE COMPRAS.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_ventas(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_ventas(self):
			sql = """
				SELECT 
				voucher,
				to_char(fechaemision::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_e,
				to_char(fechavencimiento::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_v,
				tipodocumento as td,
				serie,
				numero,
				tipodoc as tdp,
				numdoc as docp,
				partner as namep,
				coalesce(valorexp,0) as exp,
				coalesce(baseimp,0) as venta_g,
				coalesce(exonerado,0) as exo,
				coalesce(inafecto,0) as inaf,
				coalesce(isc,0) as isc_v,
				coalesce(igv,0) as igv_v,
				coalesce(otros,0) as otros_v,
				0 as icbper,
				total,
				coalesce(tipodecambio,0) as currency_rate,
				to_char(fechadm::timestamp with time zone, 'yyyy/mm/dd'::text) as f_doc_m,
				tipodocmod as td_doc_m,
				seriemod as serie_m,
				numeromod as numero_m
				FROM get_venta_1_1_1(periodo_num('%s'),periodo_num('%s')) 
				order by tipodocumento, serie, numero
			""" % (self.period.code,self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 12)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal-12, u"***REGISTRO DE VENTAS E INGRESOS DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-10,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-20, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-30, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 4
			style.alignment= 1

			data= [[Paragraph("<font size=4.5><b>N° Vou.</b></font>",style), 
				Paragraph("<font size=4.5><b>F. Emision</b></font>",style), 
				Paragraph("<font size=4.5><b>F. Venc</b></font>",style), 
				Paragraph("<font size=4.5><b>Comprobante de pago</b></font>",style), 
				'', 
				'',
				Paragraph("<font size=4.5><b>Informacion del Cliente</b></font>",style),
				'','',
				Paragraph("<font size=4.5><b>Valor Facturado de la Exportacion</b></font>",style),
				Paragraph("<font size=4.5><b>Base Imp. de la Ope. Gravada</b></font>",style),
				Paragraph("<font size=4.5><b>Imp. Total de la Operacion</b></font>",style),
				'',
				Paragraph("<font size=4.5><b>I.S.C.</b></font>",style),
				Paragraph("<font size=4.5><b>I.G.V. y/o IPM</b></font>",style),
				Paragraph("<font size=4.5><b>ICBPER</b></font>",style),
				Paragraph("<font size=4.5><b>Otros Tributos y cargos que no forman parte de la base imponible</b></font>",style),
				Paragraph("<font size=4.5><b>Importe Total</b></font>",style),
				Paragraph("<font size=4.5><b>T.C.</b></font>",style),
				Paragraph("<font size=4.5><b>Referencia del Comprobante</b></font>",style),
				'','',''],
				['','','',
				Paragraph("<font size=4.5><b>T/D</b></font>",style),
				Paragraph("<font size=4.5><b>Serie</b></font>",style),
				Paragraph("<font size=4.5><b>Numero</b></font>",style),
				Paragraph("<font size=4.5><b>Doc. de Identidad</b></font>",style),
				'',
				Paragraph("<font size=4.5><b>Apellidos y nombres o Razon Social</b></font>",style),
				'','',
				Paragraph("<font size=4.5><b>Exonerada</b></font>",style),
				Paragraph("<font size=4.5><b>Inafecta</b></font>",style),
				'','','','','','',
				Paragraph("<font size=4.5><b>Fecha</b></font>",style),
				Paragraph("<font size=4.5><b>T/D</b></font>",style),
				Paragraph("<font size=4.5><b>Serie</b></font>",style),
				Paragraph("<font size=4.5><b>Numero</b></font>",style)],
				['','','','','','',
				Paragraph("<font size=4.5><b>Doc</b></font>",style),
				Paragraph("<font size=4.5><b>Numero</b></font>",style),
				'','','','','','','','','','','','','','','']]
			t=Table(data,colWidths=size_widths, rowHeights=(14))
			t.setStyle(TableStyle([
				('SPAN',(0,0),(0,2)),
				('SPAN',(1,0),(1,2)),
				('SPAN',(2,0),(2,2)),
				('SPAN',(3,0),(5,0)),
				('SPAN',(3,1),(3,2)),
				('SPAN',(4,1),(4,2)),
				('SPAN',(5,1),(5,2)),
				('SPAN',(6,0),(8,0)),
				('SPAN',(6,1),(7,1)),
				('SPAN',(8,1),(8,2)),
				('SPAN',(9,0),(9,2)),
				('SPAN',(10,0),(10,2)),
				('SPAN',(11,0),(12,0)),
				('SPAN',(11,1),(11,2)),
				('SPAN',(12,1),(12,2)),
				('SPAN',(13,0),(13,2)),
				('SPAN',(14,0),(14,2)),
				('SPAN',(15,0),(15,2)),
				('SPAN',(16,0),(16,2)),
				('SPAN',(17,0),(17,2)),
				('SPAN',(18,0),(18,2)),
				('SPAN',(19,0),(22,0)),
				('SPAN',(19,1),(19,2)),
				('SPAN',(20,1),(20,2)),
				('SPAN',(21,1),(21,2)),
				('SPAN',(22,1),(22,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-110
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "venta_rep.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [32,28,28,14,18,32,15,33,100,40,40,30,30,40,40,40,40,40,20,28,14,18,45]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-60

		c.setFont("Helvetica", 4)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_ventas(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		td = ''
		exp, venta_g, exo, inaf, isc_v, igv_v, icbper, otros_v, total = 0, 0, 0, 0, 0, 0, 0, 0, 0
		total_exp, total_venta_g, total_exo, total_inaf, total_isc_v, total_igv_v, total_icbper, total_otros_v, total_total = 0, 0, 0, 0, 0, 0, 0, 0, 0

		for i in res:
			first_pos = 30
			
			c.setFont("Helvetica-Bold", 4)
			if cont == 0:
				td = i['td']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Tipo Doc.: ' + td )
				pos_inicial -= 10

			if td != i['td']:
				c.line(330,pos_inicial+3,667,pos_inicial+3)
				c.drawString( 280 ,pos_inicial-5,'TOTALES:')
				c.drawRightString( 367 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % exp)) )
				c.drawRightString( 407 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % venta_g)))
				c.drawRightString( 437 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % exo)))
				c.drawRightString( 467 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % inaf)))
				c.drawRightString( 507 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % isc_v)))
				c.drawRightString( 547 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % igv_v)))
				c.drawRightString( 587 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % icbper)))
				c.drawRightString( 627 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % otros_v)))
				c.drawRightString( 667 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total)))

				exp, venta_g, exo, inaf, isc_v, igv_v, icbper, otros_v, total = 0, 0, 0, 0, 0, 0, 0, 0, 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 4)

				td = i['td']
				c.drawString( first_pos+2 ,pos_inicial,'Tipo Doc.: ' + td )
				pos_inicial -= 10


			c.setFont("Helvetica", 4)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['voucher'] if i['voucher'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_e'] if i['fecha_e'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_v'] if i['fecha_v'] else '',50) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td'] if i['td'] else '',50) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['serie'] if i['serie'] else '',50) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['numero'] if i['numero'] else '',50) )
			first_pos += size_widths[5]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['tdp'] if i['tdp'] else '',50) )
			first_pos += size_widths[6]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['docp'] if i['docp'] else '',50) )
			first_pos += size_widths[7]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['namep'] if i['namep'] else '',190) )
			first_pos += size_widths[8]

			c.drawRightString( first_pos+37 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['exp'])) )
			exp += i['exp']
			total_exp += i['exp']
			first_pos += size_widths[9]

			c.drawRightString( first_pos+37 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['venta_g'])) )
			venta_g += i['venta_g']
			total_venta_g += i['venta_g']
			first_pos += size_widths[10]

			c.drawRightString( first_pos+27 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['exo'])) )
			exo += i['exo']
			total_exo += i['exo']
			first_pos += size_widths[11]

			c.drawRightString( first_pos+27 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['inaf'])) )
			inaf += i['inaf']
			total_inaf += i['inaf']
			first_pos += size_widths[12]

			c.drawRightString( first_pos+37 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['isc_v'])) )
			isc_v += i['isc_v']
			total_isc_v += i['isc_v']
			first_pos += size_widths[13]

			c.drawRightString( first_pos+37 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['igv_v'])) )
			igv_v += i['igv_v']
			total_igv_v += i['igv_v']
			first_pos += size_widths[14]

			c.drawRightString( first_pos+37 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['icbper'])) )
			icbper += i['icbper']
			total_icbper += i['icbper']
			first_pos += size_widths[15]

			c.drawRightString( first_pos+37 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['otros_v'])) )
			otros_v += i['otros_v']
			total_otros_v += i['otros_v']
			first_pos += size_widths[16]

			c.drawRightString( first_pos+37 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['total'])) )
			total += i['total']
			total_total += i['total']
			first_pos += size_widths[17]

			c.drawRightString( first_pos+17 ,pos_inicial,'{:,.4f}'.format(decimal.Decimal ("%0.4f" % i['currency_rate'])) )
			first_pos += size_widths[18]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['f_doc_m'] if i['f_doc_m'] else '',50) )
			first_pos += size_widths[19]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_doc_m'] if i['td_doc_m'] else '',50) )
			first_pos += size_widths[20]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['serie_m'] if i['serie_m'] else '',50) )
			first_pos += size_widths[21]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['numero_m'] if i['numero_m'] else '',50) )

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold", 4)
		c.line(330,pos_inicial+3,667,pos_inicial+3)
		c.drawString( 280 ,pos_inicial-5,'TOTALES:')
		c.drawRightString( 367 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % exp)) )
		c.drawRightString( 407 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % venta_g)))
		c.drawRightString( 437 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % exo)))
		c.drawRightString( 467 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % inaf)))
		c.drawRightString( 507 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % isc_v)))
		c.drawRightString( 547 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % igv_v)))
		c.drawRightString( 587 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % icbper)))
		c.drawRightString( 627 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % otros_v)))
		c.drawRightString( 667 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total)))
		
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 4)

		c.line(330,pos_inicial+3,667,pos_inicial+3)

		c.drawString( 280 ,pos_inicial-5,'TOTAL GENERAL:')
		c.drawRightString( 367 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_exp)) )
		c.drawRightString( 407 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_venta_g)))
		c.drawRightString( 437 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_exo)))
		c.drawRightString( 467 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_inaf)))
		c.drawRightString( 507 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_isc_v)))
		c.drawRightString( 547 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_igv_v)))
		c.drawRightString( 587 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_icbper)))
		c.drawRightString( 627 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_otros_v)))
		c.drawRightString( 667 ,pos_inicial-5,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total_total)))

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.line(330,pos_inicial+3,667,pos_inicial+3)	

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'REGISTRO DE VENTAS.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_libro_caja(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_caja(self):
			param = self.env['main.parameter'].search([],limit=1)
			if not param.codes_report_sunat_caja:
				raise UserError('Falta completar el campo "Prefijos Reporteador Sunat Caja" en Parametros Principales de Contabilidad')
			sql = """
				SELECT 
				T.cuentacode as cuenta,
				T.cuentaname as name_cuenta,
				T.voucher,
				to_char(T.fechaemision::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha,
				T.glosa,
				T.debe,
				T.haber
				FROM get_cajabanco_with_saldoinicial(periodo_num('%s'),periodo_num('%s')) T
				WHERE LEFT(T.cuentacode,3) in (%s)
			
			""" % (self.period.code,self.period.code,param.codes_report_sunat_caja)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 9)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO CAJA Y BANCOS -DETALLE DE LOS MOVIMIENTOS DE EFECTIVO DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-12, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-22,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-32, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-42, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>NUMERO DE VOUCHER</b></font>",style), 
				Paragraph("<font size=8><b>FECHA DE OPERACION</b></font>",style), 
				Paragraph("<font size=8><b>DESCRIPCION DE LA OPERACION</b></font>",style), 
				Paragraph("<font size=8><b>SALDOS Y MOVIMIENTOS</b></font>",style),
				''],
				['','','',
				Paragraph("<font size=8><b>DEUDOR</b></font>",style),
				Paragraph("<font size=8><b>ACREEDOR</b></font>",style)]]
			t=Table(data,colWidths=size_widths, rowHeights=(20))
			t.setStyle(TableStyle([
				('SPAN',(0,0),(0,1)),
				('SPAN',(1,0),(1,1)),
				('SPAN',(2,0),(2,1)),
				('SPAN',(3,0),(4,0)),
				('GRID',(0,0),(-1,-1), 1, colors.black),
				('ALIGN',(0,0),(-1,-1),'LEFT'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-95
			else:
				return pagina,posactual-valor

		width ,height  = A4  # 595 , 842
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "libro_caja.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= A4 )
		pos_inicial = hReal-50
		pagina = 1

		size_widths = [80,80,235,70,70]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-55

		c.setFont("Helvetica", 8)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_caja(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		cuenta = ''
		sum_debe = 0
		sum_haber = 0
		saldo_debe = 0
		saldo_haber = 0

		for i in res:
			first_pos = 30
			
			c.setFont("Helvetica-Bold", 10)
			if cont == 0:
				cuenta = i['cuenta']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Cuenta: ' + cuenta + ' ' + i['name_cuenta'])
				pos_inicial -= 15

			if cuenta != i['cuenta']:
				c.setFont("Helvetica-Bold", 9)
				c.line(425,pos_inicial+3,565,pos_inicial+3)
				c.drawString( 350 ,pos_inicial-10,'TOTALES:')
				c.drawRightString( 495,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_debe)) )
				c.drawRightString( 565 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_haber)))
				pos_inicial -= 10

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				c.line(425,pos_inicial+3,565,pos_inicial+3)
				c.drawString( 350 ,pos_inicial-10,'SALDO FINAL:')
				saldo_debe = (sum_debe - sum_haber) if sum_debe > sum_haber else 0
				saldo_haber = 0 if sum_debe > sum_haber else (sum_haber - sum_debe)

				c.drawRightString( 495,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_debe)) )
				c.drawRightString( 565 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_haber)))
				pos_inicial -= 20

				c.line(425,pos_inicial+3,565,pos_inicial+3)

				sum_debe = 0
				sum_haber = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 10)

				cuenta = i['cuenta']
				c.drawString( first_pos+2 ,pos_inicial,'Cuenta: ' + cuenta + ' ' + i['name_cuenta'])
				pos_inicial -= 15


			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['voucher'] if i['voucher'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha'] if i['fecha'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['glosa'] if i['glosa'] else '',150) )
			first_pos += size_widths[2]

			c.drawRightString( first_pos+70 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['debe'])) )
			sum_debe += i['debe']
			first_pos += size_widths[3]

			c.drawRightString( first_pos+70 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['haber'])))
			sum_haber += i['haber']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold", 9)
		c.line(425,pos_inicial+3,565,pos_inicial+3)
		c.drawString( 350 ,pos_inicial-10,'TOTALES:')
		c.drawRightString( 495,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_debe)) )
		c.drawRightString( 565 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_haber)))
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(425,pos_inicial+3,565,pos_inicial+3)
		c.drawString( 350 ,pos_inicial-10,'SALDO FINAL:')
		saldo_debe = (sum_debe - sum_haber) if sum_debe > sum_haber else 0
		saldo_haber = 0 if sum_debe > sum_haber else (sum_haber - sum_debe)

		c.drawRightString( 495,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_debe)) )
		c.drawRightString( 565 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_haber)))
		pos_inicial -= 20

		c.line(425,pos_inicial+3,565,pos_inicial+3)

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO CAJA.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_libro_banco(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_banco(self):
			sql_filter = " AND (am.fecha_special <> TRUE OR to_char(gb.fecha::timestamp with time zone, 'mmdd'::text) <> '1231')"
			if self.opening_close:
				sql_filter = ""

			param = self.env['main.parameter'].search([],limit=1)
			if not param.codes_report_sunat_bank:
				raise UserError('Falta completar el campo "Prefijos Reporteador Sunat Banco" en Parametros Principales de Contabilidad')
			sql = """
				SELECT 		
				aa.cashbank_code as code_bank,	
				aa.cashbank_number as account_number,
				gb.cuenta,
				aa.name as nombre_cuenta,
				gb.libro,
				gb.voucher,
				to_char(gb.fecha::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha,
				emp.code as medio_pago,
				gb.glosa,
				gb.partner,
				gb.nro_comprobante,
				gb.debe,
				gb.haber		
				FROM get_bancos13('%s','%s') gb
				LEFT JOIN account_account aa ON aa.id = gb.account_id
				LEFT JOIN account_move am ON am.id = gb.move_id
				LEFT JOIN einvoice_means_payment emp ON emp.id = am.means_payment_it
				WHERE LEFT(gb.cuenta,3) IN (%s)	%s
				ORDER BY gb.cuenta,gb.fecha
			
			""" % (datetime.strptime(self.period.date_start, '%Y-%m-%d').strftime("%Y/%m/%d"),
			datetime.strptime(self.period.date_stop, '%Y-%m-%d').strftime("%Y/%m/%d"),
			param.codes_report_sunat_bank, sql_filter)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 12)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO CAJA Y BANCOS -DETALLE DE LOS MOVIMIENTOS DE LA CUENTA CORRIENTE DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=7.5><b>N° VOU.</b></font>",style), 
				Paragraph("<font size=7.5><b>N° CORRELATIVO DEL LIBRO DIARIO</b></font>",style), 
				Paragraph("<font size=7.5><b>FECHA OPERACION</b></font>",style), 
				Paragraph("<font size=8><b>OPERACIONES BANCARIAS</b></font>",style),
				'', 
				'',
				'',
				Paragraph("<font size=8><b>SALDOS Y MOVIMIENTOS</b></font>",style),
				''],
				['','','',
				Paragraph("<font size=7.5>MEDIO DE PAGO</font>",style),
				Paragraph("<font size=7.5>DESC. OPERACION</font>",style),
				Paragraph("<font size=7.5>APELLIDOS Y NOMBRES, DENOMINACION O RAZON SOCIAL</font>",style),
				Paragraph("<font size=7>N. TRANSACCIÓN BANCARIA DE DOCUMENTOS O DE CONTROL INTERNO DE LA OPERACIÓN</font>",style),
				Paragraph("<font size=7.5><b>DEUDOR</b></font>",style),
				Paragraph("<font size=7.5><b>ACREEDOR</b></font>",style)]]
			t=Table(data,colWidths=size_widths, rowHeights=[18,30])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(0,1)),
				('SPAN',(1,0),(1,1)),
				('SPAN',(2,0),(2,1)),
				('SPAN',(3,0),(6,0)),
				('SPAN',(7,0),(8,0)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-110
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "banco_rep.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [45,55,55,55,150,140,120,85,85]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-60

		c.setFont("Helvetica", 7)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_banco(self))
		res = self.env.cr.dictfetchall()
		
		cont = 0
		cuenta = ''
		debe, haber, sum_debe, sum_haber, final_debe, final_haber = 0, 0, 0, 0, 0, 0

		for i in res:
			first_pos = 30
			
			c.setFont("Helvetica-Bold", 7)
			if cont == 0:
				cuenta = i['cuenta']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,i['cuenta'])
				c.setFont("Helvetica", 7)
				c.drawString( first_pos+50 ,pos_inicial,particionar_text(i['nombre_cuenta'],50))
				c.setFont("Helvetica-Bold", 6)
				c.drawString( first_pos+100 ,pos_inicial,'Cod. Ent. Financiera:')
				c.setFont("Helvetica", 7)
				c.drawString( first_pos+170 ,pos_inicial,i['code_bank'] if i['code_bank'] else '')
				c.setFont("Helvetica-Bold", 6)
				c.drawString( first_pos+400 ,pos_inicial,'Número de cuenta:')
				c.setFont("Helvetica", 7)
				c.drawString( first_pos+470 ,pos_inicial,i['account_number'] if i['account_number'] else '')
				pos_inicial -= 15

			if cuenta != i['cuenta']:
				c.line(655,pos_inicial+3,815,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'SUB TOTAL:')
				c.drawRightString( 730,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % debe)) )
				c.drawRightString( 815 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % haber)))
				pos_inicial -= 10

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 7)

				c.line(655,pos_inicial+3,815,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'SALDO FINAL:')
				saldo_debe = (debe - haber) if debe > haber else 0
				saldo_haber = 0 if debe > haber else (haber - debe)

				c.drawRightString( 730,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_debe)) )
				c.drawRightString( 815 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_haber)))
				pos_inicial -= 20

				c.line(655,pos_inicial+3,815,pos_inicial+3)

				debe, haber = 0, 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 7)

				cuenta = i['cuenta']
				c.drawString( first_pos+2 ,pos_inicial,i['cuenta'])
				c.setFont("Helvetica", 7)
				c.drawString( first_pos+50 ,pos_inicial,particionar_text(i['nombre_cuenta'],50))
				c.setFont("Helvetica-Bold", 7)
				c.drawString( first_pos+100 ,pos_inicial,'Cod. Ent. Financiera:')
				c.setFont("Helvetica", 7)
				c.drawString( first_pos+170 ,pos_inicial,i['code_bank'] if i['code_bank'] else '')
				c.setFont("Helvetica-Bold", 7)
				c.drawString( first_pos+400 ,pos_inicial,'Número de cuenta:')
				c.setFont("Helvetica", 7)
				c.drawString( first_pos+470 ,pos_inicial,i['account_number'] if i['account_number'] else '')
				pos_inicial -= 10


			c.setFont("Helvetica", 7)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['libro'] if i['libro'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['voucher'] if i['voucher'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha'] if i['fecha'] else '',50) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['medio_pago'] if i['medio_pago'] else '',50) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['glosa'] if i['glosa'] else '',130) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',120) )
			first_pos += size_widths[5]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',50) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+82 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['debe'])) )
			debe += i['debe']
			sum_debe += i['debe']
			first_pos += size_widths[7]

			c.drawRightString( first_pos+82 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['haber'])) )
			haber += i['haber']
			sum_haber += i['haber']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold", 7)
		c.line(655,pos_inicial+3,815,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SUB TOTAL:')
		c.drawRightString( 730,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % debe)) )
		c.drawRightString( 815 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % haber)))
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		##
		c.setFont("Helvetica-Bold", 7)

		c.line(655,pos_inicial+3,815,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'SALDO FINAL:')
		saldo_debe = (debe - haber) if debe > haber else 0
		saldo_haber = 0 if debe > haber else (haber - debe)

		c.drawRightString( 730,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_debe)) )
		c.drawRightString( 815 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_haber)))
		pos_inicial -= 20
		##

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 7)

		c.line(655,pos_inicial+3,815,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'TOTALES:')
		c.drawRightString( 730,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_debe)) )
		c.drawRightString( 815 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % sum_haber)))
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 7)

		c.line(655,pos_inicial+3,815,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		final_debe = (sum_debe - sum_haber) if sum_debe > sum_haber else 0
		final_haber = 0 if sum_debe > sum_haber else (sum_haber - sum_debe)

		c.drawRightString( 730,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_debe)) )
		c.drawRightString( 815 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_haber)))
		pos_inicial -= 20

		c.line(655,pos_inicial+3,815,pos_inicial+3)

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO BANCOS.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_inventario_balance(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_inventario(self):
			sql = """
				select cuenta,descripcion as nomenclatura,
				coalesce(totaldebe,0) as debe_inicial,
				coalesce(totalhaber,0) as haber_inicial, 
				coalesce(debe,0) as debe, 
				coalesce(haber,0) as haber,
				coalesce(finaldeudor,0) as saldo_deudor, 
				coalesce(finalacreedor,0) as saldo_acreedor, 
				coalesce(activo,0) as activo, coalesce(pasivo,0) as pasivo, 
				coalesce(perdidasfun,0) as perdifun, coalesce(gananciafun,0) as gananfun
				from get_reporte_hoja_registro(False,periodo_num('%s'),periodo_num('%s'))
							
			""" % (self.period.code,self.period.code)

			return sql
		
		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 12)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO INVENTARIO Y BALANCE - BALANCE DE COMPROBACION DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=9><b>CUENTA Y SUBCUENTA CONTABLE</b></font>",style),'',
				Paragraph("<font size=9><b>SALDOS INICIALES</b></font>",style),'',
				Paragraph("<font size=9><b>MOVIMIENTOS</b></font>",style),'',
				Paragraph("<font size=9><b>SALDOS FINALES</b></font>",style),'',
				Paragraph("<font size=9><b>SALDOS FINALES DEL BALANCE GENERAL</b></font>",style),'',
				Paragraph("<font size=9><b>PERDIDAS FINALES EST. DE PERDIDAS Y GANAN. POR FUNCION</b></font>",style),''],
				[Paragraph("<font size=9><b>CUENTA</b></font>",style),
				Paragraph("<font size=8.5><b>DENOMINACION</b></font>",style),
				Paragraph("<font size=8.5><b>DEUDOR</b></font>",style),
				Paragraph("<font size=8.5><b>ACREEDOR</b></font>",style),
				Paragraph("<font size=8.5><b>DEBE</b></font>",style),
				Paragraph("<font size=8.5><b>HABER</b></font>",style),
				Paragraph("<font size=8.5><b>DEUDOR</b></font>",style),
				Paragraph("<font size=8.5><b>ACREEDOR</b></font>",style),
				Paragraph("<font size=8.5><b>ACTIVO</b></font>",style),
				Paragraph("<font size=8.5><b>PASIVO</b></font>",style),
				Paragraph("<font size=8.5><b>PERDIDA</b></font>",style),
				Paragraph("<font size=8.5><b>GANANCIA</b></font>",style)]]
			t=Table(data,colWidths=size_widths, rowHeights=[30,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(1,0)),
				('SPAN',(2,0),(3,0)),
				('SPAN',(4,0),(5,0)),
				('SPAN',(6,0),(7,0)),
				('SPAN',(8,0),(9,0)),
				('SPAN',(10,0),(11,0)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-110
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "inv_rep.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [48,185,55,55,55,55,55,55,55,55,55,55]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-60

		c.setFont("Helvetica", 7)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_inventario(self))
		res = self.env.cr.dictfetchall()

		debe_inicial, haber_inicial, debe, haber, saldo_deudor, saldo_acreedor, activo, pasivo, perdifun, gananfun = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica", 7)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['cuenta'] if i['cuenta'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nomenclatura'] if i['nomenclatura'] else '',200) )
			first_pos += size_widths[1]

			c.drawRightString( first_pos+53 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['debe_inicial'])) )
			debe_inicial += i['debe_inicial']
			first_pos += size_widths[2]

			c.drawRightString( first_pos+53 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['haber_inicial'])) )
			haber_inicial += i['haber_inicial']
			first_pos += size_widths[3]

			c.drawRightString( first_pos+53 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['debe'])) )
			debe += i['debe']
			first_pos += size_widths[4]

			c.drawRightString( first_pos+53 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['haber'])) )
			haber += i['haber']
			first_pos += size_widths[5]

			c.drawRightString( first_pos+53 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_deudor'])) )
			saldo_deudor += i['saldo_deudor']
			first_pos += size_widths[6]

			c.drawRightString( first_pos+53 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_acreedor'])) )
			saldo_acreedor += i['saldo_acreedor']
			first_pos += size_widths[7]

			c.drawRightString( first_pos+53 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['activo'])) )
			activo += i['activo']
			first_pos += size_widths[8]

			c.drawRightString( first_pos+53 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['pasivo'])) )
			pasivo += i['pasivo']
			first_pos += size_widths[9]

			c.drawRightString( first_pos+53 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['perdifun'])) )
			perdifun += i['perdifun']
			first_pos += size_widths[10]

			c.drawRightString( first_pos+53 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['gananfun'])) )
			gananfun += i['gananfun']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold", 7)
		c.line(80,pos_inicial+3,815,pos_inicial+3)
		c.drawString( 80 ,pos_inicial-10,'TOTALES:')
		c.drawRightString( 316,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % debe_inicial)) )
		c.drawRightString( 371,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % haber_inicial)) )
		c.drawRightString( 426,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % debe)) )
		c.drawRightString( 481 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % haber)))
		c.drawRightString( 536 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_deudor)))
		c.drawRightString( 591 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_acreedor)))
		c.drawRightString( 646 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % activo)))
		c.drawRightString( 701 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % pasivo)))
		c.drawRightString( 756 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % perdifun)))
		c.drawRightString( 811 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % gananfun)))
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 7)

		c.line(80,pos_inicial+3,815,pos_inicial+3)
		c.drawString( 80 ,pos_inicial-10,'GANANCIA DEL EJERCICIO:')
		final_activo = abs(activo - pasivo) if (activo - pasivo) < 0 else 0
		final_pasivo = (activo - pasivo) if (activo - pasivo) > 0 else 0
		c.drawRightString( 646,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_activo)) )
		c.drawRightString( 701 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_pasivo)))
		final_perdifun = abs(perdifun - gananfun) if (perdifun - gananfun) < 0 else 0
		final_gananfun = (perdifun - gananfun) if (perdifun - gananfun) > 0 else 0
		c.drawRightString( 756 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_perdifun)))
		c.drawRightString( 811 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_gananfun)))
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 7)

		c.line(80,pos_inicial+3,815,pos_inicial+3)
		c.drawString( 80 ,pos_inicial-10,'SUMAS IGUALES:')

		c.drawRightString( 646,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % (final_activo + activo))) )
		c.drawRightString( 701,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % (final_pasivo + pasivo))) )
		c.drawRightString( 756,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % (final_perdifun + perdifun))) )
		c.drawRightString( 811,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % (final_gananfun + gananfun))) )
		pos_inicial -= 20

		c.line(80,pos_inicial+3,815,pos_inicial+3)

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO INVENTARIO Y BALANCE.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_10_caja_bancos(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_10_caja_bancos(self):
			sql = """
				SELECT 
				cuenta,
				nomenclatura,
				code_bank,
				account_number,
				moneda,
				debe,
				haber,
				(debe - haber) as saldo
				FROM get_detalle_pdf_10(periodo_num('%s'),periodo_num('%s'))
				ORDER BY cuenta
			
			""" % ('00/'+self.period.code[3:],
				self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"*** LIBRO DE INVENTARIOS Y BALANCES- DETALLE DEL SALDO DE LA CUENTA 10- EFECTIVO Y EQUIVALENTES DE EFECTIVO DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=9><b>CUENTA CONTABLE DIVISIONARIA</b></font>",style),'',
				Paragraph("<font size=9><b>REFERENCIA DE LA CUENTA</b></font>",style),'','',
				Paragraph("<font size=9><b>SALDO CONTABLE FINAL</b></font>",style),'',''],
				[Paragraph("<font size=9><b>CODIGO</b></font>",style),
				Paragraph("<font size=9><b>DENOMINACION</b></font>",style),
				Paragraph("<font size=9><b>ENT. FINANCIERA</b></font>",style),
				Paragraph("<font size=9><b>NUMERO DE CTA</b></font>",style),
				Paragraph("<font size=9><b>MONEDA</b></font>",style),
				Paragraph("<font size=9><b>DEUDOR</b></font>",style),
				Paragraph("<font size=9><b>ACREEDOR</b></font>",style),
				Paragraph("<font size=9><b>SALDO</b></font>",style)]]
			t=Table(data,colWidths=size_widths, rowHeights=[30,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(1,0)),
				('SPAN',(2,0),(4,0)),
				('SPAN',(5,0),(7,0)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-110
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "caja_10.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,200,100,130,50,73.33,73.33,73.33]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica", 7)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_10_caja_bancos(self))
		res = self.env.cr.dictfetchall()

		debe, haber, saldo = 0, 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica", 9)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['cuenta'] if i['cuenta'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nomenclatura'] if i['nomenclatura'] else '',170) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['code_bank'] if i['code_bank'] else '',50) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['account_number'] if i['account_number'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,str(i['moneda']) if i['moneda'] else '')
			first_pos += size_widths[4]

			c.drawRightString( first_pos+73.33 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['debe'])) )
			debe += i['debe']
			first_pos += size_widths[5]

			c.drawRightString( first_pos+73.33 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['haber'])) )
			haber += i['haber']
			first_pos += size_widths[6]

			c.drawRightString( first_pos+73.33 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo'])) )
			saldo += i['saldo']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(585,pos_inicial+3,780,pos_inicial+3)
		c.drawString( 500 ,pos_inicial-10,'TOTALES:')
		c.drawRightString( 633.34 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % debe)))
		c.drawRightString( 706.67 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % haber)))
		c.drawRightString( 780 ,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo)))

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 10.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_12_cliente(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_12_cliente(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='12'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
				order by rp.nro_documento
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"*** LIBRO DE INVENTARIOS Y BALANCES- DETALLE DEL SALDO DE LA CUENTA 12 -CUENTAS POR COBRAR COMERCIALES DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DEL CLIENTE</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION DEL COMP.DE PAGO</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR COBRAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO (TABLA2)</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-120
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "caja_12.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_12_cliente(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()
		
		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 12.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_13_cobrar_relacionadas(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_13_cobrar_relacionadas(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='13'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"*** LIBRO DE INVENTARIO Y BALANCE - CUENTA 13 - RELACIONADAS DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DEL CLIENTE</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION DEL COMP.DE PAGO</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR COBRAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO (TABLA2)</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-110
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "caja_13.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_13_cobrar_relacionadas(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 13.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_14_cobrar_acc_personal(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_14_cobrar_acc_personal(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='14'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"*** LIBRO DE INVENTARIOS Y BALANCES-DETALLE DEL SALDO DE LA CUENTA 14- CUENTAS POR COBRAR AL PERSONAL,")
			c.drawCentredString((wReal/2)+20,hReal-15, u"A LOS ACCIONISTAS (SOCIOS), DIRECTORES Y GERENTES MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACIÓN DEL ACCIONISTA, SOCIO O PERSONAL</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE INICIO DE LA OPERACION</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR COBRAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-110
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "caja_14.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_14_cobrar_acc_personal(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 14.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_16_cobrar_diversas(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_16_cobrar_diversas(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='16'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO DE INVENTARIOS Y BALANCES- DETALLE DEL SALDO DE LA CUENTA 16 -CUENTAS POR COBRAR DIVERSAS DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DE TERCEROS</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION COMP.DE PAGO O F. INICIO OPERACION</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR COBRAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-110
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "caja_16.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_16_cobrar_diversas(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 16.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}
	
	@api.multi
	def get_pdf_17_diversas_rel(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_17_diversas_rel(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='17'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 8)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"*** LIBRO DE INVENTARIOS Y BALANCES - DETALLE DEL SALDO DE LA CUENTA 17 - CUENTAS POR COBRAR DIVERSAS RELACIONADAS DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DEL CLIENTE</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION DEL COMP.DE PAGO</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR COBRAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO (TABLA2)</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-120
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "caja_17.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_17_diversas_rel(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()
		
		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 17.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}
	
	@api.multi
	def get_pdf_18_servicios(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_18_servicios(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='18'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 8)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"*** LIBRO DE INVENTARIOS Y BALANCES - DETALLE DEL SALDO DE LA CUENTA 18 - SERVICIOS Y OTROS CONTRATADOS POR ANTICIPADO DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DEL CLIENTE</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION DEL COMP.DE PAGO</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR COBRAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO (TABLA2)</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-120
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "caja_18.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_18_servicios(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()
		
		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 18.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_19_cobrar_dudosa(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_19_cobrar_dudosa(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='19'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO DE INVENTARIOS Y BALANCES-DETALLE DEL SALDO DE LA CUENTA 19 ESTIMACIÓN DE CUENTAS DE COBRANZA DUDOSA DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DE DEUDORES</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION COMP.DE PAGO O F. INICIO OPERACION</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR COBRAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-110
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "caja_19.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_19_cobrar_dudosa(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 19.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	####################################################
	@api.multi
	def get_pdf_34_intangibles(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_34_intangibles(self):
			sql = """
				SELECT to_char(date::timestamp with time zone, 'yyyy/mm/dd'::text) as date,
				name,type,amount_total,amortizacion,amount
				FROM catalog_34_values_sunat
				WHERE date <= '%s'
			
			""" % (datetime.strptime(self.period.date_start, '%Y-%m-%d').strftime("%Y/%m/%d"))

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"*** LIBRO DE INVENTARIOS Y BALANCES - DETALLE DEL SALDO DE LA CUENTA 34 - INTANGIBLES DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 12
			style.alignment= 1

			data= [[Paragraph(u"<font size=10><b>FECHA DE INICIO DE LA OPERACIÓN</b></font>",style),
				Paragraph(u"<font size=10><b>DESCRIPCIÓN DEL INTANGIBLE</b></font>",style),
				Paragraph(u"<font size=10><b>TIPO DE INTANGIBLE (TABLA 7)</b></font>",style),
				Paragraph(u"<font size=10><b>VALOR CONTABLE DEL INTANGIBLE</b></font>",style),
				Paragraph(u"<font size=10><b>AMORTIZACIÓN CONTABLE ACUMULADA</b></font>",style),
				Paragraph(u"<font size=10><b>VALOR NETO CONTABLE DEL INTANGIBLE</b></font>",style)]]
			t=Table(data,colWidths=size_widths, rowHeights=[54])
			t.setStyle(TableStyle([
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-110
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "caja_34.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [75,250,75,130,120,120] #770

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_34_intangibles(self))
		res = self.env.cr.dictfetchall()

		total1, total2, total3 = 0, 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['date'] if i['date'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['name'] if i['name'] else '',230) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['type'] if i['type'] else '',50) )
			first_pos += size_widths[2]

			c.drawRightString( first_pos+130 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['amount_total'])) )
			first_pos += size_widths[3]
			total1 += i['amount_total']

			c.drawRightString( first_pos+120 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['amortizacion'])))
			first_pos += size_widths[4]
			total2 += i['amortizacion']

			c.drawRightString( first_pos+120 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['amount'])) )
			total3 += i['amount']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)

		c.line(430,pos_inicial+3,800,pos_inicial+3)
		c.drawString( 430 ,pos_inicial-10,'TOTALES:')
		c.drawRightString( 560,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total1)) )
		c.drawRightString( 680,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total2)) )
		c.drawRightString( 800,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % total3)) )
		c.line(430,pos_inicial+3,800,pos_inicial+3)

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 34.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_37_activos(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_37_activos(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='37'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"*** LIBRO DE INVENTARIOS Y BALANCES - DETALLE DEL SALDO DE LA CUENTA 37 - ACTIVOS DIFERIDOS DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DEL CLIENTE</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION DEL COMP.DE PAGO</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR COBRAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO (TABLA2)</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-120
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "caja_12.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_37_activos(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()
		
		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 37.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}
	####################################################
	
	@api.multi
	def get_pdf_cuenta_40(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_40(self):
			sql = """
				select 
				aa.code as cuenta,
				aa.name as nomenclatura,
				sum(T.saldo) as saldo
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='40'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				LEFT JOIN account_account aa on aa.id = T.aa_id
				group by aa.code, aa.name
				having sum(T.saldo) <> 0
			
			""" % ('00/'+self.period.code[3:],
				self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 8)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO DE INVENTARIOS Y BALANCES-DETALLE DEL SALDO DE LA CUENTA 40: TRIBUTOS,CONTRAPRESTACIONES Y APORTES AL")
			c.drawCentredString((wReal/2)+20,hReal-15, u"SISTEMA DE PENSIONES Y DE SALUD POR PAGAR MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-12, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-22,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-32, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-42, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=9><b>CUENTA Y SUB CUENTA TRIBUTOS POR PAGAR</b></font>",style),'', 
				Paragraph("<font size=8><b>SALDO FINAL</b></font>",style)],
				[Paragraph("<font size=8><b>CODIGO</b></font>",style),
				Paragraph("<font size=8><b>DENOMINACION</b></font>",style),'']]
			t=Table(data,colWidths=size_widths, rowHeights=(20))
			t.setStyle(TableStyle([
				('SPAN',(0,0),(1,0)),
				('SPAN',(2,0),(2,1)),
				('GRID',(0,0),(-1,-1), 1, colors.black),
				('ALIGN',(0,0),(-1,-1),'LEFT'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,70,500) 
			t.drawOn(c,70,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-95
			else:
				return pagina,posactual-valor

		width ,height  = A4  # 595 , 842
		wReal = width - 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "cta_40.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= A4 )
		pos_inicial = hReal-50
		pagina = 1

		size_widths = [80,300,80]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-55

		c.setFont("Helvetica", 8)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_40(self))
		res = self.env.cr.dictfetchall()

		saldo = 0

		for i in res:
			first_pos = 70

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['cuenta'] if i['cuenta'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nomenclatura'] if i['nomenclatura'] else '',300) )
			first_pos += size_widths[1]

			c.drawRightString( first_pos+80 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo'])))
			saldo += i['saldo']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold", 9)
		c.line(450,pos_inicial+3,530,pos_inicial+3)
		c.drawString( 390 ,pos_inicial-10,'TOTAL:')
		c.drawRightString( 530,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo)) )
		pos_inicial -= 20

		c.line(450,pos_inicial+3,530,pos_inicial+3)

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 40.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_cuenta_41(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_41(self):
			sql = """
				select 
				aa.code as cuenta,
				aa.name as nomenclatura,
				rp.ref,
				rp.name as partner,
				itdp.code td_partner,
				rp.nro_documento as doc_partner,
				sum(T.saldo) as saldo
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='41'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN account_account aa on aa.id = T.aa_id
				group by aa.code, aa.name, rp.ref, rp.name, itdp.code, rp.nro_documento
			
			""" % ('00/'+self.period.code[3:],self.period.code)
			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO DE INVENTARIOS Y BALANCES -DETALLE DEL SALDO DE LA CUENTA 41 REMUNERACIONES Y PARTICIPACIONES")
			c.drawCentredString((wReal/2)+20,hReal-12, u"POR PAGAR DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-12, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-22,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-32, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-42, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=9><b>CUENTA Y SUBCUENTA REMUNERACIONES POR PAGAR</b></font>",style),'', 
				Paragraph("<font size=8><b>TRABAJADOR</b></font>",style),'','','',
				Paragraph("<font size=8><b>SALDO FINAL</b></font>",style)],
				['','',
				Paragraph("<font size=8><b>CODIGO</b></font>",style),
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES</b></font>",style),
				Paragraph("<font size=8><b>DOC DE IDENT.</b></font>",style),'',''],
				[Paragraph("<font size=7.5><b>CODIGO</b></font>",style),
				Paragraph("<font size=8><b>DENOMINACION</b></font>",style),'','',
				Paragraph("<font size=8><b>TIPO</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),'']]
			t=Table(data,colWidths=size_widths, rowHeights=(16))
			t.setStyle(TableStyle([
				('SPAN',(0,0),(1,1)),
				('SPAN',(2,0),(5,0)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,1),(3,2)),
				('SPAN',(4,1),(5,1)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1, colors.black),
				('ALIGN',(0,0),(-1,-1),'LEFT'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-115
			else:
				return pagina,posactual-valor

		width ,height  = 842,595  # 595 , 842
		wReal = width - 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "cta_40.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [70,200,80,200,60,80,80] #535

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-60

		c.setFont("Helvetica", 8)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_41(self))
		res = self.env.cr.dictfetchall()

		saldo = 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica", 7)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['cuenta'] if i['cuenta'] else '',70) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nomenclatura'] if i['nomenclatura'] else '',200) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['ref'] if i['ref'] else '',50) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',200) )
			first_pos += size_widths[3]

			c.drawString( first_pos+7 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[4]

			c.drawString( first_pos+12 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[5]

			c.drawRightString( first_pos+80 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo'])))
			saldo += i['saldo']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",10)
		c.line(720,pos_inicial+3,800,pos_inicial+3)
		c.drawString( 670 ,pos_inicial-10,'TOTAL:')
		c.drawRightString( 800,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo)) )
		pos_inicial -= 20

		c.line(720,pos_inicial+3,800,pos_inicial+3)

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 41.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_cuenta_42(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_42(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='42'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
				order by rp.nro_documento
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO DE INVENTARIOS Y BALANCES-DETALLE DEL SALDO DE LA CUENTA 42 CUENTAS POR PAGAR COMERCIALES DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DEL PROVEEDOR</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION DEL COMP.DE PAGO</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR PAGAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-120
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "cta_42.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_42(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Proveedor: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Proveedor: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 42.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_cuenta_43(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_43(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='43'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO DE INVENTARIO Y BALANCE - CUENTA 43 - CTAS POR PAGAR COMERCIALES RELACIONADAS DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DEL PROVEEDOR</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION DEL COMP.DE PAGO</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR COBRAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-120
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "cta_43.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_43(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 43.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_cuenta_44(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_44(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='44'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
				order by rp.nro_documento
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO INVENTARIOS Y BALANCES-DETALLE DEL SALDO DE LA CUENTA 44: CUENTAS POR PAGAR A LOS ACCIONISTAS (SOCIOS), DIRECTORES Y")
			c.drawCentredString((wReal/2)+20,hReal-15, u"GERENTES DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DEL PROVEEDOR</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION DEL COMP.DE PAGO</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR PAGAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-120
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "cta_44.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_44(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Proveedor: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Proveedor: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 44.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_cuenta_45(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_45(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='45'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
				order by rp.nro_documento
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO INVENTARIOS Y BALANCES -DETALLE DEL SALDO DE LA CUENTA 45: OBLIGACIONES FINANCIERAS DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DEL PROVEEDOR</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION DEL COMP.DE PAGO</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR PAGAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-120
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "cta_45.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_45(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Proveedor: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Proveedor: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 45.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_cuenta_46(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_46(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='46'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
				order by rp.nro_documento
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO DE INVENTARIOS Y BALANCES - DETALLE DEL SALDO DE LA CUENTA 46: CUENTAS POR PAGAR DIVERSAS - TERCEROS DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DEL PROVEEDOR</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION DEL COMP.DE PAGO</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR PAGAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-120
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "cta_46.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_46(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Proveedor: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Proveedor: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 46.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_cuenta_47(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_47(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='47'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
				order by rp.nro_documento
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO DE INVENTARIO Y BALANCE - CUENTA 47 - CTAS POR PAGAR DIVERSAS RELACIONADAS DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DEL PROVEEDOR</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION DEL COMP.DE PAGO</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR PAGAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-120
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "cta_47.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_47(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Proveedor: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Proveedor: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 47.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_cuenta_48(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_48(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='48'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO DE INVENTARIO Y BALANCE - CUENTA 48 - PROVISIONES DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DEL PROVEEDOR</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION DEL COMP.DE PAGO</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR COBRAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-120
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "cta_48.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_48(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Cliente: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 48.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}

	@api.multi
	def get_pdf_cuenta_49(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		def _get_sql_vst_49(self):
			sql = """
				select itdp.code td_partner,
				rp.nro_documento as doc_partner,
				rp.name as partner,
				itd.code as td_sunat,
				T.numero as nro_comprobante,
				to_char(T.date::timestamp with time zone, 'yyyy/mm/dd'::text) as fecha_doc,
				T.saldo as saldo_mn
				FROM
				(select aa_id,rp_id,itd_id,numero, sum(debe-haber) as saldo,min(fechaemision) as date,min(aml_id),min(am_id) from get_libro_diario(periodo_num('%s'),periodo_num('%s')) 
				where left(cuenta,2)='49'
				group by aa_id,rp_id,itd_id,numero
				having sum(debe-haber) <> 0)T
				lEFT JOIN res_partner rp on rp.id = T.rp_id
				LEFT JOIN einvoice_catalog_06 itdp ON itdp.id = rp.type_document_partner_it
				LEFT JOIN einvoice_catalog_01 itd ON itd.id = T.itd_id
				order by rp.nro_documento
			
			""" % ('00/'+self.period.code[3:],self.period.code)

			return sql

		def particionar_text(c,tam):
			tet = ""
			for i in range(len(c)):
				tet += c[i]
				lines = simpleSplit(tet,'Helvetica',8,tam)
				if len(lines)>1:
					return tet[:-1]
			return tet

		def pdf_header(self,c,wReal,hReal,size_widths):
			c.setFont("Helvetica-Bold", 10)
			c.setFillColor(colors.black)
			c.drawCentredString((wReal/2)+20,hReal, u"***LIBRO DE INVENTARIOS Y BALANCES - DETALLE DEL SALDO DE LA CUENTA 49 - PASIVOS DIFERIDOS DEL MES DE %s ***"%(self.period.name))
			c.setFont("Helvetica-Bold", 10)
			c.drawString(30,hReal-10, particionar_text( self.company_id.name,90))
			c.setFont("Helvetica", 9)
			c.drawString(30,hReal-20,particionar_text( self.company_id.partner_id.street if self.company_id.partner_id.street else '',100))
			c.drawString(30,hReal-30, self.company_id.partner_id.state_id.name if self.company_id.partner_id.state_id else '')
			c.drawString(30,hReal-40, self.company_id.partner_id.nro_documento if self.company_id.partner_id.nro_documento else '')


			c.setFont("Helvetica", 10)
			style = getSampleStyleSheet()["Normal"]
			style.leading = 8
			style.alignment= 1

			data= [[Paragraph("<font size=8><b>INFORMACION DEL PROVEEDOR</b></font>",style),'','',
				Paragraph("<font size=8><b>TD</b></font>",style),
				Paragraph("<font size=8><b>NUMERO DEL DOCUMENTO</b></font>",style),
				Paragraph("<font size=8><b>F. DE EMISION DEL COMP.DE PAGO</b></font>",style),
				Paragraph("<font size=8><b>MONTO DE LA CUENTA POR PAGAR</b></font>",style)],
				[Paragraph("<font size=8><b>DOCUMENTO DE IDENTIDAD</b></font>",style),'',
				Paragraph("<font size=8><b>APELLIDOS Y NOMBRES DENOMINACION O RAZON SOCIAL</b></font>",style),
				'',''],
				[Paragraph("<font size=8><b>TIPO</b></font>",style),
				Paragraph("<font size=8><b>NUMERO</b></font>",style),
				'','','']]
			t=Table(data,colWidths=size_widths, rowHeights=[18,18,18])
			t.setStyle(TableStyle([
				('SPAN',(0,0),(2,0)),
				('SPAN',(0,1),(1,1)),
				('SPAN',(2,1),(2,2)),
				('SPAN',(3,0),(3,2)),
				('SPAN',(4,0),(4,2)),
				('SPAN',(5,0),(5,2)),
				('SPAN',(6,0),(6,2)),
				('GRID',(0,0),(-1,-1), 1.5, colors.black),
				('ALIGN',(0,0),(-1,-1),'CENTER'),
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('TEXTFONT', (0, 0), (-1, -1), 'Calibri'),
				('LEFTPADDING', (0,0), (-1,-1), 2),
				('RIGHTPADDING', (0,0), (-1,-1), 2),
				('BOTTOMPADDING', (0,0), (-1,-1), 2),
				('TOPPADDING', (0,0), (-1,-1), 2),
				('FONTSIZE',(0,0),(-1,-1),4)
			]))
			t.wrapOn(c,30,500) 
			t.drawOn(c,30,hReal-100)

		def verify_linea(self,c,wReal,hReal,posactual,valor,pagina,size_widths):
			if posactual <50:
				c.showPage()
				pdf_header(self,c,wReal,hReal,size_widths)
				return pagina+1,hReal-120
			else:
				return pagina,posactual-valor

		width ,height  = 842,595
		wReal = width- 15
		hReal = height - 40

		direccion = self.env['main.parameter'].search([],limit=1).dir_create_file
		name_file = "cta_49.pdf"
		c = canvas.Canvas( direccion + name_file, pagesize= (842,595) )
		pos_inicial = hReal-40
		pagina = 1

		size_widths = [50,100,250,50,130,90,100]

		pdf_header(self,c,wReal,hReal,size_widths)

		pos_inicial = pos_inicial-70

		c.setFont("Helvetica",9)
		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		self.env.cr.execute(_get_sql_vst_49(self))
		res = self.env.cr.dictfetchall()

		cont = 0
		doc_partner = ''
		saldo_mn, final_mn = 0, 0

		for i in res:
			first_pos = 30

			c.setFont("Helvetica-Bold", 9)
			if cont == 0:
				doc_partner = i['doc_partner']
				cont += 1
				c.drawString( first_pos+2 ,pos_inicial,'Proveedor: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			if doc_partner != i['doc_partner']:
				c.line(700,pos_inicial+3,795,pos_inicial+3)
				c.drawString( 575 ,pos_inicial-5,'TOTAL:')
				c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )

				saldo_mn = 0

				pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
				c.setFont("Helvetica-Bold", 9)

				doc_partner = i['doc_partner']
				c.drawString( first_pos+2 ,pos_inicial,'Proveedor: ' + i['doc_partner'] if i['doc_partner'] else '')
				pos_inicial -= 15

			c.setFont("Helvetica", 8)
			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_partner'] if i['td_partner'] else '',50) )
			first_pos += size_widths[0]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['doc_partner'] if i['doc_partner'] else '',50) )
			first_pos += size_widths[1]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['partner'] if i['partner'] else '',230) )
			first_pos += size_widths[2]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['td_sunat'] if i['td_sunat'] else '',100) )
			first_pos += size_widths[3]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['nro_comprobante'] if i['nro_comprobante'] else '',100) )
			first_pos += size_widths[4]

			c.drawString( first_pos+2 ,pos_inicial,particionar_text( i['fecha_doc'] if i['fecha_doc'] else '',100) )
			first_pos += size_widths[6]

			c.drawRightString( first_pos+85 ,pos_inicial,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % i['saldo_mn'])) )
			saldo_mn += i['saldo_mn']
			final_mn += i['saldo_mn']

			pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)

		c.setFont("Helvetica-Bold",9)
		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-5,'TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % saldo_mn)) )
		pos_inicial -= 10

		pagina, pos_inicial = verify_linea(self,c,wReal,hReal,pos_inicial,12,pagina,size_widths)
		c.setFont("Helvetica-Bold", 9)

		c.line(700,pos_inicial+3,795,pos_inicial+3)
		c.drawString( 575 ,pos_inicial-10,'SALDO FINAL TOTAL:')
		c.drawRightString( 795,pos_inicial-10,'{:,.2f}'.format(decimal.Decimal ("%0.2f" % final_mn)) )

		c.save()

		import os

		f = open(direccion + name_file, 'rb')

		vals = {
			'file_name': 'LIBRO DE INVENTARIO Y BALANCE - DETALLE CUENTA 49.pdf',
			'binary_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['ple.file.wizard'].create(vals)

		return {
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'ple.file.wizard',
			'res_id': sfs_id.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'nodestroy': True,
		}