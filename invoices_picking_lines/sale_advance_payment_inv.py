# -*- encoding: utf-8 -*-
from odoo import models,fields ,api
from odoo.exceptions import UserError

class SaleOrder(models.Model):
	_inherit = 'sale.order'

	def action_invoice_create(self, grouped=False, final=False):
		invoice_ids = super(SaleOrder, self).action_invoice_create(grouped, final)
		Lines = self.env['account.invoice'].browse(invoice_ids).mapped('invoice_line_ids')
		active_id = self._context.get('active_id', False)

		if self.picking_ids:
			for Line in Lines:
				lines = self.picking_ids.mapped('pack_operation_product_ids').filtered(lambda p_line: p_line.product_id == Line.product_id)
				for line in lines:
					Line.name += '\nMARCA: %s PROCED: %s\n' % (line.product_id.brand_id.name or '', ','.join(line.product_id.tag_ids.mapped('name')))
					for lot_line in line.pack_lot_ids:
						if lot_line.lot_id.product_id == line.product_id:
							venc = 'VENC: %s' % lot_line.lot_id.life_date if lot_line.lot_id.life_date else ''
							Line.name += '\nLT: %s %s %s' % (lot_line.lot_id.name, lot_line.qty, venc) or ''

			# for line in self.picking_ids.mapped('pack_operation_product_ids'):
			# 	for Line in Lines.filtered(lambda l: l.product_id == line.product_id):
			# 		Line.name += '\nMARCA: %s PROCED: %s\n' % (line.product_id.brand_id.name or '', ','.join(line.product_id.tag_ids.mapped('name')))
			# 		for lot_line in line.pack_lot_ids:
			# 			if lot_line.lot_id.product_id == line.product_id:
			# 				Line.name += 'LT: %s' % lot_line.lot_id.name or ''
			# 				if lot_line.lot_id.life_date:
			# 					Line.name += 'VENC: %s\n' % lot_line.lot_id.life_date
		return invoice_ids