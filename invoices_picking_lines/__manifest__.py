# -*- encoding: utf-8 -*-
{
	'name': u'Lotes en Descripcion de Producto',
	'category': 'reports',
	'author': 'ITGRUPO-FERCO',
	'depends': ['stock', 'kardex_it'],
	'version': '10.0',
	'description':"""
	Añadiendo Lotes a las descripciones de los productos de las facturas
	""",
	'auto_install': False,
	'demo': [],
	'data':	[],
	'installable': True
}