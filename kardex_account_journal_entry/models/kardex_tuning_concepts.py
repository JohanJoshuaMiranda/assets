# -*- coding: utf-8 -*-

from odoo import models, fields, api

class KardexTuningConcepts(models.Model):
	_name = 'kardex.tuning.concepts'
	
	name = fields.Char(string='Concepto')
	account_id = fields.Many2one('account.account',string=u'Cuenta')
	journal_id = fields.Many2one('account.journal',string=u'Diario')
	account_analytic_id = fields.Many2one('account.analytic.account',string=u'Cuenta Analítica')
	account_pruduct = fields.Boolean(default=False,string='Cuenta de producto en ambos')
	is_debit = fields.Boolean(default=False,string='Es Debe')