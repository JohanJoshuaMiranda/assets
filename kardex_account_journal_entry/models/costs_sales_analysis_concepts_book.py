# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CostsSalesAnalysisConceptsBook(models.Model):
	_name = 'costs.sales.analysis.concepts.book'
	_auto = False
	
	periodo = fields.Char(string=u'Periodo')
	concepto = fields.Char(string=u'Concepto Ajuste')
	diario = fields.Char(string=u'Diario')
	cuenta_debe = fields.Char(string=u'Cuenta Debe')
	cuenta_haber = fields.Char(string=u'Cuenta Haber')
	amount = fields.Float(string='Monto', digits=(64,2))