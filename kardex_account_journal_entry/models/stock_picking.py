# -*- coding: utf-8 -*-

from odoo import models, fields, api

class StockPicking(models.Model):
	_inherit = 'stock.picking'
	
	einvoice_12_code = fields.Char(related='einvoice_12.code',string=u'Codigo Operación SUNAT', store=True)
	kardex_tuning_concepts_id = fields.Many2one('kardex.tuning.concepts',string=u'Concepto de Ajuste')