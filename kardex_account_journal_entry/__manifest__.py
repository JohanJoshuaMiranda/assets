# -*- encoding: utf-8 -*-
{
	'name': 'Analisis de Costo de Ventas',
	'category': 'stock',
	'author': 'ITGRUPO',
	'depends': ['kardex_it_ferco','import_base_it','kardex_it','stock'],
	'version': '1.0',
	'description':"""
	- Reporte de Analisis de Costo de Ventas
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		'security/ir.model.access.csv',
		'wizards/costs_sales_analysis_wizard.xml',
		'wizards/costs_sales_analysis_product_wizard.xml',
		'wizards/costs_sales_analysis_concepts_wizard.xml',
		'wizards/costs_sales_detail_concepts_wizard.xml',
		'views/costs_sales_analysis_concepts_book.xml',
		'views/kardex_tuning_concepts.xml',
		'views/stock_picking.xml'
	],
	'installable': True
}
