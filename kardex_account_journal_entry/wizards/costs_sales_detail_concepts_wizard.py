# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import *
from odoo.exceptions import UserError
import base64

class CostsSalesDetailConceptsWizard(models.TransientModel):
	_name = 'costs.sales.detail.concepts.wizard'

	name = fields.Char()
	period = fields.Many2one('account.period',string='Periodo',required=True)
	date_from = fields.Date(string='Fecha Inicial')
	date_to = fields.Date(string='Fecha Final')

	def get_excel(self):
		import io
		from xlsxwriter.workbook import Workbook
		from xlsxwriter.utility import xl_rowcol_to_cell

		direccion = self.env['main.parameter'].search([])[0].dir_create_file
		workbook = Workbook( direccion + 'reporte_cost_sales.xlsx')
		worksheet = workbook.add_worksheet("Detall Concepto Ajustes")

		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=2)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(8)
		boldbord.set_bg_color('#DCE6F1')
		numberdos = workbook.add_format({'num_format':'0.00'})
		numberdos.set_border(style=1)	
		numberdos.set_font_size(8)
		numberseis = workbook.add_format({'num_format':'0.000000'})
		numberseis.set_border(style=1)	
		numberseis.set_font_size(8)

		bord = workbook.add_format()
		bord.set_border(style=1)
		bord.set_font_size(8)

		dateformat = workbook.add_format({'num_format':'yyyy-mm-dd'})
		dateformat.set_border(style=1)
		dateformat.set_font_size(8)
		x= 1

		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		worksheet.write(0,0, u"Fecha Alm.",boldbord)
		worksheet.write(0,1, u"Fecha",boldbord)
		worksheet.write(0,2, u"Tipo",boldbord)
		worksheet.write(0,3, u"Serie",boldbord)
		worksheet.write(0,4, u"Número",boldbord)
		worksheet.write(0,5, u"Guía de remisión",boldbord)
		worksheet.write(0,6, u"Doc. Almacen",boldbord)
		worksheet.write(0,7, u"RUC",boldbord)
		worksheet.write(0,8, u"Empresa",boldbord)
		worksheet.write(0,9, u"T. OP.",boldbord)
		worksheet.write(0,10, u"Código",boldbord)
		worksheet.write(0,11, u"Producto",boldbord)
		worksheet.write(0,12, u"Unidad",boldbord)
		worksheet.write(0,13, u"Icantidad",boldbord)
		worksheet.write(0,14, u"ICosto",boldbord)
		worksheet.write(0,15, u"SCantidad",boldbord)
		worksheet.write(0,16, u"SCosto",boldbord)
		worksheet.write(0,17, u"SALDOF",boldbord)
		worksheet.write(0,18, u"SALDOV",boldbord)
		worksheet.write(0,19, u"Costo Adquisición",boldbord)
		worksheet.write(0,20, u"Costo Promedio",boldbord)
		worksheet.write(0,21, u"Ubicacion Origen",boldbord)
		worksheet.write(0,22, u"Ubicacion Destino",boldbord)
		worksheet.write(0,23, u"Almacen",boldbord)
		worksheet.write(0,24, u"Concepto de Ajuste",boldbord)
		worksheet.write(0,25, u"Cuenta Debe",boldbord)
		worksheet.write(0,26, u"Cuenta Haber",boldbord)

		self.env.cr.execute(self._get_sql(self.period.date_start, self.period.date_stop))
		res = self.env.cr.dictfetchall()

		for line in res:

			worksheet.write(x,0,line['fecha_albaran'] if line['fecha_albaran'] else '' ,dateformat )
			worksheet.write(x,1,line['fecha'] if line['fecha']  else '',dateformat )
			worksheet.write(x,2,line['type_doc'] if line['type_doc']  else '',bord)
			worksheet.write(x,3,line['serial'] if line['serial']  else '',bord)
			worksheet.write(x,4,line['nro'] if line['nro']  else '',bord)
			worksheet.write(x,5,line['numberg'] if line['numberg'] else '',bord)
			worksheet.write(x,6,line['stock_doc'] if line['stock_doc'] else '',bord)
			worksheet.write(x,7,line['doc_partner'] if line['doc_partner'] else '',bord)
			worksheet.write(x,8,line['partner'] if line['partner'] else '',bord)
			worksheet.write(x,9,line['t_op'] if line['t_op'] else '',bord)
			worksheet.write(x,10,line['default_code'] if line['default_code'] else '',bord)
			worksheet.write(x,11,line['name_template'] if line['name_template'] else '',bord)
			worksheet.write(x,12,line['unidad'] if line['unidad'] else '',bord)
			worksheet.write(x,13,line['icantidad'] if line['icantidad'] else 0,numberdos)
			worksheet.write(x,14,line['icosto'] if line['icosto'] else 0,numberdos)
			worksheet.write(x,15,line['scantidad'] if line['scantidad'] else 0,numberdos)
			worksheet.write(x,16,line['scosto'] if line['scosto'] else 0,numberdos)
			worksheet.write(x,17,line['saldof'] if line['saldof'] else 0,numberdos)
			worksheet.write(x,18,line['saldov'] if line['saldov'] else 0,numberdos)
			worksheet.write(x,19,line['cadquiere'] if line['cadquiere'] else 0,numberseis)
			worksheet.write(x,20,line['cprom'] if line['cprom'] else 0,numberseis)
			worksheet.write(x,21,line['origen'] if line['origen'] else '',bord)
			worksheet.write(x,22,line['destino'] if line['destino'] else '',bord)
			worksheet.write(x,23,line['almacen'] if line['almacen'] else '',bord)
			worksheet.write(x,24,line['concepto'] if line['concepto'] else '',bord)
			worksheet.write(x,25,line['cuenta_debe'] if line['cuenta_debe'] else '',bord)
			worksheet.write(x,26,line['cuenta_haber'] if line['cuenta_haber'] else '',bord)

			x += 1

		tam_col = [8,8,3,4,8,22,12,9,71,4,11,51,5,6,7,6,7,6,7,11,10,32,32,32,65,50,50]

		worksheet.set_column('A:A', tam_col[0])
		worksheet.set_column('B:B', tam_col[1])
		worksheet.set_column('C:C', tam_col[2])
		worksheet.set_column('D:D', tam_col[3])
		worksheet.set_column('E:E', tam_col[4])
		worksheet.set_column('F:F', tam_col[5])
		worksheet.set_column('G:G', tam_col[6])
		worksheet.set_column('H:H', tam_col[7])
		worksheet.set_column('I:I', tam_col[8])
		worksheet.set_column('J:J', tam_col[9])
		worksheet.set_column('K:K', tam_col[10])
		worksheet.set_column('L:L', tam_col[11])
		worksheet.set_column('M:M', tam_col[12])
		worksheet.set_column('N:N', tam_col[13])
		worksheet.set_column('O:O', tam_col[14])
		worksheet.set_column('P:P', tam_col[15])
		worksheet.set_column('Q:Q', tam_col[16])
		worksheet.set_column('R:R', tam_col[17])
		worksheet.set_column('S:S', tam_col[18])
		worksheet.set_column('T:T', tam_col[19])
		worksheet.set_column('U:U', tam_col[20])
		worksheet.set_column('V:V', tam_col[21])
		worksheet.set_column('W:W', tam_col[22])
		worksheet.set_column('X:X', tam_col[23])
		worksheet.set_column('Y:Y', tam_col[24])
		worksheet.set_column('Z:Z', tam_col[25])
		worksheet.set_column('AA:AA', tam_col[26])
		workbook.close()
		
		f = open(direccion + 'reporte_cost_sales.xlsx', 'rb')
		
		
		vals = {
			'output_name': 'Detalle Concepto de Ajustes.xlsx',
			'output_file': base64.encodestring(''.join(f.readlines())),		
		}

		sfs_id = self.env['export.file.save'].create(vals)
		return {
			"type": "ir.actions.act_window",
			"res_model": "export.file.save",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}

	def _get_sql(self,date_ini,date_end):

		sql = """
				select 
				gkv.fecha_albaran,
				gkv.fecha,
				gkv.type_doc,
				gkv.serial,
				gkv.nro,
				sp.numberg,
				gkv.stock_doc,
				gkv.doc_partner,
				gkv.name as partner,
				gkv.operation_type as t_op,
				gkv.default_code,
				gkv.name_template,
				gkv.unidad,
				gkv.ingreso as icantidad,
				gkv.debit as icosto,
				gkv.salida as scantidad,
				gkv.credit as scosto,
				gkv.saldof as saldof,
				gkv.saldov as saldov,
				gkv.cadquiere,
				gkv.cprom,
				gkv.origen,
				gkv.destino,
				gkv.almacen,
				ktc.name as concepto,
				case when ktc.account_pruduct <> TRUE and ktc.is_debit = TRUE then ak.code ||' '||ak.name
				else aa.code ||' '||aa.name end as cuenta_debe,
				case when ktc.account_pruduct <> TRUE and ktc.is_debit <> TRUE then ak.code ||' '||ak.name
				else aa.code ||' '||aa.name end as cuenta_haber
				from get_kardex_v({date_start_s},{date_end_s},(select array_agg(id) from product_product), (select array_agg(id) from stock_location)) gkv
				left join stock_move sm on sm.id = gkv.stock_moveid
				left join stock_picking sp on sp.id = sm.picking_id
				left join kardex_tuning_concepts ktc on ktc.id = sp.kardex_tuning_concepts_id
				LEFT JOIN account_account ak ON ktc.account_id = ak.id
				left join product_product pp on pp.id = gkv.product_id
				left join product_template pt on pt.id = pp.product_tmpl_id
				LEFT JOIN ( SELECT "substring"(ir_property.res_id::text, "position"(ir_property.res_id::text, ','::text) + 1)::integer AS categ_id,
				"substring"(ir_property.value_reference::text, "position"(ir_property.value_reference::text, ','::text) + 1)::integer AS account_id
				FROM ir_property
				WHERE ir_property.name::text = 'property_stock_valuation_account_id'::text) j ON pt.categ_id = j.categ_id
				LEFT JOIN account_account aa ON j.account_id = aa.id
				where (gkv.fecha between '{date_ini}' and '{date_end}') and sp.kardex_tuning_concepts_id is not null
				""".format(
				date_start_s = datetime.strptime(self.date_from, '%Y-%m-%d').strftime("%Y%m%d"),  
				date_end_s = datetime.strptime(self.date_to, '%Y-%m-%d').strftime("%Y%m%d"),
				date_ini =  datetime.strptime(date_ini, '%Y-%m-%d').strftime("%Y/%m/%d"),
				date_end = datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y/%m/%d")
			)
		return sql