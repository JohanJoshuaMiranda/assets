# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import *
from odoo.exceptions import UserError
import base64

class CostsSalesAnalysisConceptsWizard(models.TransientModel):
	_name = 'costs.sales.analysis.concepts.wizard'

	name = fields.Char()
	period = fields.Many2one('account.period',string='Periodo',required=True)
	date_from = fields.Date(string='Fecha Inicial')
	date_to = fields.Date(string='Fecha Final')
	type_show =  fields.Selection([('pantalla','Pantalla'),('excel','Excel')],string=u'Mostrar en', required=True, default='excel')

	def make_invoice(self):
		self.env.cr.execute(self._get_sql_asiento(self.date_from,self.date_to,self.period.date_start, self.period.date_stop))
		res = self.env.cr.dictfetchall()
		user = self.env['res.users'].browse(self.env.uid)
		doc = self.env['einvoice.catalog.01'].search([('code','=','00')],limit=1)
		journal_id = None
		lines = []
		for line in res:
			if not journal_id:
				journal_id = line['journal_id']
			if journal_id != line['journal_id']:
				move = {
					'ref': 'AJUSTE ' + self.period.code,
					'line_ids': lines,
					'date': self.period.date_stop,
					'journal_id': journal_id,
					'company_id': user.company_id.id,
				}
				move_id = self.env['account.move'].create(move)
				move_id.post()
				lines = []
				journal_id = line['journal_id']
			vals = (0,0,{
					'name': line['concepto'], 
					'ref': 'AJUSTE ' + self.period.code,
					'currency_id': False,
					'debit': line['amount'],
					'credit': 0.00, 
					'date_maturity': False, 
					'date': self.period.date_stop,
					'amount_currency':0.00, 
					'account_id': line['cuenta_debe'],
					'analytic_account_id': line['account_analytic_id'],
					'partner_id': False,
					'type_document_it': doc.id,
					'nro_comprobante': 'AJUSTE ' + self.period.code,
					})
			lines.append(vals)
			vals = (0,0,{
					'name': line['concepto'], 
					'ref': 'AJUSTE ' + self.period.code,
					'currency_id': False,
					'debit': 0.00,
					'credit': line['amount'],
					'date_maturity': False, 
					'date': self.period.date_stop,
					'amount_currency':0.00, 
					'account_id': line['cuenta_haber'],
					'analytic_account_id': line['account_analytic_id'],
					'partner_id': False,
					'type_document_it': doc.id,
					'nro_comprobante': 'AJUSTE ' + self.period.code,
					})
			lines.append(vals)
		move = {
				'ref': 'AJUSTE ' + self.period.code,
				'line_ids': lines,
				'date': self.period.date_stop,
				'journal_id': journal_id,
				'company_id': user.company_id.id,
				}
		move_id = self.env['account.move'].create(move)
		move_id.post()
			

	def get_report(self):
		self.env.cr.execute("""
		CREATE OR REPLACE view costs_sales_analysis_concepts_book as ("""+self._get_sql(self.date_from,self.date_to,self.period.code,self.period.date_start,self.period.date_stop)+""")""")
		if self.type_show == 'pantalla':
			return {
				'name': u'Resumen Asiento Ajustes',
				'type': 'ir.actions.act_window',
				'res_model': 'costs.sales.analysis.concepts.book',
				'view_mode': 'tree',
				'view_type': 'form',
			}
		if self.type_show == 'excel':
			return self.get_excel()

	def get_excel(self):
		import io
		from xlsxwriter.workbook import Workbook
		from xlsxwriter.utility import xl_rowcol_to_cell

		direccion = self.env['main.parameter'].search([])[0].dir_create_file
		workbook = Workbook( direccion + 'reporte_cost_sales.xlsx')
		worksheet = workbook.add_worksheet("Resumen Asiento Ajustes")
		bold = workbook.add_format({'bold': True})

		bold_title = workbook.add_format({'bold': True,'align': 'center','valign': 'vcenter'})
		bold_title.set_font_size(14)
		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=2)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(9)
		boldbord.set_bg_color('#DCE6F1')
		numberdos = workbook.add_format({'num_format':'0.00'})
		bord = workbook.add_format()
		bord.set_border(style=1)
		numberdos.set_border(style=1)	
		dateformat = workbook.add_format({'num_format':'d-m-yyyy'})
		dateformat.set_border(style=1)
		dateformat.set_font_size(10)
		dateformat.set_font_name('Times New Roman')	
		x= 1

		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		worksheet.write(0,0, "Periodo",boldbord)
		worksheet.write(0,1, u"Concepto Ajuste",boldbord)
		worksheet.write(0,2, u"Diario",boldbord)
		worksheet.write(0,3, u"Cuenta Debe",boldbord)
		worksheet.write(0,4, u"Cuenta Haber",boldbord)
		worksheet.write(0,5, u"Monto",boldbord)

		self.env.cr.execute(self._get_sql(self.date_from,self.date_to,self.period.code,self.period.date_start, self.period.date_stop))
		res = self.env.cr.dictfetchall()

		for line in res:

			worksheet.write(x,0,line['periodo'] if line['periodo'] else '' ,bord )
			worksheet.write(x,1,line['concepto'] if line['concepto']  else '',bord )
			worksheet.write(x,2,line['diario'] if line['diario']  else '',bord)
			worksheet.write(x,3,line['cuenta_debe'] if line['cuenta_debe']  else '',bord)
			worksheet.write(x,4,line['cuenta_haber'] if line['cuenta_haber']  else '',bord)
			worksheet.write(x,5,line['amount'] if line['amount'] else 0,numberdos)

			x += 1

		tam_col = [7,50,25,15,15,15]

		worksheet.set_column('A:A', tam_col[0])
		worksheet.set_column('B:B', tam_col[1])
		worksheet.set_column('C:C', tam_col[2])
		worksheet.set_column('D:D', tam_col[3])
		worksheet.set_column('E:E', tam_col[4])
		worksheet.set_column('F:F', tam_col[5])
		workbook.close()
		
		f = open(direccion + 'reporte_cost_sales.xlsx', 'rb')
		
		
		vals = {
			'output_name': 'Resumen Asiento Ajustes.xlsx',
			'output_file': base64.encodestring(''.join(f.readlines())),		
		}

		sfs_id = self.env['export.file.save'].create(vals)
		return {
			"type": "ir.actions.act_window",
			"res_model": "export.file.save",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}

	def _get_sql(self,date_from,date_to,periodo,date_ini,date_end):

		sql = """
				select 
				row_number() OVER () AS id, '{periodo}' as periodo,
				T2.concepto, T2.diario, T2.cuenta_debe, T2.cuenta_haber, sum(T2.amount) as amount from (
				select
				ktc.name as concepto,
				aj.name as diario,
				case when ktc.account_pruduct <> TRUE and ktc.is_debit = TRUE then ak.code
				else aa.code end as cuenta_debe,
				case when ktc.account_pruduct <> TRUE and ktc.is_debit <> TRUE then ak.code
				else aa.code end as cuenta_haber,
				T.monto as amount
				from
				(select 
				sp.kardex_tuning_concepts_id,
				gkv.product_id,
				round(sum(gkv.debit-gkv.credit),2) as monto
				from get_kardex_v({date_start_s},{date_end_s},(select array_agg(id) from product_product), (select array_agg(id) from stock_location)) gkv
				left join stock_move sm on sm.id = gkv.stock_moveid
				left join stock_picking sp on sp.id = sm.picking_id
				where (gkv.fecha between '{date_ini}' and '{date_end}') and sp.kardex_tuning_concepts_id is not null
				group by sp.kardex_tuning_concepts_id, gkv.product_id)T
				left join kardex_tuning_concepts ktc on ktc.id = T.kardex_tuning_concepts_id
				left join product_product pp on pp.id = T.product_id
				left join product_template pt on pt.id = pp.product_tmpl_id
				LEFT JOIN ( SELECT "substring"(ir_property.res_id::text, "position"(ir_property.res_id::text, ','::text) + 1)::integer AS categ_id,
				"substring"(ir_property.value_reference::text, "position"(ir_property.value_reference::text, ','::text) + 1)::integer AS account_id
				FROM ir_property
				WHERE ir_property.name::text = 'property_stock_valuation_account_id'::text) j ON pt.categ_id = j.categ_id
				LEFT JOIN account_account aa ON j.account_id = aa.id
				LEFT JOIN account_account ak ON ktc.account_id = ak.id
				LEFT JOIN account_journal aj on aj.id = ktc.journal_id)T2
				GROUP BY T2.concepto, T2.diario, T2.cuenta_debe, T2.cuenta_haber
				""".format(
				periodo = periodo,
				date_start_s = datetime.strptime(date_from, '%Y-%m-%d').strftime("%Y%m%d"),
				date_end_s = datetime.strptime(date_to, '%Y-%m-%d').strftime("%Y%m%d"),
				date_ini =  datetime.strptime(date_ini, '%Y-%m-%d').strftime("%Y/%m/%d"),
				date_end = datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y/%m/%d")
			)
		return sql
	
	def _get_sql_asiento(self,date_from,date_to,date_ini,date_end):

		sql = """
				select 
				T2.concepto, T2.journal_id, T2.account_analytic_id, T2.cuenta_debe, T2.cuenta_haber, abs(sum(T2.amount)) as amount from (
				select
				ktc.name as concepto,
				ktc.journal_id,
				ktc.account_analytic_id,
				case when ktc.account_pruduct <> TRUE and ktc.is_debit = TRUE then ak.id
				else aa.id end as cuenta_debe,
				case when ktc.account_pruduct <> TRUE and ktc.is_debit <> TRUE then ak.id
				else aa.id end as cuenta_haber,
				T.monto as amount
				from
				(select 
				sp.kardex_tuning_concepts_id,
				gkv.product_id,
				round(sum(gkv.debit-gkv.credit),2) as monto
				from get_kardex_v({date_start_s},{date_end_s},(select array_agg(id) from product_product), (select array_agg(id) from stock_location)) gkv
				left join stock_move sm on sm.id = gkv.stock_moveid
				left join stock_picking sp on sp.id = sm.picking_id
				where (gkv.fecha between '{date_ini}' and '{date_end}') and sp.kardex_tuning_concepts_id is not null
				group by sp.kardex_tuning_concepts_id, gkv.product_id)T
				left join kardex_tuning_concepts ktc on ktc.id = T.kardex_tuning_concepts_id
				left join product_product pp on pp.id = T.product_id
				left join product_template pt on pt.id = pp.product_tmpl_id
				LEFT JOIN ( SELECT "substring"(ir_property.res_id::text, "position"(ir_property.res_id::text, ','::text) + 1)::integer AS categ_id,
				"substring"(ir_property.value_reference::text, "position"(ir_property.value_reference::text, ','::text) + 1)::integer AS account_id
				FROM ir_property
				WHERE ir_property.name::text = 'property_stock_valuation_account_id'::text) j ON pt.categ_id = j.categ_id
				LEFT JOIN account_account aa ON j.account_id = aa.id
				LEFT JOIN account_account ak ON ktc.account_id = ak.id)T2
				GROUP BY T2.concepto, T2.journal_id, T2.cuenta_debe, T2.cuenta_haber, T2.account_analytic_id
				order by T2.journal_id, T2.concepto
				""".format(
				date_start_s = datetime.strptime(date_from, '%Y-%m-%d').strftime("%Y%m%d"),
				date_end_s = datetime.strptime(date_to, '%Y-%m-%d').strftime("%Y%m%d"),
				date_ini =  datetime.strptime(date_ini, '%Y-%m-%d').strftime("%Y/%m/%d"),
				date_end = datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y/%m/%d")
			)
		return sql