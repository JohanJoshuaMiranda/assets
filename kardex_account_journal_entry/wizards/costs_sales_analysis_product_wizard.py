# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import *
from odoo.exceptions import UserError
import base64

class CostsSalesAnalysisProductWizard(models.TransientModel):
	_name = 'costs.sales.analysis.product.wizard'

	name = fields.Char()
	date_from = fields.Date(string='Fecha Inicial')
	date_to = fields.Date(string='Fecha Final')

	def get_excel(self):
		import io
		from xlsxwriter.workbook import Workbook
		from xlsxwriter.utility import xl_rowcol_to_cell

		direccion = self.env['main.parameter'].search([])[0].dir_create_file
		workbook = Workbook( direccion + 'reporte_cost_sales.xlsx')
		worksheet = workbook.add_worksheet("Resumen por producto")
		bold = workbook.add_format({'bold': True})

		bold_title = workbook.add_format({'bold': True,'align': 'center','valign': 'vcenter'})
		bold_title.set_font_size(14)
		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=2)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(9)
		boldbord.set_bg_color('#DCE6F1')
		numberdos = workbook.add_format({'num_format':'0.00'})
		numberocho = workbook.add_format({'num_format':'0.00000000'})
		bord = workbook.add_format()
		bord.set_border(style=1)
		numberdos.set_border(style=1)	
		numberocho.set_border(style=1)	
		dateformat = workbook.add_format({'num_format':'d-m-yyyy'})
		dateformat.set_border(style=1)
		dateformat.set_font_size(10)
		dateformat.set_font_name('Times New Roman')	
		x= 1

		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		worksheet.write(0,0, u"Código",boldbord)
		worksheet.write(0,1, u"Producto",boldbord)
		worksheet.write(0,2, "Cuenta",boldbord)
		worksheet.write(0,3, u"UdM",boldbord)
		worksheet.write(0,4, u"Saldo F",boldbord)
		worksheet.write(0,5, u"Saldo",boldbord)
		worksheet.write(0,6, u"C. Unitario",boldbord)

		self.env.cr.execute(self._get_sql(self.date_from,self.date_to))
		res = self.env.cr.dictfetchall()

		for line in res:

			worksheet.write(x,0,line['default_code'] if line['default_code'] else '' ,bord )
			worksheet.write(x,1,line['product'] if line['product'] else '' ,bord )
			worksheet.write(x,2,line['product_account'] if line['product_account'] else '' ,bord )
			worksheet.write(x,3,line['unidad'] if line['unidad'] else '' ,bord )
			worksheet.write(x,4,line['saldo_f'] if line['saldo_f']  else 0,numberdos)
			worksheet.write(x,5,line['saldo_v'] if line['saldo_v']  else 0,numberdos)
			worksheet.write(x,6,line['cu'] if line['cu']  else 0,numberocho)

			x += 1

		tam_col = [16,30,25,12,15,15,20]

		worksheet.set_column('A:A', tam_col[0])
		worksheet.set_column('B:B', tam_col[1])
		worksheet.set_column('C:C', tam_col[2])
		worksheet.set_column('D:D', tam_col[3])
		worksheet.set_column('E:E', tam_col[4])
		worksheet.set_column('F:F', tam_col[5])
		worksheet.set_column('G:G', tam_col[6])
		workbook.close()
		
		f = open(direccion + 'reporte_cost_sales.xlsx', 'rb')
		
		
		vals = {
			'output_name': 'Resumen por Producto.xlsx',
			'output_file': base64.encodestring(''.join(f.readlines())),		
		}

		sfs_id = self.env['export.file.save'].create(vals)
		return {
			"type": "ir.actions.act_window",
			"res_model": "export.file.save",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}

	def _get_sql(self,date_from,date_to):

		sql = """select name_template as product, default_code, product_account, unidad, round(sum(ingreso),2)-round(sum(salida),2) as saldo_f,
				round(sum(debit),2)-round(sum(credit),2) as saldo_v,
				case when (round(sum(ingreso),2)-round(sum(salida),2)) <> 0 then (round(sum(debit),2)-round(sum(credit),2))/(round(sum(ingreso),2)-round(sum(salida),2)) else 0 end as cu
				from get_kardex_v({date_start_s},{date_end_s},(select array_agg(id) from product_product), (select array_agg(id) from stock_location))
				where (fecha between '{date_ini}' and '{date_end}')
				group by name_template, default_code, product_account, unidad
		""".format(
				date_start_s = datetime.strptime(date_from, '%Y-%m-%d').strftime("%Y%m%d"),
				date_end_s = datetime.strptime(date_to, '%Y-%m-%d').strftime("%Y%m%d"),
				date_ini =  datetime.strptime(date_from, '%Y-%m-%d').strftime("%Y/%m/%d"),
				date_end = datetime.strptime(date_to, '%Y-%m-%d').strftime("%Y/%m/%d")
			)
		return sql