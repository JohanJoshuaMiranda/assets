# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import *
from odoo.exceptions import UserError
import base64

class CostsSalesAnalysisWizard(models.TransientModel):
	_name = 'costs.sales.analysis.wizard'

	name = fields.Char()
	period = fields.Many2one('account.period',string='Periodo',required=True)
	date_from = fields.Date(string='Fecha Inicial')
	date_to = fields.Date(string='Fecha Final')

	def get_excel(self):
		import io
		from xlsxwriter.workbook import Workbook
		from xlsxwriter.utility import xl_rowcol_to_cell

		direccion = self.env['main.parameter'].search([])[0].dir_create_file
		workbook = Workbook( direccion + 'reporte_cost_sales.xlsx')
		worksheet = workbook.add_worksheet("Resumen por cuenta")
		bold = workbook.add_format({'bold': True})

		bold_title = workbook.add_format({'bold': True,'align': 'center','valign': 'vcenter'})
		bold_title.set_font_size(14)
		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=2)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(9)
		boldbord.set_bg_color('#DCE6F1')
		numberdos = workbook.add_format({'num_format':'0.00'})
		bord = workbook.add_format()
		bord.set_border(style=1)
		numberdos.set_border(style=1)	
		dateformat = workbook.add_format({'num_format':'d-m-yyyy'})
		dateformat.set_border(style=1)
		dateformat.set_font_size(10)
		dateformat.set_font_name('Times New Roman')	
		x= 1

		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		worksheet.write(0,0, "Cuenta Producto",boldbord)
		worksheet.write(0,1, u"Entradas",boldbord)
		worksheet.write(0,2, u"Salidas",boldbord)
		worksheet.write(0,3, u"Saldo",boldbord)

		self.env.cr.execute(self._get_sql(self.date_from,self.date_to,self.period.date_start, self.period.date_stop))
		res = self.env.cr.dictfetchall()

		for line in res:

			worksheet.write(x,0,line['cuenta'] if line['cuenta'] else '' ,bord )
			worksheet.write(x,1,line['entradas'] if line['entradas']  else 0,numberdos )
			worksheet.write(x,2,line['salidas'] if line['salidas']  else 0,numberdos)
			worksheet.write(x,3,line['saldo'] if line['saldo']  else 0,numberdos)

			x += 1

		tam_col = [30,15,15,15]

		worksheet.set_column('A:A', tam_col[0])
		worksheet.set_column('B:B', tam_col[1])
		worksheet.set_column('C:C', tam_col[2])
		worksheet.set_column('D:D', tam_col[3])
		workbook.close()
		
		f = open(direccion + 'reporte_cost_sales.xlsx', 'rb')
		
		
		vals = {
			'output_name': 'Resumen por Cuenta.xlsx',
			'output_file': base64.encodestring(''.join(f.readlines())),		
		}

		sfs_id = self.env['export.file.save'].create(vals)
		return {
			"type": "ir.actions.act_window",
			"res_model": "export.file.save",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}

	def _get_sql(self,date_from,date_to,date_ini,date_end):

		sql = """select product_account as cuenta, round(sum(debit),2) as entradas, 
				round(sum(credit),2) as salidas, round(sum(debit),2)-round(sum(credit),2) as saldo 
				from get_kardex_v({date_start_s},{date_end_s},(select array_agg(id) from product_product), (select array_agg(id) from stock_location))
				where (fecha between '{date_ini}' and '{date_end}')
				group by product_account
		""".format(
				date_start_s = datetime.strptime(date_from, '%Y-%m-%d').strftime("%Y%m%d"),
				date_end_s = datetime.strptime(date_to, '%Y-%m-%d').strftime("%Y%m%d"),
				date_ini =  datetime.strptime(date_ini, '%Y-%m-%d').strftime("%Y/%m/%d"),
				date_end = datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y/%m/%d")
			)
		return sql