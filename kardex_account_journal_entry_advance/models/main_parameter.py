# -*- coding: utf-8 -*-

from odoo import models, fields, api

class MainParameter(models.Model):
	_inherit = 'main.parameter'
	
	type_operation_outproduction = fields.Many2one('einvoice.catalog.12', string=u'Consumo de Producción')
	type_operation_inproduction = fields.Many2one('einvoice.catalog.12', string=u'Ingreso de Producción')
	type_operation_gv = fields.Many2one('einvoice.catalog.12', string=u'Gasto Vinculado')
	stock_journal_id = fields.Many2one('account.journal', string=u'Diario de Existencias')
	type_operation_sale_ids = fields.Many2many('einvoice.catalog.12','account_parameter_catalog_12_rel',string='Tipo de operaciones para ventas')