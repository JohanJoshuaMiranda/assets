# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import *
from odoo.exceptions import UserError
import base64

class KardexEntryIncomeWizard(models.TransientModel):
	_name = 'kardex.entry.income.wizard'

	name = fields.Char()
	period = fields.Many2one('account.period',string='Periodo',required=True)
	type_show =  fields.Selection([('pantalla','Pantalla'),('excel','Excel')],string=u'Mostrar en', required=True,default='pantalla')

	def get_report(self):
		self.env.cr.execute("""
		DROP VIEW IF EXISTS kardex_entry_income_book CASCADE;
		CREATE OR REPLACE view kardex_entry_income_book as ("""+self._get_sql(self.period.date_start,self.period.date_stop)+""")""")
		if self.type_show == 'pantalla':
			return {
				'name': u'Detalle de Ingresos',
				'type': 'ir.actions.act_window',
				'res_model': 'kardex.entry.income.book',
				'view_mode': 'tree',
				'view_type': 'form',
				'views': [(False, 'tree')],
			}
		if self.type_show == 'excel':
			return self.get_excel()

	def get_excel(self):
		import io
		from xlsxwriter.workbook import Workbook
		from xlsxwriter.utility import xl_rowcol_to_cell

		direccion = self.env['main.parameter'].search([])[0].dir_create_file
		workbook = Workbook( direccion + 'Detalle_ingreso.xlsx')
		worksheet = workbook.add_worksheet("DETALLE INGRESO")
		bold = workbook.add_format({'bold': True})

		bold_title = workbook.add_format({'bold': True,'align': 'center','valign': 'vcenter'})
		bold_title.set_font_size(14)
		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=2)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(9)
		boldbord.set_bg_color('#DCE6F1')
		numberdos = workbook.add_format({'num_format':'0.00'})
		numberocho = workbook.add_format({'num_format':'0.00000000'})
		bord = workbook.add_format()
		bord.set_border(style=1)
		numberdos.set_border(style=1)	
		numberocho.set_border(style=1)	
		dateformat = workbook.add_format({'num_format':'d-m-yyyy'})
		dateformat.set_border(style=1)
		dateformat.set_font_size(10)
		dateformat.set_font_name('Times New Roman')	
		x= 1

		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		worksheet.write(0,0, "FECHA",boldbord)
		worksheet.write(0,1, u"TIPO",boldbord)
		worksheet.write(0,2, u"SERIE",boldbord)
		worksheet.write(0,3, u"NÚMERO",boldbord)
		worksheet.write(0,4, u"DOC. ALMACÉN",boldbord)
		worksheet.write(0,5, u"RUC",boldbord)
		worksheet.write(0,6, u"EMPRESA",boldbord)
		worksheet.write(0,7, u"T. OP.",boldbord)
		worksheet.write(0,8, u"PRODUCTO",boldbord)
		worksheet.write(0,9, u"CODIGO PRODUCTO",boldbord)
		worksheet.write(0,10, u"UNIDAD",boldbord)
		worksheet.write(0,11, u"CANTIDAD",boldbord)
		worksheet.write(0,12, u"COSTO",boldbord)
		worksheet.write(0,13, u"CTA DEBE",boldbord)
		worksheet.write(0,14, u"CTA HABER",boldbord)
		worksheet.write(0,15, u"UBICACIÓN ORIGEN",boldbord)
		worksheet.write(0,16, u"UBICACIÓN DESTINO",boldbord)
		worksheet.write(0,17, u"ALMACÉN",boldbord)

		dic = self.env['kardex.entry.income.book'].search([])

		for line in dic:
			worksheet.write(x,0,line.fecha if line.fecha else '',dateformat)
			worksheet.write(x,1,line.tipo if line.tipo else '',bord)
			worksheet.write(x,2,line.serie if line.serie else '',bord)
			worksheet.write(x,3,line.numero if line.numero else '',bord)
			worksheet.write(x,4,line.doc_almacen if line.doc_almacen else '',bord)
			worksheet.write(x,5,line.ruc if line.ruc else '',bord)
			worksheet.write(x,6,line.empresa if line.empresa else '',bord)
			worksheet.write(x,7,line.tipo_op if line.tipo_op else '',bord)
			worksheet.write(x,8,line.producto if line.producto else '',bord)
			worksheet.write(x,9,line.default_code if line.default_code else '',bord)
			worksheet.write(x,10,line.unidad if line.unidad else '',bord)
			worksheet.write(x,11,line.qty if line.qty else 0,numberdos)
			worksheet.write(x,12,line.amount if line.amount else 0,numberocho)
			worksheet.write(x,13,line.cta_debe.code if line.cta_debe else '',bord)
			worksheet.write(x,14,line.cta_haber.code if line.cta_haber else '',bord)
			worksheet.write(x,15,line.origen if line.origen else '',bord)
			worksheet.write(x,16,line.destino if line.destino else '',bord)
			worksheet.write(x,17,line.almacen if line.almacen else '',bord)
			x += 1

		tam_col = [10,6,7,9,16,12,48,8,41,20,9,11,14,15,15,33,33,11]

		worksheet.set_column('A:A', tam_col[0])
		worksheet.set_column('B:B', tam_col[1])
		worksheet.set_column('C:C', tam_col[2])
		worksheet.set_column('D:D', tam_col[3])
		worksheet.set_column('E:E', tam_col[4])
		worksheet.set_column('F:F', tam_col[5])
		worksheet.set_column('G:G', tam_col[6])
		worksheet.set_column('H:H', tam_col[7])
		worksheet.set_column('I:I', tam_col[8])
		worksheet.set_column('J:J', tam_col[9])
		worksheet.set_column('K:K', tam_col[10])
		worksheet.set_column('L:L', tam_col[11])
		worksheet.set_column('M:M', tam_col[12])
		worksheet.set_column('N:N', tam_col[13])
		worksheet.set_column('O:O', tam_col[14])
		worksheet.set_column('P:P', tam_col[15])
		worksheet.set_column('Q:Q', tam_col[16])
		worksheet.set_column('R:R', tam_col[17])

		workbook.close()

		f = open(direccion + 'Detalle_ingreso.xlsx', 'rb')
		
		vals = {
			'output_name': 'Detalle ingresos.xlsx',
			'output_file': base64.encodestring(''.join(f.readlines())),		
		}

		sfs_id = self.env['export.file.save'].create(vals)
		
		return {
			"type": "ir.actions.act_window",
			"res_model": "export.file.save",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}

	def make_invoice(self):
		self.env.cr.execute("""
		DROP VIEW IF EXISTS kardex_entry_income_book CASCADE;
		CREATE OR REPLACE view kardex_entry_income_book as ("""+self._get_sql(self.period.date_start,self.period.date_stop)+""")""")
		self.env.cr.execute("""select tipo_op from kardex_entry_income_book
		group by tipo_op""")
		res = self.env.cr.dictfetchall()
		stock_journal_id = self.env['main.parameter'].search([],limit=1).stock_journal_id
		if not stock_journal_id:
			raise UserError(u'No existe Diario de Existencias en Parametros Principales de Contabilidad para su Compañía')
		
		register = self.env['kardex.entry.income.it'].search([('period_id','=',self.period.id)],limit=1)
		if register:
			if register.move_ids:
				for move in register.move_ids:
					if move.state != 'draft':
						move.button_cancel()
					move.line_ids.unlink()
					move.name = "/"
					move.unlink()
			
		else:
			register = self.env['kardex.entry.income.it'].create({
			'period_id': self.period.id})
		
		for top in res:
			self.env.cr.execute("""select cta_debe, sum(coalesce(amount,0)) as debit from kardex_entry_income_book
			where tipo_op = '%s'
			group by cta_debe"""%top['tipo_op'])
			dic_debit = self.env.cr.dictfetchall()
			lineas = []
			for elem in dic_debit:
				vals = (0,0,{
					'account_id': elem['cta_debe'],
					'name':'POR LOS INGRESOS EN ALMACEN MES DE %s'%(self.period.name),
					'debit': elem['debit'],
					'credit': 0,
					'company_id': self.env.user.company_id.id,
				})
				lineas.append(vals)
			self.env.cr.execute("""select cta_haber,SUM(coalesce(amount,0)) as credit from kardex_entry_income_book
			where tipo_op = '%s'
			group by cta_haber"""%top['tipo_op'])
			dic_credit = self.env.cr.dictfetchall()
			for elem in dic_credit:
				vals = (0,0,{
					'account_id': elem['cta_haber'],
					'name': 'POR LOS INGRESOS EN ALMACEN MES DE %s'%(self.period.name),
					'debit': 0,
					'credit': elem['credit'],
					'company_id': self.env.user.company_id.id,
				})
				lineas.append(vals)
			
			move_id = self.env['account.move'].create({
				'company_id': self.env.user.company_id.id,
				'journal_id': stock_journal_id.id,
				'date': self.period.date_stop,
				'line_ids':lineas,
				'ref': 'INGRESOS-%s'%(self.period.code),
				'kardex_income_id':register.id})
			
			move_id.post()

		return {
			'name': 'Asientos',
			'view_mode': 'tree',
			'view_type': 'form',
			'view_id': self.env.ref('account.view_move_tree').id,
			'res_model': 'account.move',
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', register.move_ids.ids)],
		}

	def _get_sql(self,date_ini,date_end):
		param = self.env['main.parameter'].search([],limit=1)
		if not param.type_operation_outproduction:
			raise UserError(u'Falta configurar Parámetro de "Consumo de Producción" en Parametros de Contabilidad de la Compañía.')
		if not param.type_operation_inproduction:
			raise UserError(u'Falta configurar Parámetro de "Ingreso de Producción" en Parametros de Contabilidad de la Compañía.')
		if not param.type_operation_gv:
			raise UserError(u'Falta configurar Parámetro de "Gasto Vinculado" en Parametros de Contabilidad de la Compañía.')
		if not param.type_operation_sale_ids:
			raise UserError(u'Falta configurar Parámetro de "Tipo de operaciones para ventas" en Parametros de Contabilidad de la Compañía.')

		sql = """SELECT row_number() OVER () AS id, T.* FROM(SELECT 
				GKV.fecha::date,
				GKV.type_doc as tipo,
				GKV.serial as serie,
				GKV.nro as numero,
				GKV.numdoc_cuadre as doc_almacen,
				GKV.doc_partner as ruc,
				GKV.name as empresa,
				ei12.code as tipo_op,
				GKV.name_template as producto,
				GKV.default_code,
				GKV.unidad,
				GKV.ingreso as qty,
				round(GKV.debit,6) as amount,
				CASE WHEN vst_valuation.account_id IS NOT NULL THEN vst_valuation.account_id 
				ELSE (SELECT account_id FROM vst_property_stock_valuation_account WHERE company_id = 1 AND category_id IS NULL LIMIT 1)
				END AS cta_debe,
				case when ei12.code in ({tipo_ventas}) then 
				(CASE WHEN vst_income.account_id IS NOT NULL THEN vst_income.account_id 
				ELSE (SELECT account_id FROM vst_property_account_income_categ WHERE company_id = 1 AND category_id IS NULL LIMIT 1) END) 
				else ei12.account_id end as cta_haber,
				GKV.origen,
				GKV.destino,
				GKV.almacen
				FROM get_kardex_v({date_start_s},{date_end_s},(select array_agg(id) from product_product),(select array_agg(id) from stock_location)) GKV
				LEFT JOIN stock_move SM on SM.id = GKV.stock_moveid
				LEFT JOIN stock_picking SP on  SP.id = SM.picking_id
				LEFT JOIN stock_location ST ON ST.id = GKV.ubicacion_origen
				LEFT JOIN stock_location ST2 ON ST2.id = GKV.ubicacion_destino
				LEFT JOIN einvoice_catalog_12 ei12 on ei12.id = coalesce(SP.einvoice_12 ,(case when GKV.origen is null then {gv}::integer
																											when ST.usage = 'internal' AND ST2.usage = 'production' then {consumo_produccion}::integer
																											when ST.usage = 'production' AND ST2.usage = 'internal' then {ingreso_produccion}::integer end))
				LEFT JOIN product_product PP ON PP.id = GKV.product_id
				LEFT JOIN product_template PT ON PT.id = PP.product_tmpl_id
				LEFT JOIN (SELECT category_id,account_id
				FROM vst_property_stock_valuation_account 
				WHERE company_id = 1) vst_valuation ON vst_valuation.category_id = PT.categ_id
				LEFT JOIN (SELECT category_id,account_id
				FROM vst_property_account_income_categ 
				WHERE company_id = 1) vst_income ON vst_income.category_id = PT.categ_id
				WHERE (GKV.fecha::date BETWEEN '{date_ini}' AND '{date_end}') and coalesce(GKV.debit,0) > 0)T
	
		""".format(
				date_start_s = datetime.strptime(self.period.fiscalyear_id.date_start, '%Y-%m-%d').strftime("%Y%m%d"),
				date_end_s = datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y%m%d"),
				date_ini = datetime.strptime(date_ini, '%Y-%m-%d').strftime('%Y/%m/%d'),
				date_end = datetime.strptime(date_end, '%Y-%m-%d').strftime('%Y/%m/%d'),
				consumo_produccion = param.type_operation_outproduction.id,
				gv = param.type_operation_gv.id,
				ingreso_produccion = param.type_operation_inproduction.id,
				tipo_ventas = ','.join("'%s'"%i.code for i in param.type_operation_sale_ids)
			)
		return sql