# -*- encoding: utf-8 -*-
{
	'name': 'Detalle de Movimientos en Asientos Contables',
	'category': 'stock',
	'author': 'ITGRUPO,Glenda Julia Merma Mayhua',
	'depends': ['kardex_it_ferco','import_base_it','kardex_it','stock','account_parametros_it'],
	'version': '1.0',
	'description':"""
	- Detalle de Movimientos en Asientos Contables
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		'SQL.sql',
		'security/ir.model.access.csv',
		'wizards/kardex_entry_income_wizard.xml',
		'wizards/kardex_entry_outcome_wizard.xml',
		'views/main_parameter.xml',
		'views/kardex_entry_income_book.xml',
		'views/kardex_entry_outcome_book.xml',
		'views/type_operation_kardex.xml'
	],
	'installable': True,
	'license': 'LGPL-3'
}
