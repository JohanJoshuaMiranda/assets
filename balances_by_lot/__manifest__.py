# -*- encoding: utf-8 -*-
{
	'name': 'Saldos por Lote',
	'category': 'account',
	'author': 'ITGRUPO',
	'depends': ['stock','account_balance_report'],
	'version': '1.0',
	'description':"""
	Modulo para agregar menu de Saldos por Lote
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
			'balances_by_lot.xml',
			'balances_by_lot_csv.xml'],
	'installable': True
}
