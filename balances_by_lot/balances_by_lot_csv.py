# -*- encoding: utf-8 -*-
from odoo import models, fields, api
import base64

class BalancesByLotCsv(models.Model):
	_name = 'balances.by.lot.csv'

	def get_balance_csv(self):
		direccion = self.env['main.parameter'].search([])[0].dir_create_file
		sql = """
			select 
			sl.complete_name,
			coalesce(pp.default_code,pt.default_code) as code,
			(vsl.entrada - vsl.salida) as saldo_fisico,
			vsl.saldo,
			coalesce(pp.campo_name_get,pt.name) as product_name,
			spl.name
			from vst_saldos_por_lote vsl
			inner join product_product pp on pp.id = vsl.product_id
			inner join product_template pt on pt.id = pp.product_tmpl_id
			inner join stock_production_lot spl on spl.id = vsl.lot_id
			inner join stock_location sl on sl.id = vsl.location_id
			where (vsl.entrada - vsl.salida) >= 0
		"""
		self.env.cr.execute("COPY (%s) TO '%saccount_balance_by_lot_csv.csv' DELIMITER '|' CSV" % (sql,direccion))
		f = open(direccion + 'account_balance_by_lot_csv.csv', 'rb')
		vals = {
			'output_name': 'Reporte de Saldos por Lote.csv',
			'output_file': base64.encodestring(''.join(f.readlines())),
		}
		sfs_id = self.env['custom.export.file'].create(vals)

		return {
			"type": "ir.actions.act_window",
			"res_model": "custom.export.file",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}