# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError
from datetime import *

class BalancesByLot(models.Model):
	_name = 'balances.by.lot'

	product_id = fields.Many2one('product.product',string='Producto')
	lot_id = fields.Many2one('stock.production.lot',string='Lote')
	location_id = fields.Many2one('stock.location',string='Almacen')
	life_date = fields.Datetime(string='Fecha Caducidad')
	move_in = fields.Float(string='Entrada')
	move_out = fields.Float(string='Salida')
	balance = fields.Float(string='Saldo')

	@api.multi
	def get_data(self):
		balances = self.env['balances.by.lot'].search([])
		if balances:
			balances.unlink()
		self.env.cr.execute("""select * from vst_saldos_por_lote""")
		data = self.env.cr.dictfetchall()
		for i in data:
			lot_object = self.env['stock.production.lot'].browse(i['lot_id'])
			self.env['balances.by.lot'].create({'product_id':i['product_id'],
												'lot_id':i['lot_id'],
												'location_id':i['location_id'],
												'life_date':lot_object.life_date if lot_object else '',
												'move_in':i['entrada'],
												'move_out':i['salida'],
												'balance':i['saldo']})
		return {
				'name': 'Saldos por Lote',
				'type':'ir.actions.act_window',
				'view_type':'form',
				'view_mode':'tree',
				'res_model':'balances.by.lot',
				'views':[[self.env.ref('balances_by_lot.balances_by_lot_tree_view').id,'tree']]
			}