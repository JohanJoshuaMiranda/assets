# -*- encoding: utf-8 -*-
{
	'name': 'Analisis Ventas',
	'category': 'account',
	'author': 'ITGRUPO-FERCO',
	'depends': ['sale','sales_team','add_field_type_venta_js_it'],
	'version': '1.0',
	'description':"""
	Analisis Ventas
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		'sql_view.sql',
		'analisis_ventas.xml',
		'security/security.xml'
		],
	'installable': True
}
