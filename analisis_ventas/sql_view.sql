DROP VIEW IF EXISTS public.sale_analysis_book CASCADE;

CREATE OR REPLACE VIEW public.sale_analysis_book
 AS
SELECT account_invoice.reference AS "NumDoc",
			account_invoice.date_invoice AS "Fch_Emision",
			rp.name AS "Cliente",
			account_invoice.origin AS origin,
CASE WHEN order_partner.partner is not null then order_partner.partner else
			rp2.name END AS "Direccion de Entrega",
			account_invoice.state AS "Estado",
			account_invoice_line.price_unit AS "Precio_Unitario",
			account_invoice_line.quantity AS "Cantidad",
			account_invoice_line.quantity AS "Ind_Cantidad",
			account_invoice.currency_rate_auto AS "TipoC",
			so.client_order_ref as reference_client,
			so.type_sale_js as type_sale_js ,
				CASE
					WHEN account_invoice_line.price_unit > 0::numeric AND account_invoice_line.quantity > 0::numeric THEN 100::numeric - account_invoice_line.price_subtotal * 100::numeric / (account_invoice_line.price_unit * account_invoice_line.quantity)
					ELSE 0::numeric
				END AS "Descuento",
				CASE
					WHEN res_currency.name::text = 'PEN'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Soles",
				CASE
					WHEN res_currency.name::text = 'USD'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Dolares",
				CASE
					WHEN res_currency.name::text = 'PEN'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					WHEN res_currency.name::text = 'USD'::text THEN account_invoice_line.price_subtotal * account_invoice.currency_rate_auto *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Tot_Soles",
			product_template.name as "Producto",
			product_template.default_code as "Codigo",
			product_category.name AS "Categoria",
			product_brand.name AS "Marca",
			product_uom.name AS "Unidad",
			hr_employee.name_related AS "Ejecutivo",
			res_partner.name AS "Vendedor",
			crm_team.name AS "Equipo",
			date_part('YEAR'::text, account_invoice.date_invoice) AS "Anio",
			date_part('MONTH'::text, account_invoice.date_invoice) AS "Mes",
			date_part('DAY'::text, account_invoice.date_invoice) AS "Dia",
			eic.name AS "TipoDoc",
			itser.name AS "Serie",
			pc1.name AS "Categoria1",
			pc2.name AS "Categoria2",
			account_invoice.id as invoice_id
			FROM account_invoice
			 JOIN res_partner rp ON account_invoice.partner_id = rp.id
			 LEFT JOIN res_partner rp2 ON account_invoice.partner_shipping_id = rp2.id
             LEFT JOIN (select distinct
	                     ai.id as invoice_id,
	                     so.id as so_id,
	                     rp.name as partner
                        from  sale_order_line_invoice_rel rel
                        inner join sale_order_line sol on rel.order_line_id = sol.id
                        inner join sale_order so on so.id = sol.order_id
                        inner join res_partner rp on rp.id = so.partner_invoice_id
                        inner join account_invoice_line ail on ail.id = rel.invoice_line_id
                        inner join account_invoice ai on ai.id = ail.invoice_id) order_partner on order_partner.invoice_id = account_invoice.id
             LEFT JOIN sale_order so ON so.id = order_partner.so_id
			 JOIN res_currency ON account_invoice.currency_id = res_currency.id
			 LEFT JOIN einvoice_catalog_01 eic ON eic.id = account_invoice.it_type_document
			 LEFT JOIN it_invoice_serie itser ON itser.id = account_invoice.serie_id
			 JOIN account_invoice_line ON account_invoice.id = account_invoice_line.invoice_id
			 LEFT JOIN product_product ON product_product.id = account_invoice_line.product_id
			 LEFT JOIN product_template ON product_template.id = product_product.product_tmpl_id
			 LEFT JOIN product_brand ON product_brand.id = product_template.brand_id
			 LEFT JOIN product_uom ON product_uom.id = account_invoice_line.uom_id
			 LEFT JOIN res_users ON account_invoice.user_id = res_users.id
			 LEFT JOIN hr_employee ON account_invoice.sales_executive_id = hr_employee.id
			 LEFT JOIN crm_team ON crm_team.id = account_invoice.team_id
			 LEFT JOIN res_partner ON res_partner.id = res_users.partner_id
			 LEFT JOIN product_category ON product_category.id = product_template.categ_id
			 LEFT JOIN product_category pc1 ON pc1.id = product_category.parent_id
			 LEFT JOIN product_category pc2 ON pc2.id = pc1.parent_id
		  WHERE (account_invoice.move_id IN ( SELECT get_venta_1.am_id
			FROM get_venta_1(201900, 201913) get_venta_1(comprobante, am_id, clasifica, base_impuesto, monto, record_sale))) AND (res_currency.name::text = 'PEN'::text OR res_currency.name::text = 'USD'::text)
		UNION ALL
		 SELECT account_invoice.reference AS "NumDoc",
			account_invoice.date_invoice AS "Fch_Emision",
			rp.name AS "Cliente",
			account_invoice.origin AS origin,
CASE WHEN order_partner.partner is not null THEN order_partner.partner  ELSE
						rp2.name END AS "Direccion de Entrega",
			account_invoice.state AS "Estado",
			account_invoice_line.price_unit AS "Precio_Unitario",
			account_invoice_line.quantity AS "Cantidad",
			account_invoice_line.quantity AS "Ind_Cantidad",
			account_invoice.currency_rate_auto AS "TipoC",
			so.client_order_ref as reference_client,
			so.type_sale_js as type_sale_js ,
				CASE
					WHEN account_invoice_line.price_unit > 0::numeric AND account_invoice_line.quantity > 0::numeric THEN 100::numeric - account_invoice_line.price_subtotal * 100::numeric / (account_invoice_line.price_unit * account_invoice_line.quantity)
					ELSE 0::numeric
				END AS "Descuento",
				CASE
					WHEN res_currency.name::text = 'PEN'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Soles",
				CASE
					WHEN res_currency.name::text = 'USD'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Dolares",
				CASE
					WHEN res_currency.name::text = 'PEN'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					WHEN res_currency.name::text = 'USD'::text THEN account_invoice_line.price_subtotal * account_invoice.currency_rate_auto *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Tot_Soles",
			product_template.name as "Producto",
			product_template.default_code as "Codigo",
			product_category.name AS "Categoria",
			product_brand.name AS "Marca",
			product_uom.name AS "Unidad",
			hr_employee.name_related AS "Ejecutivo",
			res_partner.name AS "Vendedor",
			crm_team.name AS "Equipo",
			date_part('YEAR'::text, account_invoice.date_invoice) AS "Anio",
			date_part('MONTH'::text, account_invoice.date_invoice) AS "Mes",
			date_part('DAY'::text, account_invoice.date_invoice) AS "Dia",
			eic.name AS "TipoDoc",
			itser.name AS "Serie",
			pc1.name AS "Categoria1",
			pc2.name AS "Categoria2",
			account_invoice.id as invoice_id
			FROM account_invoice
			 JOIN res_partner rp ON account_invoice.partner_id = rp.id
			LEFT JOIN res_partner rp2 on account_invoice.partner_shipping_id = rp2.id
LEFT JOIN (
select distinct
	ai.id as invoice_id,
	so.id as so_id,
	rp.name as partner
from
sale_order_line_invoice_rel rel
inner join sale_order_line sol on rel.order_line_id = sol.id
inner join sale_order so on so.id = sol.order_id
inner join res_partner rp on rp.id = so.partner_invoice_id
inner join account_invoice_line ail on ail.id = rel.invoice_line_id
inner join account_invoice ai on ai.id = ail.invoice_id) order_partner on order_partner.invoice_id = account_invoice.id
             LEFT JOIN sale_order so ON so.id = order_partner.so_id
			 JOIN res_currency ON account_invoice.currency_id = res_currency.id
			 LEFT JOIN einvoice_catalog_01 eic ON eic.id = account_invoice.it_type_document
			 LEFT JOIN it_invoice_serie itser ON itser.id = account_invoice.serie_id
			 JOIN account_invoice_line ON account_invoice.id = account_invoice_line.invoice_id
			 LEFT JOIN product_product ON product_product.id = account_invoice_line.product_id
			 LEFT JOIN product_template ON product_template.id = product_product.product_tmpl_id
			 LEFT JOIN product_brand ON product_brand.id = product_template.brand_id
			 LEFT JOIN product_uom ON product_uom.id = account_invoice_line.uom_id
			 LEFT JOIN res_users ON account_invoice.user_id = res_users.id
			 LEFT JOIN hr_employee ON account_invoice.sales_executive_id = hr_employee.id
			 LEFT JOIN crm_team ON crm_team.id = account_invoice.team_id
			 LEFT JOIN res_partner ON res_partner.id = res_users.partner_id
			 LEFT JOIN product_category ON product_category.id = product_template.categ_id
			 LEFT JOIN product_category pc1 ON pc1.id = product_category.parent_id
			 LEFT JOIN product_category pc2 ON pc2.id = pc1.parent_id
			WHERE (account_invoice.move_id IN ( SELECT get_venta_1.am_id
					FROM get_venta_1(202000, 202013) get_venta_1(comprobante, am_id, clasifica, base_impuesto, monto, record_sale))) AND (res_currency.name::text = 'PEN'::text OR res_currency.name::text = 'USD'::text)
UNION ALL
		 SELECT account_invoice.reference AS "NumDoc",
			account_invoice.date_invoice AS "Fch_Emision",
			rp.name AS "Cliente",
			account_invoice.origin AS origin,
CASE WHEN order_partner.partner is not null THEN order_partner.partner  ELSE
						rp2.name END AS "Direccion de Entrega",
			account_invoice.state AS "Estado",
			account_invoice_line.price_unit AS "Precio_Unitario",
			account_invoice_line.quantity AS "Cantidad",
			account_invoice_line.quantity AS "Ind_Cantidad",
			account_invoice.currency_rate_auto AS "TipoC",
			so.client_order_ref as reference_client,
			so.type_sale_js as type_sale_js ,
				CASE
					WHEN account_invoice_line.price_unit > 0::numeric AND account_invoice_line.quantity > 0::numeric THEN 100::numeric - account_invoice_line.price_subtotal * 100::numeric / (account_invoice_line.price_unit * account_invoice_line.quantity)
					ELSE 0::numeric
				END AS "Descuento",
				CASE
					WHEN res_currency.name::text = 'PEN'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Soles",
				CASE
					WHEN res_currency.name::text = 'USD'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Dolares",
				CASE
					WHEN res_currency.name::text = 'PEN'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					WHEN res_currency.name::text = 'USD'::text THEN account_invoice_line.price_subtotal * account_invoice.currency_rate_auto *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Tot_Soles",
			product_template.name as "Producto",
			product_template.default_code as "Codigo",
			product_category.name AS "Categoria",
			product_brand.name AS "Marca",
			product_uom.name AS "Unidad",
			hr_employee.name_related AS "Ejecutivo",
			res_partner.name AS "Vendedor",
			crm_team.name AS "Equipo",
			date_part('YEAR'::text, account_invoice.date_invoice) AS "Anio",
			date_part('MONTH'::text, account_invoice.date_invoice) AS "Mes",
			date_part('DAY'::text, account_invoice.date_invoice) AS "Dia",
			eic.name AS "TipoDoc",
			itser.name AS "Serie",
			pc1.name AS "Categoria1",
			pc2.name AS "Categoria2",
			account_invoice.id as invoice_id
			FROM account_invoice
			 JOIN res_partner rp ON account_invoice.partner_id = rp.id
			LEFT JOIN res_partner rp2 on account_invoice.partner_shipping_id = rp2.id
LEFT JOIN (
select distinct
	ai.id as invoice_id,
	so.id as so_id,
	rp.name as partner
from
sale_order_line_invoice_rel rel
inner join sale_order_line sol on rel.order_line_id = sol.id
inner join sale_order so on so.id = sol.order_id
inner join res_partner rp on rp.id = so.partner_invoice_id
inner join account_invoice_line ail on ail.id = rel.invoice_line_id
inner join account_invoice ai on ai.id = ail.invoice_id) order_partner on order_partner.invoice_id = account_invoice.id
             LEFT JOIN sale_order so ON so.id = order_partner.so_id
			 JOIN res_currency ON account_invoice.currency_id = res_currency.id
			 LEFT JOIN einvoice_catalog_01 eic ON eic.id = account_invoice.it_type_document
			 LEFT JOIN it_invoice_serie itser ON itser.id = account_invoice.serie_id
			 JOIN account_invoice_line ON account_invoice.id = account_invoice_line.invoice_id
			 LEFT JOIN product_product ON product_product.id = account_invoice_line.product_id
			 LEFT JOIN product_template ON product_template.id = product_product.product_tmpl_id
			 LEFT JOIN product_brand ON product_brand.id = product_template.brand_id
			 LEFT JOIN product_uom ON product_uom.id = account_invoice_line.uom_id
			 LEFT JOIN res_users ON account_invoice.user_id = res_users.id
			 LEFT JOIN hr_employee ON account_invoice.sales_executive_id = hr_employee.id
			 LEFT JOIN crm_team ON crm_team.id = account_invoice.team_id
			 LEFT JOIN res_partner ON res_partner.id = res_users.partner_id
			 LEFT JOIN product_category ON product_category.id = product_template.categ_id
			 LEFT JOIN product_category pc1 ON pc1.id = product_category.parent_id
			 LEFT JOIN product_category pc2 ON pc2.id = pc1.parent_id
			WHERE (account_invoice.move_id IN ( SELECT get_venta_1.am_id
					FROM get_venta_1(202100, 202113) get_venta_1(comprobante, am_id, clasifica, base_impuesto, monto, record_sale))) AND (res_currency.name::text = 'PEN'::text OR res_currency.name::text = 'USD'::text)
UNION ALL
SELECT account_invoice.reference AS "NumDoc",
			account_invoice.date_invoice AS "Fch_Emision",
			rp.name AS "Cliente",
			account_invoice.origin AS origin,
CASE WHEN order_partner.partner is not null THEN order_partner.partner  ELSE
						rp2.name END AS "Direccion de Entrega",
			account_invoice.state AS "Estado",
			account_invoice_line.price_unit AS "Precio_Unitario",
			account_invoice_line.quantity AS "Cantidad",
			account_invoice_line.quantity AS "Ind_Cantidad",
			account_invoice.currency_rate_auto AS "TipoC",
			so.client_order_ref as reference_client,
			so.type_sale_js as type_sale_js ,
				CASE
					WHEN account_invoice_line.price_unit > 0::numeric AND account_invoice_line.quantity > 0::numeric THEN 100::numeric - account_invoice_line.price_subtotal * 100::numeric / (account_invoice_line.price_unit * account_invoice_line.quantity)
					ELSE 0::numeric
				END AS "Descuento",
				CASE
					WHEN res_currency.name::text = 'PEN'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Soles",
				CASE
					WHEN res_currency.name::text = 'USD'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Dolares",
				CASE
					WHEN res_currency.name::text = 'PEN'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					WHEN res_currency.name::text = 'USD'::text THEN account_invoice_line.price_subtotal * account_invoice.currency_rate_auto *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Tot_Soles",
			product_template.name as "Producto",
			product_template.default_code as "Codigo",
			product_category.name AS "Categoria",
			product_brand.name AS "Marca",
			product_uom.name AS "Unidad",
			hr_employee.name_related AS "Ejecutivo",
			res_partner.name AS "Vendedor",
			crm_team.name AS "Equipo",
			date_part('YEAR'::text, account_invoice.date_invoice) AS "Anio",
			date_part('MONTH'::text, account_invoice.date_invoice) AS "Mes",
			date_part('DAY'::text, account_invoice.date_invoice) AS "Dia",
			eic.name AS "TipoDoc",
			itser.name AS "Serie",
			pc1.name AS "Categoria1",
			pc2.name AS "Categoria2",
			account_invoice.id as invoice_id
			FROM account_invoice
			 JOIN res_partner rp ON account_invoice.partner_id = rp.id
			LEFT JOIN res_partner rp2 on account_invoice.partner_shipping_id = rp2.id
LEFT JOIN (
select distinct
	ai.id as invoice_id,
	so.id as so_id,
	rp.name as partner
from
sale_order_line_invoice_rel rel
inner join sale_order_line sol on rel.order_line_id = sol.id
inner join sale_order so on so.id = sol.order_id
inner join res_partner rp on rp.id = so.partner_invoice_id
inner join account_invoice_line ail on ail.id = rel.invoice_line_id
inner join account_invoice ai on ai.id = ail.invoice_id) order_partner on order_partner.invoice_id = account_invoice.id
			 LEFT JOIN sale_order so ON so.id = order_partner.so_id
			 JOIN res_currency ON account_invoice.currency_id = res_currency.id
			 LEFT JOIN einvoice_catalog_01 eic ON eic.id = account_invoice.it_type_document
			 LEFT JOIN it_invoice_serie itser ON itser.id = account_invoice.serie_id
			 JOIN account_invoice_line ON account_invoice.id = account_invoice_line.invoice_id
			 LEFT JOIN product_product ON product_product.id = account_invoice_line.product_id
			 LEFT JOIN product_template ON product_template.id = product_product.product_tmpl_id
			 LEFT JOIN product_brand ON product_brand.id = product_template.brand_id
			 LEFT JOIN product_uom ON product_uom.id = account_invoice_line.uom_id
			 LEFT JOIN res_users ON account_invoice.user_id = res_users.id
			 LEFT JOIN hr_employee ON account_invoice.sales_executive_id = hr_employee.id
			 LEFT JOIN crm_team ON crm_team.id = account_invoice.team_id
			 LEFT JOIN res_partner ON res_partner.id = res_users.partner_id
			 LEFT JOIN product_category ON product_category.id = product_template.categ_id
			 LEFT JOIN product_category pc1 ON pc1.id = product_category.parent_id
			 LEFT JOIN product_category pc2 ON pc2.id = pc1.parent_id
			WHERE (account_invoice.move_id IN ( SELECT get_venta_1.am_id
					FROM get_venta_1(202200, 202213) get_venta_1(comprobante, am_id, clasifica, base_impuesto, monto, record_sale))) AND (res_currency.name::text = 'PEN'::text OR res_currency.name::text = 'USD'::text)
UNION ALL
SELECT account_invoice.reference AS "NumDoc",
			account_invoice.date_invoice AS "Fch_Emision",
			rp.name AS "Cliente",
			account_invoice.origin AS origin,
CASE WHEN order_partner.partner is not null THEN order_partner.partner  ELSE
						rp2.name END AS "Direccion de Entrega",
			account_invoice.state AS "Estado",
			account_invoice_line.price_unit AS "Precio_Unitario",
			account_invoice_line.quantity AS "Cantidad",
			account_invoice_line.quantity AS "Ind_Cantidad",
			account_invoice.currency_rate_auto AS "TipoC",
			so.client_order_ref as reference_client,
			so.type_sale_js as type_sale_js ,
				CASE
					WHEN account_invoice_line.price_unit > 0::numeric AND account_invoice_line.quantity > 0::numeric THEN 100::numeric - account_invoice_line.price_subtotal * 100::numeric / (account_invoice_line.price_unit * account_invoice_line.quantity)
					ELSE 0::numeric
				END AS "Descuento",
				CASE
					WHEN res_currency.name::text = 'PEN'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Soles",
				CASE
					WHEN res_currency.name::text = 'USD'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Dolares",
				CASE
					WHEN res_currency.name::text = 'PEN'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					WHEN res_currency.name::text = 'USD'::text THEN account_invoice_line.price_subtotal * account_invoice.currency_rate_auto *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Tot_Soles",
			product_template.name as "Producto",
			product_template.default_code as "Codigo",
			product_category.name AS "Categoria",
			product_brand.name AS "Marca",
			product_uom.name AS "Unidad",
			hr_employee.name_related AS "Ejecutivo",
			res_partner.name AS "Vendedor",
			crm_team.name AS "Equipo",
			date_part('YEAR'::text, account_invoice.date_invoice) AS "Anio",
			date_part('MONTH'::text, account_invoice.date_invoice) AS "Mes",
			date_part('DAY'::text, account_invoice.date_invoice) AS "Dia",
			eic.name AS "TipoDoc",
			itser.name AS "Serie",
			pc1.name AS "Categoria1",
			pc2.name AS "Categoria2",
			account_invoice.id as invoice_id
			FROM account_invoice
			 JOIN res_partner rp ON account_invoice.partner_id = rp.id
			LEFT JOIN res_partner rp2 on account_invoice.partner_shipping_id = rp2.id
LEFT JOIN (
select distinct
	ai.id as invoice_id,
	so.id as so_id,
	rp.name as partner
from
sale_order_line_invoice_rel rel
inner join sale_order_line sol on rel.order_line_id = sol.id
inner join sale_order so on so.id = sol.order_id
inner join res_partner rp on rp.id = so.partner_invoice_id
inner join account_invoice_line ail on ail.id = rel.invoice_line_id
inner join account_invoice ai on ai.id = ail.invoice_id) order_partner on order_partner.invoice_id = account_invoice.id
             LEFT JOIN sale_order so ON so.id = order_partner.so_id
			 JOIN res_currency ON account_invoice.currency_id = res_currency.id
			 LEFT JOIN einvoice_catalog_01 eic ON eic.id = account_invoice.it_type_document
			 LEFT JOIN it_invoice_serie itser ON itser.id = account_invoice.serie_id
			 JOIN account_invoice_line ON account_invoice.id = account_invoice_line.invoice_id
			 LEFT JOIN product_product ON product_product.id = account_invoice_line.product_id
			 LEFT JOIN product_template ON product_template.id = product_product.product_tmpl_id
			 LEFT JOIN product_brand ON product_brand.id = product_template.brand_id
			 LEFT JOIN product_uom ON product_uom.id = account_invoice_line.uom_id
			 LEFT JOIN res_users ON account_invoice.user_id = res_users.id
			 LEFT JOIN hr_employee ON account_invoice.sales_executive_id = hr_employee.id
			 LEFT JOIN crm_team ON crm_team.id = account_invoice.team_id
			 LEFT JOIN res_partner ON res_partner.id = res_users.partner_id
			 LEFT JOIN product_category ON product_category.id = product_template.categ_id
			 LEFT JOIN product_category pc1 ON pc1.id = product_category.parent_id
			 LEFT JOIN product_category pc2 ON pc2.id = pc1.parent_id
			WHERE (account_invoice.move_id IN ( SELECT get_venta_1.am_id
					FROM get_venta_1(202300, 202313) get_venta_1(comprobante, am_id, clasifica, base_impuesto, monto, record_sale))) AND (res_currency.name::text = 'PEN'::text OR res_currency.name::text = 'USD'::text)
UNION ALL
SELECT account_invoice.reference AS "NumDoc",
			account_invoice.date_invoice AS "Fch_Emision",
			rp.name AS "Cliente",
			account_invoice.origin AS origin,
CASE WHEN order_partner.partner is not null THEN order_partner.partner  ELSE
						rp2.name END AS "Direccion de Entrega",
			account_invoice.state AS "Estado",
			account_invoice_line.price_unit AS "Precio_Unitario",
			account_invoice_line.quantity AS "Cantidad",
			account_invoice_line.quantity AS "Ind_Cantidad",
			account_invoice.currency_rate_auto AS "TipoC",
			so.client_order_ref as reference_client,
			so.type_sale_js as type_sale_js ,
				CASE
					WHEN account_invoice_line.price_unit > 0::numeric AND account_invoice_line.quantity > 0::numeric THEN 100::numeric - account_invoice_line.price_subtotal * 100::numeric / (account_invoice_line.price_unit * account_invoice_line.quantity)
					ELSE 0::numeric
				END AS "Descuento",
				CASE
					WHEN res_currency.name::text = 'PEN'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Soles",
				CASE
					WHEN res_currency.name::text = 'USD'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Dolares",
				CASE
					WHEN res_currency.name::text = 'PEN'::text THEN account_invoice_line.price_subtotal *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					WHEN res_currency.name::text = 'USD'::text THEN account_invoice_line.price_subtotal * account_invoice.currency_rate_auto *
					CASE
						WHEN eic.name::text = 'Nota de crédito'::text THEN '-1'::integer
						ELSE 1
					END::numeric
					ELSE 0::numeric
				END AS "Tot_Soles",
			product_template.name as "Producto",
			product_template.default_code as "Codigo",
			product_category.name AS "Categoria",
			product_brand.name AS "Marca",
			product_uom.name AS "Unidad",
			hr_employee.name_related AS "Ejecutivo",
			res_partner.name AS "Vendedor",
			crm_team.name AS "Equipo",
			date_part('YEAR'::text, account_invoice.date_invoice) AS "Anio",
			date_part('MONTH'::text, account_invoice.date_invoice) AS "Mes",
			date_part('DAY'::text, account_invoice.date_invoice) AS "Dia",
			eic.name AS "TipoDoc",
			itser.name AS "Serie",
			pc1.name AS "Categoria1",
			pc2.name AS "Categoria2",
			account_invoice.id as invoice_id
			FROM account_invoice
			 JOIN res_partner rp ON account_invoice.partner_id = rp.id
			LEFT JOIN res_partner rp2 on account_invoice.partner_shipping_id = rp2.id
LEFT JOIN (
select distinct
	ai.id as invoice_id,
	so.id as so_id,
	rp.name as partner
from
sale_order_line_invoice_rel rel
inner join sale_order_line sol on rel.order_line_id = sol.id
inner join sale_order so on so.id = sol.order_id
inner join res_partner rp on rp.id = so.partner_invoice_id
inner join account_invoice_line ail on ail.id = rel.invoice_line_id
inner join account_invoice ai on ai.id = ail.invoice_id) order_partner on order_partner.invoice_id = account_invoice.id
             LEFT JOIN sale_order so ON so.id = order_partner.so_id
			 JOIN res_currency ON account_invoice.currency_id = res_currency.id
			 LEFT JOIN einvoice_catalog_01 eic ON eic.id = account_invoice.it_type_document
			 LEFT JOIN it_invoice_serie itser ON itser.id = account_invoice.serie_id
			 JOIN account_invoice_line ON account_invoice.id = account_invoice_line.invoice_id
			 LEFT JOIN product_product ON product_product.id = account_invoice_line.product_id
			 LEFT JOIN product_template ON product_template.id = product_product.product_tmpl_id
			 LEFT JOIN product_brand ON product_brand.id = product_template.brand_id
			 LEFT JOIN product_uom ON product_uom.id = account_invoice_line.uom_id
			 LEFT JOIN res_users ON account_invoice.user_id = res_users.id
			 LEFT JOIN hr_employee ON account_invoice.sales_executive_id = hr_employee.id
			 LEFT JOIN crm_team ON crm_team.id = account_invoice.team_id
			 LEFT JOIN res_partner ON res_partner.id = res_users.partner_id
			 LEFT JOIN product_category ON product_category.id = product_template.categ_id
			 LEFT JOIN product_category pc1 ON pc1.id = product_category.parent_id
			 LEFT JOIN product_category pc2 ON pc2.id = pc1.parent_id
			WHERE (account_invoice.move_id IN ( SELECT get_venta_1.am_id
					FROM get_venta_1(202400, 202413) get_venta_1(comprobante, am_id, clasifica, base_impuesto, monto, record_sale))) AND (res_currency.name::text = 'PEN'::text OR res_currency.name::text = 'USD'::text)
 ;