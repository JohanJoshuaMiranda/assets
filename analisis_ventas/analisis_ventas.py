# -*- coding: utf-8 -*-

from odoo import api, fields, models
import base64
from odoo.exceptions import UserError, ValidationError


class AnalisisVentasWizard(models.TransientModel):
    _name = "analisis.ventas.wizard"

    def _get_sql(self):
        sql = """
			SELECT *  FROM sale_analysis_book 
			WHERE invoice_id NOT IN (SELECT ail.invoice_id FROM account_invoice_line ail
									LEFT JOIN product_product ON product_product.id = ail.product_id
									LEFT JOIN product_template ON product_template.id = product_product.product_tmpl_id
									WHERE product_template.default_code = 'TRANS-GRA')
			"""
        return sql

    @api.model
    def get_excel(self):
        import io
        from xlsxwriter.workbook import Workbook

        direccion = self.env['main.parameter'].search([])[0].dir_create_file

        workbook = Workbook(direccion + 'analisis_ventas.xlsx')

        boldbord = workbook.add_format({'bold': True})
        boldbord.set_border(style=2)
        boldbord.set_align('center')
        boldbord.set_align('vcenter')
        boldbord.set_text_wrap()
        boldbord.set_font_size(10)
        boldbord.set_bg_color('#DCE6F1')
        boldbord.set_font_name('Times New Roman')

        especial1 = workbook.add_format()
        especial1.set_align('center')
        especial1.set_align('vcenter')
        especial1.set_border(style=1)
        especial1.set_text_wrap()
        especial1.set_font_size(10)
        especial1.set_font_name('Times New Roman')

        especial3 = workbook.add_format({'bold': True})
        especial3.set_align('center')
        especial3.set_align('vcenter')
        especial3.set_border(style=1)
        especial3.set_text_wrap()
        especial3.set_bg_color('#DCE6F1')
        especial3.set_font_size(15)
        especial3.set_font_name('Times New Roman')

        numberuno = workbook.add_format({'num_format': '0.0000'})
        numberuno.set_border(style=1)
        numberuno.set_font_size(10)
        numberuno.set_font_name('Times New Roman')

        numberdos = workbook.add_format({'num_format': '0.000'})
        numberdos.set_border(style=1)
        numberdos.set_font_size(10)
        numberdos.set_font_name('Times New Roman')

        dateformat = workbook.add_format({'num_format': 'd-m-yyyy'})
        dateformat.set_border(style=1)
        dateformat.set_font_size(10)
        dateformat.set_font_name('Times New Roman')

        hourformat = workbook.add_format({'num_format': 'hh:mm'})
        hourformat.set_align('center')
        hourformat.set_align('vcenter')
        hourformat.set_border(style=1)
        hourformat.set_font_size(10)
        hourformat.set_font_name('Times New Roman')

        import sys
        reload(sys)
        sys.setdefaultencoding('iso-8859-1')

        ##########ASISTENCIAS############
        worksheet = workbook.add_worksheet("ANALISIS VENTAS")
        worksheet.set_tab_color('blue')

        worksheet.merge_range(1, 0, 1, 26, "ANALISIS VENTAS", especial3)

        x = 5

        worksheet.write(x, 0, "NUMERO DE DOCUMENTO", boldbord)
        worksheet.write(x, 1, "FECHA DE EMISION", boldbord)
        worksheet.write(x, 2, "CLIENTE", boldbord)
        # este campo agrego jesus sanchez
        worksheet.write(x, 3, "REFERENCIA DEL CLIENTE", boldbord)
        worksheet.write(x, 4, "TIPO VENTA", boldbord)

        worksheet.write(x, 5, "DIRECCION DE FACTURA", boldbord)
        worksheet.write(x, 6, "ESTADO", boldbord)
        worksheet.write(x, 7, "PRECIO UNITARIO", boldbord)
        worksheet.write(x, 8, "CANTIDAD", boldbord)
        worksheet.write(x, 9, "TC", boldbord)
        worksheet.write(x, 10, "DESCUENTO", boldbord)
        worksheet.write(x, 11, "SOLES", boldbord)
        worksheet.write(x, 12, "DOLARES", boldbord)
        worksheet.write(x, 13, "TOTAL SOLES", boldbord)
        worksheet.write(x, 14, "CODIGO", boldbord)
        worksheet.write(x, 15, "PRODUCTO", boldbord)
        worksheet.write(x, 16, "UNIDAD", boldbord)
        worksheet.write(x, 17, "MARCA", boldbord)
        worksheet.write(x, 18, "CATEGORIA", boldbord)
        worksheet.write(x, 19, "EJECUTIVO", boldbord)
        worksheet.write(x, 20, "VENDEDOR", boldbord)
        worksheet.write(x, 21, "EQUIPO", boldbord)
        worksheet.write(x, 22, u'AÑO', boldbord)
        worksheet.write(x, 23, "MES", boldbord)
        worksheet.write(x, 24, "DIA", boldbord)
        worksheet.write(x, 25, "TIPO DOCUMENTO", boldbord)
        worksheet.write(x, 26, "SERIE", boldbord)
        worksheet.write(x, 27, "CATEGORIA 1", boldbord)
        worksheet.write(x, 28, "CATEGORIA 2", boldbord)
        x = 6

        self.env.cr.execute(self._get_sql())
        data = self.env.cr.dictfetchall()

        for line in data:


            worksheet.write(x, 0, line['NumDoc'] if line['NumDoc'] else '', especial1)
            worksheet.write(x, 1, line['Fch_Emision'] if line['Fch_Emision'] else '', dateformat)
            worksheet.write(x, 2, line['Cliente'] if line['Cliente'] else '', especial1)
            # este campo agrego jesus sanchez
            worksheet.write(x, 3, line['reference_client'] if line['reference_client'] else '', especial1)
            worksheet.write(x, 4, line['type_sale_js'] if line['type_sale_js'] else '', especial1)

            worksheet.write(x, 5, line['Direccion de Entrega'] if line['Direccion de Entrega'] else '', especial1)
            worksheet.write(x, 6, line['Estado'] if line['Estado'] else '', especial1)
            worksheet.write(x, 7, line['Precio_Unitario'] if line['Precio_Unitario'] else '0.0000', numberuno)
            worksheet.write(x, 8, line['Cantidad'] if line['Cantidad'] else '', numberdos)
            worksheet.write(x, 9, line['TipoC'] if line['TipoC'] else '0.000', numberdos)
            worksheet.write(x, 10, line['Descuento'] if line['Descuento'] else '0.0000', numberuno)
            worksheet.write(x, 11, line['Soles'] if line['Soles'] else '0.000', numberdos)
            worksheet.write(x, 12, line['Dolares'] if line['Dolares'] else '0', numberdos)
            worksheet.write(x, 13, line['Tot_Soles'] if line['Tot_Soles'] else '0.0000', numberuno)
            worksheet.write(x, 14, line['Codigo'] if line['Codigo'] else '', especial1)
            worksheet.write(x, 15, line['Producto'] if line['Producto'] else '', especial1)
            worksheet.write(x, 16, line['Unidad'] if line['Unidad'] else '', especial1)
            worksheet.write(x, 17, line['Marca'] if line['Marca'] else '', especial1)
            worksheet.write(x, 18, line['Categoria'] if line['Categoria'] else '', especial1)
            worksheet.write(x, 19, line['Ejecutivo'] if line['Ejecutivo'] else '', especial1)
            worksheet.write(x, 20, line['Vendedor'] if line['Vendedor'] else '', especial1)
            worksheet.write(x, 21, line['Equipo'] if line['Equipo'] else '', especial1)
            worksheet.write(x, 22, line['Anio'] if line['Anio'] else '', especial1)
            worksheet.write(x, 23, line['Mes'] if line['Mes'] else '', especial1)
            worksheet.write(x, 24, line['Dia'] if line['Dia'] else '', especial1)
            worksheet.write(x, 25, line['TipoDoc'] if line['TipoDoc'] else '', especial1)
            worksheet.write(x, 26, line['Serie'] if line['Serie'] else '', especial1)
            worksheet.write(x, 27, line['Categoria1'] if line['Categoria1'] else '', especial1)
            worksheet.write(x, 28, line['Categoria2'] if line['Categoria2'] else '', especial1)
            x += 1

        tam_col = [24, 17, 38, 38, 38,9, 10, 10, 7, 10,
                   10, 10, 12, 12, 50, 17, 17, 17, 30, 30,
                   20,7, 5, 5, 12, 10, 20,17]

        worksheet.set_column('A:A', tam_col[0])
        worksheet.set_column('B:B', tam_col[1])
        worksheet.set_column('C:C', tam_col[2])
        worksheet.set_column('D:D', tam_col[3])
        worksheet.set_column('E:E', tam_col[4])
        worksheet.set_column('F:F', tam_col[5])
        worksheet.set_column('G:G', tam_col[6])
        worksheet.set_column('H:H', tam_col[7])
        worksheet.set_column('I:I', tam_col[8])
        worksheet.set_column('J:J', tam_col[9])
        worksheet.set_column('K:K', tam_col[10])
        worksheet.set_column('L:L', tam_col[11])
        worksheet.set_column('M:M', tam_col[12])
        worksheet.set_column('N:N', tam_col[13])
        worksheet.set_column('O:O', tam_col[14])
        worksheet.set_column('P:P', tam_col[15])
        worksheet.set_column('Q:Q', tam_col[16])
        worksheet.set_column('R:R', tam_col[17])
        worksheet.set_column('S:S', tam_col[18])
        worksheet.set_column('T:T', tam_col[19])
        worksheet.set_column('U:U', tam_col[20])
        worksheet.set_column('V:V', tam_col[21])
        worksheet.set_column('W:W', tam_col[22])
        worksheet.set_column('X:X', tam_col[23])
        worksheet.set_column('Y:Y', tam_col[24])
        worksheet.set_column('Z:Z', tam_col[25])
        worksheet.set_column('AA:AA', tam_col[26])

        workbook.close()

        f = open(direccion + 'analisis_ventas.xlsx', 'rb')

        vals = {
            'output_name': 'Analisis de Ventas.xlsx',
            'output_file': base64.encodestring(''.join(f.readlines()))
        }

        sfs_id = self.env['custom.export.file'].create(vals)

        return {
            "type": "ir.actions.act_window",
            "res_model": "custom.export.file",
            "views": [[False, "form"]],
            "res_id": sfs_id.id,
            "target": "new",
        }

    def _get_sql_free(self):
        sql = """
			SELECT * FROM sale_analysis_book 
			WHERE invoice_id IN (SELECT ail.invoice_id FROM account_invoice_line ail
									LEFT JOIN product_product ON product_product.id = ail.product_id
									LEFT JOIN product_template ON product_template.id = product_product.product_tmpl_id
									WHERE product_template.default_code = 'TRANS-GRA')
			"""
        return sql

    @api.model
    def get_excel_free_transfer(self):
        import io
        from xlsxwriter.workbook import Workbook

        direccion = self.env['main.parameter'].search([])[0].dir_create_file

        workbook = Workbook(direccion + 'analisis_tranferencias_gratuitas.xlsx')

        boldbord = workbook.add_format({'bold': True})
        boldbord.set_border(style=2)
        boldbord.set_align('center')
        boldbord.set_align('vcenter')
        boldbord.set_text_wrap()
        boldbord.set_font_size(10)
        boldbord.set_bg_color('#DCE6F1')
        boldbord.set_font_name('Times New Roman')

        especial1 = workbook.add_format()
        especial1.set_align('center')
        especial1.set_align('vcenter')
        especial1.set_border(style=1)
        especial1.set_text_wrap()
        especial1.set_font_size(10)
        especial1.set_font_name('Times New Roman')

        especial3 = workbook.add_format({'bold': True})
        especial3.set_align('center')
        especial3.set_align('vcenter')
        especial3.set_border(style=1)
        especial3.set_text_wrap()
        especial3.set_bg_color('#DCE6F1')
        especial3.set_font_size(15)
        especial3.set_font_name('Times New Roman')

        numberuno = workbook.add_format({'num_format': '0.0000'})
        numberuno.set_border(style=1)
        numberuno.set_font_size(10)
        numberuno.set_font_name('Times New Roman')

        numberdos = workbook.add_format({'num_format': '0.000'})
        numberdos.set_border(style=1)
        numberdos.set_font_size(10)
        numberdos.set_font_name('Times New Roman')

        dateformat = workbook.add_format({'num_format': 'd-m-yyyy'})
        dateformat.set_border(style=1)
        dateformat.set_font_size(10)
        dateformat.set_font_name('Times New Roman')

        hourformat = workbook.add_format({'num_format': 'hh:mm'})
        hourformat.set_align('center')
        hourformat.set_align('vcenter')
        hourformat.set_border(style=1)
        hourformat.set_font_size(10)
        hourformat.set_font_name('Times New Roman')

        import sys
        reload(sys)
        sys.setdefaultencoding('iso-8859-1')

        ##########ASISTENCIAS############
        worksheet = workbook.add_worksheet("ANALISIS TRANSF. GRATUITAS")
        worksheet.set_tab_color('blue')

        worksheet.merge_range(1, 0, 1, 26, "ANALISIS TRANSFERENCIAS GRATUITAS", especial3)

        x = 5

        worksheet.write(x, 0, "NUMERO DE DOCUMENTO", boldbord)
        worksheet.write(x, 1, "FECHA DE EMISION", boldbord)
        worksheet.write(x, 2, "CLIENTE", boldbord)
        worksheet.write(x, 3, "DIRECCION DE FACTURA", boldbord)
        worksheet.write(x, 4, "ESTADO", boldbord)
        worksheet.write(x, 5, "PRECIO UNITARIO", boldbord)
        worksheet.write(x, 6, "CANTIDAD", boldbord)
        worksheet.write(x, 7, "TC", boldbord)
        worksheet.write(x, 8, "DESCUENTO", boldbord)
        worksheet.write(x, 9, "SOLES", boldbord)
        worksheet.write(x, 10, "DOLARES", boldbord)
        worksheet.write(x, 11, "TOTAL SOLES", boldbord)
        worksheet.write(x, 12, "CODIGO", boldbord)
        worksheet.write(x, 13, "PRODUCTO", boldbord)
        worksheet.write(x, 14, "UNIDAD", boldbord)
        worksheet.write(x, 15, "MARCA", boldbord)
        worksheet.write(x, 16, "CATEGORIA", boldbord)
        worksheet.write(x, 17, "EJECUTIVO", boldbord)
        worksheet.write(x, 18, "VENDEDOR", boldbord)
        worksheet.write(x, 19, "EQUIPO", boldbord)
        worksheet.write(x, 20, u'AÑO', boldbord)
        worksheet.write(x, 21, "MES", boldbord)
        worksheet.write(x, 22, "DIA", boldbord)
        worksheet.write(x, 23, "TIPO DOCUMENTO", boldbord)
        worksheet.write(x, 24, "SERIE", boldbord)
        worksheet.write(x, 25, "CATEGORIA 1", boldbord)
        worksheet.write(x, 26, "CATEGORIA 2", boldbord)
        x = 6

        self.env.cr.execute(self._get_sql_free())
        data = self.env.cr.dictfetchall()

        for line in data:
            worksheet.write(x, 0, line['NumDoc'] if line['NumDoc'] else '', especial1)
            worksheet.write(x, 1, line['Fch_Emision'] if line['Fch_Emision'] else '', dateformat)
            worksheet.write(x, 2, line['Cliente'] if line['Cliente'] else '', especial1)
            worksheet.write(x, 3, line['Direccion de Entrega'] if line['Direccion de Entrega'] else '', especial1)
            worksheet.write(x, 4, line['Estado'] if line['Estado'] else '', especial1)
            worksheet.write(x, 5, line['Precio_Unitario'] if line['Precio_Unitario'] else '0.0000', numberuno)
            worksheet.write(x, 6, line['Cantidad'] if line['Cantidad'] else '', numberdos)
            worksheet.write(x, 7, line['TipoC'] if line['TipoC'] else '0.000', numberdos)
            worksheet.write(x, 8, line['Descuento'] if line['Descuento'] else '0.0000', numberuno)
            worksheet.write(x, 9, line['Soles'] if line['Soles'] else '0.000', numberdos)
            worksheet.write(x, 10, line['Dolares'] if line['Dolares'] else '0', numberdos)
            worksheet.write(x, 11, line['Tot_Soles'] if line['Tot_Soles'] else '0.0000', numberuno)
            worksheet.write(x, 12, line['Codigo'] if line['Codigo'] else '', especial1)
            worksheet.write(x, 13, line['Producto'] if line['Producto'] else '', especial1)
            worksheet.write(x, 14, line['Unidad'] if line['Unidad'] else '', especial1)
            worksheet.write(x, 15, line['Marca'] if line['Marca'] else '', especial1)
            worksheet.write(x, 16, line['Categoria'] if line['Categoria'] else '', especial1)
            worksheet.write(x, 17, line['Ejecutivo'] if line['Ejecutivo'] else '', especial1)
            worksheet.write(x, 18, line['Vendedor'] if line['Vendedor'] else '', especial1)
            worksheet.write(x, 19, line['Equipo'] if line['Equipo'] else '', especial1)
            worksheet.write(x, 20, line['Anio'] if line['Anio'] else '', especial1)
            worksheet.write(x, 21, line['Mes'] if line['Mes'] else '', especial1)
            worksheet.write(x, 22, line['Dia'] if line['Dia'] else '', especial1)
            worksheet.write(x, 23, line['TipoDoc'] if line['TipoDoc'] else '', especial1)
            worksheet.write(x, 24, line['Serie'] if line['Serie'] else '', especial1)
            worksheet.write(x, 25, line['Categoria1'] if line['Categoria1'] else '', especial1)
            worksheet.write(x, 26, line['Categoria2'] if line['Categoria2'] else '', especial1)
            x += 1

        tam_col = [24, 17, 38, 38, 9, 10, 10, 7, 10, 10, 10, 12, 12, 50, 17, 17, 17, 30, 30, 20, 7, 5, 5, 12, 10, 20,
                   17]

        worksheet.set_column('A:A', tam_col[0])
        worksheet.set_column('B:B', tam_col[1])
        worksheet.set_column('C:C', tam_col[2])
        worksheet.set_column('D:D', tam_col[3])
        worksheet.set_column('E:E', tam_col[4])
        worksheet.set_column('F:F', tam_col[5])
        worksheet.set_column('G:G', tam_col[6])
        worksheet.set_column('H:H', tam_col[7])
        worksheet.set_column('I:I', tam_col[8])
        worksheet.set_column('J:J', tam_col[9])
        worksheet.set_column('K:K', tam_col[10])
        worksheet.set_column('L:L', tam_col[11])
        worksheet.set_column('M:M', tam_col[12])
        worksheet.set_column('N:N', tam_col[13])
        worksheet.set_column('O:O', tam_col[14])
        worksheet.set_column('P:P', tam_col[15])
        worksheet.set_column('Q:Q', tam_col[16])
        worksheet.set_column('R:R', tam_col[17])
        worksheet.set_column('S:S', tam_col[18])
        worksheet.set_column('T:T', tam_col[19])
        worksheet.set_column('U:U', tam_col[20])
        worksheet.set_column('V:V', tam_col[21])
        worksheet.set_column('W:W', tam_col[22])
        worksheet.set_column('X:X', tam_col[23])
        worksheet.set_column('Y:Y', tam_col[24])
        worksheet.set_column('Z:Z', tam_col[25])
        worksheet.set_column('AA:AA', tam_col[26])

        workbook.close()

        f = open(direccion + 'analisis_tranferencias_gratuitas.xlsx', 'rb')

        vals = {
            'output_name': 'Analisis de Transferencias Gratuitas.xlsx',
            'output_file': base64.encodestring(''.join(f.readlines()))
        }

        sfs_id = self.env['custom.export.file'].create(vals)

        return {
            "type": "ir.actions.act_window",
            "res_model": "custom.export.file",
            "views": [[False, "form"]],
            "res_id": sfs_id.id,
            "target": "new",
        }
