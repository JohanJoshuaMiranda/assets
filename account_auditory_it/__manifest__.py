# -*- encoding: utf-8 -*-
{
	'name': 'Auditoria IT',
	'category': 'account',
	'author': 'ITGRUPO',
	'depends': ['account','base','purchase','sale','stock','mail'],
	'version': '1.0',
	'description':"""
	Modulo para Auditorias 
	""",
	'auto_install': False,
	'application': True,
	'demo': [],
	'data':	[
			'views/account_auditory_move_book.xml',
			'views/account_auditory_invoice_book.xml',
			'views/sale_order_auditory_book.xml',
			'views/purchase_order_auditory_book.xml',
			'views/stock_picking_auditory_book.xml',
			'views/account_auditory_menuitem.xml',
			'wizard/account_auditory_move_wizard.xml',
			'wizard/account_auditory_invoice_wizard.xml',
			'wizard/sale_order_auditory_wizard.xml',
			'wizard/purchase_order_auditory_wizard.xml',
			'wizard/stock_picking_auditory_wizard.xml'
			],
	'installable': True
}
