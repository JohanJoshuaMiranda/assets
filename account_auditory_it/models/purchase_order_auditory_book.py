# -*- coding: utf-8 -*-

from openerp import models, fields, api

class PurchaseOrderAuditoryBook(models.Model):
	_name='purchase.order.auditory.book'
	_auto = False

	name = fields.Char(string=u'Número de Pedido')
	date_order = fields.Datetime(string="Fecha de Pedido")
	partner = fields.Char(string='Cliente')
	date_planned = fields.Datetime(string="Fecha Prevista")
	origin = fields.Char(string='Documento Origen')
	amount_untaxed = fields.Float(string='Base Imponible', digits=(12,2))
	amount_total = fields.Float(string='Total', digits=(12,2))
	state = fields.Selection([
        ('draft', u'Petición Presupuesto'),
        ('sent', u'Petición Presupuesto Enviada'),
        ('to approve', 'Para Aprobar'),
        ('purchase', 'Orden de Compra'),
        ('done', 'Bloqueado'),
        ('cancel', 'Cancelado')
        ],string='Estado')
	create_user = fields.Char(string='Creado Por')
	create_date = fields.Datetime(string='Creado En')
	hour_create = fields.Float(string='Creado Hora')
	write_user = fields.Char(string='Modificado Por')
	write_date = fields.Datetime(string='Modificado En')
	hour_write = fields.Float(string='Modificado Hora')
