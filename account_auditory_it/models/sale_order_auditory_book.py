# -*- coding: utf-8 -*-

from openerp import models, fields, api

class SaleOrderAuditoryBook(models.Model):
	_name='sale.order.auditory.book'
	_auto = False

	name = fields.Char(string=u'Número de Pedido')
	date_order = fields.Datetime(string="Fecha de Pedido")
	partner = fields.Char(string='Cliente')
	amount_total = fields.Float(string='Total', digits=(12,2))
	state = fields.Selection([
        ('draft', u'Cotización'),
        ('sent', 'Presupuesto Enviado'),
        ('sale', 'Órdenes de Venta'),
        ('done', 'Bloqueado'),
        ('cancel', 'Cancelado'),
        ],string='Estado')
	create_user = fields.Char(string='Creado Por')
	create_date = fields.Datetime(string='Creado En')
	hour_create = fields.Float(string='Creado Hora')
	write_user = fields.Char(string='Modificado Por')
	write_date = fields.Datetime(string='Modificado En')
	hour_write = fields.Float(string='Modificado Hora')
