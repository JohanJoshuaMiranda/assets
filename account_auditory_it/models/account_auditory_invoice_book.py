# -*- coding: utf-8 -*-

from openerp import models, fields, api

class AccountAuditoryInvoiceBook(models.Model):
	_name='account.auditory.invoice.book'
	_auto = False

	partner = fields.Char(string='Partner')
	date_invoice = fields.Date(string="Fecha de Factura")
	number = fields.Char(string=u'Número')
	journal = fields.Char(string='Diario')
	origin = fields.Char(string='Doc. Origen')
	amount_total_signed = fields.Float(string='Total', digits=(12,2))
	state = fields.Selection([
            ('draft','Borrador'),
            ('proforma', 'Pro-forma'),
            ('proforma2', 'Pro-forma'),
            ('open', 'Abierto'),
            ('paid', 'Pagado'),
            ('cancel', 'Cancelado'),
        ],string='Estado')
	currency = fields.Char(string='Moneda')
	create_user = fields.Char(string='Creado Por')
	create_date = fields.Datetime(string='Creado En')
	hour_create = fields.Float(string='Creado Hora')
	write_user = fields.Char(string='Modificado Por')
	write_date = fields.Datetime(string='Modificado En')
	hour_write = fields.Float(string='Modificado Hora')
