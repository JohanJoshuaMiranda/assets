# -*- coding: utf-8 -*-

from openerp import models, fields, api

class StockPickingAuditoryBook(models.Model):
	_name='stock.picking.auditory.book'
	_auto = False

	name = fields.Char(string='Referencia')
	partner = fields.Char(string='Cliente')
	date = fields.Datetime(string="Fecha Prevista")
	origin = fields.Char(string='Documento Origen')
	state = fields.Selection([
        ('draft', 'Borrador'), ('cancel', 'Cancelado'),
        ('waiting', u'Esperando otra operación'),
        ('confirmed', 'Esperando disponibilidad'),
        ('partially_available', 'Parcialmente Disponible'),
        ('assigned', 'Disponible'), ('done', 'Realizado')],string='Estado')
	create_user = fields.Char(string='Creado Por')
	create_date = fields.Datetime(string='Creado En')
	hour_create = fields.Float(string='Creado Hora')
	write_user = fields.Char(string='Modificado Por')
	write_date = fields.Datetime(string='Modificado En')
	hour_write = fields.Float(string='Modificado Hora')
