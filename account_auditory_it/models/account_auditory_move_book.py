# -*- coding: utf-8 -*-

from openerp import models, fields, api

class AccountAuditoryMoveBook(models.Model):
	_name='account.auditory.move.book'
	_auto = False

	date = fields.Date(string="Fecha")
	name = fields.Char(string=u'Número')
	partner = fields.Char(string='Partner')
	ref = fields.Char(string='Referencia')
	journal = fields.Char(string='Diario')
	amount = fields.Float(string='Monto', digits=(12,2))
	state = fields.Selection([('draft', 'Sin Publicar'), ('posted', 'Contabilizado')],string='Estado')
	currency = fields.Char(string='Moneda')
	create_user = fields.Char(string='Creado Por')
	create_date = fields.Datetime(string='Creado En')
	hour_create = fields.Float(string='Creado Hora')
	write_user = fields.Char(string='Modificado Por')
	write_date = fields.Datetime(string='Modificado En')
	hour_write = fields.Float(string='Modificado Hora')
