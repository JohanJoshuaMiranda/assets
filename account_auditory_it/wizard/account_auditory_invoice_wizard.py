# -*- encoding: utf-8 -*-

import base64
from openerp import models, fields, api
from odoo.exceptions import UserError, ValidationError
from datetime import *

class AccountAuditoryInvoiceWizard(models.TransientModel):
	_name='account.auditory.invoice.wizard'

	name = fields.Char()
	type = fields.Selection([('1','Creado en'),('2','Modificado en')],string='Tipo',required=True,default='1')
	date_ini = fields.Date(string=u'Fecha Inicial',required=True,default=fields.Date.context_today)
	date_end = fields.Date(string=u'Fecha Final',required=True,default=fields.Date.context_today)

	@api.multi
	def do_rebuild(self):
		self.env.cr.execute("""
			CREATE OR REPLACE view account_auditory_invoice_book as ("""+self._get_sql(self.date_ini,self.date_end,self.type)+""")""")
		return {
			'name': 'Auditoria Facturas',
			'type': 'ir.actions.act_window',
			'res_model': 'account.auditory.invoice.book',
			'view_mode': 'tree,pivot,graph',
			'view_type': 'form',
		}

	def _get_sql(self,date_ini,date_end,type):
		if type == '1':
			sql_date = "WHERE (ai.create_date::date BETWEEN '%s' and '%s')" % (datetime.strptime(date_ini, '%Y-%m-%d').strftime("%Y/%m/%d"),datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y/%m/%d"))
		else:
			sql_date = "WHERE (ai.write_date::date BETWEEN '%s' and '%s')" % (datetime.strptime(date_ini, '%Y-%m-%d').strftime("%Y/%m/%d"),datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y/%m/%d"))
		sql = """
				SELECT row_number() OVER () AS id,
				rp.name as partner,
				ai.date_invoice,
				ai.number,
				aj.name as journal,
				ai.origin,
				ai.amount_total_signed,
				ai.state,
				rc.name as currency,
				rup.name as create_user,
				ai.create_date,
				case when extract(hour from ai.create_date)-5<0  then extract(hour from ai.create_date)+19 
				else extract(hour from ai.create_date)-5
				end
				as hour_create,
				rup2.name as write_user,
				ai.write_date,
				case when extract(hour from ai.write_date)-5<0  then extract(hour from ai.write_date)+19 
				else extract(hour from ai.write_date)-5
				end
				as hour_write
				FROM account_invoice ai
				LEFT JOIN res_partner rp on rp.id = ai.partner_id
				LEFT JOIN res_currency rc on rc.id = ai.currency_id
				LEFT JOIN account_journal aj on aj.id = ai.journal_id
				LEFT JOIN res_users ru on ru.id = ai.create_uid
				LEFT JOIN res_partner rup on rup.id = ru.partner_id
				LEFT JOIN res_users ru2 on ru2.id = ai.write_uid
				LEFT JOIN res_partner rup2 on rup2.id = ru2.partner_id
				%s
		""" % (sql_date)

		return sql