# -*- encoding: utf-8 -*-

import base64
from openerp import models, fields, api
from odoo.exceptions import UserError, ValidationError
from datetime import *

class AccountAuditoryMoveWizard(models.TransientModel):
	_name='account.auditory.move.wizard'

	name = fields.Char()
	type = fields.Selection([('1','Creado en'),('2','Modificado en')],string='Tipo',required=True,default='1')
	date_ini = fields.Date(string=u'Fecha Inicial',required=True,default=fields.Date.context_today)
	date_end = fields.Date(string=u'Fecha Final',required=True,default=fields.Date.context_today)

	@api.multi
	def do_rebuild(self):
		self.env.cr.execute("""
			CREATE OR REPLACE view account_auditory_move_book as ("""+self._get_sql(self.date_ini,self.date_end,self.type)+""")""")
		
		return {
			'name': 'Auditoria Asientos Contables',
			'type': 'ir.actions.act_window',
			'res_model': 'account.auditory.move.book',
			'view_mode': 'tree,pivot,graph',
			'view_type': 'form',
		}

	def _get_sql(self,date_ini,date_end,type):
		if type == '1':
			sql_date = "WHERE (am.create_date::date BETWEEN '%s' and '%s')" % (datetime.strptime(date_ini, '%Y-%m-%d').strftime("%Y/%m/%d"),datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y/%m/%d"))
		else:
			sql_date = "WHERE (am.write_date::date BETWEEN '%s' and '%s')" % (datetime.strptime(date_ini, '%Y-%m-%d').strftime("%Y/%m/%d"),datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y/%m/%d"))
		sql = """
				SELECT row_number() OVER () AS id,
				am.date,
				am.name,
				rp.name as partner,
				am.ref,
				aj.name as journal,
				am.amount,
				am.state,
				rc.name as currency,
				rup.name as create_user,
				am.create_date,
				case when extract(hour from am.create_date)-5<0  then extract(hour from am.create_date)+19 
				else extract(hour from am.create_date)-5
				end
				as hour_create,
				rup2.name as write_user,
				am.write_date,
				case when extract(hour from am.write_date)-5<0  then extract(hour from am.write_date)+19 
				else extract(hour from am.write_date)-5
				end
				as hour_write
				FROM account_move am
				LEFT JOIN res_partner rp on rp.id = am.partner_id
				LEFT JOIN res_currency rc on rc.id = am.currency_id
				LEFT JOIN account_journal aj on aj.id = am.journal_id
				LEFT JOIN res_users ru on ru.id = am.create_uid
				LEFT JOIN res_partner rup on rup.id = ru.partner_id
				LEFT JOIN res_users ru2 on ru2.id = am.write_uid
				LEFT JOIN res_partner rup2 on rup2.id = ru2.partner_id
				%s
		""" % (sql_date)

		return sql