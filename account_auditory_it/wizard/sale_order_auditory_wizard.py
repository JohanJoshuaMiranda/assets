# -*- encoding: utf-8 -*-

import base64
from openerp import models, fields, api
from odoo.exceptions import UserError, ValidationError
from datetime import *

class SaleOrderAuditoryWizard(models.TransientModel):
	_name='sale.order.auditory.wizard'

	name = fields.Char()
	type = fields.Selection([('1','Creado en'),('2','Modificado en')],string='Tipo',required=True,default='1')
	date_ini = fields.Date(string=u'Fecha Inicial',required=True,default=fields.Date.context_today)
	date_end = fields.Date(string=u'Fecha Final',required=True,default=fields.Date.context_today)

	@api.multi
	def do_rebuild(self):
		self.env.cr.execute("""
			CREATE OR REPLACE view sale_order_auditory_book as ("""+self._get_sql(self.date_ini,self.date_end,self.type)+""")""")
		return {
			'name': 'Auditoria Pedidos de Venta',
			'type': 'ir.actions.act_window',
			'res_model': 'sale.order.auditory.book',
			'view_mode': 'tree,pivot,graph',
			'view_type': 'form',
		}

	def get_excel(self):
		raise UserError("Aun no esta disponible esta funcion")

	def _get_sql(self,date_ini,date_end,type):
		if type == '1':
			sql_date = "WHERE (so.create_date::date BETWEEN '%s' and '%s')" % (datetime.strptime(date_ini, '%Y-%m-%d').strftime("%Y/%m/%d"),datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y/%m/%d"))
		else:
			sql_date = "WHERE (so.write_date::date BETWEEN '%s' and '%s')" % (datetime.strptime(date_ini, '%Y-%m-%d').strftime("%Y/%m/%d"),datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y/%m/%d"))
		sql = """
				SELECT row_number() OVER () AS id,
				so.name,
				so.date_order,
				rp.name as partner,
				so.amount_total,
				so.state,
				rup.name as create_user,
				so.create_date,
				case when extract(hour from so.create_date)-5<0  then extract(hour from so.create_date)+19 
				else extract(hour from so.create_date)-5
				end
				as hour_create,
				rup2.name as write_user,
				so.write_date,
				case when extract(hour from so.write_date)-5<0  then extract(hour from so.write_date)+19 
				else extract(hour from so.write_date)-5
				end
				as hour_write
				FROM sale_order so
				LEFT JOIN res_partner rp on rp.id = so.partner_id
				LEFT JOIN res_users ru on ru.id = so.create_uid
				LEFT JOIN res_partner rup on rup.id = ru.partner_id
				LEFT JOIN res_users ru2 on ru2.id = so.write_uid
				LEFT JOIN res_partner rup2 on rup2.id = ru2.partner_id
				%s
		""" % (sql_date)

		return sql