# -*- encoding: utf-8 -*-

import base64
from openerp import models, fields, api
from odoo.exceptions import UserError, ValidationError
from datetime import *

class PurchaseOrderAuditoryWizard(models.TransientModel):
	_name='purchase.order.auditory.wizard'

	name = fields.Char()
	type = fields.Selection([('1','Creado en'),('2','Modificado en')],string='Tipo',required=True,default='1')
	date_ini = fields.Date(string=u'Fecha Inicial',required=True,default=fields.Date.context_today)
	date_end = fields.Date(string=u'Fecha Final',required=True,default=fields.Date.context_today)

	@api.multi
	def do_rebuild(self):
		self.env.cr.execute("""
			CREATE OR REPLACE view purchase_order_auditory_book as ("""+self._get_sql(self.date_ini,self.date_end,self.type)+""")""")
		return {
			'name': 'Auditoria Pedidos de Compra',
			'type': 'ir.actions.act_window',
			'res_model': 'purchase.order.auditory.book',
			'view_mode': 'tree,pivot,graph',
			'view_type': 'form',
		}

	def _get_sql(self,date_ini,date_end,type):
		if type == '1':
			sql_date = "WHERE (po.create_date::date BETWEEN '%s' and '%s')" % (datetime.strptime(date_ini, '%Y-%m-%d').strftime("%Y/%m/%d"),datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y/%m/%d"))
		else:
			sql_date = "WHERE (po.write_date::date BETWEEN '%s' and '%s')" % (datetime.strptime(date_ini, '%Y-%m-%d').strftime("%Y/%m/%d"),datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y/%m/%d"))
		sql = """
				SELECT row_number() OVER () AS id,
				po.name,
				po.date_order,
				rp.name as partner,
				po.date_planned,
				po.origin,
				po.amount_untaxed,
				po.amount_total,
				po.state,
				rup.name as create_user,
				po.create_date,
				case when extract(hour from po.create_date)-5<0  then extract(hour from po.create_date)+19 
				else extract(hour from po.create_date)-5
				end
				as hour_create,
				rup2.name as write_user,
				po.write_date,
				case when extract(hour from po.write_date)-5<0  then extract(hour from po.write_date)+19 
				else extract(hour from po.write_date)-5
				end
				as hour_write
				FROM purchase_order po
				LEFT JOIN res_partner rp on rp.id = po.partner_id
				LEFT JOIN res_users ru on ru.id = po.create_uid
				LEFT JOIN res_partner rup on rup.id = ru.partner_id
				LEFT JOIN res_users ru2 on ru2.id = po.write_uid
				LEFT JOIN res_partner rup2 on rup2.id = ru2.partner_id
				%s
		""" % (sql_date)

		return sql