# -*- coding: utf-8 -*-
from openerp import models, fields, api
from odoo.exceptions import UserError
import json
class SaleAdvancePaymentInv(models.TransientModel):
	_inherit = 'sale.advance.payment.inv'

	@api.multi
	def create_invoices(self):
		t = super(SaleAdvancePaymentInv,self).create_invoices()
		aux = ''
		lista = []
		lista2 = []
		if len(self.albaranes) > 0:
			for albaran in self.albaranes:
				for spo in albaran.pack_operation_product_ids:
					aux_dict = {}
					aux_dict['product_id'] = spo.product_id.id
					aux += '\nMARCA: %s PROCED: %s\n' % (spo.product_id.brand_id.name or '', ','.join(spo.product_id.tag_ids.mapped('name')))
					for lote in spo.pack_lot_ids:
						if spo.product_id == lote.lot_id.product_id:
							venc = 'VENC : ' + lote.lot_id.life_date[:10] if lote.lot_id.life_date else ''
							aux += '\nLOTE : %s %s %s' % (lote.lot_id.name, lote.qty, venc)
					aux_dict['aux'] = aux
					aux = ''
					lista.append(aux_dict)
					lista2.append(json.dumps(aux_dict))
			#raise UserError(lista2)
			for line in self.albaranes[0].invoice_id.invoice_line_ids:
				producto = next(iter(filter(lambda l:l['product_id'] == line.product_id.id,lista)),None)
				if producto:
					line.name += '%s'%(producto['aux'])
		return t