# -*- encoding: utf-8 -*-
{
	'name'    : 'Descripcion Automatizada',
	'category': 'account',
	'author'  : 'ITGRUPO-FERCO',
	'version' : '1.0',

	'description':"""
		Nueva descripcion para Facturas
	""",
	
	'auto_install': False,
	'demo'        : [],
	'depends'     : ['kardex_it'],
	'data'        :	[],
	'installable' : True
}
