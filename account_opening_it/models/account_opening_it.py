# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
import base64
from io import BytesIO
import uuid
from datetime import datetime

class AccountOpeningIt(models.Model):
	_name = 'account.opening.it'
	_description = 'Account Opening It'

	@api.depends('from_fiscal_year_id','to_fiscal_year_id')
	def _get_name(self):
		for i in self:
			i.name = (i.from_fiscal_year_id.name if i.from_fiscal_year_id else '') + ' - ' + (i.to_fiscal_year_id.name if i.to_fiscal_year_id else '')

	name = fields.Char(compute=_get_name,store=True)
	from_fiscal_year_id = fields.Many2one('account.fiscalyear',string=u'Ejercicio Anterior',required=True)
	to_fiscal_year_id = fields.Many2one('account.fiscalyear',string=u'Ejercicio Actual',required=True)
	journal_id = fields.Many2one('account.journal',string='Diario Apertura',required=True)
	account_id = fields.Many2one('account.account',string='Cuenta de Resultado Acumulado',required=True)
	partner_id = fields.Many2one('res.partner',string='Partner Apertura',required=True)
	ref = fields.Char(string='Documento Apertura',required=True)
	state = fields.Selection([('draft','BORRADOR'),
							('done','REALIZADO')],string='Estado',default='draft')
	move_ids = fields.One2many('account.move','opening_id_it',string='Asientos de Apertura')
	company_id = fields.Many2one('res.company',string=u'Compañía',default=lambda self: self.env.user.company_id)

	def unlink(self):
		if self.state == 'done':
			raise UserError("No se puede eliminar una Apertura Contable si no esta en estado Borrador.")
		return super(AccountOpeningIt,self).unlink()
	
	def preview_opening(self):
		import io
		from xlsxwriter.workbook import Workbook

		direccion = self.env['main.parameter'].search([])[0].dir_create_file

		if not direccion:
			raise UserError(u'No existe un Directorio Exportadores configurado en Parametros Principales de Contabilidad para su Compañía')

		workbook = Workbook(direccion +'Apertura_contable.xlsx')

		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=2)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(10)
		boldbord.set_bg_color('#DCE6F1')
		boldbord.set_font_name('Times New Roman')

		especial1 = workbook.add_format()
		especial1.set_align('center')
		especial1.set_align('vcenter')
		especial1.set_border(style=1)
		especial1.set_text_wrap()
		especial1.set_font_size(10)
		especial1.set_font_name('Times New Roman')

		numberdos = workbook.add_format({'num_format': '0.000'})
		numberdos.set_border(style=1)
		numberdos.set_font_size(10)
		numberdos.set_font_name('Times New Roman')

		dateformat = workbook.add_format({'num_format':'d-m-yyyy'})
		dateformat.set_border(style=1)
		dateformat.set_font_size(10)
		dateformat.set_font_name('Times New Roman')	

		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		worksheet = workbook.add_worksheet("ASIENTO PRINCIPAL")
		worksheet.set_tab_color('blue')

		u=0
		HEADERS = ['CUENTA','DEBE','HABER','MONEDA','IMPORTE EN MONEDA','TC']
		for he in HEADERS:
			worksheet.write(0, u, he, boldbord)
			u+=1
		x=1
		accounts = []

		self.env.cr.execute("""select a.account_id,c.currency_id,
							case when sum(coalesce(a.balance,0))> 0 then sum(coalesce(a.balance,0)) else 0 end as activo,
							case when sum(coalesce(a.balance,0))< 0 then abs(sum(coalesce(a.balance,0))) else 0 end as pasivo,
							sum(coalesce(a.amount_currency,0)) as amount_currency
							from account_move_line a
							left join account_move b on b.id=a.move_id
							left join account_account c on c.id=a.account_id
							where b.date between '%s' and '%s' and b.company_id=%d and b.state='posted' and c.clasification_sheet='1' AND a.account_id IS NOT NULL
							group by a.account_id,c.currency_id
							having sum(coalesce(a.balance,0)) <> 0"""%(str(self.from_fiscal_year_id.name)+'/01/01',
			str(self.from_fiscal_year_id.name)+'/12/31',
			self.company_id.id))

		res = self.env.cr.dictfetchall()
		doc = self.env['einvoice.catalog.01'].search([('code','=','00')],limit=1)
		sum_debit = sum_credit = 0
		for elem in res:
			account_id = self.env['account.account'].browse(elem['account_id'])
			if account_id.internal_type in ('receivable','payable'):
				accounts.append(elem)
			worksheet.write(x,0,account_id.code if account_id else '',especial1)
			worksheet.write(x,1,elem['activo'] if elem['activo'] else 0,numberdos)
			worksheet.write(x,2,elem['pasivo'] if elem['pasivo'] else 0,numberdos)
			worksheet.write(x,3,account_id.currency_id.name if account_id.currency_id else '',especial1)
			worksheet.write(x,4, elem['amount_currency'] if account_id.currency_id else 0,numberdos)
			worksheet.write(x,5,(abs(elem['activo'] - elem['pasivo'])/abs(elem['amount_currency']) if abs(elem['amount_currency']) != 0 else 0) if account_id.currency_id else 1,numberdos)
			sum_debit += elem['activo']
			sum_credit += elem['pasivo']
			x += 1
		
		if sum_debit > sum_credit:
			worksheet.write(x,0,self.account_id.code if self.account_id else '',especial1)
			worksheet.write(x,1,0,numberdos)
			worksheet.write(x,2,sum_debit-sum_credit,numberdos)
			worksheet.write(x,3,'',especial1)
			worksheet.write(x,4,0,numberdos)
			worksheet.write(x,5,1,numberdos)
		
		if sum_debit < sum_credit:
			worksheet.write(x,0,self.account_id.code if self.account_id else '',especial1)
			worksheet.write(x,1,sum_credit-sum_debit,numberdos)
			worksheet.write(x,2,0,numberdos)
			worksheet.write(x,3,'',especial1)
			worksheet.write(x,4,0,numberdos)
			worksheet.write(x,5,1,numberdos)
			
		
		widths = [15,12,12,10,15,7]
		worksheet.set_column('A:A', widths[0])
		worksheet.set_column('B:B', widths[1])
		worksheet.set_column('C:C', widths[2])
		worksheet.set_column('D:D', widths[3])
		worksheet.set_column('E:E', widths[4])
		worksheet.set_column('F:F', widths[5])

		worksheet = workbook.add_worksheet("DETALLADO")
		worksheet.set_tab_color('green')
		u=0
		HEADERS = ['CUENTA','TD','NRO COMPROBANTE','SOCIO','FECHA VEN','DEBE','HABER','MONEDA','IMPORTE EN MONEDA','TC']
		for he in HEADERS:
			worksheet.write(0, u, he, boldbord)
			u+=1
		x=1
		for account in accounts:
			account_id = self.env['account.account'].browse(account['account_id'])
			self.env.cr.execute("""SELECT T.*, ei.code as td_sunat, rp.nro_documento as doc_partner FROM (select a.partner_id,a.account_id,a.type_document_it,
			a.nro_comprobante,b.currency_id,sum(coalesce(a.balance,0)) as saldo_mn,
			sum(coalesce(a.amount_currency,0)) as saldo_me, min(a.date) as fecha_con, min(a.date_maturity) as fecha_ven from account_move_line a
			left join account_account b on b.id=a.account_id
			left join account_move c on c.id=a.move_id
			where (c.date between '%s' and '%s') and c.state='posted' and b.internal_type in ('receivable','payable') AND a.account_id = %d
			and a.company_id = %d 
			group by a.partner_id,a.account_id,a.type_document_it,a.nro_comprobante,b.currency_id
			having sum(coalesce(a.balance,0))<>0)T
			LEFT JOIN einvoice_catalog_01 ei ON ei.id = T.type_document_it
			LEFT JOIN res_partner rp on rp.id = T.partner_id"""%(str(self.from_fiscal_year_id.name)+'/01/01',
																str(self.from_fiscal_year_id.name)+'/12/31',
																account_id.id,
																self.company_id.id))
			line_data = self.env.cr.dictfetchall()
			for line in line_data:
				worksheet.write(x,0,account_id.code if account_id else '',especial1)
				worksheet.write(x,1,line['td_sunat'] if line['td_sunat'] else '',especial1)
				worksheet.write(x,2,line['nro_comprobante'] if line['nro_comprobante'] else '',especial1)
				worksheet.write(x,3,line['doc_partner'] if line['doc_partner'] else '',especial1)
				worksheet.write(x,4,line['fecha_ven'] if line['fecha_ven'] else '',dateformat)
				worksheet.write(x,5,line['saldo_mn'] if line['saldo_mn'] > 0 else 0,numberdos)
				worksheet.write(x,6,0 if line['saldo_mn'] > 0 else abs(line['saldo_mn']),numberdos)
				worksheet.write(x,7,account_id.currency_id.name if account_id.currency_id else '',especial1)
				worksheet.write(x,8,line['saldo_me'] if account_id.currency_id else 0,numberdos)
				worksheet.write(x,9,(abs(line['saldo_mn'])/abs(line['saldo_me']) if abs(line['saldo_me']) != 0 else 1) if account_id.currency_id else 1,numberdos)
				x += 1
			
			worksheet.write(x,0,account_id.code if account_id else '',especial1)
			worksheet.write(x,1,doc.code,especial1)
			worksheet.write(x,2,self.ref,especial1)
			worksheet.write(x,3,self.partner_id.name,especial1)
			worksheet.write(x,4,'',dateformat)
			worksheet.write(x,5,account['pasivo'],numberdos)
			worksheet.write(x,6,account['activo'],numberdos)
			worksheet.write(x,7,account_id.currency_id.name if account_id.currency_id else '',especial1)
			worksheet.write(x,8,(account['amount_currency'] *-1) if account_id.currency_id else 0,numberdos)
			worksheet.write(x,9,(abs(account['pasivo']-account['activo'])/abs(account['amount_currency']) if abs(account['amount_currency']) != 0 else 1) if account_id.currency_id else 1,numberdos)
			x += 1
		tam_col = [15,10,17,15,12,12,12,10,15,7]
		
		worksheet.set_column('A:A', tam_col[0])
		worksheet.set_column('B:B', tam_col[1])
		worksheet.set_column('C:C', tam_col[2])
		worksheet.set_column('D:D', tam_col[3])
		worksheet.set_column('E:E', tam_col[4])
		worksheet.set_column('F:F', tam_col[5])
		worksheet.set_column('G:G', tam_col[6])
		worksheet.set_column('H:H', tam_col[7])
		worksheet.set_column('I:I', tam_col[8])
		worksheet.set_column('J:J', tam_col[9])

		workbook.close()

		f = open(direccion + 'Apertura_contable.xlsx', 'rb')

		vals = {
			'output_name': 'Preview - Apertura Contable.xlsx',
			'output_file': base64.encodestring(''.join(f.readlines())),
			'respetar':1,		
		}
		sfs_id = self.env['export.file.save'].create(vals)
		
		return {
			"type": "ir.actions.act_window",
			"res_model": "export.file.save",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}

	def generate_opening(self):
		lines = []
		accounts = []
		#######PRIMER ASIENTO
		self.env.cr.execute("""select a.account_id,c.currency_id,
							case when sum(coalesce(a.balance,0))> 0 then sum(coalesce(a.balance,0)) else 0 end as activo,
							case when sum(coalesce(a.balance,0))< 0 then abs(sum(coalesce(a.balance,0))) else 0 end as pasivo,
							sum(coalesce(a.amount_currency,0)) as amount_currency
							from account_move_line a
							left join account_move b on b.id=a.move_id
							left join account_account c on c.id=a.account_id
							where b.date between '%s' and '%s' and b.company_id=%d and b.state='posted' and c.clasification_sheet='1' AND a.account_id IS NOT NULL
							group by a.account_id,c.currency_id
							having sum(coalesce(a.balance,0)) <> 0"""%(str(self.from_fiscal_year_id.name)+'/01/01',
																		str(self.from_fiscal_year_id.name)+'/12/31',
																		self.company_id.id))

		res = self.env.cr.dictfetchall()

		doc = self.env['einvoice.catalog.01'].search([('code','=','00')],limit=1)
		sum_debit = sum_credit = 0

		for elem in res:
			account_id = self.env['account.account'].browse(elem['account_id'])
			if account_id.internal_type in ('receivable','payable'):
				accounts.append(elem)
			vals = (0,0,{
				'account_id': account_id.id,
				'name': 'POR LOS SALDOS INICIALES AL APERTURAR EL EJERCICIO %s'%(self.to_fiscal_year_id.name),
				'debit': elem['activo'],
				'credit': elem['pasivo'],
				'currency_id': account_id.currency_id.id if account_id.currency_id else None,
				'amount_currency': elem['amount_currency'] if account_id.currency_id else 0,
				'partner_id': self.partner_id.id if account_id.internal_type in ('receivable','payable') else None,
				'type_document_it': doc.id if account_id.internal_type in ('receivable','payable') else None,
				'nro_comprobante': self.ref if account_id.internal_type in ('receivable','payable') else None,
				'company_id': self.company_id.id,
				'amount_residual':0,
				'amount_residual_currency':0,
				'reconciled': True,
				'tc': (abs(elem['activo'] - elem['pasivo'])/abs(elem['amount_currency']) if abs(elem['amount_currency']) != 0 else 0) if account_id.currency_id else 1,
			})
			sum_debit += elem['activo']
			sum_credit += elem['pasivo']
			lines.append(vals)
		
		if sum_debit > sum_credit:
			vals = (0,0,{
				'account_id': self.account_id.id,
				'name': 'POR LOS SALDOS INICIALES AL APERTURAR EL EJERCICIO %s'%(self.to_fiscal_year_id.name),
				'debit': 0,
				'credit': sum_debit-sum_credit,
				'company_id': self.company_id.id,
				'amount_residual':0,
				'amount_residual_currency':0,
				'reconciled': True,
			})
			lines.append(vals)
		
		if sum_debit < sum_credit:
			vals = (0,0,{
				'account_id': self.account_id.id,
				'name': 'POR LOS SALDOS INICIALES AL APERTURAR EL EJERCICIO %s'%(self.to_fiscal_year_id.name),
				'debit': sum_credit-sum_debit,
				'credit': 0,
				'company_id': self.company_id.id,
				'amount_residual':0,
				'amount_residual_currency':0,
				'reconciled': True,
			})
			lines.append(vals)
		
		move = self.env['account.move'].create({
				'company_id': self.company_id.id,
				'journal_id': self.journal_id.id,
				'date': str(self.to_fiscal_year_id.name)+'/01/01',
				'line_ids':lines,
				'ref': 'APERTURA-'+str(self.to_fiscal_year_id.name),
				'fecha_special':True,
				'opening_id_it': self.id})
		
		move.post()

		for account in accounts:
			account_id = self.env['account.account'].browse(account['account_id'])

			self.env.cr.execute("""select a.partner_id,a.account_id,a.type_document_it,a.nro_comprobante,b.currency_id,sum(coalesce(a.balance,0)) as saldo_mn,
			sum(coalesce(a.amount_currency,0)) as saldo_me, min(a.date) as fecha_con, min(a.date_maturity) as fecha_ven from account_move_line a
			left join account_account b on b.id=a.account_id
			left join account_move c on c.id=a.move_id
			where (c.date between '%s' and '%s')  and c.state='posted' and b.internal_type in ('receivable','payable') AND a.account_id = %d
			and a.company_id = %d 
			group by a.partner_id,a.account_id,a.type_document_it,a.nro_comprobante,b.currency_id
			having sum(coalesce(a.balance,0))<>0"""%(str(self.from_fiscal_year_id.name)+'/01/01',
																str(self.from_fiscal_year_id.name)+'/12/31',
																account_id.id,
																self.company_id.id))
			
			line_data = self.env.cr.dictfetchall()
			line_account = []
			for line in line_data:
				vals = (0,0,{
					'account_id': account_id.id,
					'name': 'POR LOS SALDOS INICIALES AL APERTURAR EL EJERCICIO %s'%(self.to_fiscal_year_id.name),
					'debit': line['saldo_mn'] if line['saldo_mn'] > 0 else 0,
					'credit': 0 if line['saldo_mn'] > 0 else abs(line['saldo_mn']),
					'currency_id': account_id.currency_id.id if account_id.currency_id else None,
					'amount_currency': line['saldo_me'] if account_id.currency_id else 0,
					'partner_id': line['partner_id'],
					'type_document_it': line['type_document_it'],
					'nro_comprobante': line['nro_comprobante'],
					'company_id': self.company_id.id,
					'amount_residual':0,
					'amount_residual_currency':0,
					'reconciled': True,
					'date_maturity': line['fecha_ven'] if line['fecha_ven'] else None,
					'tc': (abs(line['saldo_mn'])/abs(line['saldo_me']) if abs(line['saldo_me']) != 0 else 1) if account_id.currency_id else 1,
				})
				line_account.append(vals)
			
			vals = (0,0,{
					'account_id': account_id.id,
					'name': 'POR LOS SALDOS INICIALES AL APERTURAR EL EJERCICIO %s'%(self.to_fiscal_year_id.name),
					'debit': account['pasivo'],
					'credit': account['activo'],
					'currency_id': account_id.currency_id.id if account_id.currency_id else None,
					'amount_currency': (account['amount_currency'] *-1) if account_id.currency_id else 0,
					'partner_id': self.partner_id.id,
					'type_document_it': doc.id,
					'nro_comprobante': self.ref,
					'company_id': self.company_id.id,
					'amount_residual':0,
					'amount_residual_currency':0,
					'reconciled': True,
					'tc': (abs(account['pasivo']-account['activo'])/abs(account['amount_currency']) if abs(account['amount_currency']) != 0 else 1) if account_id.currency_id else 1,
				})
			line_account.append(vals)

			move_account = self.env['account.move'].create({
				'company_id': self.company_id.id,
				'journal_id': self.journal_id.id,
				'date': str(self.to_fiscal_year_id.name)+'/01/01',
				'line_ids':line_account,
				'ref': 'APERTURA %s - %s'%(account_id.code,self.to_fiscal_year_id.name),
				'fecha_special':True,
				'opening_id_it': self.id,
				#'invoice_payment_state': 'paid',
				})
		
			move_account.post()

		self.state = 'done'

	def get_amount_currency_account(self,account_id):
		self.env.cr.execute("""SELECT sum(coalesce(aml.amount_currency,0)) as amount_currency from account_move_line aml
		left join account_move am on am.id = aml.move_id
		where aml.account_id = %d and am.state = 'posted' and aml.company_id = %d
		AND (CASE
				WHEN am.fecha_special = true AND to_char(am.date::timestamp with time zone, 'mmdd'::text) = '0101'::text THEN to_char(am.date::timestamp with time zone, 'yyyy'::text) || '00'::text
				WHEN am.fecha_special = true AND to_char(am.date::timestamp with time zone, 'mmdd'::text) = '1231'::text THEN to_char(am.date::timestamp with time zone, 'yyyy'::text) || '13'::text
				ELSE to_char(am.date::timestamp with time zone, 'yyyymm'::text)
			END::integer BETWEEN '%s' AND '%s')"""%(account_id.id,self.company_id.id,self.from_fiscal_year_id.name + '00',self.from_fiscal_year_id.name+'12'))

		res = self.env.cr.dictfetchall()
		if len(res) > 0:
			amount_currency = res[0]['amount_currency'] if res[0]['amount_currency'] else 0
			return amount_currency
		else:
			return 0

	def cancel_opening(self):
		for move in self.move_ids:
			if move.state =='draft':
				pass
			else:
				#for mm in move.line_ids:
				#	mm.remove_move_reconcile()
				move.button_cancel()
			move.line_ids.unlink()
			move.name = "/"
			move.unlink()

		self.state = 'draft'
	
	def open_entries(self):
		return {
			'name': 'Asientos Contables de Cierre',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'account.move',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', self.move_ids.ids)],
		}