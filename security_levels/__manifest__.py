# -*- encoding: utf-8 -*-
{
    'name': 'Niveles de Seguridad - Ceelimp',
    'category': 'payroll',
    'author': 'ITGRUPO-CEELIMP',
    'depends': ['planilla'],
    'version': '1.0',
    'description':"""
        Modulo administrar niveles de seguridad en Nomina
    """,
    'auto_install': False,
    'demo': [],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'hr_employee.xml',
        'hr_menus.xml'
        ],
    'installable': True
}