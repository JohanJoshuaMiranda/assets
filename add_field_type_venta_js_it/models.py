from openerp import models, fields, api


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    type_sale_js = fields.Char(string="Tipo de Venta")
