# -*- coding: utf-8 -*-
from odoo import fields, models, api
from ast import literal_eval
import pudb


class SaleOrder(models.Model):
    _inherit = "sale.order"

    approval_line = fields.One2many('sale.order.approval', 'approval_id', string='Motivos', )

    @api.one
    def approve_order(self):
        """El Usuario Solicitando Aprobacion"""
        self.approval_line.unlink()

        # revisando linea de crédito
        self.check_credit_limit(True)

        # revisando morosidad
        self.check_invoice_overdue(True)

        require_approval = False
        for line in self.order_line:
            if line.require_approval:
                require_approval = True

                move_line_1 = {
                    'reason': 'El producto: ' + line.product_id.name + ', tiene descuento extra del ' + str(line.discount2) + u' %',
                    'date': fields.Date.context_today(self),
                    'state': 'detained',
                }
                move_vals = {
                    'approval_line': [(0, 0, move_line_1)],
                }
                self.write(move_vals)

        #Actualiza el sale.order
        self.write({'state': 'forapprove', 'require_approval': require_approval})

    @api.multi
    def action_confirm(self):
        resp = False
        if self.state in ('draft', 'cancel', 'forapprove'):
            self.approval_line.unlink()
            # revisando linea de crédito
            req_check_credit_limit = self.check_credit_limit(True)
            # revisando morosidad
            req_check_invoice_overdue = self.check_invoice_overdue(True)

            if not self.hide_buttons_order and (req_check_credit_limit[0] or req_check_invoice_overdue[0]):
                self.hide_buttons_order = True
                self.state = 'forapprove'
                resp = True

            else:
                resp = super(SaleOrder, self).action_confirm()
        else:
            resp = super(SaleOrder, self).action_confirm()

        return resp

    @api.multi
    def print_quotation(self):
        self.filtered(lambda s: s.state == 'draft')
        res = self.env['report'].get_action(self, 'sale.report_saleorder')
        return res

    # @api.multi
    # def action_quotation_send(self):
    #     if self.state == 'sent':
    #         self.write({'state': 'draft'})
    #     resp_evaluacion = self.action_confirm(True)
    #     res = super(SaleOrder, self).action_quotation_send()
    #
    #     if not resp_evaluacion or self.state == 'sent':
    #         self.write({'state': 'sent'})
    #     return res

    @api.multi
    def action_quotation_send(self):
        '''
        This function opens a window to compose an email, with the edi sale template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('sale', 'email_template_edi_sale')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'sale.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_so_as_sent': True,
            'custom_layout': "sale.mail_template_data_notification_email_sale_order"
        })
        if self.state not in ('forapprove','approved','sale', 'done', 'cancel'):
            self.write({'state': 'sent'})
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.one
    def approve_sale_order(self):
        """El admin Aprobando la orden"""

        #pudb.set_trace()
        move_line_1 = {
            'reason': 'Se aprueba el(los) descuento(s) adicional(es) (descuento 2) solicitado en el detalle de la Cotización.',
            'date': fields.Date.context_today(self),
            'state': 'approved',
        }
        self.write({'state': 'approved', 'require_approval': True, 'approval_line': [(0, 0, move_line_1)]})

    @api.one
    @api.depends('order_line')
    def _compute_approv_group_permission(self):

        tipo_precio_venta = self.env['ir.values'].get_default('sale.config.settings', 'sale_pricelist_setting')

        if tipo_precio_venta == 'formula':

            exist_discount2 = False
            require_approval = literal_eval(
                self.env["ir.config_parameter"].get_param('fxo_sale_three_discounts.validate_discounts', 'False'))
            for item in self.order_line:
                if float(item.discount2) > 0.00:
                    exist_discount2 = True
                    break

            # Agregando validacion para el descuento1
            exist_discount1 = False
            require_approval1 = literal_eval(
                self.env["ir.config_parameter"].get_param('fxo_sale_three_discounts.validate_discounts1', 'False'))
            for item in self.order_line:
                if float(item.discount1) > 0.00:
                    exist_discount1 = True
                    break

            # revisando linea de crédito
            # req_check_credit_limit = self.check_credit_limit()

            # revisando morosidad
            #req_check_invoice_overdue = self.check_invoice_overdue()

            # Agregando condicion (Si esta marcado la opcion de la configuracion y hay decsuento requiere aprobacion) sfr
            # if (require_approval and exist_discount2) and (require_approval1 and exist_discount1):
            #             #or req_check_credit_limit[0] \
            #             #or req_check_invoice_overdue[0]):
            #     self.hide_buttons_order = True


            # Agregando condicion (Si esta marcado la opcion de la configuracion y hay decsuento requiere aprobacion) sfr
            if (require_approval and exist_discount2) and (require_approval1 and exist_discount1):
                self.hide_buttons_order = True

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('sale.order') or 'New'

        if any(f not in vals for f in ['partner_invoice_id', 'partner_shipping_id', 'pricelist_id']):
            partner = self.env['res.partner'].browse(vals.get('partner_id'))
            addr = partner.address_get(['delivery', 'invoice'])
            vals['partner_invoice_id'] = vals.setdefault('partner_invoice_id', addr['invoice'])
            vals['partner_shipping_id'] = vals.setdefault('partner_shipping_id', addr['delivery'])
            vals['pricelist_id'] = vals.setdefault('pricelist_id', partner.property_product_pricelist and partner.property_product_pricelist.id)

        return super(SaleOrder, self).create(vals)

    state = fields.Selection([
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('forapprove', 'Por aprobar'),
        ('approved', 'Aprobado'),
        ('sale', 'Sale Order'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')

    hide_buttons_order = fields.Boolean(compute=_compute_approv_group_permission)