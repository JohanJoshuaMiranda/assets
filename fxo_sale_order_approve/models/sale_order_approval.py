from odoo import fields, models, api
from odoo.tools.translate import _
#import pudb


class RequireApproval(models.Model):

    _name = "sale.order.approval"

    name = fields.Char(string='Nombre', copy=False)
    reason = fields.Char(string='Motivo', copy=False)
    date = fields.Date(string='Fecha', copy=False)
    state = fields.Selection([
        ('detained', 'Retenido'),
        ('refused', 'Rechazado'),
        ('approved', 'Aprobado'),
    ], string='Estado', readonly=True, copy=False, index=True)
    user_id = fields.Many2one('res.users', string='Usuario', index=True, track_visibility='onchange', default=lambda self: self.env.user)
    approval_id = fields.Many2one('sale.order', string='Order Reference', required=False, ondelete='cascade', index=True, copy=False)
    notes = fields.Text('Notes')

    @api.one
    def act_confirm(self):
        result = 'A'
        self.write({'state': 'approved'})

        approvals = self.env['sale.order.approval'].search([('approval_id', '=', self.approval_id.id)])
        for item in approvals:
            if item['state'] in ('refused') and result == 'A':
                result = 'R'
            elif item['state'] in ('detained') and result == 'A':
                result = 'D'
        if result == 'A':
            self.approval_id.hide_buttons_order = False
            self.approval_id.state = "approved"
    @api.one
    def state_refused(self):
        self.approval_id.hide_buttons_order = True
        self.write({'state': 'refused'})

    @api.one
    def state_cancel(self):
        self.write({'state': 'detained'})
