# -*- coding: utf-8 -*-

from odoo import models, fields, exceptions, api

class AccountInvoice(models.Model):
	_inherit = "account.invoice"

	@api.multi
	def action_invoice_open(self):
		res = super(AccountInvoice, self).action_invoice_open()
		for invoice in self:
			for invoice_l in invoice.invoice_line_ids:
				for line in invoice.move_id.line_ids.filtered(lambda x: x.account_id.id == invoice_l.account_id.id):
					line.name = invoice_l.name
		return res