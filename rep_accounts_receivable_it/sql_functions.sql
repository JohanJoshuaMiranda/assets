DROP VIEW IF EXISTS public.vst_letras_sin_canje CASCADE;

CREATE OR REPLACE VIEW public.vst_letras_sin_canje
 AS
 SELECT aml.id,
    am.id AS move_id,
    aml.partner_id,
    aml.account_id,
    aml.type_document_it,
    aml.nro_comprobante,
    aml.date_emision,
    aml.date_maturity
   FROM account_move_line aml
     LEFT JOIN account_move am ON am.id = aml.move_id
     LEFT JOIN account_account aa ON aa.id = aml.account_id
  WHERE am.is_canje_pa = true AND "left"(aa.code::text, 3) = '123'::text AND am.state::text = 'posted'::text AND date_part('year'::text, am.fecha_contable) = (( SELECT main_parameter.fiscalyear
           FROM main_parameter))::double precision AND aml.debit <> 0::numeric;

----------------------------------------------------------------------------------------------------------------

DROP VIEW IF EXISTS public.saldos_cc_12 CASCADE;

CREATE OR REPLACE VIEW public.saldos_cc_12
AS
SELECT a1.partner_id,
    a1.account_id,
    a1.type_document_it,
    a1.nro_comprobante,
    sum(
        CASE
            WHEN a2.currency_id IS NULL THEN a1.debit
            ELSE 0::numeric
        END) AS pventa_mn,
    sum(
        CASE
            WHEN a2.currency_id IS NULL THEN a1.credit
            ELSE 0::numeric
        END) AS pagos_mn,
    sum(
        CASE
            WHEN a2.currency_id IS NOT NULL THEN
            CASE
                WHEN a1.amount_currency > 0::numeric THEN a1.amount_currency
                ELSE 0::numeric
            END
            ELSE 0::numeric
        END) AS pventa_me,
    sum(
        CASE
            WHEN a2.currency_id IS NOT NULL THEN
            CASE
                WHEN a1.amount_currency < 0::numeric THEN abs(a1.amount_currency)
                ELSE 0::numeric
            END
            ELSE 0::numeric
        END) AS pagos_me,
        min(a1.id) as aml_id
FROM account_move_line a1
    LEFT JOIN account_account a2 ON a2.id = a1.account_id
    LEFT JOIN account_move a3 ON a3.id = a1.move_id
WHERE "left"(a2.code::text, 2) = '12'::text AND date_part('year'::text, a3.fecha_contable) = (( SELECT main_parameter.fiscalyear
        FROM main_parameter))::double precision AND a1.nro_comprobante IS NOT NULL AND a3.state::text = 'posted'::text
GROUP BY a1.partner_id, a1.account_id, a1.type_document_it, a1.nro_comprobante;