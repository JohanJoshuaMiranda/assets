# -*- coding: utf-8 -*-
from odoo import api, fields, models

class SaleOrder(models.Model):
	_inherit = 'sale.order'

	date_internment_rep = fields.Date(string="Fecha de Internamiento")
	date_due_rep = fields.Date(string="Fecha Venc. segun Int.")
	siaf_number_rep = fields.Char(string="Numero de SIAF")

	file_signed_rep_order = fields.Binary(string='Adjunto Ventas')
	name_file_rep_order = fields.Char(string='Nombre de Archivo')

	@api.onchange('date_internment_rep','payment_term_id')
	def _calculate_date_due_rep(self):
		date_internment_rep = self.date_internment_rep
		if not date_internment_rep:
			date_internment_rep = fields.Date.context_today(self)
		if not self.payment_term_id:
			# When no payment term defined
			self.date_due_rep = self.date_due_rep or date_internment_rep
		else:
			pterm = self.payment_term_id
			pterm_list = pterm.with_context(currency_id=self.company_id.currency_id.id).compute(value=1, date_ref=date_internment_rep)[0]
			self.date_due_rep = max(line[0] for line in pterm_list)

	@api.multi
	def _prepare_invoice(self):
		invoice_vals = super(SaleOrder,self)._prepare_invoice()
		invoice_vals['date_internment_rep'] = self.date_internment_rep
		invoice_vals['date_due_rep'] = self.date_due_rep
		invoice_vals['siaf_number_rep'] = self.siaf_number_rep
		return invoice_vals

class SaleAdvancePaymentInv(models.TransientModel):
	_inherit = "sale.advance.payment.inv"

	@api.multi
	def _create_invoice(self, order, so_line, amount):
		invoice = super(SaleAdvancePaymentInv,self)._create_invoice(order, so_line, amount)
		invoice.date_internment_rep = order.date_internment_rep
		invoice.date_due_rep = order.date_due_rep
		invoice.siaf_number_rep = order.siaf_number_rep
		return invoice