# -*- coding: utf-8 -*-
from odoo import api, fields, models

class AccountMove(models.Model):
	_inherit = 'account.move'

	is_canje_pa = fields.Boolean(string='Usar Datos para Reporte',default=False)