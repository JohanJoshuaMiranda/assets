# -*- coding: utf-8 -*-
from odoo import api, fields, models

class AccountInvoice(models.Model):
	_inherit = 'account.invoice'

	file_signed_rep = fields.Binary(string='Firmada por el Cliente')
	name_file_rep = fields.Char(string='Nombre de Archivo')
	date_internment_rep = fields.Date(string="Fecha de Internamiento")
	date_due_rep = fields.Date(string="Fecha Venc. segun Int.")
	siaf_number_rep = fields.Char(string="Numero de SIAF")

	stock_picking_count_rep = fields.Integer(compute='_compute_stock_count_it', string="Guias")

	@api.multi
	def _compute_stock_count_it(self):
		invoice_data = self.env['stock.picking'].read_group([('invoice_id', 'in', self.ids)], ['invoice_id'], ['invoice_id'])
		mapped_data = dict([(invoice['invoice_id'][0], invoice['invoice_id_count']) for invoice in invoice_data])
		for acc in self:
			acc.stock_picking_count_rep = mapped_data.get(acc.id, 0)

	@api.onchange('date_internment_rep','payment_term_id')
	def _calculate_date_due_rep(self):
		date_internment_rep = self.date_internment_rep
		if not date_internment_rep:
			date_internment_rep = fields.Date.context_today(self)
		if not self.payment_term_id:
			# When no payment term defined
			self.date_due_rep = self.date_due_rep or date_internment_rep
		else:
			pterm = self.payment_term_id
			pterm_list = pterm.with_context(currency_id=self.company_id.currency_id.id).compute(value=1, date_ref=date_internment_rep)[0]
			self.date_due_rep = max(line[0] for line in pterm_list)