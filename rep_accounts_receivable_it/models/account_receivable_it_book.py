# -*- coding: utf-8 -*-

from openerp import models, fields, api

class AccountReceivableItBook(models.Model):
	_name='account.receivable.it.book'
	_auto = False
	_order = 'cuenta'

	invoice_id = fields.Many2one('account.invoice',string='Factura')
	cuenta = fields.Char(string='Cuenta')
	ruc = fields.Char(string='RUC')
	cliente = fields.Char(string='Cliente')
	cliente_factura = fields.Char(string='Cliente Factura')
	td = fields.Char(string='TD')
	reference = fields.Char(string='Nro Comprobante')
	moneda = fields.Char(string='Moneda')
	fecha_emision = fields.Date(string='F. Emision')
	fecha_vencimiento = fields.Date(string='F. Ven')
	pventa_mn = fields.Float(string='P. Venta MN',digits=(12,2))
	pventa_me = fields.Float(string='P. Venta ME',digits=(12,2))
	pagos_mn = fields.Float(string='Pagos MN',digits=(12,2))
	pagos_me = fields.Float(string='Pagos ME',digits=(12,2))
	saldo_mn = fields.Float(string='Saldo MN',digits=(12,2))
	saldo_me = fields.Float(string='Saldo ME',digits=(12,2))
	url_pdf2 = fields.Char(related='invoice_id.url_pdf2')
	file_signed_rep = fields.Binary(related='invoice_id.file_signed_rep')
	name_file_rep = fields.Char(related='invoice_id.name_file_rep')
	date_internment_rep = fields.Date(string='Fecha de Internamiento',related='invoice_id.date_internment_rep')
	date_due_rep = fields.Date(string='Fecha Venc. segun Int.',related='invoice_id.date_due_rep')
	siaf_number_rep = fields.Char(string='Numero de SIAF',related='invoice_id.siaf_number_rep')
	stock_picking_count_rep = fields.Integer(related='invoice_id.stock_picking_count_rep', string="Guias")
	letra_id = fields.Many2one('account.letras.payment',string='Letra')
	sale_id = fields.Many2one('sale.order',string='Pedido de Venta')
	file_signed_rep_order = fields.Binary(related='sale_id.file_signed_rep_order')
	name_file_rep_order = fields.Char(related='sale_id.name_file_rep_order')