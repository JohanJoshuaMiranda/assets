# -*- encoding: utf-8 -*-
from openerp import models, fields, api
from datetime import *
import base64

class AccountReceivableItWizard(models.TransientModel):
	_name='account.receivable.it.wizard'

	name = fields.Char()
	type_show = fields.Selection([('pantalla','Pantalla'),('excel','Excel')],string=u'Mostrar en',default='pantalla')

	@api.multi
	def do_rebuild(self):
		self.env.cr.execute("""
			CREATE OR REPLACE view account_receivable_it_book as ("""+self._get_sql(self.env.uid)+""")""")
		if self.type_show == 'pantalla':
			return {
				'name': 'Saldos de Facturas',
				'type': 'ir.actions.act_window',
				'res_model': 'account.receivable.it.book',
				'view_mode': 'tree,form',
				'view_type': 'form',
			}
		else:
			import io
			from xlsxwriter.workbook import Workbook
			output = io.BytesIO()

			direccion = self.env['main.parameter'].search([])[0].dir_create_file

			workbook = Workbook(direccion +'saldofacturas.xlsx')
			worksheet = workbook.add_worksheet("Reporte de Saldo de Facturas")

			bold = workbook.add_format({'bold': True})
			normal = workbook.add_format()
			boldbord = workbook.add_format({'bold': True})
			boldbord.set_border(style=2)
			boldbord.set_align('center')
			boldbord.set_align('vcenter')
			boldbord.set_text_wrap()
			boldbord.set_font_size(9)
			boldbord.set_bg_color('#DCE6F1')
			numberdos = workbook.add_format({'num_format':'0.00'})
			bord = workbook.add_format()
			bord.set_border(style=1)
			bord.set_text_wrap()
			bord.set_align('justify')
			numberdos.set_border(style=1)
			numberdos.set_align('right')

			title = workbook.add_format({'bold': True})
			title.set_align('center')
			title.set_align('vcenter')
			title.set_text_wrap()
			title.set_font_size(20)
			worksheet.set_row(0, 30)

			dateformat = workbook.add_format({'num_format':'yyyy-mm-dd'})
			dateformat.set_align('justify')
			dateformat.set_align('vcenter')
			dateformat.set_border(style=1)
			dateformat.set_font_size(10)

			import sys
			reload(sys)
			sys.setdefaultencoding('iso-8859-1')

			worksheet.write(0,0, u"RUC",boldbord)
			worksheet.write(0,1, u"Cliente",boldbord)
			worksheet.write(0,2, u"Cliente Factura",boldbord)
			worksheet.write(0,3, u"TD",boldbord)
			worksheet.write(0,4, u"Nro Comprobante",boldbord)
			worksheet.write(0,5, u"Moneda",boldbord)
			worksheet.write(0,6, u"Fecha Emision",boldbord)
			worksheet.write(0,7, u"Fecha Vencimiento",boldbord)
			worksheet.write(0,8, u"P. Venta MN",boldbord)
			worksheet.write(0,9, u"P. Venta ME",boldbord)
			worksheet.write(0,10, u"Pagos MN",boldbord)
			worksheet.write(0,11, u"Pagos ME",boldbord)
			worksheet.write(0,12, u"Saldo MN",boldbord)
			worksheet.write(0,13, u"Saldo ME",boldbord)
			worksheet.write(0,14, u"Canje Letra",boldbord)
			worksheet.write(0,15, u"Cuenta",boldbord)

			x = 1

			for line in self.env['account.receivable.it.book'].search([]):
				worksheet.write(x,0,line.ruc if line.ruc  else '',bord )
				worksheet.write(x,1,line.cliente if line.cliente  else '',bord)
				worksheet.write(x,2,line.cliente_factura if line.cliente_factura  else '',bord)
				worksheet.write(x,3,line.td if line.td  else '',bord)
				worksheet.write(x,4,line.reference if line.reference  else '',bord)
				worksheet.write(x,5,line.moneda if line.moneda  else '',bord)
				worksheet.write(x,6,line.fecha_emision if line.fecha_emision  else '',dateformat)
				worksheet.write(x,7,line.fecha_vencimiento if line.fecha_vencimiento  else '',dateformat)
				worksheet.write(x,8,line.pventa_mn if line.pventa_mn else '0.00' ,numberdos)
				worksheet.write(x,9,line.pventa_me if line.pventa_me else '0.00' ,numberdos)
				worksheet.write(x,10,line.pagos_mn if line.pagos_mn else '0.00' ,numberdos)
				worksheet.write(x,11,line.pagos_me if line.pagos_me else '0.00' ,numberdos)
				worksheet.write(x,12,line.saldo_mn if line.saldo_mn else '0.00' ,numberdos)
				worksheet.write(x,13,line.saldo_me if line.saldo_me else '0.00' ,numberdos)
				worksheet.write(x,14,line.letra_id.name if line.letra_id else '' ,bord)
				worksheet.write(x,15,line.cuenta if line.cuenta else '' ,bord )
				x += 1

			tam_col = [12,45,45,10,12,7,15,15,12,12,12,12,12,12,15,10]


			worksheet.set_column('A:A', tam_col[0])
			worksheet.set_column('B:B', tam_col[1])
			worksheet.set_column('C:C', tam_col[2])
			worksheet.set_column('D:D', tam_col[3])
			worksheet.set_column('E:E', tam_col[4])
			worksheet.set_column('F:F', tam_col[5])
			worksheet.set_column('G:G', tam_col[6])
			worksheet.set_column('H:H', tam_col[7])
			worksheet.set_column('I:I', tam_col[8])
			worksheet.set_column('J:J', tam_col[9])
			worksheet.set_column('K:K', tam_col[10])
			worksheet.set_column('L:L', tam_col[11])
			worksheet.set_column('M:M', tam_col[12])
			worksheet.set_column('N:N', tam_col[13])
			worksheet.set_column('O:O', tam_col[14])
			worksheet.set_column('P:P', tam_col[15])

			workbook.close()
			
			f = open(direccion + 'saldofacturas.xlsx', 'rb')
			
			vals = {
				'output_name': 'saldofacturas.xlsx',
				'output_file': base64.encodestring(''.join(f.readlines())),		
			}

			sfs_id = self.env['custom.export.file'].create(vals)

			return {
			"type": "ir.actions.act_window",
			"res_model": "custom.export.file",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
			}

	def _get_sql(self,usuario):
		grupo = self.env['res.groups'].search([('name','=','Ver Todas las Facturas PC')]).id
		self.env.cr.execute("""
			select *
			from res_groups_users_rel
			where gid = """+str(grupo)+""" and uid = """+str(usuario))
		res = self.env.cr.dictfetchall()
		sql_vend = "and a5.user_id = %s" % (str(usuario))
		if len(res) > 0:
			sql_vend = ""
		sql = """
			select row_number() OVER () AS id,  T.* from
			(select 
			a5.id as invoice_id,
			a3.code as cuenta,
			a2.nro_documento as ruc,
			a2.name as cliente,
			rpi.name as cliente_factura,
			a4.code || ' ' || a4.name as td,
			a1.nro_comprobante as reference,
			case when rc.name is not null then rc.name::character varying else 'PEN'::character varying end as moneda,
			case 
			 when a5.date_invoice is not null then a5.date_invoice::date
			 when a6.fecha_emision is not null then a6.fecha_emision::date 
			 else a7.date_emision::date
			 end as fecha_emision,
			case 
			 when a5.date_due is not null then a5.date_due::date
			 when a6.fecha_vencimiento is not null then a6.fecha_vencimiento::date 
			 else a7.date_maturity::date
			 end as fecha_vencimiento,
			a1.pventa_mn,
			a1.pagos_mn,
			a1.pventa_me,
			a1.pagos_me,
			a1.pventa_mn-a1.pagos_mn as saldo_mn,
			a1.pventa_me-a1.pagos_me as saldo_me,
			a6.letra_id,
			so.id as sale_id
			from saldos_cc_12 a1
			left join res_partner a2 on a2.id=a1.partner_id
			left join account_account a3 on a3.id=a1.account_id
			left join res_currency rc on rc.id = a3.currency_id
			--join para datos de fcatura
			left join einvoice_catalog_01 a4 on a4.id=a1.type_document_it
			left join (select * from account_invoice where type in ('out_invoice','out_refund')and state not in ('draft','cancel'))a5
			on concat(a5.partner_id,a5.account_id,a5.it_type_document,a5.reference)=
			concat(a1.partner_id,a1.account_id,a1.type_document_it,a1.nro_comprobante)
			left join res_partner rpi on rpi.id=a5.partner_shipping_id
			left join sale_order so on so.name = a5.origin
			-- join para datos de letras en canje 
			left join (
			select 
			alp.partner_id,
			alp.fecha_canje as fecha_emision,
			alplm.fecha_vencimiento,
			alplm.nro_letra,
			alp.id as letra_id
			from account_letras_payment_letra_manual alplm
			left join account_letras_payment alp on alp.id = alplm.letra_payment_id
			where alp.tipo = '1' and alplm.letra_payment_id is not null
			) a6 on concat(a6.nro_letra,a6.partner_id) = concat(a1.nro_comprobante,a1.partner_id)
			left join vst_letras_sin_canje a7 on concat(a7.partner_id,a7.nro_comprobante,a7.account_id,a7.type_document_it) = concat(a1.partner_id,a1.nro_comprobante,a1.account_id,a1.type_document_it)
			where left(a3.code,4)<>'1211' and (a1.pventa_mn-a1.pagos_mn <> 0 or a1.pventa_me-a1.pagos_me <> 0 )
			%s
			order by a3.code, a2.name)T
		""" % (sql_vend)

		return sql
