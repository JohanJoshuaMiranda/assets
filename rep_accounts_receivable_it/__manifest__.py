# -*- encoding: utf-8 -*-
{
	'name': 'Reporte de Cuentas Corrientes Personalizado',
	'category': 'account',
	'author': 'ITGRUPO',
	'depends': ['account','sale','base','kardex_it','print_guia_remision_it','account_letras_it','account_balance_report'],
	'version': '1.0',
	'description':"""
	- Este modulo agrega campos a account.invoice y sale.order
	- Modifica Vista Tree de stock.picking
	- Crear Reporte en Menu de Ventas
	- Agrega un nuevo permiso
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
			'security/security.xml',
			'wizard/account_receivable_it_wizard.xml',
			'views/stock_picking.xml',
			'views/account_receivable_it_book.xml',
			'views/account_invoice.xml',
			'views/sale_order.xml',
			'sql_functions.sql'
			],
	'installable': True
}
