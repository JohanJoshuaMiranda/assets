# -*- encoding: utf-8 -*-
{
	'name': 'Purchase Third Validation',
	'category': 'purchase',
	'author': 'ITGRUPO-FERCO',
	'depends': ['purchase'],
	'version': '1.0',
	'description':"""
	Opcion a una tercera validacion en Compras
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
			'security/security.xml',
			'security/ir.model.access.csv',
			'views/res_config.xml',
			'views/purchase.xml'],
	'installable': True
}