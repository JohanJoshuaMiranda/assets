# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models

class Company(models.Model):
	_inherit = 'res.company'

	po_double_validation = fields.Selection([
		('one_step', 'Confirm purchase orders in one step'),
		('two_step', 'Get 2 levels of approvals to confirm a purchase order'),
		('three_step', 'Get 3 levels of approvals and two validators to confirm a purchase order')
		])
	po_triple_validation_amount = fields.Monetary(string='Triple validation amount', default=10000,
		help="Minimum amount for which a triple validation is required")
	approval_user_ids = fields.Many2many('res.users', 'company_users_default_rel', string='Usuarios Aprobadores Tercer Nivel')