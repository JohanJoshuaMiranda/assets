# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models

class PurchaseConfigSettings(models.TransientModel):
	_inherit = 'res.config.settings'

	company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
	po_triple_validation_amount = fields.Monetary(related='company_id.po_triple_validation_amount', string="Importe triple validacion", currency_field='company_currency_id')
	approval_user_ids = fields.Many2many(related='company_id.approval_user_ids', string="Usuarios Dos Aprobaciones")