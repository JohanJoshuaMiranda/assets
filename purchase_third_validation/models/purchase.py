# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
from odoo.exceptions import UserError

class PurchaseOrder(models.Model):
	_inherit = "purchase.order"

	approval_lines_ids = fields.One2many('purchase.order.approval', 'order_id')

	@api.multi
	def button_approve(self):
		def finish_process():
			self.write({'state': 'purchase', 'date_approve': fields.Date.context_today(self)})
			self._create_picking()
			self.filtered(lambda p: p.company_id.po_lock == 'lock').write({'state': 'done'})
			if self._context.get('manual_approve', False):
				self.env['purchase.order.approval'].create({
															'order_id': self.id,
															'user_id': self.env.user.id,
															'date': fields.Date.context_today(self),
															'type': self.company_id.po_double_validation 
															})
			return {}

		if self.company_id.po_double_validation == 'three_step' and \
		   self._context.get('manual_approve', False) and \
		   self.amount_total >= self.env.user.company_id.currency_id.compute(self.company_id.po_triple_validation_amount, self.currency_id):
			Approval_Users = self.company_id.approval_user_ids.ids
			if not len(Approval_Users) > 1:
				raise UserError('No se han especificado al menos 2 Usuarios para validacion de Tercer Nivel')
			Approval_Lines = self.approval_lines_ids.filtered(lambda line: line.user_id.id in Approval_Users and line.type == 'three_step')
			if Approval_Lines.filtered(lambda line: line.user_id == self.env.user and line.type == 'three_step'):
				raise UserError('Usted ya dio una aprobacion para esta Orden de Compra')
			if self.env.user.id in Approval_Users and Approval_Lines:
				finish_process()
			else:
				if self._context.get('manual_approve', False) and self.env.user.id in Approval_Users:
					self.env['purchase.order.approval'].create({
															'order_id': self.id,
															'user_id': self.env.user.id,
															'date': fields.Date.context_today(self),
															'type': self.company_id.po_double_validation
															})
				return self.write({'state': 'to approve'})
		elif self.company_id.po_double_validation == 'three_step' and \
			 self.amount_total < self.env.user.company_id.currency_id.compute(self.company_id.po_double_validation_amount, self.currency_id):
			finish_process()
		else:
			if self.user_has_groups('purchase_third_validation.second_level_approver'):
				finish_process()
			else:
				raise UserError('Necesita ser Aprobador de Segundo Nivel')

	@api.multi
	def button_confirm(self):
		for order in self:
			if order.state not in ['draft', 'sent']:
				continue
			order._add_supplier_to_product()
			# Deal with double validation process
			if order.company_id.po_double_validation == 'one_step'\
					or (order.company_id.po_double_validation == 'two_step'\
						and order.amount_total < self.env.user.company_id.currency_id.compute(order.company_id.po_double_validation_amount, order.currency_id))\
					or (order.company_id.po_double_validation == 'three_step'\
						and order.amount_total < self.env.user.company_id.currency_id.compute(order.company_id.po_double_validation_amount, order.currency_id)):
				order.button_approve()
			else:
				order.write({'state': 'to approve'})
		return True

class PurchaseOrderApproval(models.Model):
	_name = "purchase.order.approval"

	order_id = fields.Many2one('purchase.order', ondelete="cascade")
	user_id = fields.Many2one('res.users', string='Usuario')
	date = fields.Datetime(string='Fecha')
	type = fields.Selection([('one_step', '1'),
							('two_step', '2'),
							('three_step', '3')])