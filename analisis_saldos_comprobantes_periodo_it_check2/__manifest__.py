# -*- encoding: utf-8 -*-
{
	'name': 'Saldos Comprobantes Analisis IT Check',
	'category': 'account',
	'author': 'ITGRUPO-COMPATIBLE-BO',
	'depends': ['analisis_saldos_comprobantes_periodo_it_extended'],
	'version': '1.0',
	'description':"""
	Analisis de Saldos por Comprobantes version 2017
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		'security/ir.model.access.csv',
		'views/saldo_comprobante_periodo_check.xml',
		'wizard/account_contable_period_check_view.xml',
		'SQL.sql'],
	'installable': True
}
