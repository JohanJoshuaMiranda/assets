# -*- coding: utf-8 -*-
from odoo import models, fields, api, osv

class SaldoComprobantePeriodoCheckBook(models.Model):
	_name = "saldo.comprobante.periodo.check.book"
	_auto = False


	periodo = fields.Char(string='Periodo')
	fecha_emision = fields.Date(string='Fecha Emision')
	ruc = fields.Char(string='RUC')
	empresa = fields.Char(string='Empresa')
	tipo_cuenta = fields.Char(string='Tipo')
	code = fields.Char(string='Cuenta')
	tipo = fields.Char(string='TD')
	nro_comprobante = fields.Char(string='Nro Comprobante')
	debe = fields.Float(string='Debe',digits=(64,2))
	haber = fields.Float(string='Haber',digits=(64,2))
	saldo = fields.Float(string='Saldo',digits=(64,2))
	divisa = fields.Char(string='Divisa')
	tc = fields.Float(string='TC',digits=(64,3))
	amount_currency = fields.Float(string='Importe',digits=(64,2))
	fecha_venci = fields.Date(string='Fecha Vencimiento')