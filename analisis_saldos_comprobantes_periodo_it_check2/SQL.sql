DROP FUNCTION IF EXISTS public.get_saldo_g(date,date) CASCADE;

CREATE OR REPLACE FUNCTION public.get_saldo_g(
    IN date_from date,
    IN date_to date)
  RETURNS TABLE(periodo character varying, fecha_emision date, fecha_venci date, ruc character varying, empresa character varying, tipo_cuenta character varying,
				code character varying, tipo character varying, nro_comprobante character varying, debe numeric, haber numeric, saldo numeric,
				divisa character varying, amount_currency numeric, voucher character varying, tc numeric) AS
				$BODY$
				BEGIN
				RETURN QUERY 
				select 
				''::character varying as periodo,
				case when ft.date_invoice is null then aml.date else ft.date_invoice end as fecha_emision, --- invoice si es nulo en funcion al min(aml) ->date
				case when ft.date_due is null then aml.date_maturity else ft.date_due end as fecha_venci, --- invoice si es nulo en funcion al min(aml) ->date_maturity
				rp.nro_documento as ruc, --- T
				rp.name as empresa, --- T
				CASE WHEN aat.type= 'payable' THEN 'A pagar'::character varying  ELSE 'A cobrar'::character varying END as tipo_cuenta,
				aa.code, ---T
				itd.code as tipo, --T
				T.nro_comprobante::character varying as nro_comprobante, --T
				T.debe,
				T.haber,				
				CASE WHEN abs(T.saldo) < 0.01 then 0 else T.saldo end as saldo,
				rc.name as divisa, --- en funcion a cuenta
				T.amount_currency,
				ft.name as voucher,
				0::numeric as tc
				from (
				select account_move_line.partner_id,account_id,type_document_it,TRIM(account_move_line.nro_comprobante)as nro_comprobante,
				sum(debit)as debe,sum(credit) as haber, sum(debit)-sum(credit) as saldo, 
				sum(account_move_line.amount_currency) as amount_currency, min(account_move_line.id), array_agg(account_move_line.id) as aml_ids 
				from account_move_line
				left join account_move ami on ami.id = account_move_line.move_id
				left join account_account on account_account.id=account_move_line.account_id
				left join account_account_type aat on aat.id = account_account.user_type_id
				where
				aat.type in ('payable','receivable') and 
				ami.state != 'draft'
				and ami.fecha_contable >= $1 and ami.fecha_contable <= $2
				group by account_move_line.partner_id,account_id,type_document_it,TRIM(account_move_line.nro_comprobante)) as T
				left join account_move_line aml on aml.id = T.min
				left join account_invoice ft on ft.it_type_document=T.type_document_it and ft.account_id = T.account_id and
				TRIM(ft.reference) = T.nro_comprobante and ft.partner_id = T.partner_id
				left join res_partner rp on rp.id = T.partner_id
				left join einvoice_catalog_01 itd on itd.id = T.type_document_it
				left join account_account aa on aa.id = T.account_id
				left join res_currency rc on rc.id = aa.currency_id
				left join account_account_type aat on aat.id = aa.user_type_id
				order by empresa, code, nro_comprobante;
				END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;