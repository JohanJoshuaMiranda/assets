# -*- encoding: utf-8 -*-

from openerp.osv import osv
import base64
from openerp import models, fields, api
import codecs
import pprint
from datetime import *
from odoo.exceptions import UserError, ValidationError
	
class saldo_comprobante_periodo_check_wizard(models.TransientModel):
	_name='saldo.comprobante.periodo.check.wizard'

	period_inicial = fields.Many2one('account.period',string='Periodo Inicial')
	period_final = fields.Many2one('account.period',string='Periodo Final')
	empresa = fields.Many2one('res.partner',string='Empresa')
	only_pending = fields.Boolean(string="Solo Pendientes",default=False)
	type_account = fields.Selection([('payable','Por Pagar'),('receivable','Por Cobrar')],string=u'Tipo')
	cuenta = fields.Many2one('account.account',string='Cuenta')
	mostrar =  fields.Selection([('pantalla','Pantalla'),('excel','Excel'),('csv','CSV'),('newwindow','Emergente')], 'Mostrar en', required=True)

	@api.multi
	def do_rebuild(self):
		self.env.cr.execute("""  DROP VIEW IF EXISTS saldo_comprobante_periodo_check_book;
			create or replace view saldo_comprobante_periodo_check_book as (select row_number() OVER () AS id, T.* 
			from get_saldo_g('"""+ self.period_inicial.date_start +"""'::date,'"""+ self.period_final.date_stop +"""'::date)T);"""
		)
		filtro = []

		if self.cuenta:
			filtro.append( ('code','=', self.cuenta.code ) )

		if self.empresa:
			filtro.append( ('empresa','=', self.empresa.name ) )
		if self.type_account == 'payable':
			filtro.append( ('tipo_cuenta','=', 'A pagar' ) )
		if self.type_account == 'receivable':
			filtro.append( ('tipo_cuenta','=', 'A cobrar' ) )
		if self.only_pending:
			filtro.append( ('saldo','!=', 0 ) )
		move_obj = self.env['saldo.comprobante.periodo.check.book']
		lstidsmove= move_obj.search(filtro)
		if (len(lstidsmove) == 0) and self.mostrar != 'newwindow':
			raise osv.except_osv('Alerta','No contiene datos.')

		if self.mostrar == 'pantalla':		
			return {
				'name': 'Saldo por Fecha Contable',
				'domain' : filtro,
				'type': 'ir.actions.act_window',
				'res_model': 'saldo.comprobante.periodo.check.book',
				'view_mode': 'tree',
				'view_type': 'form',
				'views': [(False, 'tree')],
			}

		if self.mostrar == 'newwindow':		
			return {
				'domain' : filtro,
				'type': 'ir.actions.act_window',
				'res_model': 'saldo.comprobante.periodo.check.book',
				'view_mode': 'tree',
				'view_type': 'form',
				'views': [(False, 'tree')],
				"target": "new",
			}

		#DSC_
		if self.mostrar == 'csv':
			direccion = self.env['main.parameter'].search([])[0].dir_create_file
			docname = 'SaldoPeriodo.csv'
			#CSV
			sql_query = """	COPY (SELECT * FROM saldo_comprobante_periodo_check_book )TO '"""+direccion+docname+"""'   WITH DELIMITER ',' CSV HEADER			
							"""
			self.env.cr.execute(sql_query)
			#Caracteres Especiales
			import sys
			reload(sys)
			sys.setdefaultencoding('iso-8859-1')
			f = open(direccion + docname, 'rb')			
			vals = {
				'output_name': docname,
				'output_file': base64.encodestring(''.join(f.readlines())),		
			}
			sfs_id = self.env['export.file.save'].create(vals)
			return {
				"type": "ir.actions.act_window",
				"res_model": "export.file.save",
				"views": [[False, "form"]],
				"res_id": sfs_id.id,
				"target": "new",
			}

			
		if self.mostrar == 'excel':
			import io
			from xlsxwriter.workbook import Workbook
			output = io.BytesIO()
			########### PRIMERA HOJA DE LA DATA EN TABLA
			#workbook = Workbook(output, {'in_memory': True})

			direccion = self.env['main.parameter'].search([])[0].dir_create_file

			workbook = Workbook(direccion +'saldoperiodo.xlsx')
			worksheet = workbook.add_worksheet("Analisis Saldo x Periodo")
			#Print Format
			worksheet.set_landscape() #Horizontal
			worksheet.set_paper(9) #A-4
			worksheet.set_margins(left=0.75, right=0.75, top=1, bottom=1)
			worksheet.fit_to_pages(1, 0)  # Ajustar por Columna	

			bold = workbook.add_format({'bold': True})
			normal = workbook.add_format()
			boldbord = workbook.add_format({'bold': True})
			boldbord.set_border(style=2)
			boldbord.set_align('center')
			boldbord.set_align('vcenter')
			boldbord.set_text_wrap()
			boldbord.set_font_size(9)
			boldbord.set_bg_color('#DCE6F1')
			numbertres = workbook.add_format({'num_format':'0.000'})
			numberdos = workbook.add_format({'num_format':'0.00'})
			bord = workbook.add_format()
			bord.set_border(style=1)
			bord.set_text_wrap()
			numberdos.set_border(style=1)
			numbertres.set_border(style=1)	


			title = workbook.add_format({'bold': True})
			title.set_align('center')
			title.set_align('vcenter')
			title.set_text_wrap()
			title.set_font_size(20)
			worksheet.set_row(0, 30)

			x= 10				
			tam_col = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
			import sys
			reload(sys)
			sys.setdefaultencoding('iso-8859-1')

			worksheet.merge_range(0,0,0,11, u"Análisis de Saldos x Periodo", title)

			worksheet.write(2,0, u"Fecha Inicial", bold)
			worksheet.write(2,1, self.period_inicial.date_start, normal)

			worksheet.write(3,0, u"Fecha Final", bold)
			worksheet.write(3,1, self.period_final.date_stop, normal)

			worksheet.write(5,0, u"Empresa", bold)
			worksheet.write(5,1, self.empresa.name if self.empresa.name else '', normal)

			worksheet.write(6,0, u"Cuenta", bold)
			worksheet.write(6,1, self.cuenta.name if self.cuenta.name else '', normal)


			worksheet.write(9,0, u"Fecha Emisión",boldbord)
			worksheet.write(9,1, u"Ruc",boldbord)
			worksheet.write(9,2, u"Empresa",boldbord)
			worksheet.write(9,3, u"Tipo Cuenta",boldbord)
			worksheet.write(9,4, u"Cuenta",boldbord)
			worksheet.write(9,5, u"Tipo Documento",boldbord)
			worksheet.write(9,6, u"Nro. Comprobante",boldbord)
			worksheet.write(9,7, u"Debe",boldbord)
			worksheet.write(9,8, u"Haber",boldbord)
			worksheet.write(9,9, u"Saldo",boldbord)
			worksheet.write(9,10, u"Divisa",boldbord)
			worksheet.write(9,11, u"Importe",boldbord)
			worksheet.write(9,12, u"Fecha Vencimiento",boldbord)



			for line in self.env['saldo.comprobante.periodo.check.book'].search(filtro):
				worksheet.write(x,0,line.fecha_emision if line.fecha_emision  else '',bord )
				worksheet.write(x,1,line.ruc if line.ruc  else '',bord)
				worksheet.write(x,2,line.empresa if line.empresa  else '',bord)
				worksheet.write(x,3,line.tipo_cuenta if line.tipo_cuenta  else '',bord)
				worksheet.write(x,4,line.code if line.code  else '',bord)
				worksheet.write(x,5,line.tipo if line.tipo  else '',bord)
				worksheet.write(x,6,line.nro_comprobante if line.nro_comprobante  else '',bord)
				worksheet.write(x,7,line.debe ,numberdos)
				worksheet.write(x,8,line.haber ,numberdos)
				worksheet.write(x,9,line.saldo ,numberdos)
				worksheet.write(x,10,line.divisa if  line.divisa else '',bord)
				worksheet.write(x,11,line.amount_currency ,numberdos)
				worksheet.write(x,12,line.fecha_venci if line.fecha_venci  else '',bord)
				

				x = x +1

			tam_col = [11,36,45,9,25,12,13,11,10,14,14,14,13,14,10,16,16,20]


			worksheet.set_column('A:A', tam_col[0])
			worksheet.set_column('B:B', tam_col[1])
			worksheet.set_column('C:C', tam_col[2])
			worksheet.set_column('D:D', tam_col[3])
			worksheet.set_column('E:E', tam_col[4])
			worksheet.set_column('F:F', tam_col[5])
			worksheet.set_column('G:G', tam_col[6])
			worksheet.set_column('H:H', tam_col[7])
			worksheet.set_column('I:I', tam_col[8])
			worksheet.set_column('J:J', tam_col[9])
			worksheet.set_column('K:K', tam_col[10])
			worksheet.set_column('L:L', tam_col[11])
			worksheet.set_column('M:M', tam_col[12])
			worksheet.set_column('N:N', tam_col[13])
			worksheet.set_column('O:O', tam_col[14])
			worksheet.set_column('P:P', tam_col[15])
			worksheet.set_column('Q:Q', tam_col[16])
			worksheet.set_column('R:R', tam_col[17])

			workbook.close()
			
			f = open(direccion + 'saldoperiodo.xlsx', 'rb')
			
			vals = {
				'output_name': 'SaldoPeriodo.xlsx',
				'output_file': base64.encodestring(''.join(f.readlines())),		
			}

			sfs_id = self.env['export.file.save'].create(vals)

			return {
			    "type": "ir.actions.act_window",
			    "res_model": "export.file.save",
			    "views": [[False, "form"]],
			    "res_id": sfs_id.id,
			    "target": "new",
			}