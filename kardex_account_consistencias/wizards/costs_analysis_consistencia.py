# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import *
from odoo.exceptions import UserError
import base64

class CostsAnalysisConsistenciaWizard(models.TransientModel):
	_name = 'costs.analysis.consistencia.wizard'

	name = fields.Char()
	period = fields.Many2one('account.period',string='Periodo',required=True)

	def get_excel(self):
		import io
		from xlsxwriter.workbook import Workbook
		from xlsxwriter.utility import xl_rowcol_to_cell

		direccion = self.env['main.parameter'].search([])[0].dir_create_file
		workbook = Workbook( direccion + 'reporte_consistencia_99.xlsx')
		worksheet = workbook.add_worksheet("Consistencia")
		bold = workbook.add_format({'bold': True})

		bold_title = workbook.add_format({'bold': True,'align': 'center','valign': 'vcenter'})
		bold_title.set_font_size(14)
		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=2)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(9)
		boldbord.set_bg_color('#DCE6F1')
		numberdos = workbook.add_format({'num_format':'0.00'})
		bord = workbook.add_format()
		bord.set_border(style=1)
		numberdos.set_border(style=1)	
		dateformat = workbook.add_format({'num_format':'d-m-yyyy'})
		dateformat.set_border(style=1)
		dateformat.set_font_size(10)
		dateformat.set_font_name('Times New Roman')	
		x= 1

		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		worksheet.write(0,0, "Fecha",boldbord)
		worksheet.write(0,1, u"Documento",boldbord)
		worksheet.write(0,2, u"Tipo de Operacion",boldbord)

		self.env.cr.execute(self._get_sql(self.period.date_start, self.period.date_stop))
		res = self.env.cr.dictfetchall()

		for line in res:

			worksheet.write(x,0,line['fecha'] if line['fecha'] else '' ,dateformat )
			worksheet.write(x,1,line['documento_almacen'] if line['documento_almacen']  else 0,bord )
			worksheet.write(x,2,line['tipo_operacion'] if line['tipo_operacion']  else 0,bord)

			x += 1

		tam_col = [12,20,15]

		worksheet.set_column('A:A', tam_col[0])
		worksheet.set_column('B:B', tam_col[1])
		worksheet.set_column('C:C', tam_col[2])
		workbook.close()
		
		f = open(direccion + 'reporte_consistencia_99.xlsx', 'rb')
		
		
		vals = {
			'output_name': 'Consistencia de Conceptos de Ajuste.xlsx',
			'output_file': base64.encodestring(''.join(f.readlines())),		
		}

		sfs_id = self.env['export.file.save'].create(vals)
		return {
			"type": "ir.actions.act_window",
			"res_model": "export.file.save",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}

	def _get_sql(self,date_ini,date_end):

		sql = """select gkv.fecha, gkv.stock_doc as documento_almacen,gkv.operation_type as tipo_operacion
				from get_kardex_v({date_start_s},{date_end_s},(select array_agg(id) from product_product), (select array_agg(id) from stock_location))gkv
				left join stock_move sm on sm.id = gkv.stock_moveid
				left join stock_picking sp on sp.id = sm.picking_id
				where (gkv.fecha between '{date_ini}' and '{date_end}') and sp.kardex_tuning_concepts_id is null and gkv.operation_type = '99'
				group by gkv.fecha, gkv.stock_doc, gkv.operation_type
		""".format(
				date_start_s = datetime.strptime(date_ini, '%Y-%m-%d').strftime("%Y")+'0101',  
				date_end_s = datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y%m%d"),
				date_ini =  datetime.strptime(date_ini, '%Y-%m-%d').strftime("%Y/%m/%d"),
				date_end = datetime.strptime(date_end, '%Y-%m-%d').strftime("%Y/%m/%d")
			)
		return sql