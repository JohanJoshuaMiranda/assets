# -*- encoding: utf-8 -*-
{
	'name': 'Consistencia',
	'category': 'stock',
	'author': 'ITGRUPO',
	'depends': ['kardex_account_journal_entry'],
	'version': '1.0',
	'description':"""
	- Reporte de Consistencia
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		'wizards/costs_analysis_consistencia.xml'
	],
	'installable': True
}
